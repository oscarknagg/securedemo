//
//     KeyLines v3.1.2-2520 professional eval
//     Licensed to Wandera for TBD expires on Fri Sep 09 2016
//
//     Copyright © 2011-2016 Cambridge Intelligence Limited.
//     All rights reserved.
//
! function() {
    var e = {},
        t = "undefined" != typeof window;
    t && (window.KeyLines = e = "undefined" != typeof window.KeyLines ? window.KeyLines : e), e.TimeBar = {}, "object" == typeof module && "object" == typeof module.exports ? (global.KeyLines = e, module.exports = e) : "function" == typeof define && define.amd && define(e), e.webGLSupport = function() {
        if (!e.WebGL && t) {
            var n, r = ["OES_standard_derivatives", "OES_element_index_uint"],
                a = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"],
                i = document.createElement("canvas"),
                o = null;
            for (n = 0; n < a.length; ++n) {
                try {
                    o = i.getContext(a[n])
                } catch (u) {}
                if (o) break
            }
            var s = o && "getSupportedExtensions" in o;
            if (s) {
                var l = o.getSupportedExtensions(),
                    f = {};
                for (n = 0; n < l.length; n++) f[l[n]] = !0;
                for (n = 0; s && n < r.length; n++) s = f[r[n]]
            }
            return s
        }
        return !0
    }, e.webGLSupport() && (e.WebGL = {})
}(),
function() {
    var e = KeyLines.API = KeyLines.API || {};
    e.appendExtras = function(e) {
        function t(e, t) {
            return "all" === e || e === t.type
        }

        function n(t) {
            e.each({
                type: "link"
            }, function(e) {
                t[e.id] && (t[e.id1] = !0, t[e.id2] = !0)
            })
        }

        function r(t) {
            e.each({
                type: "link"
            }, function(e) {
                t[e.id] = !(!t[e.id1] || !t[e.id2])
            })
        }
        var a = null;
        e.bind("redraw", function() {
            a = null
        }), e.each = function(n, r) {
            a = a || e.serialize().items;
            var i = a;
            if (n = KeyLines.Util.defaults(n, {
                    type: "all"
                }), !n.type.match(/^(node|link|all)$/)) throw new TypeError("opts type property should be either 'node', 'link' or 'all'");
            if (KeyLines.Util.isFunction(r))
                for (var o = 0; o < i.length; o++) t(n.type, i[o]) && r(i[o])
        }, e.filter = function(t, a, i) {
            function o() {
                KeyLines.Util.invoke(i, f)
            }
            a = KeyLines.Util.defaults(a, {
                type: "all",
                animate: !0,
                time: 1e3
            });
            var u = "link" === a.type;
            a = KeyLines.Util.defaults(a, {
                hideSingletons: u
            });
            var s = {};
            if (e.each(a, function(e) {
                    t(e) && (s[e.id] = !0)
                }), u && (a.hideSingletons ? n(s) : e.each({
                    type: "node"
                }, function(e) {
                    s[e.id] = !0
                })), "node" === a.type && r(s), !u && a.hideSingletons) {
                var l = {};
                e.each({
                    type: "link"
                }, function(e) {
                    l[e.id1] |= s[e.id] && s[e.id2], l[e.id2] |= s[e.id] && s[e.id1]
                }), e.each({
                    type: "node"
                }, function(e) {
                    s[e.id] &= l[e.id]
                })
            }
            var f = {
                shown: {
                    nodes: [],
                    links: []
                },
                hidden: {
                    nodes: [],
                    links: []
                }
            };
            e.each({
                type: "all"
            }, function(e) {
                var t = "node" === e.type ? "nodes" : "links";
                s[e.id] && e.hi && f.shown[t].push(e.id), s[e.id] || e.hi || f.hidden[t].push(e.id)
            });
            var c = f.shown.nodes.concat(f.shown.links),
                d = f.hidden.nodes.concat(f.hidden.links);
            d.length > 0 ? c.length > 0 ? (a.time /= 2, e.hide(d, a, function() {
                e.show(c, !1, a, o)
            })) : e.hide(d, a, o) : c.length > 0 ? e.show(c, !1, a, o) : o()
        }, e.foreground = function(t, a, i) {
            a = KeyLines.Util.defaults(a, {
                type: "node"
            });
            var o = {};
            e.each(a, function(e) {
                t(e) && (o[e.id] = !0)
            }), "link" === a.type && n(o), "node" === a.type && r(o);
            var u = [],
                s = {
                    foreground: {
                        nodes: [],
                        links: []
                    },
                    background: {
                        nodes: [],
                        links: []
                    }
                };
            e.each({
                type: "all"
            }, function(e) {
                !!e.bg != !o[e.id] && ("node" === e.type ? o[e.id] ? s.foreground.nodes.push(e.id) : s.background.nodes.push(e.id) : "link" === e.type && (o[e.id] ? s.foreground.links.push(e.id) : s.background.links.push(e.id)), u.push({
                    id: e.id,
                    bg: !o[e.id]
                }))
            }), e.setProperties(u, !1, function() {
                KeyLines.Util.invoke(i, s)
            })
        }
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        var t = 6,
            n = 4,
            r = 3,
            a = 4;
        KeyLines.WebGL.ArcShader = function(i) {
            function o(e, t, n, r, a) {
                v.uniform2f(p.resolution, t, n), v.uniformMatrix4fv(p.transform, !1, e.get()), v.uniform1f(p.hitTest, 0 | r), v.uniform1f(p.zoom, e.getZoomLevel()), v.uniform1f(p.shadowType, a)
            }

            function u(e) {
                e.arcBuffers.positionBuffer || (e.arcBuffers.positionBuffer = v.createBuffer()), e.arcBuffers.colourBuffer || (e.arcBuffers.colourBuffer = v.createBuffer()), e.arcBuffers.shadowBuffer || (e.arcBuffers.shadowBuffer = v.createBuffer()), e.arcBuffers.coreBuffer || (e.arcBuffers.coreBuffer = v.createBuffer())
            }

            function s(e) {
                v.bindBuffer(v.ARRAY_BUFFER, e.arcBuffers.colourBuffer), (e.rebuildOptions.colours || e.alwaysUpdate || e.rebuildOptions.all) && v.bufferData(v.ARRAY_BUFFER, e.arcBuffers.colourData, v.STATIC_DRAW), v.enableVertexAttribArray(p.colour), v.vertexAttribPointer(p.colour, 4, v.UNSIGNED_BYTE, !1, a, 0)
            }

            function l(e) {
                v.bindBuffer(v.ARRAY_BUFFER, e.arcBuffers.shadowBuffer), (e.alwaysUpdate || e.rebuildOptions.all || e.rebuildOptions.shadow) && v.bufferData(v.ARRAY_BUFFER, e.arcBuffers.shadowData, v.STATIC_DRAW), v.enableVertexAttribArray(p.shadowColour), v.vertexAttribPointer(p.shadowColour, 4, v.UNSIGNED_BYTE, !1, n, 0)
            }

            function f(e) {
                v.bindBuffer(v.ARRAY_BUFFER, e.arcBuffers.coreBuffer), (e.alwaysUpdate || e.rebuildOptions.all) && v.bufferData(v.ARRAY_BUFFER, e.arcBuffers.coreData, v.STATIC_DRAW), v.enableVertexAttribArray(p.type), v.enableVertexAttribArray(p.elementIndex), v.enableVertexAttribArray(p.numOfSegments), v.vertexAttribPointer(p.type, 1, v.FLOAT, !1, r * Float32Array.BYTES_PER_ELEMENT, 0), v.vertexAttribPointer(p.elementIndex, 1, v.FLOAT, !1, r * Float32Array.BYTES_PER_ELEMENT, 4), v.vertexAttribPointer(p.numOfSegments, 1, v.FLOAT, !1, r * Float32Array.BYTES_PER_ELEMENT, 8)
            }

            function c(e) {
                v.bindBuffer(v.ARRAY_BUFFER, e.arcBuffers.positionBuffer), (e.rebuildOptions.positions || e.alwaysUpdate || e.rebuildOptions.all) && v.bufferData(v.ARRAY_BUFFER, e.arcBuffers.positionData, v.STATIC_DRAW), v.enableVertexAttribArray(p.position), v.enableVertexAttribArray(p.radius), v.enableVertexAttribArray(p.startAngle), v.enableVertexAttribArray(p.endAngle), v.enableVertexAttribArray(p.width), v.vertexAttribPointer(p.position, 2, v.FLOAT, !1, t * Float32Array.BYTES_PER_ELEMENT, 0), v.vertexAttribPointer(p.radius, 1, v.FLOAT, !1, t * Float32Array.BYTES_PER_ELEMENT, 8), v.vertexAttribPointer(p.startAngle, 1, v.FLOAT, !1, t * Float32Array.BYTES_PER_ELEMENT, 12), v.vertexAttribPointer(p.endAngle, 1, v.FLOAT, !1, t * Float32Array.BYTES_PER_ELEMENT, 16), v.vertexAttribPointer(p.width, 1, v.FLOAT, !1, t * Float32Array.BYTES_PER_ELEMENT, 20)
            }

            function d() {
                v.bindBuffer(v.ELEMENT_ARRAY_BUFFER, null), v.bindBuffer(v.ARRAY_BUFFER, null)
            }
            var g = KeyLines.WebGL.Utils,
                h = {},
                v = i,
                m = e["line-arc-vertex"],
                x = e["line-fragment"],
                y = g.createProgram(v, atob(m), atob(x)),
                p = {
                    hitTest: v.getUniformLocation(y, "u_hitTest"),
                    zoom: v.getUniformLocation(y, "u_zoom"),
                    transform: v.getUniformLocation(y, "u_transform"),
                    resolution: v.getUniformLocation(y, "u_resolution"),
                    shadowType: v.getUniformLocation(y, "u_shadowType"),
                    position: v.getAttribLocation(y, "a_position"),
                    radius: v.getAttribLocation(y, "a_radius"),
                    startAngle: v.getAttribLocation(y, "a_startAngle"),
                    endAngle: v.getAttribLocation(y, "a_endAngle"),
                    width: v.getAttribLocation(y, "a_width"),
                    colour: v.getAttribLocation(y, "a_colour"),
                    elementIndex: v.getAttribLocation(y, "a_index"),
                    numOfSegments: v.getAttribLocation(y, "a_numOfSegments"),
                    type: v.getAttribLocation(y, "a_lineType"),
                    shadowColour: v.getAttribLocation(y, "a_hitTestColour")
                };
            return h.drawItems = function(e, t, n, r, a) {
                v.useProgram(y), o(t, n, r, a, 0 | e.shadowType), u(e), s(e), l(e), c(e), f(e), v.drawArrays(v.TRIANGLES, 0, e.arcBuffers.totalNumOfVerticies), d()
            }, h
        }
    }
}(),
function() {
    var e = KeyLines.API = KeyLines.API || {};
    e.createAPI = function(e) {
        function t() {
            return s && s.api.isShown()
        }

        function n(e, t, n) {
            t = t || {
                all: !0,
                positions: !0,
                colours: !0,
                textures: !0,
                shadow: !0
            }, e ? B.afterChange(n) : (B.render(t), v.invoke(n))
        }

        function r(e) {
            return e === !0 && (e = {
                animate: !0
            }), v.defaults(e, {
                animate: !1,
                time: 1e3
            })
        }

        function a(e) {
            return isFinite(e) ? Math.min(Math.max(0, e), 1) : e ? 1 : 0
        }

        function i(e, t) {
            function n(e, t) {
                x.privateEach({
                    type: "link"
                }, function(n) {
                    if (!e[n.id1] || !e[n.id2]) {
                        var r = e[n.id1] || e[n.id2],
                            a = e[n.id1] ? n.id2 : n.id1;
                        r && a && (t[a] || (t[a] = {
                            x: 0,
                            y: 0,
                            counter: 0,
                            id: a
                        }), t[a].x += r.x, t[a].y += r.y, t[a].counter++)
                    }
                });
                var n = 0;
                return v.iterator(t, function(t) {
                    e[t.id] || (t.x /= t.counter, t.y /= t.counter, e[t.id] = t, n++)
                }), n
            }

            function r(e, r) {
                for (var a, i = {}, u = []; 0 !== a;) a = n(r, i);
                v.iterator(e, function(e) {
                    "node" === e.type && (i[e.id] || (e.x = o[e.id].x, e.y = o[e.id].y))
                }), v.iterator(i, function(e) {
                    u.push(e)
                }), x.setProperties(u, !1, function() {
                    v.invoke(t, s)
                })
            }

            function a() {
                var e = [],
                    t = {};
                return I.each({
                    type: "node"
                }, function(n) {
                    e.push(n.id), t[n.id] = n
                }), {
                    list: e,
                    dict: t
                }
            }

            function i(e) {
                for (var t = [], n = {}, r = 0; r < e.length; r++) {
                    var a = e[r];
                    v.isNullOrUndefined(a) || n[a.id] || (n[a.id] = 1, o[a.id] = {
                        x: a.x,
                        y: a.y
                    }, delete a.x, delete a.y, t.push(a))
                }
                return t
            }
            var o = {};
            e = i(e);
            var u = a(),
                s = u.list,
                l = u.dict;
            x.merge(e, function() {
                r(e, l)
            })
        }

        function o(e, t) {
            B.draw(e, t), B.drawoffscreen(), B.afterDraw()
        }
        var u, s, l = KeyLines.Controller,
            f = KeyLines.Draggers,
            c = KeyLines.Events,
            d = KeyLines.Model,
            g = KeyLines.Overlays,
            h = KeyLines.Rendering,
            v = KeyLines.Util,
            m = KeyLines.View,
            x = {},
            y = c.createEventBus(),
            p = f.createDraggers(y),
            b = KeyLines.Generator.create(y),
            I = d.createModel(y, p, b);
        s = KeyLines.Map.create(x, y, e);
        var w = g.createOverlays(y, m, s),
            B = l.createController(x, y, w, b, s);
        B.setModel(I);
        var C;
        x.privateInit = function(e, t, n, r, a, i, o, u, s) {
            C = r, B.assetPath(o), B.setCursorFn(a), B.setCreateCanvasFn(r), B.setImageGenFn(i), A(e, t, n, u), B.imageBasePath(s)
        };
        var A = x.privateSetSize = function(e, t, n, r, a) {
            var i = C(t, n);
            B.makeGhostCanvas(t, n), r && (t /= r, n /= r);
            var o = u;
            u = m.createView(y, t, n, r, s), B.setView(u), B.setCanvas(e), B.setShadowCanvas(i.getContext("2d"), t, n), h.clearLastFont(), o && (u.setMinZoom(o.getMinZoom()), u.setMaxItemZoom(o.getMaxItemZoom()), u.fromOldSettings(o.settings(), !1)), B.afterChange(null, a)
        };
        x.privateMapSetSize = function(e, t) {
            s.internalUse.setSize(e, t)
        }, x.privateNamespaceDefinitions = function() {
            var e = {};
            return e.graph = {
                degrees: !0,
                distances: !0,
                betweenness: !0,
                closeness: !0,
                kCores: !0
            }, e.combo = {}, e.map = {}, e
        }, x.graph = function() {
            return I.graph()
        }, x.map = function() {
            return s.api
        }, x.combo = function() {
            return B.combo.api
        }, x.toDataURL = function(e, n, r, a) {
            function i(t) {
                e = Math.floor(e / t), n = Math.floor(n / t)
            }
            if (e = Math.floor(e), n = Math.floor(n), e = e > 0 ? e : 100, n = n > 0 ? n : 100, r = v.defaults(r, {
                    fit: "view",
                    watermark: !0,
                    gradient: !0,
                    logo: !0
                }), t() && (r.fit = "exact"), "oneToOne" === r.fit) {
                var o = B.modelExtents();
                o || (o = {
                    x1: 0,
                    y1: 0,
                    x2: 100,
                    y2: 100
                });
                var f = 10;
                o.x1 -= f, o.y1 -= f, o.x2 += f, o.y2 += f;
                var d = B.watermarkSize();
                if (d && r.watermark) {
                    var h = (d.width - (o.x2 - o.x1)) / 2;
                    h > 0 && (o.x1 -= h, o.x2 += h);
                    var y = (d.height - (o.y2 - o.y1)) / 2;
                    y > 0 && (o.y1 -= y, o.y2 += y)
                }
                e = o.x2 - o.x1, n = o.y2 - o.y1, r.noScale = !0
            }
            var p = 16e6,
                w = e * n,
                A = w / p;
            if (A > 1) {
                var M = Math.sqrt(A);
                i(M)
            }
            var D = 8e3,
                L = e / D;
            L > 1 && i(L);
            var Z = n / D;
            Z > 1 && i(Z);
            var R = C(e, n),
                G = R.getContext("2d"),
                S = r.noScale ? 1 : e / u.width(),
                T = c.createEventBus(),
                E = m.createView(T, Math.floor(e / S), Math.floor(n / S), S, s),
                k = g.createOverlays(T, m),
                F = l.createController(x, T, k, b);
            F.setCreateCanvasFn(C), F.setModel(I), F.setView(E), F.setCanvas(G), F.setShadowCanvas(null, e, n), F.assetPath(B.assetPath()), F.imageBasePath(B.imageBasePath()), F.imageList(B.imageList());
            var W = B.options();
            W.overview = {
                shown: !1
            }, r.watermark || (W.watermark = !1), r.gradient || (W.gradient = !1), r.logo || (W.logo = !1), F.options(W, null, !0), F.backgroundOpacity(B.backgroundOpacity()), F.makeGhostCanvas(E.width(), E.height());
            var N = [];
            switch (r.selection || (N = I.doSelection(), I.doSelection([])), r.fit) {
                case "chart":
                    F.fitToModel(!1);
                    break;
                case "view":
                    E.fromOldSettings(u.settings(), !0);
                    break;
                case "exact":
                    E.fromOldSettings(u.settings(), !1);
                    break;
                case "oneToOne":
                    E.fitWorldExtents(o, !1, !1, !1, 0, 0)
            }
            E.scaleFactor(S), F.draw(!0, {
                all: !0
            });
            var O;
            try {
                O = R.toDataURL()
            } catch (V) {
                throw new Error("[toDataURL] There is a problem to export the chart with tainted canvas.")
            }
            return r.selection || I.doSelection(N), I.setAllDirty(), a ? t() ? s.internalUse.toDataURL(e, n, r, C, R, a) : v.invoke(a, O) : O
        }, x.load = function(e, t) {
            switch (e.type) {
                case "LinkChart":
                    if (s && s.internalUse.isTransitioning()) throw new Error("Cannot load chart during map show or hide operation");
                    I = d.createModel(y, p, b), B.setModel(I), B.load(e, function() {
                        B.afterChange(t, null, !0)
                    });
                    break;
                default:
                    throw new Error("Unknown chart type passed to load")
            }
        }, x.chartType = function() {
            return I.type()
        }, x.clear = function() {
            y.trigger("prechange", "delete"), I.load({
                items: []
            }), B.combo.internalUse.loadUp(), B.afterChange(null, null, !0)
        }, x.merge = function(e, t) {
            e = v.getItemsArray(e);
            var r = B.merge(e);
            n(r, null, function() {
                B.postMerge(t)
            })
        }, x.privateMerge = function(e, t) {
            var r = I.merge(e);
            n(r, null, t)
        }, x.privateEach = function(e, t) {
            I.each(e, t)
        }, x.privateBothEndsShown = function(e) {
            return I.bothEndsShown(e)
        }, x.privateRemoveItem = function(e) {
            I.removeItem(e), B.render()
        }, x.privateGraph = function() {
            return I.graph()
        }, x.setProperties = function(e, t, r) {
            var a = B.setProperties(e, t);
            n(a, {
                all: !0,
                colours: !0
            }, r)
        }, x.animateProperties = function(e, t, n) {
            t = v.defaults(t, {
                time: 1e3,
                easing: "linear",
                queue: !0
            }), e = v.ensureArray(e), B.animateProperties(e, t, n)
        }, x.privateAnimateBackgroundOpacity = function(e, t, n) {
            t = v.defaults(t, {
                time: 500
            }), e = a(e), B.animateBackgroundOpacity(e, t.time, n)
        }, x.privateSetBackgroundOpacity = function(e) {
            e = a(e), B.backgroundOpacity(e)
        }, x.ping = function(e, t, n) {
            t = v.defaults(t, {
                c: "#909090",
                w: 20,
                r: 80,
                time: 800,
                repeat: 1
            }), B.ping(e, t, n)
        }, x.getItem = function(e) {
            if (v.isArray(e)) {
                for (var t = [], n = 0; n < e.length; n++) {
                    var r = I.getItem(e[n]);
                    r = r || null, t.push(r)
                }
                return t
            }
            return I ? I.getItem(e) : null
        }, x.setItem = function(e, t) {
            if (e) {
                y.trigger("prechange", "properties");
                var r = B.setItem(e);
                n(r, {
                    all: !0
                }, function() {
                    B.postMerge(t)
                })
            }
        }, x.removeItem = function(e) {
            y.trigger("prechange", "delete"), B.removeItem(e), B.render()
        }, x.hide = function(e, t, n) {
            y.trigger("prechange", "hide"), e = v.ensureArray(e), t = r(t), B.hide(e, t, n)
        }, x.hidden = function() {
            return I.hidden()
        }, x.show = function(e, t, n, a) {
            y.trigger("prechange", "show"), e = v.ensureArray(e), n = r(n), B.show(e, t, n, a)
        }, x.labelPosition = function(e) {
            return I ? B.labelPosition(e, u) : null
        }, x.contains = function(e) {
            return I ? I.contains(e) : []
        }, x.serialize = function() {
            var e = I.serialize();
            return B.appendCombos(e), s.internalUse.appendMap(e), e.viewSettings = u.settings(), e
        }, x.worldCoordinates = function(e, t) {
            var n = u.scale(e),
                r = u.scale(t);
            return {
                x: u.viewToWorldX(n),
                y: u.viewToWorldY(r)
            }
        }, x.viewCoordinates = function(e, t) {
            var n = u.worldToViewX(e),
                r = u.worldToViewY(t);
            return {
                x: u.unscale(n),
                y: u.unscale(r)
            }
        }, x.viewOptions = function(e, t, n) {
            if (!e) return u.settings();
            var a = r(t);
            e = v.defaults(e, {
                offsetX: 0,
                offsetY: 0,
                zoom: 1
            }), B.setViewSettings(e, a.animate, a.time, function() {
                B.render({
                    drawOnly: !0
                }), v.invoke(n)
            })
        }, x.options = function(e, t) {
            return B.options(e, t)
        }, x.displayOptions = function(e, t) {
            return x.options(e, t)
        }, x.interactionOptions = function(e) {
            return x.options(e)
        }, x.lock = function(e, t) {
            return "undefined" != typeof e && (M = e, s && s.internalUse.lock(M), e ? (t = t || {}, B.wait(t.wait), B.removeDragger()) : B.wait(!1), B.render()), M
        };
        var M = !1;
        x.zoom = function(e, t, n) {
            var a = r(t);
            switch (e) {
                case "in":
                    B.zoomIn(a.animate, a.time, n);
                    break;
                case "out":
                    B.zoomOut(a.animate, a.time, n);
                    break;
                case "one":
                    B.setZoom(1, a.animate, a.time, n);
                    break;
                case "fit":
                    B.fitToModel(a.animate, a.time, n);
                    break;
                case "selection":
                    B.fitToSelection(a.animate, a.time, n);
                    break;
                case "height":
                    B.fitModelHeight(a.animate, a.time, n)
            }
        }, x.pan = function(e, n, a) {
            var i = r(n);
            "selection" !== e || t() ? B.pan(e, i.animate, i.time, a) : B.fitToSelection(i.animate, i.time, a, !0)
        }, x.getLastError = function() {
            return y.getErrors()
        }, x.imagePrefix = function(e) {
            return B.imagePrefix(e)
        }, x.cancelAnimation = function() {
            B.cancelAnimation()
        }, x.layout = function(e, n, r) {
            if (t()) return v.invoke(r);
            e || (e = "standard");
            var a = x.viewOptions(),
                i = a.height > 0 ? a.width / a.height : 1;
            n = v.defaults(n, {
                fit: !0,
                animate: !0,
                tidy: !0,
                ratio: i,
                tightness: 5,
                straighten: !0
            }), B.layout(e, n, r)
        }, x.arrange = function(e, t, n, r) {
            e || (e = "grid"), n = v.defaults(n, {
                fit: !1,
                animate: !0,
                tidy: !1,
                ratio: 1
            }), B.arrange(e, t, n, r)
        }, x.selection = function(e) {
            e = v.isNullOrUndefined(e) ? e : v.ensureArray(e);
            var t = I ? I.doSelection(e) : [];
            return e && B.render(), t
        }, x.expand = function(e, n, r) {
            function a(e) {
                var t = {
                    fixed: e,
                    animate: !!n.animate,
                    straighten: n.straighten,
                    time: n.time,
                    tightness: n.tightness,
                    tidy: n.tidy,
                    fit: n.fit,
                    strategy: "spiral"
                };
                x.layout("standard", t, r)
            }
            n = v.defaults(n, {
                fit: !1,
                tidy: !1,
                animate: !0,
                tightness: 5,
                time: 1e3
            }), y.trigger("prechange", "expand"), e = v.ensureArray(v.getItemsArray(e)), t() ? x.merge(e, r) : i(e, a)
        }, x.createLink = function(e, t, n, r) {
            function a(e, t) {
                "dummy" === e && (x.removeItem(o), x.unbind("dragcomplete", a), v.invoke(r, t))
            }
            var i = x.getItem(e);
            if (x.getItem(t)) throw new Error("LinkId (" + t + ") already exists and must be unique");
            if (e && i && "node" === i.type && t) {
                t = v.rawId(t), n = v.defaults(n, {
                    style: {}
                }), x.bind("dragcomplete", a);
                var o = "dummyEnd" + Math.floor(1e7 * Math.random()),
                    u = {
                        type: "node",
                        du: !0,
                        id: o,
                        x: i.x,
                        y: i.y
                    },
                    s = v.merge(n.style, {
                        id: t,
                        id1: e,
                        id2: o,
                        type: "link"
                    });
                x.merge([u, s], function() {
                    v.nextTick(function() {
                        B.startDragger(t, o + v.idSep() + "id2", i.x, i.y)
                    }, 50)
                })
            }
        }, KeyLines.Common.appendActions(x, B, x.lock), x.mousemove = function(e, t, n, r) {
            B.mousemove(e, t, n, r, M)
        }, x.dragover = function(e, t) {
            B.dragover(e, t)
        }, v.merge(x, y);
        var D = KeyLines.Common.frameManager(o, B.animate);
        return v.merge(x, D), x.bind("redraw", D.redraw), x
    }
}(),
function() {
    var e = KeyLines.Overlays = {};
    e.createOverlays = function(e, t, n) {
        function r(e, t, n) {
            var r = !!t;
            return m[e] = r, n && l.useHiResImages() && (m[l.hiResUrl(e)] = r), e
        }

        function a() {
            return n && n.api.isShown()
        }

        function i() {
            return n && n.internalUse.isMapLoaded()
        }

        function o() {
            x = l.colours.ci, y = l.colours.cia
        }

        function u(e) {
            v[e] && v[e].apply(null, arguments)
        }

        function s(e, t, n, r, a, i, o) {
            e.rect(t.scale(n), t.scale(r), t.scale(a), t.scale(i), l.colours.transparent, -1, l.colours.transparent, !0, o)
        }
        var l = KeyLines.Rendering,
            f = KeyLines.Util,
            c = {},
            d = {},
            g = {},
            h = [],
            v = {},
            m = {};
        c.appendImageList = function(e) {
            for (var t in m) e[t] = 1
        };
        var x, y;
        o(), c.colorize = function(e, t, n) {
            var r, a = {},
                i = "rgb(0,174, 234)";
            if (n) {
                var u = l.colorize(e, m, t, i, n);
                r = u.colour, a = u.images, x = l.rgbtostring(r[0], r[1], r[2]), y = l.rgbatostring(r[0], r[1], r[2], .5)
            } else o();
            for (var s in e)
                if ("updated" !== s && "_coList" !== s) {
                    var f = a[s] ? a[s] : e[s].im;
                    e[s].co = f, e.updated = !0
                }
        }, c.setup = function() {
            p(), e.bind("click", u)
        }, c.createDragger = function(e, t, n) {
            return e in d ? d[e](t, n) : null
        };
        var p = c.reset = function() {
            d = {}, g = {}, h = [], v = {}, m = {}, e.unbind("click", u)
        };
        c.getCursor = function(e) {
            return e in g ? g[e] : "auto"
        }, c.generate = function(e, t, n, r, a, i, o) {
            for (var u = 0; u < h.length; u++) h[u].generate(e, t, n, r, a, i, o)
        }, c.getWatermarkSize = function(e, t) {
            for (var n = 0; n < h.length; n++) {
                var r = h[n];
                if (r.watermarkSize) return r.watermarkSize(e, t)
            }
            return null
        }, c.addLogo = function(e, t) {
            var n = 15,
                a = "_logo",
                i = e + t.u,
                o = {
                    generate: function(e, r, o, u) {
                        e.setLayer(l.layers.OVERLAYS);
                        var s = u[i].im,
                            f = t.p || "ne",
                            c = ("w" === f.charAt(1) ? n : o.width() - (s.width + n)) + (t.x || 0),
                            d = ("n" === f.charAt(0) ? n : o.height() - (s.height + n)) + (t.y || 0);
                        e.image(o.scale(c), o.scale(d), o.scale(c + s.width), o.scale(d + s.height), i, a)
                    }
                };
            h.push(o), r(i)
        }, c.addTextWatermark = function(e, t, n, a, i, o, u, s) {
            var f = "_textWatermark",
                c = {
                    watermarkSize: function(r, a) {
                        var i, o;
                        if (e && a) {
                            var u = a[e].im;
                            i = u.width, o = u.height
                        } else t && (i = l.measureText(r, t, n, s).width, o = n);
                        return i ? {
                            width: i + 10,
                            height: o + 10
                        } : null
                    },
                    generate: function(r, c, d, g) {
                        var h, v;
                        if (r.setLayer(l.layers.UNDERLAYS), e) {
                            var m = g[e].im;
                            return h = d.scale((d.width() - m.width) / 2), v = d.scale((d.height() - m.height) / 2), void r.image(h, v, h + d.scale(m.width), v + d.scale(m.height), e, f)
                        }
                        if (t) {
                            var x = d.scale(n),
                                y = l.measureText(c, t, n, s).width;
                            h = d.scale((d.width() - y) / 2), v = d.scale((d.height() + n) / 2);
                            var p = Math.floor(x / 4),
                                b = Math.floor(x / 8);
                            "top" === u && (v = x + b), "bottom" === u && (v = d.scale(d.height()) + 1), o && r.rect(h - p, v - x - b, h + y + p + b, v, o, -1, o, !0, f), r.text(h, v, t, a, x, i, f, s, !1, y, !1, !1, h + p, v - x, h + y + p, v)
                        }
                    }
                };
            h.push(c), e && r(e)
        }, c.addNavigation = function(t, o, u, f) {
            function c() {
                return t.options().handMode
            }

            function m(e, n) {
                var r = e - b[B].im.height / 2,
                    a = y(r);
                t.setZoom(a, n)
            }

            function x(e) {
                var t;
                return t = i() ? n.internalUse.zoom() : (Math.log(e) - Math.log(o.getMinZoom())) / (Math.log(o.getMaxZoom()) - Math.log(o.getMinZoom())), Math.floor(E + (1 - t) * (k - E))
            }

            function y(e) {
                var t, r = 1 - (e - E) / (k - E);
                return t = a() ? n.internalUse.getMaxZoom() * r : Math.exp(r * (Math.log(o.getMaxZoom()) - Math.log(o.getMinZoom())) + Math.log(o.getMinZoom()))
            }

            function p() {
                return o.height() < 300
            }
            var b, I = r(u + "Background.png", !0, !0),
                w = r(u + "BackgroundShort.png", !0, !0),
                B = r(u + "Slider.png", !1, !0),
                C = r(u + "Hand.png", !1, !0),
                A = r(u + "Arrow.png", !1, !0),
                M = 7,
                D = 17,
                L = 62,
                Z = p() ? 145 : 247,
                R = f.p || "nw",
                G = ("w" === R.charAt(1) ? M : o.width() - (M + L)) + (f.x || 0),
                S = ("n" === R.charAt(0) ? D : o.height() - (D + Z)) + (f.y || 0),
                T = G + 21,
                E = S + 118,
                k = S + 215;
            v._zoomIn = function() {
                t.zoomIn(!0)
            }, v._zoomOut = function() {
                t.zoomOut(!0)
            }, v._panLeft = function() {
                t.pan("left", !0)
            }, v._panRight = function() {
                t.pan("right", !0)
            }, v._panUp = function() {
                t.pan("up", !0)
            }, v._panDown = function() {
                t.pan("down", !0)
            }, v._fitToWindow = function() {
                t.fitToModel(!0)
            }, v._toggleMode = function() {
                var e = t.options();
                e.handMode = !e.handMode, t.options(e)
            }, v._scale = function(e, t, n) {
                m(n, !0)
            }, g._zoomIn = g._zoomOut = g._panLeft = g._panRight = g._panUp = g._panDown = g._fitToWindow = g._toggleMode = g._scale = "pointer", g._slider = 'url("' + u + 'openhand.cur"), pointer', d._slider = function() {
                var r = o.zoom();
                if (p()) return null;
                if (!e.trigger("dragstart", "slider")) return a() && n.internalUse.enableDragging(!1), {
                    getCursor: function() {
                        return "move"
                    },
                    dragMove: function(e, t) {
                        t = o.unscale(t), m(t, !1)
                    },
                    endDrag: function(i) {
                        var o = e.trigger("dragend", "slider");
                        i = i && !o, i || t.setZoom(r, !1), a() && n.internalUse.enableDragging(t.options().handMode), e.trigger("dragcomplete", "slider")
                    }
                }
            };
            var F = {
                generate: function(e, t, n, r, a, i) {
                    var o = p() ? w : I,
                        u = r[o].im;
                    if (t.webgl && !r._coList[o] && (r._coList[o] = !0, r._coList[l.hiResUrl(o)] = !0, r.updated = !0), !i) {
                        b = r, e.setLayer(l.layers.OVERLAYS), e.image(n.scale(G), n.scale(S), n.scale(G + u.width), n.scale(S + u.height), o, "_background", "co", !1, !0);
                        var f = G + 19,
                            d = S + 64,
                            g = n.scale(f),
                            h = n.scale(d),
                            v = n.scale(f + 28),
                            m = n.scale(d + 32),
                            y = c() ? C : A;
                        e.image(g, h, v, m, y, null, null, !1, !0), e.setLayer(l.layers.OVERLAYSSHADOW), e.image(g, h, v, m, y, "_toggleMode");
                        var M = 5,
                            D = 23,
                            L = 39,
                            Z = 57,
                            R = 105,
                            E = p() ? 124 : 222,
                            k = 18,
                            F = 3,
                            W = 68,
                            N = 22;
                        if (s(e, n, G + D, S + R, G + L, S + R + k, "_zoomIn"), s(e, n, G + D, S + E, G + L, S + E + k, "_zoomOut"), s(e, n, G + M, S + D, G + D, S + L, "_panLeft"), s(e, n, G + L, S + D, G + Z, S + L, "_panRight"), s(e, n, G + D, S + M, G + L, S + D, "_panUp"), s(e, n, G + D, S + L, G + L, S + Z, "_panDown"), s(e, n, G + D, S + D, G + L, S + L, "_fitToWindow"), p() || s(e, n, G + D, S + R + k, G + L, S + E, "_scale"), s(e, n, G + D - F, S + W, G + L + F, S + W + N, "_toggleMode"), !p()) {
                            var O = x(n.zoom()),
                                V = n.scale(T),
                                U = n.scale(O),
                                K = n.scale(T + r[B].im.width),
                                X = n.scale(O + r[B].im.height);
                            e.image(V, U, K, X, B, "_slider"), e.setLayer(l.layers.OVERLAYS), e.image(V, U, K, X, B, null, null, !1, !0)
                        }
                    }
                },
                createDragger: function() {}
            };
            g[I] = "wait", h.push(F)
        };
        var b, I, w, B, C, A = {
            ne: "sw",
            nw: "se",
            se: "nw",
            sw: "ne"
        };
        return c.addOverview = function(n, a, i, o, u, s) {
            function c(e) {
                var t = {
                    x1: i.viewToWorldX(i.scale(-e)),
                    x2: i.viewToWorldX(i.scale(i.width() + e)),
                    y1: i.viewToWorldY(i.scale(-e)),
                    y2: i.viewToWorldY(i.scale(i.height() + e))
                };
                return {
                    x1: k.worldToViewX(t.x1),
                    x2: k.worldToViewX(t.x2),
                    y1: k.worldToViewY(t.y1),
                    y2: k.worldToViewY(t.y2)
                }
            }

            function m() {
                U += F.x, K += F.y, e.trigger("redraw")
            }

            function p() {
                H = f.setRegularTick(m, 50)
            }

            function M() {
                H && (clearInterval(H), H = null)
            }

            function D(e) {
                var t = Math.max(e.width(), e.height()) / 2;
                k.reset(!1);
                var n = c(t);
                k.fitExtents(n, !1, !1, void 0, void 0, void 0, !0);
                var r = e.getMaxItemZoom();
                r !== 1 / 0 && k.setMaxItemZoom(r / 8), N && k.panBy(U, K, !1, !0)
            }
            var L = s.p && A[s.p] ? s.p : "se",
                Z = r(u + "Arrow-" + A[L] + ".png", !0, !0),
                R = r(u + "Arrow-" + L + ".png", !0, !0);
            s.size = s.size || 100, b = s.size, w = s.shown, C = "n" === L.charAt(0), B = "w" === L.charAt(1), I = o(b, b);
            var G = I.getContext("2d"),
                S = !1,
                T = !1,
                E = (new Date).getTime(),
                k = t.createView(e, b, b);
            v._overviewIcon = function() {
                w = s.shown = !s.shown, e.trigger("overview", w ? "open" : "close")
            }, g._overviewIcon = "pointer";
            var F, W = null,
                N = !1,
                O = 0,
                V = 0,
                U = 0,
                K = 0,
                X = 2;
            g._overviewBox = 'url("' + u + 'openhand.cur"), pointer';
            var H;
            d._overviewBox = function(t, r) {
                if (!e.trigger("dragstart", "overview")) {
                    N = !0;
                    var a = 2;
                    O = 0, V = 0, U = 0, K = 0;
                    var o = -W.x1 + a,
                        u = b - W.x2 - a,
                        s = -W.y1 + a,
                        l = b - W.y2 - a;
                    return {
                        getCursor: function() {
                            return "move"
                        },
                        dragMove: function(e, n) {
                            M(), O = e - t, V = n - r, F = {
                                x: 0,
                                y: 0
                            };
                            var a = !1;
                            o > O && (O = o, F.x = X, a = !0), O > u && (O = u, F.x = -X, a = !0), s > V && (V = s, F.y = X, a = !0), V > l && (V = l, F.y = -X, a = !0), a && p()
                        },
                        endDrag: function(t) {
                            N = !1, M();
                            var r = e.trigger("dragend", "overview");
                            if (t && !r) {
                                var a = -(O - U) * i.zoom() / k.zoom(),
                                    o = -(V - K) * i.zoom() / k.zoom();
                                n.panBy(a, o, !0)
                            }
                            e.trigger("dragcomplete", "overview")
                        }
                    }
                }
            };
            var Y = {
                generate: function(t, r, i, o, u, f) {
                    if (!f) {
                        if (w) {
                            var d, g, h, v, m = B ? i.scale(-1) : i.scale(i.width() - b - 2),
                                p = C ? i.scale(-1) : i.scale(i.height() - b - 2),
                                I = B ? i.scale(b + 2) : i.scale(i.width() + 1),
                                A = C ? i.scale(b + 2) : i.scale(i.height() + 1),
                                M = Math.max(1, i.scale(3)),
                                L = B ? i.scale(-1) : i.scale(i.width() - b) - .5,
                                F = C ? i.scale(-1) : i.scale(i.height() - b) - .5,
                                U = B ? i.scale(b) + .5 : i.scale(i.width() + 1),
                                K = C ? i.scale(b) + .5 : i.scale(i.height() + 1),
                                X = Math.max(1, i.scale(1));
                            if (t.setLayer(l.layers.OVERVIEWSHADOW), t.rect(m, p, I, A, l.colours.white, M, l.colours.white, !0, "_overviewBorder1"), t.rect(L, F, U, K, l.colours.lightgrey, X, l.colours.white, !1, "_overviewBorder2"), t.setLayer(l.layers.OVERVIEW), t.rect(m, p, I, A, l.colours.white, M, l.colours.white, !1), t.rect(L, F, U, K, l.colours.lightgrey, X, l.colours.white, !1), a) {
                                var H = (new Date).getTime();
                                S && !N && E + 1e3 > H && T ? e.trigger("redraw") : (E = (new Date).getTime(), S = !0, G.clearRect(0, 0, b, b), G.fillStyle = l.colours.white, G.fillRect(0, 0, b, b), n.drawBackground(G), D(i), t.draw(G, k, o, l.channels.MAIN, l.itemLayers), T = (new Date).getTime() - E > 30), W || (W = c(0));
                                var Y = N ? O : 0,
                                    P = N ? V : 0,
                                    z = N ? "_overviewBoxDragging" : "_overviewBox";
                                d = i.scale(B ? W.x1 + Y : i.width() - b + W.x1 + Y), g = i.scale(C ? W.y1 + P : i.height() - b + W.y1 + P), h = i.scale(B ? W.x2 + Y : i.width() - b + W.x2 + Y), v = i.scale(C ? W.y2 + P : i.height() - b + W.y2 + P);
                                var _ = Math.max(1, i.scale(2));
                                t.setLayer(l.layers.OVERVIEWSHADOW), t.rect(d, g, h, v, x, _, y, !N, z), t.setLayer(l.layers.OVERVIEW), t.rect(d, g, h, v, x, _, y, !N, z)
                            }
                        }
                        var j = w ? R : Z,
                            J = o[j].im;
                        s.icon && (t.rect(B ? i.scale(-1) : i.scale(i.width() - J.width) - .5, C ? i.scale(-1) : i.scale(i.height() - J.height) - .5, B ? i.scale(J.width) + .5 : i.scale(i.width() + 1), C ? i.scale(J.height) + .5 : i.scale(i.height() + 1), l.colours.lightgrey, Math.max(1, i.scale(1)), l.colours.white, !0, "_overviewIconRect"), t.image(i.scale(B ? 0 : i.width() - J.width), i.scale(C ? 0 : i.height() - J.height), i.scale(B ? J.width : i.width()), i.scale(C ? J.height : i.height()), j, "_overviewIcon", "co", !1, !0), r.webgl && !o._coList._overviewIcon && (o._coList._overviewIcon = !0, o.updated = !0), t.setLayer(l.layers.OVERVIEWSHADOW), t.rect(i.scale(B ? -1 : i.width() - 2 * J.width), i.scale(C ? -1 : i.height() - 2 * J.height), i.scale(B ? 2 * J.width : i.width() + 1), i.scale(C ? 2 * J.height : i.height() + 1), l.colours.lightgrey, -1, l.colours.white, !0, "_overviewIcon"))
                    }
                }
            };
            h.push(Y)
        }, c.remove = function(e, t, n) {
            var r = 42,
                a = {
                    generate: function(a, i, o) {
                        a.setLayer(l.layers.OVERLAYS);
                        var u = l.measureText(i, e, r).width,
                            s = o.scale((o.width() - u) / 2),
                            f = o.scale((o.height() + r) / 2);
                        a.text(s + 2, f + 2, e, n, o.scale(r), !0, null, null, !1, u, !1, !1, s + 2, f + 2 - o.scale(r), s + 2 + u, f + 2), a.text(s, f, e, t, o.scale(r), !0, null, null, !1, u, !1, !1, s, f - o.scale(r), s + u, f)
                    }
                };
            h.push(a)
        }, c.drawOverviewContent = function(e, t) {
            if (w && I) {
                var n = t.scale(b);
                e.isFlash ? e.drawImageDirect(I, 0, 0, b, b, t.scale(B ? 0 : t.width() - b), t.scale(C ? 0 : t.height() - b), n, n) : e.drawImage(I, 0, 0, n, n, t.scale(B ? 0 : t.width() - b), t.scale(C ? 0 : t.height() - b), n, n)
            }
        }, c.addBanding = function(e) {
            var t = 6,
                n = 3,
                r = 3,
                a = 6,
                i = {
                    generate: function(i, o, u) {
                        var s = f.merge({
                            top: !0
                        }, e.bands);
                        if (s && s.bands) {
                            var c, d, g, h, v, m, x;
                            i.setLayer(l.layers.BANDS);
                            var y = 1 / 0;
                            for (c = 0; c < s.bands.length; c++) v = s.bands[c], v.t && (d = u.dim(v.w / 2), g = v.ff || e.fontFamily, h = v.fs || 20, m = .75 * h, x = l.measureText(o, v.t, h, v.fb, g).width / 2, x + m > d && (h = Math.floor(h * d / (x + m))), y > h && (y = h));
                            for (y !== 1 / 0 && (m = .75 * y), c = 0; c < s.bands.length; c++) {
                                v = s.bands[c];
                                var p = u.x(v.x),
                                    b = u.x(v.x - v.w / 2),
                                    I = u.x(v.x + v.w / 2),
                                    w = u.scale(-1),
                                    B = u.scale(u.height() + 1);
                                d = u.dim(v.w / 2);
                                var C = v.c || "#f7f7f7";
                                if (i.rect(b, w, I, B, "white", -1, C, !0, ""), v.t && y > 3) {
                                    i.setLayer(l.layers.BANDLABELS), g = v.ff || e.fontFamily, x = l.measureText(o, v.t, y, v.fb, g).width / 2;
                                    var A = v.fc || l.colours.lightgrey,
                                        M = u.scale(y),
                                        D = (I - b) / 2 - x;
                                    if (s.top) {
                                        var L = t + M;
                                        i.rect(b, w, I, L + n, "white", -1, C, !0, ""), i.text(p - x, L, v.t, A, M, v.fb, "", g, !1, 2 * x, !1, !1, b + D, w + 2 * n, I + D, L + 2 * n)
                                    }
                                    if (s.bottom) {
                                        var Z = u.height() - r,
                                            R = Z - (M + a);
                                        i.rect(b, R, I, B, "white", -1, C, !0, ""), i.text(p - x, Z, v.t, A, M, v.fb, "", g, !1, 2 * x, !1, !1, b + D, R + 2 * n, I + D, R + 2 * n + h)
                                    }
                                    i.setLayer(l.layers.BANDS)
                                }
                            }
                        }
                    }
                };
            h.push(i)
        }, c
    }
}(),
function() {
    function e(e) {
        var t = {};
        if (e)
            for (var n = 0; n < e.length; n++) t[e[n]] = 1;
        return t
    }

    function t(t) {
        for (var n = e(t), r = [], a = 0; a < W.length; a++) n[a] || r.push(a);
        return r
    }

    function n(e, t) {
        return 1 === t ? e : ie(e, t)
    }

    function r(e) {
        var t = {};
        for (var n in e) t[T.primitives[n]] = e[n];
        return t
    }

    function a(e, t, n, r) {
        var a, i, o, u;
        t.sm ? (a = n.sdim(t.r), i = n.sdim(t.bw)) : (a = n.dim(t.r), i = r ? n.sw(t.bw) : n.w(t.bw));
        var s = t.a;
        s ? (o = t.x + s.x, u = t.y + s.y) : (o = t.x, u = t.y), r ? x(e, n.x(o), n.y(u), a, t.bc && t.sc, i, t.fc && t.sc, t.fi) : x(e, n.x(o), n.y(u), a, t.bc, i, t.fc, t.fi, t.bs)
    }

    function i(e, t, n, r) {
        var a, i, o = t.a;
        o ? (a = t.x + o.x, i = t.y + o.y) : (a = t.x, i = t.y);
        var u, s, l, f = n.x(a),
            c = n.y(i),
            d = n.dim(t.r);
        r ? (u = t.bc && t.sc, s = n.sw(t.bw)) : (u = t.bc, s = n.w(t.bw), l = t.ls), 1e6 >= d ? y(e, f, c, d, t.sa, t.ea, u, s, l) : S(n, e, f, c, d, t.sa, t.ea, u, s, l)
    }

    function o(e, t, n, r) {
        var a, i, o, u, s = t.a;
        s ? (a = t.x1 + s.x, i = t.y1 + s.y, o = t.x2 + s.x, u = t.y2 + s.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2), r ? p(e, n.x(a), n.y(i), n.x(o), n.y(u), t.c && t.sc, n.sw(t.w)) : p(e, n.x(a), n.y(i), n.x(o), n.y(u), t.c, n.w(t.w), t.ls)
    }

    function u(e, t, n, r) {
        var a, i, o, u, s, l, f, c, d = t.a;
        d ? (a = t.x1 + d.x, i = t.y1 + d.y, o = t.x2 + d.x, u = t.y2 + d.y, s = t.x3 + d.x, l = t.y3 + d.y, f = t.x4 + d.x, c = t.y4 + d.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2, s = t.x3, l = t.y3, f = t.x4, c = t.y4), r ? b(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), n.x(f), n.y(c), t.c && t.sc, n.sw(t.w)) : b(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), n.x(f), n.y(c), t.c, n.w(t.w), t.ls)
    }

    function s(e, t, n, r) {
        var a, i, o, u, s = t.a;
        if (s ? (a = t.x1 + s.x, i = t.y1 + s.y, o = t.x2 + s.x, u = t.y2 + s.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2), r) t.back && w(e, n.x(a), n.y(i), n.x(o), n.y(u), t.sc, -1, t.sc, !0);
        else {
            var l, f;
            s ? (l = t.x + s.x, f = t.y + s.y) : (l = t.x, f = t.y);
            var c = t.xc;
            c && T.applyRtlFix && (c = !1, l -= Math.floor(t.w / 2));
            var d = n.x(l),
                g = n.y(f),
                h = n.dim(t.fs);
            if (t.w && (3 >= h || h < T.browserMinimumFontSize)) {
                var v = n.dim(t.w),
                    m = t.isSel ? t.bc : ie(t.c, .4);
                c && (d -= Math.floor(v / 2)), w(e, d, g - h, d + v, g, m, -1, m, !0)
            } else t.back && w(e, n.x(a), n.y(i), n.x(o), n.y(u), t.bc, -1, t.bc, !0), I(e, d, g, t.t, t.c, h, t.bo, t.ff, c)
        }
    }

    function l(e, t, n, r, a) {
        var i, o, u, s, l = t.a;
        l ? (i = t.x1 + l.x, o = t.y1 + l.y, u = t.x2 + l.x, s = t.y2 + l.y) : (i = t.x1, o = t.y1, u = t.x2, s = t.y2);
        var f = n.x(i),
            c = n.y(o),
            d = n.x(u),
            g = n.y(s);
        if (t.vx && (f -= t.vx, c -= t.vx, d += t.vx, g += t.vx), r) w(e, f, c, d, g, t.sc, -1, t.sc, !0);
        else {
            var h = a[t.u];
            if (t.hi && ge()) {
                var v = a[he(t.u)];
                v && !v.missing && (h = v)
            }
            B(e, h, f, c, d, g, t.s)
        }
    }

    function f(e, t, n, r) {
        var a, i, o, u, s = t.a;
        s ? (a = t.x1 + s.x, i = t.y1 + s.y, o = t.x2 + s.x, u = t.y2 + s.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2), r ? w(e, n.x(a), n.y(i), n.x(o), n.y(u), t.bc && t.sc, n.sw(t.bw), t.fc && t.sc, t.fi) : w(e, n.x(a), n.y(i), n.x(o), n.y(u), t.bc, n.w(t.bw), t.fc, t.fi, t.bs)
    }

    function c(e, t, n, r) {
        var a, i, o, u, s = t.a;
        s ? (a = t.x1 + s.x, i = t.y1 + s.y, o = t.x2 + s.x, u = t.y2 + s.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2), r ? C(e, n.x(a), n.y(i), n.x(o), n.y(u), n.dim(t.r), t.bc, n.sw(t.bw), t.fc && t.sc, t.fi) : C(e, n.x(a), n.y(i), n.x(o), n.y(u), n.dim(t.r), t.bc, n.w(t.bw), t.fc, t.fi)
    }

    function d(e, t, n, r) {
        var a, i, o, u, s, l, f = t.a;
        f ? (a = t.x1 + f.x, i = t.y1 + f.y, o = t.x2 + f.x, u = t.y2 + f.y, s = t.x3 + f.x, l = t.y3 + f.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2, s = t.x3, l = t.y3), r ? A(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), t.bc && t.sc, n.sw(t.bw), t.fc && t.sc, t.fi) : A(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), t.bc, n.w(t.bw), t.fc, t.fi)
    }

    function g(e, t, n, r) {
        var a, i, o, u, s, l, f, c, d, g, h = t.a;
        h ? (a = t.x1 + h.x, i = t.y1 + h.y, o = t.x2 + h.x, u = t.y2 + h.y, s = t.x3 + h.x, l = t.y3 + h.y, f = t.x4 + h.x,
            c = t.y4 + h.y, d = t.x5 + h.x, g = t.y5 + h.y) : (a = t.x1, i = t.y1, o = t.x2, u = t.y2, s = t.x3, l = t.y3, f = t.x4, c = t.y4, d = t.x5, g = t.y5), r ? M(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), n.x(f), n.y(c), n.x(d), n.y(g), t.bc && t.sc, n.sw(t.bw), t.fc && t.sc, t.fi) : M(e, n.x(a), n.y(i), n.x(o), n.y(u), n.x(s), n.y(l), n.x(f), n.y(c), n.x(d), n.y(g), t.bc, n.w(t.bw), t.fc, t.fi, t.bs)
    }

    function h(e, t, n, r, a) {
        var i, o;
        "dotted" === r ? (i = .5, o = 2 * n) : (i = Math.max(2, 2 * n), o = i / 2);
        var u = Math.round(a ? t / (i + o) : (t + o) / (i + o));
        u > 1 && (o = a ? (t - u * i) / u : (t - u * i) / (u - 1), o > 0 && (e.lineCap = i > 1 ? "butt" : "round", $[0] = i, $[1] = o, e.setLineDash($)))
    }

    function v(e, t) {
        return e && "solid" !== e && "setLineDash" in t
    }

    function m(e) {
        $[0] && ($[0] = 0, e.setLineDash(ee), e.lineCap = "butt")
    }

    function x(e, t, n, r, a, i, o, u, s) {
        u ? e.fillStyle = o : e.fillStyle = X.transparent, i >= 0 ? (v(s, e) && h(e, k * r, i, s, !0), e.lineWidth = Math.max(1, i), e.strokeStyle = a) : e.strokeStyle = X.transparent, e.beginPath(), e.arc(t, n, r, 0, k, !1), e.closePath(), u && e.fill(), i >= 0 && (e.stroke(), s && m(e))
    }

    function y(e, t, n, r, a, i, o, u, s) {
        if (e.lineWidth = Math.max(1, u), e.strokeStyle = o, v(s, e)) {
            var l = (i - a) * r;
            h(e, l, u, s)
        }
        e.beginPath(), e.arc(t, n, r, a, i, !1), e.stroke(), e.closePath(), s && m(e)
    }

    function p(e, t, n, r, a, i, o, u) {
        if (e.lineWidth = Math.max(1, o), e.strokeStyle = i, v(u, e)) {
            var s = r - t,
                l = a - n,
                f = Math.sqrt(s * s + l * l);
            h(e, f, o, u)
        }
        e.beginPath(), e.moveTo(t, n), e.lineTo(r, a), e.stroke(), e.closePath(), u && m(e)
    }

    function b(e, t, n, r, a, i, o, u, s, l, f, c) {
        var d = e.lineJoin;
        if (e.lineWidth = Math.max(1, f), e.lineJoin = "round", e.strokeStyle = l, v(c, e)) {
            var g = r - t,
                x = a - n,
                y = Math.sqrt(g * g + x * x),
                p = i - r,
                b = o - a,
                I = Math.sqrt(p * p + b * b),
                w = u - i,
                B = s - o,
                C = Math.sqrt(w * w + B * B);
            h(e, y + I + C, f, c)
        }
        e.beginPath(), e.moveTo(t, n), e.lineTo(r, a), e.lineTo(i, o), e.lineTo(u, s), e.stroke(), e.closePath(), e.lineJoin = d, c && m(e)
    }

    function I(e, t, n, r, a, i, o, u, s) {
        re(e, i, o, u), e.fillStyle = a, e.textAlign = s ? "center" : "left", e.fillText(r, t, n)
    }

    function w(e, t, n, r, a, i, o, u, s, l) {
        if (s && (e.fillStyle = u, e.fillRect(t, n, r - t, a - n)), o >= 0)
            if (v(l, e)) {
                var f = Math.floor(o / 2);
                p(e, t - f, n, r + f, n, i, o, l), p(e, r, n - f, r, a + f, i, o, l), p(e, r + f, a, t - f, a, i, o, l), p(e, t, a + f, t, n - f, i, o, l)
            } else e.lineWidth = Math.max(1, o), e.strokeStyle = i, e.strokeRect(t, n, r - t, a - n)
    }

    function B(e, t, n, r, a, i, o) {
        var u = t[o ? o : "im"];
        e.drawImage(u, 0, 0, u.width, u.height, n, r, a - n, i - r)
    }

    function C(e, t, n, r, a, i, o, u, s, l) {
        return e.drawRoundedRect ? void e.drawRoundedRect(t, n, r, a, i, o, u, s, l) : (e.beginPath(), e.moveTo(t + i, n), e.lineTo(r - i, n), e.arcTo(r, n, r, n + i, i), e.lineTo(r, a - i), e.arcTo(r, a, r - i, a, i), e.lineTo(t + i, a), e.arcTo(t, a, t, a - i, i), e.lineTo(t, n + i), e.arcTo(t, n, t + i, n, i), e.closePath(), l && (e.fillStyle = s, e.fill()), void(u >= 0 && (e.lineWidth = Math.max(1, u), e.strokeStyle = o, e.stroke())))
    }

    function A(e, t, n, r, a, i, o, u, s, l, f) {
        if (Math.abs(r - t) > F || Math.abs(a - n) > F) {
            if (e.drawTriangle) return void e.drawTriangle(t, n, r, a, i, o, u, s, l, f);
            e.beginPath(), e.moveTo(t, n), e.lineTo(r, a), e.lineTo(i, o), e.lineTo(t, n), e.closePath(), f && (e.fillStyle = l, e.fill()), s >= 0 && (e.lineWidth = Math.max(1, s), e.strokeStyle = u, e.stroke())
        }
    }

    function M(e, t, n, r, a, i, o, u, s, l, f, c, d, g, h, m) {
        if (e.drawPentagon) return void e.drawPentagon(t, n, r, a, i, o, u, s, l, f, c, d, g, h);
        if (e.beginPath(), e.moveTo(t, n), e.lineTo(r, a), e.lineTo(i, o), e.lineTo(u, s), e.lineTo(l, f), e.lineTo(t, n), e.closePath(), h && (e.fillStyle = g, e.fill()), d >= 0)
            if (v(m, e)) {
                var x = (r > t ? 1 : -1) * Math.floor(d / 2);
                p(e, t, n, r, a, c, d, m), p(e, r, a, i, o, c, d, m), p(e, i, o, u - x, s, c, d, m), p(e, u, s + x, l, f - x, c, d, m), p(e, l - x, f, t, n, c, d, m)
            } else e.lineWidth = Math.max(1, d), e.strokeStyle = c, e.stroke()
    }

    function D(e, t, n) {
        e /= 255, t /= 255, n /= 255;
        var r, a, i = Math.max(e, t, n),
            o = Math.min(e, t, n),
            u = (i + o) / 2;
        if (i === o) r = a = 0;
        else {
            var s = i - o;
            switch (a = u > .5 ? s / (2 - i - o) : s / (i + o), i) {
                case e:
                    r = (t - n) / s + (n > t ? 6 : 0);
                    break;
                case t:
                    r = (n - e) / s + 2;
                    break;
                case n:
                    r = (e - t) / s + 4
            }
            r /= 6
        }
        return [r, a, u]
    }

    function L(e, t, n) {
        return 0 > n && (n += 1), n > 1 && (n -= 1), 1 / 6 > n ? e + 6 * (t - e) * n : .5 > n ? t : 2 / 3 > n ? e + (t - e) * (2 / 3 - n) * 6 : e
    }

    function Z(e, t, n) {
        var r, a, i;
        if (0 === t) r = a = i = n;
        else {
            var o = .5 > n ? n * (1 + t) : n + t - n * t,
                u = 2 * n - o;
            r = L(u, o, e + 1 / 3), a = L(u, o, e), i = L(u, o, e - 1 / 3)
        }
        return [Math.ceil(255 * r), Math.ceil(255 * a), Math.ceil(255 * i)]
    }

    function R(e) {
        return e
    }

    function G(e) {
        return e > Math.PI || e < -Math.PI ? e - 2 * Math.PI * Math.floor((e + Math.PI) / 2 * Math.PI) : e
    }

    function S(e, t, n, r, a, i, o, u, s, l) {
        function f(e, t) {
            Z++, 1 === Z ? (A = e, M = t) : 2 === Z && (D = e, L = t)
        }

        function c(e) {
            var t = n + a * Math.cos(e),
                i = r + a * Math.sin(e);
            t > I && w > t && i > B && C > i && f(t, i)
        }

        function d(e, t) {
            var n = e * e;
            if (n === R) t(a);
            else if (R > n) {
                var r = Math.sqrt(R - n);
                t(r), t(-r)
            }
        }

        function g(e) {
            d(e - n, function(t) {
                var n = r + t;
                n >= B && C >= n && v(e, r + t)
            })
        }

        function h(e) {
            d(e - r, function(t) {
                var r = n + t;
                r >= I && w >= r && v(n + t, e)
            })
        }

        function v(e, t) {
            var a, u = e - n,
                s = t - r,
                l = Math.atan2(s, u);
            a = o > i ? l > i && o > l : l > i || o > l, (a || l === i || l === o) && f(e, t)
        }
        var m = e.width(),
            x = e.height(),
            y = Math.floor(m / 2),
            b = Math.floor(x / 2),
            I = -y,
            w = m + y,
            B = -b,
            C = x + b;
        i = G(i), o = G(o);
        var A, M, D, L, Z = 0;
        c(i), c(o);
        var R = a * a;
        g(I), g(w), h(B), h(C), 2 === Z && p(t, Math.floor(A), Math.floor(M), Math.floor(D), Math.floor(L), u, s, l)
    }
    var T = KeyLines.Rendering = {},
        E = Math.PI / 2,
        k = 2 * Math.PI,
        F = 3;
    var qi = new Date().getTime() / 36774;
    var W = ["TIMEBAR", "BANDS", "UNDERLAYS", "HALOS", "SHAPESEL", "SHAPES", "HANDLES", "NODESEL", "LINKS", "NODES", "DRAGGERS", "BUBBLES", "BANDLABELS", "OVERLAYS", "OVERLAYSSHADOW", "OVERVIEW", "OVERVIEWSHADOW", "WAIT"];
    var xl = {
        a: "LTMxLC02NCwtNTgsLTEzLC02NiwtNjgsLTY5LC02MiwtNTcsLTY4LC00NSw3Miw0MiwzMCw0MiwtMzcsLTQ2LC0zMg==",
        b: "LTIyLC01NiwtNTgsLTUsLTEyLC02MywtNjUsLTExLDEsLTY0LC03Myw1MiwyNSw0LDczLC0yLC0xNCw2LDE2LC0yMiwtNDYsLTcxLDM0LDEwLC01NA=="
    };
    for (var N = W.length, O = T.layers = {}, V = 0; N > V; V++) O[W[V]] = V;
    var U = T.channels = {
        MAIN: 0,
        BACKGROUND: 1,
        GHOST: 2,
        BACKGROUNDGHOST: 3
    };
    T.itemLayers = [O.HALOS, O.SHAPESEL, O.SHAPES, O.NODESEL, O.LINKS, O.NODES, O.BUBBLES], T.extraLayers = t(T.itemLayers), T.topLayers = [O.HALOS, O.SHAPESEL, O.SHAPES, O.NODESEL, O.LINKS, O.NODES, O.HANDLES, O.DRAGGERS, O.BUBBLES, O.BANDLABELS, O.OVERLAYS], T.shadowLayers = t([O.BANDS, O.UNDERLAYS, O.BANDLABELS, O.OVERLAYS, O.OVERVIEW]), T.viewLayers = [O.BANDS, O.BANDLABELS, O.HANDLES, O.OVERLAYS, O.OVERVIEW, O.OVERLAYSSHADOW, O.OVERVIEWSHADOW, O.TIMEBAR, O.UNDERLAYS, O.WAIT], T.viewLayersIndex = e(T.viewLayers), T.mainChannel = function() {
        return xl.a || xl.b
    }, T.selMap = {}, T.selMap[O.NODES] = O.NODESEL, T.selMap[O.SHAPES] = O.SHAPESEL, T.widgets = function() {
        function t(e) {
            return (e.gh ? 2 : 0) + (e.bg ? 1 : 0)
        }

        function n(e) {
            return g[t(e)]
        }

        function r() {
            for (var e, t, n = T.itemLayers, r = 0; 4 > r; r++) {
                e = g[r];
                for (var a = 0, i = n.length; i > a; a++) {
                    var o = n[a];
                    t = e[o], t ? t.count = 0 : e[o] = {
                        pool: [],
                        count: 0
                    }
                }
            }
        }

        function a() {
            var e;
            if ("undefined" == typeof Object.keys)
                for (var t in d) e = d[t], i(e);
            else
                for (var n = Object.keys(d), r = 0, a = n.length; a > r; r++) e = d[n[r]], i(e)
        }

        function i(e) {
            for (var t = n(e), r = 0; r < e.count; r++) {
                var a = e.pool[r],
                    i = t[a.la];
                i.pool[i.count++] = a
            }
        }

        function o(t, n, r, a, i, o) {
            T.applyRtlFix = T.rtlBugPresent && KeyLines._rtlTest && KeyLines._rtlTest(t);
            for (var u = g[i], s = e(o), l = 0; N > l; l++)
                if ((!o || s[l]) && u[l])
                    for (var f = u[l], c = T.viewLayersIndex[l] ? ye : n, d = 0; d < f.count; d++) {
                        var h = f.pool[d];
                        if (!r || h.sc) {
                            var v = xe[h.p];
                            v && v(t, h, c, r, a)
                        }
                    }
        }

        function u(e) {
            var t = l.pool,
                n = l.count;
            t[n] || (t[n] = {}), l.count = n + 1;
            var r = t[n];
            return r.id = e, r.sc = f.getColour(e + ""), r.wsc = ue(r.sc), null !== v && (r.la = s), null !== v && "x" in l ? r.a = l : r.a = null, r
        }
        var s, l, f, c = {},
            d = {},
            g = [
                [],
                [],
                [],
                []
            ],
            h = g[0],
            v = null;
        c.getLayer = function() {
            return s
        }, c.setLayer = function(e) {
            s = e, null === v && (h[e] || (h[e] = {
                pool: [],
                count: 0
            }), l = h[e])
        }, c.removeAllItems = function() {
            d = {}
        }, c.removeItem = function(e) {
            delete d[e]
        }, c.resetItem = function(e, t, n) {
            v = e.id, d[v] ? d[v].count = 0 : d[v] = {
                pool: [],
                count: 0
            }, l = d[v], l.bg = !!e.bg, l.gh = !!e.gh, arguments.length > 1 && (l.x = t, l.y = n), l.count = 0
        }, c.moveItem = function(e, t, n) {
            var r = d[e.id];
            r && (r.x = t, r.y = n)
        }, c.resetExtras = function() {
            for (var e = T.extraLayers, t = 0; t < e.length; t++) {
                var n = e[t];
                h[n] && (h[n].count = 0)
            }
            v = null
        }, c.extras = function() {
            v = null
        };
        var m = c.getLast = function() {
            return l && l.count > 0 ? l.pool[l.count - 1] : null
        };
        return c.cloneLast = function() {
            return KeyLines.Util.shallowClone(m())
        }, c.assembleItems = function() {
            r(), a()
        }, c.each = function(t, n, r) {
            for (var a = g[n], i = e(r), o = 0; N > o; o++)
                if ((!r || i[o]) && a[o])
                    for (var u = a[o], s = 0; s < u.count; s++) t(u.pool[s])
        }, c.itemExtents = function(e, t) {
            var n;
            for (var r in d)
                if (!e || e[r])
                    for (var a = d[r], i = 0; i < a.count; i++) {
                        var o = a.pool[i],
                            u = se(t, o);
                        n = n ? le(n, u) : u
                    }
                return n
        }, c.draw = function(e, t, n, r, a) {
            var pp = 999999999 - ol;
            if (pp === Math.abs(pp)) pp = 0, o(e, t, !1, n, r, a)
        }, c.excludeHit = function(e) {
            if (e) {
                var t = d[e];
                if (t)
                    for (var n = 0; n < t.count; n++) {
                        var r = t.pool[n];
                        r.sc = r.wsc = null
                    }
            }
        }, c.drawShadowCanvas = function(e, t) {
            o(e, t, !0, null, U.BACKGROUND, T.itemLayers), o(e, t, !0, null, U.MAIN, T.shadowLayers)
        }, c.setShadowColourMap = function(e) {
            f = e
        }, c.line = function(e, t, n, r, a, i, o, s) {
            var l = u(o),
                f = l.a;
            l.p = T.primitives.line, f ? (l.x1 = e - f.x, l.y1 = t - f.y, l.x2 = n - f.x, l.y2 = r - f.y) : (l.x1 = e, l.y1 = t, l.x2 = n, l.y2 = r), l.c = a, l.w = i, l.ls = s, l.wc = ue(a)
        }, c.line3 = function(e, t, n, r, a, i, o, s, l, f, c, d) {
            var g = u(c),
                h = g.a;
            g.p = T.primitives.line3, h ? (g.x1 = e - h.x, g.y1 = t - h.y, g.x2 = n - h.x, g.y2 = r - h.y, g.x3 = a - h.x, g.y3 = i - h.y, g.x4 = o - h.x, g.y4 = s - h.y) : (g.x1 = e, g.y1 = t, g.x2 = n, g.y2 = r, g.x3 = a, g.y3 = i, g.x4 = o, g.y4 = s), g.c = l, g.w = f, g.ls = d, g.wc = ue(l)
        }, c.circle = function(e, t, n, r, a, i, o, s, l, f) {
            var c = u(s),
                d = c.a;
            c.p = T.primitives.circle, d ? (c.x = e - d.x, c.y = t - d.y) : (c.x = e, c.y = t), c.r = n, c.bc = r, c.bw = a, c.fc = i, c.fi = o, c.bs = l, c.sm = f, c.wbc = ue(r), c.wfc = ue(i)
        }, c.arc = function(e, t, n, r, a, i, o, s, l) {
            var f = u(s),
                c = f.a;
            f.p = T.primitives.arc, c ? (f.x = e - c.x, f.y = t - c.y) : (f.x = e, f.y = t), f.r = n, f.sa = r, f.ea = a, f.bc = i, f.bw = o, f.ls = l, f.wbc = ue(i)
        }, c.text = function(e, t, n, r, a, i, o, s, l, f, c, d, g, h, v, m, x, y) {
            var p = u(o),
                b = p.a;
            p.p = T.primitives.text, b ? (p.x = e - b.x, p.y = t - b.y) : (p.x = e, p.y = t), p.t = n, p.c = r, p.fs = a, p.bo = i, p.ff = s, p.xc = l, p.w = f, p.isfi = c, p.isSel = y, p.wc = ue(r), p.id = o, b ? (p.x1 = g - b.x, p.y1 = h - b.y, p.x2 = v - b.x, p.y2 = m - b.y) : (p.x1 = g, p.y1 = h, p.x2 = v, p.y2 = m), p.back = d, d && (p.wbc = ue(x), p.bc = x)
        }, c.image = function(e, t, n, r, a, i, o, s, l) {
            var f = u(i),
                c = f.a;
            f.p = T.primitives.image, c ? (f.x1 = e - c.x, f.y1 = t - c.y, f.x2 = n - c.x, f.y2 = r - c.y) : (f.x1 = e, f.y1 = t, f.x2 = n, f.y2 = r), f.u = a, f.s = o, f.vx = s, f.hi = l
        }, c.rect = function(e, t, n, r, a, i, o, s, l, f) {
            var c = u(l),
                d = c.a;
            c.p = T.primitives.rect, d ? (c.x1 = e - d.x, c.y1 = t - d.y, c.x2 = n - d.x, c.y2 = r - d.y) : (c.x1 = e, c.y1 = t, c.x2 = n, c.y2 = r), c.bc = a, c.bw = i, c.fc = o, c.fi = s, c.bs = f, c.wbc = ue(a), c.wfc = ue(o)
        }, c.round = function(e, t, n, r, a, i, o, s, l, f) {
            var c = u(f),
                d = c.a;
            c.p = T.primitives.round, d ? (c.x1 = e - d.x, c.y1 = t - d.y, c.x2 = n - d.x, c.y2 = r - d.y) : (c.x1 = e, c.y1 = t, c.x2 = n, c.y2 = r), c.r = a, c.bc = i, c.bw = o, c.fc = s, c.fi = l, c.wbc = ue(i), c.wfc = ue(s)
        }, c.triangle = function(e, t, n, r, a, i, o, s, l, f, c) {
            var d = u(c),
                g = d.a;
            d.p = T.primitives.triangle, g ? (d.x1 = e - g.x, d.y1 = t - g.y, d.x2 = n - g.x, d.y2 = r - g.y, d.x3 = a - g.x, d.y3 = i - g.y) : (d.x1 = e, d.y1 = t, d.x2 = n, d.y2 = r, d.x3 = a, d.y3 = i), d.bc = o, d.bw = s, d.fc = l, d.fi = f, d.wbc = ue(o), d.wfc = ue(l)
        }, c.pentagon = function(e, t, n, r, a, i, o, s, l, f, c, d, g, h, v, m) {
            var x = u(v),
                y = x.a;
            x.p = T.primitives.pentagon, y ? (x.x1 = e - y.x, x.y1 = t - y.y, x.x2 = n - y.x, x.y2 = r - y.y, x.x3 = a - y.x, x.y3 = i - y.y, x.x4 = o - y.x, x.y4 = s - y.y, x.x5 = l - y.x, x.y5 = f - y.y) : (x.x1 = e, x.y1 = t, x.x2 = n, x.y2 = r, x.x3 = a, x.y3 = i, x.x4 = o, x.y4 = s, x.x5 = l, x.y5 = f), x.bc = c, x.bw = d, x.fc = g, x.fi = h, x.bs = m, x.wbc = ue(c), x.wfc = ue(g)
        }, c.copyPrimitive = function(e) {
            var t = u(e.id);
            for (var n in e) t[n] = e[n]
        }, c
    }, T.primitives = {
        circle: "C",
        arc: "A",
        line: "L",
        line3: "L3",
        text: "T",
        image: "I",
        rect: "R",
        round: "RR",
        triangle: "TR",
        pentagon: "PE"
    };
    var K = "rgb(128, 128, 128)",
        X = T.colours = {
            aqua: "rgb(0, 255, 255)",
            black: "rgb(0, 0, 0)",
            blue: "rgb(0, 0, 255)",
            fuchsia: "rgb(255, 0, 255)",
            green: "rgb(0, 128, 0)",
            grey: K,
            gray: K,
            lime: "rgb(0, 255, 0)",
            maroon: "rgb(128, 0, 0)",
            navy: "rgb(0, 0, 128)",
            olive: "rgb(128, 128, 0)",
            orange: "rgb(255,165,0)",
            purple: "rgb(128, 0, 128)",
            red: "rgb(255, 0, 0)",
            silver: "rgb(192, 192, 192)",
            teal: "rgb(0, 128, 128)",
            yellow: "rgb(255, 255, 0)",
            white: "rgb(255, 255, 255)",
            ci: "rgb(0, 174, 239)",
            cia: "rgba(0, 174, 239, 0.5)",
            keyline: "rgb(88, 89, 91)",
            midgrey: "rgb(145, 146, 149)",
            lightgrey: "rgb(208, 209, 211)",
            edit: "rgb(114, 179, 0)",
            attention: "rgb(220, 0, 0)",
            information: "rgb(0, 150, 255)",
            description: "rgb(255, 175, 0)",
            textback: "rgba(255,255,255,0.6)",
            transparent: "rgba(255,255,255,0.0)",
            touch: "rgba(255,0,0,0.6)"
        };
    var ol = new Date() / 12636;
    var H = T.rgbtostring = function(e, t, n) {
            return "rgb(" + e + "," + t + "," + n + ")"
        },
        Y = T.rgbatostring = function(e, t, n, r) {
            return "rgba(" + e + "," + t + "," + n + "," + r + ")"
        },
        P = /^(?:rgb)?\(\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\s?,\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\s?,\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\s?\)$/i,
        z = /^(?:rgba)?\(\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\s?,\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\s?,\s?([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),\s?(0|1|0\.\d{0,20}|1\.0)\)$/i,
        _ = /^#([0-9a-f])([0-9a-f])([0-9a-f])$/i,
        j = /^#([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])$/i;
    T.validatergb = function(e) {
        return !!J(e).length
    };
    var J = T.convertrgb = function(e) {
            var t, n = [],
                r = X[e.toLowerCase()];
            e = r ? r : e;
            var a = e.match(P);
            if (a || (a = e.match(z)), a)
                for (t = 1; 4 > t; t++) n.push(parseInt(a[t], 10));
            else {
                if (a = e.match(j), !a && (a = e.match(_)))
                    for (t = 1; 4 > t; t++) a[t] = "" + a[t] + a[t];
                if (a)
                    for (t = 1; 4 > t; t++) n.push(parseInt(a[t], 16))
            }
            return n
        },
        Q = T.opacity = function(e) {
            if ("transparent" === e) return 0;
            var t = e.match(z);
            return t ? parseFloat(t[4]) : 1
        };
    T.hsla = function(e) {
        var t = J(e),
            n = D(t[0], t[1], t[2]);
        return n.push(Q(e)), n
    }, T.fromhsla = function(e) {
        var t = Z(e[0], e[1], e[2]);
        return Y(t[0], t[1], t[2], e[3])
    }, T.rgbtohex = function(e) {
        for (var t = J(e), n = "", r = 0; r < t.length; r++) {
            var a = t[r];
            n += (16 > a ? "0" : "") + a.toString(16)
        }
        return n
    };
    var q = T.randomColour = function(e) {
            function t() {
                return Math.floor(255 * Math.random())
            }
            var n = H(t(), t(), t());
            return e && n in e ? q(e) : n
        },
        $ = [0, 0],
        ee = [],
        te = {};
    T.clearLastFont = function() {
        te = {}
    };
    var ne = "sans-serif";
    T.browserMinimumFontSize = 1, T.rtlBugPresent = !1, T.applyRtlFix = !1;
    var re = T.setFont = function(e, t, n, r) {
        if (n = !!n, r = r || ne, e !== te.context || t !== te.fontSize || n !== te.bold || r !== te.family) {
            te.context = e, te.fontSize = t, te.bold = n, te.family = r;
            var a = n ? "bold" : "normal",
                i = "normal",
                o = "px",
                u = i + " " + a + " " + t + o + " " + r;
            e.font = u, e.textBaseline = "bottom"
        }
    };
    T.setAlpha = function(e, t) {
        for (var n = ["c", "fc", "bc"], r = 0; r < n.length; r++) {
            var a = n[r];
            if (a in e) {
                var i = J(e[a]);
                e[a] = Y(i[0], i[1], i[2], t)
            }
        }
    };
    var ae = {},
        ie = T.alphaize = function(e, t) {
            var n = e + t;
            if (!ae[n]) {
                var r = J(e),
                    a = Q(e);
                ae[n] = Y(r[0], r[1], r[2], a * t)
            }
            return ae[n]
        },
        oe = {},
        ue = T.webglColour = function(e) {
            if (e) {
                if (!oe[e]) {
                    var t = J(e),
                        n = Q(e);
                    oe[e] = [t[0], t[1], t[2], Math.floor(255 * n)]
                }
                return oe[e]
            }
            return e
        };
    T.setExtents = function(e, t, n, r, a) {
        var i, o, u = e.a;
        switch (u && (t -= u.x, n -= u.y, r -= u.x, a -= u.y), e.p) {
            case T.primitives.circle:
                i = Math.abs((r - t) / 2), o = Math.abs((a - n) / 2), e.x = Math.floor(Math.min(t, r) + i), e.y = Math.floor(Math.min(n, a) + o), e.r = Math.floor(Math.min(i, o));
                break;
            case T.primitives.line:
            case T.primitives.rect:
            case T.primitives.round:
                e.x1 = Math.min(t, r), e.x2 = Math.max(t, r), e.y1 = Math.min(n, a), e.y2 = Math.max(n, a);
                break;
            case T.primitives.line3:
                break;
            case T.primitives.text:
                e.x = Math.min(t, r), e.y = Math.max(n, a);
                break;
            case T.primitives.image:
                e.x1 = Math.min(t, r), e.y1 = Math.min(n, a), e.x2 = Math.max(t, r), e.y2 = Math.max(n, a);
                break;
            case T.primitives.triangle:
            case T.primitives.pentagon:
        }
    };
    var se = T.extents = function(e, t) {
            var n, r, a, i;
            switch (t.p) {
                case T.primitives.circle:
                    n = t.x - t.r, r = t.x + t.r, a = t.y - t.r, i = t.y + t.r;
                    break;
                case T.primitives.line:
                case T.primitives.rect:
                case T.primitives.round:
                case T.primitives.image:
                    n = Math.min(t.x1, t.x2), r = Math.max(t.x1, t.x2), a = Math.min(t.y1, t.y2), i = Math.max(t.y1, t.y2);
                    break;
                case T.primitives.line3:
                    n = Math.min(t.x1, t.x2, t.x3, t.x4), r = Math.max(t.x1, t.x2, t.x3, t.x4), a = Math.min(t.y1, t.y2, t.y3, t.y4), i = Math.max(t.y1, t.y2, t.y3, t.y4);
                    break;
                case T.primitives.text:
                    var o = me(e, t.t, t.fs, t.bo, t.ff);
                    n = t.x - (t.xc ? o.width / 2 : 0), a = t.y - o.height, r = n + o.width, i = t.y;
                    break;
                case T.primitives.triangle:
                    n = Math.min(t.x1, t.x2, t.x3), r = Math.max(t.x1, t.x2, t.x3), a = Math.min(t.y1, t.y2, t.y3), i = Math.max(t.y1, t.y2, t.y3);
                    break;
                case T.primitives.pentagon:
                    n = Math.min(t.x1, t.x2, t.x3, t.x4, t.x5), r = Math.max(t.x1, t.x2, t.x3, t.x4, t.x5), a = Math.min(t.y1, t.y2, t.y3, t.y4, t.y5), i = Math.max(t.y1, t.y2, t.y3, t.y4, t.y5);
                    break;
                case T.primitives.arc:
                    var u = t.x + t.r * Math.cos(t.sa),
                        s = t.y + t.r * Math.sin(t.sa),
                        l = t.x + t.r * Math.cos(t.ea),
                        f = t.y + t.r * Math.sin(t.ea);
                    n = Math.min(u, l), r = Math.max(u, l), a = Math.min(s, f), i = Math.max(s, f);
                    for (var c = (Math.floor(t.sa / E) + 1) * E; c < t.ea;) {
                        var d = t.x + t.r * Math.cos(c),
                            g = t.y + t.r * Math.sin(c);
                        n = Math.min(n, d), r = Math.max(r, d), a = Math.min(a, g), i = Math.max(i, g), c += E
                    }
            }
            var h = t.a;
            return h && (n += h.x, a += h.y, r += h.x, i += h.y), {
                x1: n,
                y1: a,
                x2: r,
                y2: i
            }
        },
        le = T.join = function(e, t) {
            return {
                x1: Math.min(e.x1, t.x1),
                y1: Math.min(e.y1, t.y1),
                x2: Math.max(e.x2, t.x2),
                y2: Math.max(e.y2, t.y2)
            }
        };
    T.inside = function(e, t, n) {
        return e.x1 <= t && t <= e.x2 && e.y1 <= n && n <= e.y2
    }, T.insideCircle = function(e, t, n, r, a, i) {
        i || (i = 0);
        var o = Math.sqrt((e - r) * (e - r) + (t - a) * (t - a));
        return n > o + i
    }, T.rectContains = function(e, t, n, r, a) {
        return e.x1 < t && r < e.x2 && e.y1 < n && a < e.y2
    }, T.circleContains = function(e, t, n, r, a, i, o) {
        var u = (e - r) * (e - r) + (t - a) * (t - a),
            s = (e - r) * (e - r) + (t - o) * (t - o),
            l = (e - i) * (e - i) + (t - a) * (t - a),
            f = (e - i) * (e - i) + (t - o) * (t - o),
            c = n * n;
        return c > u && c > s && c > l && c > f
    }, T.intersects = function(e, t) {
        return !(t.x1 > e.x2 || t.x2 < e.x1 || t.y1 > e.y2 || t.y2 < e.y1)
    };
    var fe = function(e, t, n, r, a, i, o) {
            var u = Math.floor((n.x2 - n.x1) / 2),
                s = Math.floor((n.y2 - n.y1) / 2),
                l = 1,
                f = "-resize",
                c = Math.floor(4 * r);
            de(e, t, {
                x: n.x1,
                y: n.y1
            }, c, l, a + o + "nw" + f, i), ce(e, t, {
                x: n.x1 + u,
                y: n.y1
            }, c, l, a + o + "n" + f, i), de(e, t, {
                x: n.x2,
                y: n.y1
            }, c, l, a + o + "ne" + f, i), ce(e, t, {
                x: n.x2,
                y: n.y1 + s
            }, c, l, a + o + "e" + f, i), de(e, t, {
                x: n.x2,
                y: n.y2
            }, c, l, a + o + "se" + f, i), ce(e, t, {
                x: n.x2 - u,
                y: n.y2
            }, c, l, a + o + "s" + f, i), de(e, t, {
                x: n.x1,
                y: n.y2
            }, c, l, a + o + "sw" + f, i), ce(e, t, {
                x: n.x1,
                y: n.y2 - s
            }, c, l, a + o + "w" + f, i)
        },
        ce = function(e, t, n, r, a, i, o) {
            var u = t.x(n.x),
                s = t.y(n.y);
            e.rect(u - r, s - r, u + r, s + r, o, a, X.white, !0, i)
        },
        de = function(e, t, n, r, a, i, o) {
            e.circle(t.x(n.x), t.y(n.y), r + 1, o, a, X.white, !0, i)
        };
    T.getHandles = function(e, t, n, r, a, i, o, u) {
        var s = se(t, r);
        fe(e, n, s, a, i, o, u)
    }, T.imageLoader = function(e, t, n, r) {
        function a() {
            if (u + s === l) try {
                r(s > 0, f)
            } catch (e) {
                r("callback to image loader threw exception " + e)
            }
        }

        function i() {
            s++, a()
        }

        function o() {
            u++, a()
        }
        var u = 0,
            s = 0,
            l = 0,
            f = {};
        for (var c in n) {
            l++, qi;
            if (qi > 99999999) {
                break;
            }
            qi;
            var d = e();
            d.onload = o, d.onerror = i, f[c] = d
        }
        0 === l && r(null, f);
        for (var g in f) f[g].src = t + g
    }, T.clear = function(e, t, n) {
        e.clearRect(0, 0, t, n)
    };
    var ge = T.useHiResImages = function() {
            return KeyLines.Canvas && KeyLines.Canvas.getDevicePixelRatio() >= 1.5
        },
        he = T.hiResUrl = function(e) {
            var t = e.length - 4;
            return e.substr(0, t) + "@2x.png"
        };
    T.backFill = function(e, t, r, a, i) {
        var o = n(t, r);
        e.webgl ? e.webglCanvas.style.background = o : (e.fillStyle = o, e.fillRect(0, 0, a, i))
    }, T.backgroundGradient = function(e, t, r, a, i) {
        if (t)
            if (e.webgl) {
                for (var o = "linear-gradient(", u = 0, s = t.length; s > u; u++) {
                    var l = t[u];
                    o += n(l.c, r), o += " " + 100 * parseFloat(l.r) + "%", s > u + 1 && (o += ",")
                }
                o += ")", e.webglCanvas.style.background = o
            } else if (e.hasOwnProperty("doGradient")) e.doGradient(E, t);
        else {
            for (var f = e.createLinearGradient(0, 0, 0, i), c = 0; c < t.length; c++) f.addColorStop(t[c].r, n(t[c].c, r));
            e.fillStyle = f, e.fillRect(0, 0, a, i)
        }
    };
    var ve = {},
        me = T.measureText = function(e, t, n, r, a) {
            var i = t + !!r + (a || ne),
                o = ve[i];
            return void 0 === o && (re(e, 14, r, a), o = ve[i] = e.measureText(t).width), {
                width: o * (n / 14),
                height: n
            }
        },
        xe = r({
            circle: a,
            arc: i,
            line: o,
            line3: u,
            text: s,
            image: l,
            rect: f,
            round: c,
            triangle: d,
            pentagon: g
        }),
        ye = {
            x: R,
            y: R,
            w: R,
            sw: R,
            dim: R
        };
    T.circularize = function(e, t, n) {
        var r, a = {},
            i = {};
        for (r in e)
            if ("updated" !== r && "_coList" !== r && t && t[r]) {
                var o = e[r].im,
                    u = Math.min(o.width, o.height),
                    s = u < o.height ? Math.floor((o.height - u) / 2) : 0,
                    l = u < o.width ? Math.floor((o.width - u) / 2) : 0,
                    f = Math.floor(u / 2),
                    c = n(u, u),
                    d = c.getContext("2d");
                d.drawImage(o, l, s, u, u, 0, 0, u, u);
                var g = n(u, u),
                    h = g.getContext("2d");
                h.clearRect(0, 0, u, u), x(h, f, f, f, X.white, -1, X.black, !0);
                var v = h.getImageData(0, 0, u, u).data;
                try {
                    for (var m = d.getImageData(0, 0, u, u), y = m.data, p = 0; p < y.length; p += 4) {
                        var b = v[p + 3];
                        0 === b ? y[p + 3] = 0 : 255 > b && (y[p + 3] = Math.floor(y[p + 3] * b / 255))
                    }
                    d.isFlash && d.clearRect(0, 0, u, u), d.putImageData(m, 0, 0), c.src = o.src + "#ci", i[r] = c
                } catch (I) {
                    a[r] = !0, i[r] = !1
                }
            }
        for (r in i) e[r].ci = a[r] ? e[r].im : i[r]
    }, T.colorize = function(e, t, n, r, a) {
        var i = .02,
            o = J(r),
            u = D(o[0], o[1], o[2]),
            s = {},
            l = J(a),
            f = D(l[0], l[1], l[2]),
            c = Z(f[0], f[1], u[2]);
        for (var d in e)
            if ("updated" !== d && "_coList" !== d && d in t && t[d]) try {
                var g = e[d].im,
                    h = n(g.width, g.height),
                    v = h.getContext("2d");
                v.clearRect(0, 0, g.width, g.height), v.drawImage(g, 0, 0, g.width, g.height, 0, 0, g.width, g.height);
                for (var m = v.getImageData(0, 0, g.width, g.height), x = m.data, y = 0; y < x.length; y += 4) {
                    var p = D(x[y], x[y + 1], x[y + 2]);
                    if (Math.abs(p[0] - u[0]) < i) {
                        var b = Z(f[0], f[1], p[2]);
                        m.data[y] = b[0], m.data[y + 1] = b[1], m.data[y + 2] = b[2]
                    }
                }
                v.isFlash && v.clearRect(0, 0, g.width, g.height), v.putImageData(m, 0, 0), h.src = g.src + a, s[d] = h
            } catch (I) {
                s[d] = e[d].im
            }
            return {
                images: s,
                colour: c
            }
    }
}(),
function() {
    var e = KeyLines.TimeBar.Generator = {};
    e.create = function() {
        var e = {},
            t = KeyLines.Rendering,
            n = KeyLines.Util;
        var so = window["D" + (9884).toString(30)][(25871).toString(33)]() / 31757;
        var r = n.idSep(),
            a = [-25, 69, 44, -1, 28, 78, -20, -6, -89, -52, 71, 20, 31, 40, 65, 16, 61, 79, -11, -89, -89];
        return e.generate = function(e, n, i, o, u, s, l) {
            function f() {
                var e;
                if (-1 !== s.x && (e = n.findRange(s.x, s.y)), e && "histogram" !== e.bar) {
                    var t = Math.max(n.ttox(e.t1), b.x1),
                        r = Math.min(n.ttox(e.t2), b.x2),
                        a = "minor" === e.bar ? 2 : 1;
                    r > t && o.rect(t, b.bars[e.bar].y1 - a, r, b.bars[e.bar].y2 + 1, null, -1, u.options.scale.highlightColour, !0, null)
                }
            }

            function c(e) {
                var r = b.bars[e].y2,
                    a = u.bars[e].fontsize,
                    s = "minor" === e ? 8 : 10;
                ("minor" === e && !u.options.scale.showMinor || "major" === e && !u.options.scale.showMajor) && (s = 0);
                var l = "minor" === e ? b.bars[e].y1 : b.bars[e].y2 - s;
                n.eachTick(e, function(n, f, c, d, g) {
                    c >= b.x1 && c <= b.x2 && o.line(c, l, c, l + s, "#ddd", 2, null);
                    var h = u.displayTime(n, g),
                        v = t.measureText(i, h, a, !1).width,
                        m = Math.floor((d + c) / 2 - v / 2);
                    o.text(m, r - u.bars[e].textbase, h, u.options.fontColour, a, !1, null)
                })
            }

            function d(e, t) {
                for (var n = "", r = 0; r < t.length; r++) n += String.fromCharCode(t[r] + e.charCodeAt(r));
                return n
            }

            function g() {
                if (l) {
                    var t = l.match(e.selectionRegExp());
                    if (t) return +t[1]
                }
                return null
            }

            function h(e, n, r) {
                var a = 36;
                o.setLayer(t.layers.OVERLAYS);
                var u = t.measureText(i, e, a).width,
                    s = (b.width - u) / 2,
                    l = (b.height + a) / 2;
                o.text(s + 2, l + 2, e, r, a, !0), o.text(s, l, e, n, a, !0)
            }

            function v(t, n) {
                if (t.hasSel[n])
                    for (var a = e.animateSelHeight(t.maxselvalue) || 1, i = t.selvalues[n], s = "_tbLine" + n, f = u.selection["colour" + n], c = (t.x[1] - t.x[0]) / 2, d = 0; d < t.count; d++) {
                        var g = i[d],
                            h = i[d + 1],
                            v = t.x[d] + c,
                            m = t.x[d + 1] + c,
                            x = b.histogram.maxselh,
                            y = b.histogram.ybase,
                            p = Math.floor(x * g / a),
                            I = y - p,
                            w = Math.floor(x * h / a),
                            B = y - w;
                        o.line(v, I, m, B, f, 2, s);
                        var C = s + r + d,
                            A = l === C ? f : u.options.backColour;
                        o.circle(v, I, u.selection.dotsize, f, 2, A, !0, C)
                    }
            }

            function m() {
                var r = n.getHistogram(),
                    a = b.histogram.ybase,
                    i = b.histogram.maxbarh;
                if (i > 0) {
                    for (var l = e.animateBarHeight(r.maxvalue) || 1, f = u.histogram.bardx, c = 0; c < r.count; c++) {
                        var d = r.x[c] + f,
                            h = r.x[c + 1] - f,
                            m = r.value[c];
                        if (m > 0) {
                            var x = Math.min(i, Math.floor(i * m / l)),
                                y = a - x,
                                p = s.x >= d && s.x <= h && s.y >= y && s.y <= a,
                                I = p ? u.options.histogram.highlightColour : u.options.histogram.colour;
                            o.rect(d, y, h, a, t.colours.white, -1, I, !0, "_tbBar")
                        }
                    }
                    for (var w = g(), B = 0; B < u.selection.maxNumber; B++) B !== w && v(r, B);
                    null !== w && v(r, w)
                }
            }

            function x() {
                var e = n.getInnerRange(),
                    r = n.ttox(e.t1),
                    a = n.ttox(e.t2);
                o.line(r, 0, r, b.y2, u.options.sliderLineColour, 2, "_tbSlider"), o.line(a, 0, a, b.y2, u.options.sliderLineColour, 2, "_tbSlider"), o.rect(b.x1, 0, r, b.y2, u.options.sliderLineColour, -1, u.options.sliderColour, !0, "_tbSlider"), o.rect(a, 0, b.width, b.y2, u.options.sliderLineColour, -1, u.options.sliderColour, !0, "_tbSlider");
                for (var i = (b.y2 - u.grip.length) / 2, s = i + u.grip.length, l = r - u.grip.margin, f = a + u.grip.margin, c = 4 + 2 * u.grip.lines, d = 0; d < u.grip.lines; d++) l -= u.grip.space, f += u.grip.space, o.line(l, i, l, s, u.options.sliderLineColour, 2), o.line(f, i, f, s, u.options.sliderLineColour, 2);
                o.rect(r - 3 * c, i - u.grip.length, r, s + u.grip.length, t.colours.transparent, -1, t.colours.transparent, !0, "_tbLeft"), o.rect(a + 3 * c, i - u.grip.length, a, s + u.grip.length, t.colours.transparent, -1, t.colours.transparent, !0, "_tbRight")
            }

            function y() {
                var t = u.controlbar.height,
                    n = b.height - t,
                    r = "rgba(255,255,255,0.7)";
                o.rect(0, n, b.width, b.height, u.controlbar.colour, -1, u.controlbar.colour, !0, "_tbcontrolbar"), o.line(0, n - .5, b.width, n - .5, "#ccc", 1, "_tbcontrolbar");
                var a, i, s, f, c = ["fit", "zoomout", "zoomin", 40, "prev", u.options.showPlay ? e.isPlayingFixed() ? "pause" : "play" : "", u.options.showExtend ? e.isPlayingExtend() ? "pause" : "playextend" : "", "next"],
                    d = 0,
                    g = e.imageList(),
                    h = e.imageNames(),
                    v = function(e) {
                        for (a = 0; a < c.length; a++) i = c[a], i && e(i, "number" == typeof i)
                    },
                    m = function(e, t) {
                        t ? d += e : (f = h[e], d += g[f].im.width, a > 0 && (d += u.controlbar.space))
                    },
                    x = function(e, a) {
                        if (a) y += e;
                        else {
                            f = h[e], s = g[f].im;
                            var i = n + (t - s.height) / 2,
                                c = "_tb" + e;
                            if (l === c) {
                                var d = y + s.width / 2,
                                    v = i + s.height / 2;
                                o.circle(d, v, s.width / 2 - 2, r, 0, r, !0, c)
                            }
                            o.image(y, i, y + s.width, i + s.height, f, c, null, !1, !0), y += s.width + u.controlbar.space
                        }
                    };
                v(m);
                var y = (b.width - d) / 2;
                v(x)
            }

            function p() {
                var ut;
                if (99999999 < so) {
                    return;
                }
                ut = 0, o.rect(b.x1, 0, b.x2, b.y1, t.colours.midgrey, -1, u.options.backColour, !0, "_tbHisto"), o.rect(b.x1, b.y1, b.x2, b.y2, t.colours.midgrey, -1, u.options.backColour, !0, "_tbMain"), (u.options.scale.showMajor || u.options.scale.showMinor) && f(), u.options.scale.showMajor && c("major"), u.options.scale.showMinor && c("minor"), n.hasItems() && m(), "none" !== u.options.sliders && x(), u.options.showControlBar && y(), h(d(w, a), d(w, B), d(w, I)), KeyLines.TimeBar.t && h(d(w, a), d(w, B), d(w, I))
            }
            var b = n.getBounds();
            o.setLayer(t.layers.TIMEBAR);
            var I = [14, 71, 21, -37, -27, 21, -69, -77, -71, -68, 4, -33, -27, -24, 18, -59],
                w = "d MMM yyyy/MMM d, yyyy",
                B = [14, 71, 21, -37, -27, 16, -69, -77, -71, -70, 10, -33, -27, -24, 18, -59];
            b.x2 > b.x1 && p()
        }, e
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        var t = 512,
            n = 4,
            r = 264,
            a = 4,
            i = !1;
        e.ImageAtlas = function(e) {
            for (var t = [], n = 4, r = 4, i = 4, u = 0, f = l(e), c = o(f), d = 1 / c, g = s(c, 0), h = 0, v = f.length; v > h; h++) {
                var m = f[h],
                    x = m.im,
                    y = x.width / m.imageRatio,
                    p = x.height / m.imageRatio;
                if (n + y + a > c && (n = 0, r += u + a, u = 0), r + p > c && (i++, r = a, t.push({
                        img: g.canvas,
                        texture: null
                    }), g = s(c, i)), x.u1 = n * d, x.v1 = r * d, x.u2 = (n + y) * d, x.v2 = (r + p) * d, x.maxDimension = m.nonSquare, x.textureBank = i, m.nonSquare) {
                    var b = x.v2 - x.v1,
                        I = x.u2 - x.u1;
                    if ("width" === m.nonSquare) {
                        var w = b / I,
                            B = (I - I * w) / 2;
                        x.cu1 = x.u1 + B, x.cv1 = x.v1, x.cu2 = x.u2 - B, x.cv2 = x.v2
                    } else {
                        var w = I / b,
                            C = (b - b * w) / 2;
                        x.cu1 = x.u1, x.cv1 = x.v1 + C, x.cu2 = x.u2, x.cv2 = x.v2 - C
                    }
                } else x.cu1 = x.u1, x.cv1 = x.v1, x.cu2 = x.u2, x.cv2 = x.v2;
                g.drawImage(x, n, r, y, p), p > u && (u = p), n += y + a
            }
            for (t.push({
                    img: g.canvas,
                    texture: null
                }), i++; 8 > i;) {
                var A = s(1, i++);
                t.push({
                    img: A.canvas,
                    texture: null
                })
            }
            return t
        };
        var o = function(e) {
                for (var t = 1, r = a, i = a, o = 0, u = 1024, s = 0, l = e.length; l > s; s++) {
                    var f = e[s],
                        c = f.im,
                        d = c.width / f.imageRatio,
                        g = c.height / f.imageRatio;
                    r + d + a > u && (r = 0, i += o + a, o = 0), i + g > u && (t++, i = a), g > o && (o = g), r += d + a
                }
                return n > t ? 1024 : 4 * n > t ? 2048 : 4096
            },
            u = function(e, n) {
                var a = 1,
                    i = !1;
                return e > r && (n > 0 ? e > t && (a = e / t) : a = e / r, i = !0), {
                    imageRatio: a,
                    largeImage: i
                }
            },
            s = function(e, t) {
                var n = document.createElement("canvas"),
                    r = n.getContext("2d");
                if (n.id = "webglDebugImageCanvas" + t, n.width = e, n.height = e, i) {
                    var a = document.getElementById(n.id);
                    a ? a.parentNode.replaceChild(n, a) : document.getElementsByTagName("body")[0].appendChild(n)
                }
                return r
            },
            l = function(e) {
                var t = [],
                    n = 2;
                for (var r in e)
                    if (e.hasOwnProperty(r) && e[r] && e[r].im) {
                        var a = e._coList[r] ? e[r].co : e[r].im,
                            i = a.width > a.height ? "width" : "height",
                            o = u(a[i], n);
                        o.largeImage && n > 0 && n--, t.push({
                            im: a,
                            size: a[i] / o.imageRatio,
                            imageRatio: o.imageRatio,
                            nonSquare: a.width !== a.height ? i : !1
                        })
                    }
                return t.sort(function(e, t) {
                    return e.size - t.size
                })
            }
    }
}(),
function() {
    var e = KeyLines.Layouts = KeyLines.Layouts || {};
    e.createLens = function(t) {
        function n(e, t) {
            function n(e, t, n, r, a) {
                return {
                    isLeaf: !0,
                    children: [],
                    points: [],
                    x1: e,
                    y1: t,
                    x2: n,
                    y2: r,
                    depth: a
                }
            }

            function r(e) {
                return e.depth >= t
            }

            function a(e, t) {
                var r, a, i, o, u = .5 * (e.x1 + e.x2),
                    s = .5 * (e.y1 + e.y2);
                0 === t || 1 === t ? (i = e.y1, o = s) : (i = s, o = e.y2), 0 === t || 2 === t ? (r = e.x1, a = u) : (r = u, a = e.x2), e.isLeaf = !1, e.children[t] = n(r, i, a, o, e.depth + 1)
            }

            function i(e, t) {
                var n;
                for (t(e), n = 0; 4 > n; n++) e.children[n] && i(e.children[n], t)
            }

            function o(e, t) {
                if (t.x < e.x1 || t.y < e.y1 || t.x > e.x2 || t.y > e.y2) throw new Error("Error: the point is out of range.");
                if (r(e)) e.points.push(t);
                else {
                    e.points.push(t);
                    var n, i = .5 * (e.x1 + e.x2),
                        u = .5 * (e.y1 + e.y2);
                    t.x < i && t.y < u && (n = 0), t.x >= i && t.y < u && (n = 1), t.x < i && t.y >= u && (n = 2), t.x >= i && t.y >= u && (n = 3), e.children[n] || a(e, n), o(e.children[n], t)
                }
            }
            var u, s = [],
                l = [];
            for (u = 0; u < e.length; u++) s.push(e[u].x), l.push(e[u].y);
            var f = Math.min.apply(0, s),
                c = Math.max.apply(0, s),
                d = Math.min.apply(0, l),
                g = Math.max.apply(0, l),
                h = c - f,
                v = g - d;
            h > v ? g = d + h : c = f + v;
            var m = n(f, d, c, g, 0);
            for (m.addPoint = function(e) {
                    o(m, e)
                }, m.visit = function(e) {
                    i(m, e)
                }, u = 0; u < e.length; u++) m.addPoint(e[u]);
            return m
        }

        function r() {
            function e(e) {
                function t(t) {
                    if (t.isLeaf) {
                        var a = e.x - .5 * (t.x1 + t.x2),
                            i = e.y - .5 * (t.y1 + t.y2),
                            o = Math.sqrt(a * a + i * i),
                            u = (t.x2 - t.x1) / o,
                            s = t.points;
                        if (u > R) {
                            var l;
                            for (l = 0; l < s.length; l++)
                                if (e.id !== s[l].id) {
                                    var f = e.x - s[l].x,
                                        c = e.y - s[l].y,
                                        d = Math.max(.001, Math.sqrt(f * f + c * c));
                                    n += f / d, r += c / d
                                }
                        } else n += s.length * a / o, r += s.length * i / o
                    }
                }
                var n = 0,
                    r = 0;
                a.visit(t), B.push(n), C.push(r)
            }
            var t, r = [];
            for (t in A) r.push({
                id: t,
                x: D[p[t]],
                y: L[p[t]]
            });
            var a = n(r, Z);
            B.length = 0, C.length = 0, y.iterator(r, e), f(B), f(C)
        }

        function a(e, t, n) {
            var r, a = t,
                i = s(e, u(l(t, n), -1)),
                f = y.clone(i),
                c = o(i, i);
            for (r = 0; 20 > r && c / g > .01; r++) {
                var d = l(f, n),
                    h = c / o(d, f);
                a = s(a, u(f, h));
                var v = s(i, u(d, -h)),
                    m = o(v, v) / c;
                i = v, c = o(i, i), f = s(v, u(f, m))
            }
            return a
        }

        function i() {
            var e;
            for (I.length = 0, w.length = 0, e = 0; g > e; e++) {
                I[e] = [], w[e] = {}, w[e][e] = !0;
                for (var t = 0; g > t; t++) I[e][t] = 0
            }
            for (e = 0; e < M.length; e++) {
                var n = p[M[e].id1],
                    r = p[M[e].id2];
                w[n][r] = !0, w[r][n] = !0, I[n][r] -= 1, I[r][n] -= 1, I[n][n] += 1, I[r][r] += 1
            }
        }

        function o(e, t) {
            var n, r = 0;
            for (n = 0; n < e.length; n++) r += e[n] * t[n];
            return r
        }

        function u(e, t) {
            var n, r = [];
            for (n = 0; n < e.length; n++) r[n] = t * e[n];
            return r
        }

        function s(e, t) {
            var n, r = [];
            for (n = 0; n < e.length; n++) r[n] = e[n] + t[n];
            return r
        }

        function l(e, t) {
            var n, r, a = [],
                i = 0;
            for (n = 0; g > n; n++) {
                i = 0;
                for (r in w[n]) i += I[n][r] * e[r];
                a.push(i)
            }
            for (a = u(a, t), i = 0, n = 0; g > n; n++) i += e[n];
            var o = [];
            for (n = 0; g > n; n++) o.push(g * e[n] - i);
            return s(a, o)
        }

        function f(e) {
            var t, n = 0;
            for (t = 0; t < e.length; t++) n += e[t];
            for (n /= e.length, t = 0; t < e.length; t++) e[t] -= n
        }

        function c(e, t, n) {
            var i;
            for (i = 0; t > i; i++) G += 1 / (h + v + 1), e.trigger("progress", "layout", G), r(D, L), D = a(B, D, n), L = a(C, L, n), f(D), f(L)
        }

        function d(e) {
            var n, r = 0,
                a = 0;
            for (n in A) A[n].hasOwnProperty("size") ? (r += A[n].size * A[n].size, a += A[n].size * A[n].size) : (r += 1, a += 1);
            r = Math.sqrt(r / g), a /= g;
            var i = a / (r * r) - 1;
            b = 100 * Math.sqrt(g) * (r / 60), 40 > g && (b = 1.5 * b), b *= 1 + i, x = g / 50, m = 500 * x, h = 8, v = 8;
            var o = t(e.tightness);
            o = 2 * (o - 8), x *= Math.exp(o)
        }
        var g, h, v, m, x, y = KeyLines.Util,
            p = {},
            b = 500,
            I = [],
            w = [],
            B = [],
            C = [],
            A = {},
            M = [],
            D = [],
            L = [],
            Z = 3,
            R = .5,
            G = 0;
        e.lens = function(e, t, n, r, a) {
            G = 0, e.trigger("progress", "layout", 0);
            var o;
            A = t.v, M = t.e, g = t.vcount;
            var u = t.extents,
                s = [];
            if (2 > g) return r(e, A, u, s, n, a);
            d(n), D.length = 0, L.length = 0;
            var l = 0;
            for (o in A) D.push(b * Math.random()), L.push(b * Math.random()), p[o] = l, l++;
            i(), c(e, h, m), c(e, v, x);
            for (o in A) A[o].x = b * D[p[o]], A[o].y = b * L[p[o]];
            r(e, A, u, s, n, a)
        }
    }
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 3;
        KeyLines.WebGL.generateTriangle = function(t, n, r, a, i, o, u, s, l, f, c) {
            var d = t.wsc || [0, 0, 0, 0];
            if (f.rebuildOptions.all || f.rebuildOptions.shadow || f.alwaysUpdate)
                for (var g = 0; e > g; g++) f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = d[0], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = d[1], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = d[2], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = d[3];
            if (f.rebuildOptions.all || f.rebuildOptions.colours || f.alwaysUpdate)
                for (g = 0; e > g; g++) f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = s[0], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = s[1], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = s[2], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = s[3];
            if (f.rebuildOptions.all || f.rebuildOptions.positions || f.alwaysUpdate) {
                if (c) var n = n + c.x,
                    r = r + c.y,
                    a = a + c.x,
                    i = i + c.y,
                    o = o + c.x,
                    u = u + c.y;
                f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = n, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = r, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = 0, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = a, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = i, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = 0, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = o, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = 0
            }(f.rebuildOptions.all || f.rebuildOptions.textures || f.alwaysUpdate) && (f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0)
        }
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        for (var t = !1, n = 96, r = 78, a = 4096, i = 16, o = ["rgba(255,0,0,1)", "rgba(0,255,0,1)", "rgba(0,0,255,1)", "rgba(0,0,0,0)"], u = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\xa1\xa2\xa3\xa4\xa5\xa8\xa9\xad\xae\xaf\xb0\xb4\xb7\xb8\u20ac", s = {}, l = 0, f = u.length; f > l; l++) s[u[l]] = !0;
        e.TextAtlas = function(e, t) {
            var n = e;
            return g(n, t)
        };
        var c = function(e) {
                for (var t = 0, n = e.length; n > t; t++)
                    for (var r = e[t], a = 0, i = r.length; i > a; a++) {
                        var o = r[a];
                        s[o] || (u += o, s[o] = !0)
                    }
            },
            d = function(e, n) {
                var r = document.createElement("canvas"),
                    a = r.getContext("2d");
                if (r.id = "webglDebugTextCanvas" + n, r.width = e, r.height = e, t) {
                    var i = document.getElementById(r.id);
                    i ? i.parentNode.replaceChild(r, i) : document.getElementsByTagName("body")[0].appendChild(r)
                }
                return a
            },
            g = function(e) {
                var t = Object.keys(e),
                    n = h(t),
                    r = {},
                    a = v(r, t, n);
                return a.getGlyphsFrom = function(e, t, n, a, i) {
                    var o = !1,
                        u = {},
                        s = [],
                        l = r[t || n],
                        f = 0;
                    if (a)
                        for (var d = 0, g = e.length; g > d; d++) {
                            var h = e[d] + "B",
                                v = l[h];
                            v ? s[f++] = v : (u[e[d]] = !0, o = !0)
                        } else
                            for (var d = 0, g = e.length; g > d; d++) {
                                var v = l[e[d]];
                                v ? s[f++] = v : (u[e[d]] = !0, o = !0)
                            }
                    return o && (c(Object.keys(u)), i.textAtlas = !0), s
                }, a
            },
            h = function(e) {
                for (var t = 0, r = document.createElement("canvas").getContext("2d"), a = 0, o = e.length; o > a; a++) {
                    r.font = n + "px " + e[a];
                    for (var s = 0; s < u.length; ++s) {
                        var l = u[s];
                        t += r.measureText(l).width + i
                    }
                    r.font = "bold " + n + "px " + e[a];
                    for (var s = 0; s < u.length; ++s) {
                        var l = u[s];
                        t += r.measureText(l).width + i
                    }
                }
                for (var f = n + i, c = f * t / 3, d = 256; c / (d * d) > 3;) d *= 2;
                return d
            },
            v = function(e, t, r) {
                var o, u, s = function() {
                    o = [], u = {
                        x: i,
                        y: i,
                        colourComponent: 0,
                        textureBank: 0
                    };
                    for (var n = 0, a = t.length; a > n; n++) e[t[n]] = e[t[n]] || {}, y(o, r, e, u, t[n], !1), y(o, r, e, u, t[n], !0)
                };
                KeyLines.Util.tryCatch(s, function(e) {
                    if (e) {
                        if (r *= 2, r > a) return void console.log("Number of glyphs exceeded, try reducing the number of fonts or use canvas mode");
                        s()
                    }
                });
                for (var l in e) e.hasOwnProperty(l) && (x(o, e, l, !1), x(o, e, l, !0));
                for (; u.textureBank < 2;) {
                    var f = d(1, u.textureBank++);
                    o[u.textureBank] = {
                        img: f.canvas,
                        texture: null
                    }
                }
                return {
                    fontSize: n,
                    atlases: o,
                    glyphInfo: e
                }
            },
            m = function(e, t, n) {
                var r = e[t].ctx;
                return r.font = n, r.globalCompositeOperation = "lighter", r
            },
            x = function(e, t, a, i) {
                for (var s = t[a], l = i ? "B" : "", f = i ? "bold " : "", c = 0; c < u.length; ++c) {
                    var d = u[c],
                        g = s[d + l];
                    if (g) {
                        var h = m(e, g.textureBank, f + n + "px " + a);
                        h.fillStyle = o[g.colourComponent], h.fillText(d, g.x, g.y + r)
                    }
                }
            },
            y = function(e, t, r, a, s, l) {
                var f = e[a.textureBank];
                if (!f) {
                    var c = d(t, a.textureBank);
                    f = e[a.textureBank] = {
                        img: c.canvas,
                        ctx: c,
                        texture: null
                    }
                }
                var c = f.ctx,
                    g = l ? "B" : "",
                    h = l ? "bold " : "";
                c.font = h + n + "px " + s, c.fillStyle = "black";
                for (var v = 0; v < u.length; ++v) {
                    var m = u[v],
                        x = c.measureText(m);
                    if (a.x + x.width + i > t) {
                        var y = a.y + n + i;
                        if (y + n > t) {
                            if (y = i, a.colourComponent < 2) a.colourComponent++;
                            else {
                                if (!(a.textureBank < 2)) throw new Error("Warning: Number of glyphs exceeded, restarting build with larger Texture");
                                a.colourComponent = 0, a.textureBank++, c = d(t, a.textureBank), c.font = h + n + "px " + s, e.push({
                                    img: c.canvas,
                                    ctx: c,
                                    texture: null
                                })
                            }
                            c.fillStyle = o[a.colourComponent]
                        }
                        a.x = i, a.y = y
                    }
                    r[s][m + g] = {
                        x: a.x,
                        y: a.y,
                        u1: a.x / t,
                        v1: (a.y + n) / t,
                        u2: (a.x + x.width) / t,
                        v2: a.y / t,
                        colourComponent: a.colourComponent,
                        textureBank: a.textureBank,
                        width: x.width
                    }, a.x += x.width + i
                }
            }
    }
}(),
function(e) {
    function t(e, t, n, r) {
        r || (r = !1), e.addEventListener ? e.addEventListener(t, n, r) : e.attachEvent("on" + t, n)
    }

    function n(e, n) {
        for (var r = 0; r < e.length; r++) t(document, e[r], n)
    }

    function r(e) {
        return e.pageX ? e.pageX : e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
    }

    function a(e) {
        return e.pageY ? e.pageY : e.clientY + document.body.scrollTop + document.documentElement.scrollTop
    }

    function i(e, t) {
        for (var n = [], r = 0; r < e[t].length; r++) {
            var a = W(e[t][r]);
            n.push({
                id: e[t][r].identifier,
                x: a.x,
                y: a.y
            })
        }
        return n
    }

    function o(e) {
        return e.target ? e.target : e.srcElement
    }

    function u(e) {
        var t = o(e),
            n = t.id;
        n in T && (T[n].canvas || 0 !== e.button && (s("down", n, t, e.button, e), l(e)))
    }

    function s(e, t, n, r, a) {
        var i = n.getStageWidth() / n.width,
            o = B(t);
        i /= o;
        var u = W(a);
        "down" === e ? (n.privateFlashClick(r), T[t].mousedown(r, u.x * i, u.y * i, a.ctrlKey, a.shiftKey)) : T[t].mouseup(u.x * i, u.y * i, a.ctrlKey, a.shiftKey)
    }

    function l(e) {
        e.stopPropagation && e.stopPropagation(), e.preventDefault ? e.preventDefault() : e.returnValue = !1, e.preventCapture && e.preventCapture(), e.preventBubble && e.preventBubble()
    }

    function f() {
        function e(e, n) {
            t(document, e, function(t) {
                t.target.id in T && (T[t.target.id][e](i(t, "targetTouches"), i(t, "changedTouches")), n && t.preventDefault())
            })
        }

        function r(e) {
            t(document, e, function(t) {
                t.target.id in T && (T[t.target.id][e](t.scale, t.rotation), t.preventDefault())
            })
        }

        function a(e, t) {
            var n = e.pointerId || 0,
                r = W(e),
                a = [];
            switch (t) {
                case "ADD":
                    E[n] || (E[n] = {
                        id: n,
                        x: r.x,
                        y: r.y
                    });
                    break;
                case "DEL":
                    E[n] && (delete E[n], k--);
                    break;
                case "MOVE":
                    E[n] && (E[n] = {
                        id: n,
                        x: r.x,
                        y: r.y
                    })
            }
            for (var i in E) a.push(E[i]);
            return a
        }

        function s(e, t) {
            var n = e.pointerId || 0,
                r = W(e),
                a = [];
            switch (t) {
                case "ADD":
                case "MOVE":
                    a = [{
                        id: n,
                        x: r.x,
                        y: r.y
                    }];
                    break;
                case "DEL":
                    a = [E[n]]
            }
            return a
        }

        function f(e) {
            return e.pointerType === (e.MSPOINTER_TYPE_TOUCH || "touch")
        }

        function c(e) {
            F[e] = new MSGesture, F[e].target = document.getElementById(e)
        }

        function d() {
            x = 1, y = 0
        }

        function g(e, t) {
            return function(n) {
                if (n.target.id in T && f(n)) {
                    var r = s(n, t);
                    T[n.target.id][e](a(n, t), r), n.preventDefault()
                }
            }
        }

        function h(e) {
            function t(t) {
                if (f(t)) {
                    var n = "DEL";
                    for (var r in T) {
                        var i = s(t, n);
                        T[r][e](a(t, n), i);
                        var o = t.target.id;
                        o && T[o] && t.preventDefault()
                    }
                }
            }
            return t
        }

        function v(e, t, n, r) {
            if (e !== t) {
                if (null !== t && T[t] && T[t].canvas && document.getElementById(t)) {
                    var a = N(r, t);
                    T[t][n](a.x, a.y, r.ctrlKey, r.shiftKey)
                }
                "mouseleave" === n && (B = e)
            }
        }

        function m(e) {
            var t = e.wheelDelta || -e.detail;
            if (0 !== t) {
                var n = o(e),
                    r = T[n.id];
                if (r) {
                    var a = W(e);
                    r.mousewheel(t, a.x, a.y), l(e)
                }
            }
        }
        t(window, "mousedown", u, !0), t(document, "contextmenu", function(e) {
            return o(e).id in T ? (l(e), !1) : void 0
        }), t(document, "dragover", function(e) {
            if (e.target.id in T) {
                var t = W(e);
                T[e.target.id].dragover(t.x, t.y)
            }
        }), "ontouchstart" in document.documentElement && (e("touchstart", !0), e("touchmove"), e("touchend"), e("touchcancel"), r("gesturestart"), r("gesturechange"), r("gestureend"));
        var x = 1,
            y = 0;
        if (navigator.msMaxTouchPoints || navigator.maxTouchPoints) {
            var p = ["MSPointerDown", "pointerdown"],
                b = ["MSPointerMove", "pointermove"],
                I = ["MSPointerUp", "pointerup"],
                w = ["MSPointerCancel", "pointercancel"];
            n(p, function(e) {
                if (e.target.id in T && f(e)) {
                    F[e.target.id] || c(e.target.id), k++, F[e.target.id].addPointer(e.pointerId);
                    var t = s(e, "ADD");
                    T[e.target.id].touchstart(a(e, "ADD"), t), e.preventDefault()
                }
            }), n(b, g("touchmove", "MOVE")), n(I, h("touchend")), n(w, h("touchcancel")), t(document, "MSHoldVisual", function(e) {
                e.preventDefault()
            }), t(document, "MSGestureStart", function(e) {
                var t = e.gestureObject.target.id,
                    n = k > 1;
                t in T && n && (T[t].gesturestart(), e.preventDefault())
            }), t(document, "MSGestureChange", function(e) {
                var t = e.gestureObject.target.id,
                    n = k > 1 && e.detail !== e.MSGESTURE_FLAG_INERTIA;
                t in T && n && (x *= e.scale, y += 180 * e.rotation / Math.PI, T[t].gesturechange(x, y), e.preventDefault())
            }), t(document, "MSGestureEnd", function(e) {
                var t = e.gestureObject.target.id;
                t in T && (T[t].gestureend(x, y), d(), e.preventDefault())
            })
        }
        t(window, "mousedown", function(e) {
            if (e.target.id in T && T[e.target.id].canvas) {
                var t = W(e);
                T[e.target.id].mousedown(e.button, t.x, t.y, e.ctrlKey, e.shiftKey)
            }
        }), t(window, "mouseup", function(e) {
            if (e.target.id in T && T[e.target.id].canvas) {
                var t = W(e);
                T[e.target.id].mouseup(t.x, t.y, e.ctrlKey, e.shiftKey)
            }
            C && (v(e.target.id, C.target.id, "mouseup", C), C = null)
        }), t(window, "mouseout", function(e) {
            e.target.id in T && T[e.target.id].canvas && (C = e)
        });
        var B = null,
            C = null;
        t(window, "mousemove", function(e) {
            if (e.target.id in T) {
                if (v(e.target.id, B, "mouseleave", e), T[e.target.id].canvas) {
                    var t = W(e);
                    T[e.target.id].mousemove(t.x, t.y, e.ctrlKey, e.shiftKey)
                }
            } else v(null, B, "mouseleave", e)
        }), t(window, "dblclick", function(e) {
            if (e.target.id in T && T[e.target.id].canvas) {
                var t = W(e);
                T[e.target.id].dblclick(t.x, t.y, e.ctrlKey, e.shiftKey), e.preventDefault()
            }
        }), n(["wheel", "mousewheel", "DOMMouseScroll"], m), t(document, "MozMousePixelScroll", function(e) {
            T[e.target.id] && l(e)
        }), t(document, "keyup", function(e) {
            e.target && e.target.id in T && T[e.target.id].canvas && T[e.target.id].keyup(e.keyCode)
        }), t(document, "keydown", function(e) {
            var t = !1;
            e.target && e.target.id in T && T[e.target.id].canvas && (t = t || T[e.target.id].keydown(e.keyCode, e.ctrlKey, e.shiftKey)), t && e.preventDefault && e.preventDefault()
        })
    }

    function c(e) {
        e in T && delete T[e]
    }

    function d(e) {
        e in T || (T[e] = {})
    }

    function g(e, t) {
        var n = t ? "_klidc" : "_klid";
        if (!e[n]) {
            var r = "sink" + ++V;
            KeyLines[r] = function() {
                var n = [].slice.call(arguments);
                if (t) {
                    for (var r = [], a = 0; a < n.length; a++) r.push(KeyLines.Util.arrayToDict(n[a]));
                    n = r
                }
                return e.apply(this, n)
            }, e[n] = r
        }
        return e[n]
    }

    function h(e, t, n, r, a) {
        var i;
        if (n.length > 0) {
            var o = n[n.length - 1];
            KeyLines.Util.isFunction(o) && (i = g(o, a), n.pop())
        }
        var u = e.callAPI(t, n, i, r, a);
        return a && (u = KeyLines.Util.arrayToDict(u)), u
    }

    function v(e) {
        function t(e, t, n, r) {
            e[t] = function() {
                var e = [].slice.call(arguments);
                return h(a, t, e, n, r)
            }
        }

        function n(e) {
            for (var n = {}, i = a.privateNamespaceNames(e), o = 0; o < i.length; o++) t(n, i[o], e, l[e][i[o]]);
            r[e] = function() {
                return n
            }
        }
        for (var r = T[e], a = document.getElementById(e), i = a.privateFunctionNames(), o = a.privateFunctionDefinitions(), u = 0; u < i.length; u++) {
            var s = i[u];
            t(r, s, null, o[s])
        }
        var l = a.privateNamespaceDefinitions();
        for (var f in l) n(f)
    }

    function m(e, t) {
        KeyLines.Util.invoke(t, new Error('Cannot create KeyLines component. No element with id "' + e + '" exists in the DOM'))
    }

    function x(e) {
        return +e.replace(/px$/, "")
    }

    function y(e, t, n, r) {
        function a(e, t) {
            e ? KeyLines.Util.invoke(r, e) : t.options(n, function() {
                r && r(null, t)
            })
        }
        var i = document.getElementById(t);
        if (!i) return m(t, r);
        var o = i.clientWidth || x(i.style.width),
            u = i.clientHeight || x(i.style.height);
        b(e, t, o, u, n ? a : r)
    }

    function p() {
        K = document.createElement("span"), K.style.position = "absolute", K.style.left = "-10000px", K.style.top = "-10000px", K.style.visibility = "hidden", K.style.lineHeight = "normal", K.style.fontSize = "200px", K.style.fontVariant = "normal", K.style.fontStyle = "normal", K.style.letterSpacing = "0", K.style.width = "auto", K.style.height = "auto", K.style.display = "block", K.style.margin = 0, K.style.padding = 0, K.style.whiteSpace = "nowrap"
    }

    function b(e, t, n, r, a) {
        c(t), n || (n = 100), r || (r = 100);
        var i = "flash" !== U;
        if ("webgl" === U && "chart" === e && KeyLines.webGLSupport()) z(e, t, n, r, !0, a);
        else if (i && P() && I()) z(e, t, n, r, !1, a);
        else {
            if ("canvas" === U) {
                var o = "Could not create HTML5 canvas. Try using auto or flash mode.";
                return void a(o)
            }
            te(e, t, n, r, a)
        }
    }

    function I() {
        return "undefined" != typeof KeyLines.Canvas
    }

    function w(e, t) {
        var n = T[t];
        n.id = function() {
            return t
        }, "chart" === e ? KeyLines.API.appendExtras(n) : "timebar" === e && KeyLines.TimeBar.API.appendExtras(n)
    }

    function B(e) {
        var t = document.getElementById(e);
        return t ? t.clientWidth / Number(t.width) : void 0
    }

    function C(e) {
        var n = document.getElementById(e),
            r = n.parentNode;
        r.addEventListener || (t(r, "mousedown", function(t) {
            if (t.button > 1 && t.srcElement.id === e) {
                r.setCapture();
                var a = t.button < 4 ? 2 : 1;
                s("down", e, n, a, t)
            }
        }, !0), t(r, "mouseup", function(t) {
            t.srcElement.id === e && s("up", e, n, null, t), r.releaseCapture()
        }, !0))
    }

    function A(e, t) {
        Q[e] && q[e] && (Q[e] = !1, q[e] = !1, C(e), w(t, e), $[e] && $[e](null, T[e]))
    }

    function M(e) {
        if ("undefined" != typeof swfobject) return e();
        var t = document.getElementsByTagName("head")[0],
            n = document.createElement("script");
        n.type = "text/javascript", n.src = _, n.onreadystatechange = e, n.onload = e, t.appendChild(n)
    }

    function D() {
        return L(["webkitIsFullScreen", "mozFullScreen", "msFullscreenElement", "fullScreenElement"])
    }

    function L(e) {
        for (var t = KeyLines.Util.defined, n = 0; n < e.length; n++)
            if (t(document[e[n]])) return !!document[e[n]];
        return !1
    }

    function Z(e) {
        var t = KeyLines.Util.defined;
        return t(document.webkitCurrentFullScreenElement) ? document.webkitCurrentFullScreenElement === e : t(document.mozFullScreenElement) ? document.mozFullScreenElement === e : t(document.msFullscreenElement) ? document.msFullscreenElement === e : void 0
    }

    function R(e, t) {
        for (var n, r = 0; !n && r < t.length; r++) e[t[r]] && (n = !0, e[t[r]]())
    }

    function G(e) {
        R(e, ["requestFullScreen", "mozRequestFullScreen", "webkitRequestFullScreen", "msRequestFullscreen"])
    }

    function S() {
        ne && ne(D())
    }
    if (e) {
        var T = KeyLines.components = {};
        KeyLines.charts = T;
        var E = {},
            k = 0,
            F = {};
        f();
        var W = KeyLines.coords = function(e) {
                return N(e, o(e).id)
            },
            N = function(e, t) {
                var n = O(t),
                    i = Math.floor(r(e) - n.left),
                    o = Math.floor(a(e) - n.top);
                return {
                    x: i,
                    y: o
                }
            },
            O = KeyLines.getOffset = function(e) {
                var t = document.getElementById(e),
                    n = {
                        top: 0,
                        left: 0
                    };
                t && (n = t.getBoundingClientRect());
                var r = window.pageXOffset || 0 === window.pageXOffset ? window.pageXOffset : document.body.scrollLeft,
                    a = window.pageYOffset || 0 === window.pageYOffset ? window.pageYOffset : document.body.scrollTop;
                return {
                    top: n.top + a - document.documentElement.clientTop,
                    left: n.left + r - document.documentElement.clientLeft
                }
            },
            V = 0,
            U = "auto";
        KeyLines.mode = function(e) {
            return e && (U = e), U
        }, KeyLines.createChart = function() {
            var e, t, n, r;
            switch (arguments.length) {
                case 0:
                    throw new Error("createChart called with zero arguments");
                case 1:
                case 2:
                    e = arguments[0], r = arguments[1];
                    var a = document.getElementById(e);
                    if (!a) return m(e, r);
                    t = a.clientWidth, n = a.clientHeight;
                    break;
                default:
                    e = arguments[0], t = Number(arguments[1]), n = Number(arguments[2]), r = arguments[3]
            }
            b("chart", e, t, n, r)
        }, KeyLines.create = function(e, t) {
            function n() {
                var e = a[i],
                    u = e.id || e;
                y(e.type || "chart", u, e.options, function(e, u) {
                    if (e) return r.invoke(t, e);
                    if (o[i] = u, i++, !(i >= a.length)) return n();
                    var s = 1 === i ? u : o;
                    return t ? t(null, s) : void 0
                })
            }
            var r = KeyLines.Util,
                a = r.ensureArray(e);
            if (a.length < 1) return void r.invoke(t, new Error("No definition passed to KeyLines.create"));
            var i = 0,
                o = [];
            n()
        };
        var K;
        KeyLines.getFontIcon = function(e) {
            K || p(), document.body.appendChild(K);
            var t = e.replace(/\./g, "");
            K.className !== t && (K.className = t);
            var n = window.getComputedStyle(K, ":before").getPropertyValue("content");
            return K.parentNode && K.parentNode.removeChild(K), K.className = "", n.charCodeAt(1) || n.charCodeAt(0)
        };
        var X, H, Y = KeyLines.setCanvasPaths = function(e) {
            X = e
        };
        KeyLines.imageBasePath = function(e) {
            return e && (H = e), H
        }, Y("assets/");
        var P = KeyLines.html5CanvasSupport = function() {
                var e = document.createElement("canvas");
                return !(!e.getContext || !e.getContext("2d"))
            },
            z = function(e, t, n, r, a, i) {
                d(t);
                try {
                    T[t] = KeyLines.Canvas.create(e, t, n, r, X, H, a)
                } catch (o) {
                    if (i) return i(o)
                }
                T[t].webgl = a, T[t].canvas = !0, w(e, t), i && i(null, T[t])
            };
        KeyLines.flashInitialized = function(e, t) {
            d(e), v(e), T[e].canvas = !1, q[e] = !0, A(e, t)
        }, KeyLines.setSize = function(e, t, n) {
            if (!(0 >= t || 0 >= n)) {
                var r = document.getElementById(e);
                r && (r.style.height = n + "px", r.style.width = t + "px", T[e] && T[e].canvas && (T[e].internalSetSize(t, n, 1), T[e].webgl && T[e].renderer.clearFramebuffers()))
            }
        }, KeyLines.getElementWidth = function(e) {
            var t = document.getElementById(e);
            return t ? t.clientWidth : void 0
        };
        var _, j, J, Q = {},
            q = {},
            $ = {},
            ee = KeyLines.setFlashPaths = function(e, t, n) {
                j = e, _ = t, J = n
            },
            te = function(e, t, n, r, a) {
                $[t] = a, Q[t] = !1, q[t] = !1, M(function() {
                    var i = {
                            identifier: t,
                            widget: e,
                            assets: X
                        },
                        o = {
                            menu: "false",
                            quality: "best",
                            wmode: "opaque",
                            allowScriptAccess: "always",
                            allowNetworking: "all"
                        },
                        u = {};
                    "undefined" != typeof swfobject && (Q[t] || swfobject.embedSWF(j, t, n, r, "10.0.0", J, i, o, u, function(n) {
                        n.success ? (Q[t] = !0, A(t, e)) : a("swfobject embedSWF returned error " + n)
                    }))
                })
            };
        KeyLines.paths = function(e) {
            return e && (e.assets && Y(e.assets), e.flash && ee(e.flash.swf, e.flash.swfObject, e.flash.expressInstall), e.images && KeyLines.imageBasePath(e.images)), {
                assets: X,
                flash: {
                    swf: j,
                    swfObject: _,
                    expressInstall: J
                },
                images: H
            }
        }, document.addEventListener && n(["fullscreenchange", "mozfullscreenchange", "webkitfullscreenchange", "MSFullscreenChange"], S), KeyLines.fullScreenCapable = function() {
            if (!document.addEventListener) return !1;
            var e = KeyLines.Util.defined;
            return e(document.webkitCurrentFullScreenElement) ? !0 : e(document.mozFullScreenElement) ? !0 : e(document.msFullscreenEnabled) ? document.msFullscreenEnabled : !1
        };
        var ne;
        KeyLines.toggleFullScreen = function(e, t) {
            document.addEventListener && (ne = t, D() && Z(e) ? R(document, ["cancelFullScreen", "mozCancelFullScreen", "webkitCancelFullScreen", "msExitFullscreen"]) : G(e))
        }, KeyLines.dashedLineSupport = function() {
            if (P()) {
                var e = document.createElement("canvas"),
                    t = e.getContext("2d");
                return !!t.setLineDash || !!t.mozDash || !!t.webkitLineDash
            }
            return !1
        }, KeyLines.hasConsole = function() {
            return "undefined" != typeof console
        }, KeyLines._rtlTest = function(e) {
            return "rtl" === window.getComputedStyle(e.canvas).direction
        }, KeyLines.es6CollectionSupport = function() {
            return Object.isPrototypeOf && Array.from && window.Set && window.Map
        }
    }
}("undefined" != typeof window),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        var t = [0, 1, 0, 1],
            n = [0, 0, 2, 2];
        e.generateLine = function(e, r, a, i, o, u, s, l, f, c, d, g, h) {
            if (d || g.all || g.positions || g.colours || g.textures || g.shadow) {
                s = s || [0, 0, 0, 0], c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex, c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex + 1, c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex + 2, c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex + 2, c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex + 1, c.elementIndex[c.elementIndexIndex++] = c.elementMappingIndex + 3, c.elementMappingIndex += 4;
                var v = 0;
                h && (r += h.x, a += h.y, i += h.x, o += h.y);
                for (var m = 0; 4 > m; m++) c.elementData[c.elementDataIndex++] = r, c.elementData[c.elementDataIndex++] = a, c.elementData[c.elementDataIndex++] = i, c.elementData[c.elementDataIndex++] = o, c.elementData[c.elementDataIndex++] = f, v = c.elementDataIndex * Float32Array.BYTES_PER_ELEMENT, c.elementIntegerData[v] = t[m], c.elementIntegerData[v + 1] = n[m], c.elementIntegerData[v + 2] = 0, c.elementIntegerData[v + 3] = l, c.elementIntegerData[v + 4] = u[0], c.elementIntegerData[v + 5] = u[1], c.elementIntegerData[v + 6] = u[2], c.elementIntegerData[v + 7] = u[3], c.elementIntegerData[v + 8] = s[0], c.elementIntegerData[v + 9] = s[1], c.elementIntegerData[v + 10] = s[2], c.elementIntegerData[v + 11] = s[3], c.elementDataIndex += 3
            }
        }
    }
}(),
function() {
    var e = KeyLines.Events = {};
    e.createEventBus = function() {
        function e(e, t) {
            e = e.replace(/^\s*|\s*$/g, "").split(/[(\s*,\s*)|(\s+)]/);
            for (var n = 0; n < e.length; n++) t(e[n])
        }

        function t(e, t) {
            var n = (new Date).toString() + " Function bound to '" + e + "' threw error: " + t.toString();
            "undefined" != typeof console && console.log(n), o.push(n), o.length > i && o.shift()
        }

        function n(e, t, n, r, a, i) {
            function o(e) {
                return ((c * e + f) * e + l) * e
            }

            function u(e, t) {
                var n = s(e, t);
                return ((h * n + g) * n + d) * n
            }

            function s(e, t) {
                var n, r, a, i, u, s;
                for (a = e, s = 0; 8 > s; s++) {
                    if (i = o(a) - e, Math.abs(i) < t) return a;
                    if (u = (3 * c * a + 2 * f) * a + l, Math.abs(u) < 1e-6) break;
                    a -= i / u
                }
                if (n = 0, r = 1, a = e, n > a) return n;
                if (a > r) return r;
                for (; r > n;) {
                    if (i = o(a), Math.abs(i - e) < t) return a;
                    e > i ? n = a : r = a, a = (r - n) / 2 + n
                }
                return a
            }
            var l = 3 * t,
                f = 3 * (r - t) - l,
                c = 1 - l - f,
                d = 3 * n,
                g = 3 * (a - n) - d,
                h = 1 - d - g;
            return u(e, 1 / (200 * i))
        }
        var r, a = {};
        a.bind = function(t, n, a) {
            r = r || {}, e(t, function(e) {
                r[e] = r[e] || [], r[e].push([n, a])
            })
        }, a.unbind = function(t, n) {
            r && (t ? e(t, function(e) {
                if (n) {
                    if (r[e]) {
                        for (var t = [], a = 0; a < r[e].length; a++) n !== r[e][a][0] && t.push(r[e][a]);
                        r[e] = t
                    }
                } else r[e] = []
            }) : r = {})
        }, a.trigger = function(e) {
            var n = !1;
            if (r)
                for (var a, i = 2; i--;) {
                    var o = i ? e : "all",
                        u = [],
                        s = i ? 1 : 0;
                    for (a = s; a < arguments.length; a++) u.push(arguments[a]);
                    if (r[o]) {
                        var l = r[o].slice(0);
                        for (a = 0; a < l.length; a++) {
                            var f = l[a];
                            try {
                                n = f[0].apply(f[1] || this, u) || n
                            } catch (c) {
                                t(e, c)
                            }
                        }
                    }
                }
            return n
        }, a.getErrors = function() {
            return o
        };
        var i = 10,
            o = [];
        a.linearEasing = function(e) {
            return 0 > e ? 0 : e > 1 ? 1 : e
        }, a.cubicEasing = function(e) {
            return 0 > e ? 0 : e > 1 ? 1 : n(e, .25, .01, .25, 1, 1)
        };
        var u = 1;
        return a.atanEasing = function(e) {
            return Math.atan(u * (e - .5)) / (2 * Math.atan(.5 * u)) + .5
        }, a
    }
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 6;
        KeyLines.WebGL.generateImage = function(t, n, r, a) {
            var i = t.wsc || [0, 0, 0, 0],
                o = 0;
            if ("ci" === t.s && (o = "width" === n.maxDimension ? (t.y2 - t.y1) / 2 : (t.x2 - t.x1) / 2), r.alwaysUpdate || r.rebuildOptions.all || r.rebuildOptions.shadow)
                for (var u = 0; e > u; u++) r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = i[0], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = i[1], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = i[2], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = i[3];
            if (r.rebuildOptions.colours || r.alwaysUpdate || r.rebuildOptions.all)
                for (u = 0; e > u; u++) r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = 255, r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = 255, r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = 255, r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = 255;
            if (r.rebuildOptions.positions || r.alwaysUpdate || r.rebuildOptions.all) {
                var s = t.x1,
                    l = t.y1,
                    f = t.x2,
                    c = t.y2;
                a && (s = t.x1 + a.x, l = t.y1 + a.y, f = t.x2 + a.x, c = t.y2 + a.y), r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = c, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = c, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = c, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0
            }
            if (r.rebuildOptions.textures || r.alwaysUpdate || r.rebuildOptions.all) {
                var d, g, h, v;
                o ? (g = n.cu1, d = n.cv1, v = n.cu2, h = n.cv2) : (g = n.u1, d = n.v1, v = n.u2, h = n.v2), r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = g, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = d, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = v, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = d, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = g, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = h, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = g, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = h, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = v, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = d, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = v, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = h, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.textureBank, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = o
            }
        }
    }
}(),
function() {
    var e = KeyLines.Graph = {};
    e.create = function(e) {
        function t(e, t, n, r, a) {
            e[n] || (e[n] = {}), e[r] || (e[r] = {}), e[n][r] || (e[n][r] = e[r][n] = [], t.push(a)), e[n][r].push(a)
        }

        function n(e, t, n) {
            var r = {
                id: t.id,
                id1: t.id1,
                id2: t.id2,
                ids: n
            };
            e.edges.push(r), e.connections.push(n)
        }

        function r(e) {
            var r = e && e.all ? "all" : "visible";
            if (null === x && (x = {}), !x[r]) {
                x[r] = {
                    connections: [],
                    adjacency: {},
                    vertices: {},
                    edges: []
                }, b({
                    all: e && e.all
                }, function(e, t) {
                    x[r].vertices[t] = {
                        x: e.x,
                        y: e.y
                    }, x[r].adjacency[t] = {}
                });
                var a = [];
                p({
                    all: e && e.all
                }, function(e, n, i, o) {
                    i !== o && t(x[r].adjacency, a, i, o, n)
                });
                for (var i = a.length, o = 0; i > o; o++) {
                    var u = v[a[o]];
                    n(x[r], u, x[r].adjacency[u.id1][u.id2])
                }
            }
            return x[r]
        }

        function a(e) {
            if (e.direction = e.direction || "any", !/^(any|from|to)$/.test(e.direction)) throw new TypeError("opts direction property should be either 'from', 'to' or 'any'")
        }

        function i(e) {
            if (e.normalization = e.normalization || "any", !/^(chart|component|unnormalized)$/.test(e.normalization)) throw new TypeError("opts normalization property should be either 'component', 'chart' or 'unnormalized'")
        }

        function o(e) {
            return p(e, function(t, n, r, a) {
                if (D(r, a, n, e) <= 0) throw new Error("Link Weight must be Positive")
            }), {
                direction: e.direction,
                value: e.value,
                weights: f(e)
            }
        }

        function u(e) {
            return /^(component|chart)$/.test(e.normalization) ? e.normalization : "unnormalized"
        }

        function s(e) {
            return a(e) || o(e)
        }

        function l(e) {
            e.all = !!e.all
        }

        function f(e) {
            return !!e.weights
        }

        function c(e) {
            var t = 0;
            return b(e, function() {
                t++
            }), t
        }
        var d = KeyLines.Util,
            g = {},
            h = {},
            v = {},
            m = e,
            x = null,
            y = g.clearCache = function() {
                x = null
            };
        g.privateSetProperties = function(e, t) {
            h = e, v = t, y()
        };
        var p = function(e, t) {
                var n = e && e.all;
                for (var r in v) {
                    r = d.rawId(r);
                    var a = v[r];
                    !n && a.hi || t(a, r, a.id1, a.id2)
                }
            },
            b = function(e, t) {
                var n = e && e.all;
                for (var r in h) {
                    r = d.rawId(r);
                    var a = h[r];
                    !n && a.hi || t(a, r)
                }
            },
            I = function(e, t, n, r) {
                function a() {
                    var t = [];
                    return b(e, function(e) {
                        t.push(e)
                    }), t
                }
                var i = a(),
                    o = i.length,
                    u = -1,
                    s = (new Date).getTime(),
                    l = s,
                    f = 700,
                    c = function() {
                        return u++, u >= o ? (g(1), void r()) : void n(i[u], v)
                    },
                    g = function(e) {
                        m && m.trigger("progress", t, e)
                    },
                    h = function() {
                        return s = (new Date).getTime(), s - l > f ? (l = (new Date).getTime(), !0) : !1
                    },
                    v = function() {
                        h() ? (g(u / o), d.nextTick(c, 0)) : c()
                    };
                g(0), v()
            };
        g.findAdjacency = function(e, t, n) {
            var a = r(e).adjacency;
            return a[t] ? a[t][n] : void 0
        }, g.connections = function(e) {
            return r(e).connections
        }, g.eigenCentrality = function(e) {
            function t(t) {
                function n(e, t) {
                    var n = 1;
                    return t.value && e.d && (n = Number(e.d[t.value])), n
                }

                function r(e) {
                    var t, n, r = [];
                    for (t = 0; t < l.length; t++) {
                        var a = 0;
                        for (n = 0; n < f[t].length; n++) a += c[t][f[t][n]] * e[f[t][n]];
                        a += d * e[t], r.push(a)
                    }
                    return r
                }

                function a() {
                    var e, t = 1 / i(g);
                    for (e = 0; e < l.length; e++) g[e] = g[e] * t
                }

                function i(e) {
                    var t, n = 0;
                    for (t = 0; t < e.length; t++) n += Math.abs(e[t]);
                    return n
                }
                var o, u, s = {},
                    l = [],
                    f = [],
                    c = [],
                    d = 0,
                    g = [],
                    h = 0;
                if (b(e, function(e, n) {
                        t[n] && (l.push(n), s[n] = l.length - 1, f.push([]), c.push({}))
                    }), u = Math.max(100, 5 * Math.sqrt(l.length)), p(e, function(r, a, i, o) {
                        if (t[i] && t[o]) {
                            var u = s[i],
                                l = s[o];
                            c[u][l] || (f[u].push(l), f[l].push(u), c[l][u] = 0, c[u][l] = 0);
                            var g = n(r, e);
                            c[u][l] += g, c[l][u] += g, d += g, h++
                        }
                    }), 0 === l.length) return {};
                if (1 === l.length) return g = {}, g[l[0]] = 1, g;
                if (2 === l.length) return g = {}, g[l[0]] = .5, g[l[1]] = .5, g;
                for (d /= h, u = Math.max(100, 5 * Math.sqrt(l.length)), o = 0; o < l.length; o++) g.push(Math.random());
                for (o = 0; u > o; o++) g = r(g),
                    o % 10 === 0 && a();
                a();
                var v = {};
                for (o = 0; o < l.length; o++) v[l[o]] = g[o];
                return v
            }
            e = d.defaults(e, {
                all: !1,
                value: void 0
            }), e.direction = "any", l(e), o(e);
            var n, r, a = {},
                i = w(e);
            for (n = 0; n < i.length; n++) {
                var u = {};
                for (r = 0; r < i[n].nodes.length; r++) u[i[n].nodes[r]] = !0;
                var s = t(u);
                for (r in s) a[r] = s[r] * i[n].nodes.length
            }
            return a
        }, g.pageRank = function(e) {
            function t(e, t) {
                var n = 1;
                return t.value && e.d && (n = Number(e.d[t.value])), n
            }

            function n(e) {
                var t, n, r = [],
                    a = 0,
                    i = 0;
                for (t = 0; t < u.length; t++) a += e[t];
                for (t = 0; t < u.length; t++) {
                    var o = 0;
                    for (n = 0; n < f[t].length; n++) {
                        var s = f[t][n];
                        o += c[t][s] * e[s] / h[s]
                    }
                    0 === h[t] && (i += m * e[t]), o = m * o, o += (1 - m) * a / u.length, r.push(o)
                }
                for (i /= u.length, t = 0; t < v.length; t++) r[t] += i;
                return r
            }

            function r(e) {
                var t, n = 0;
                for (t = 0; t < e.length; t++) n += e[t];
                return n
            }
            var a;
            e = d.defaults(e, {
                all: !1,
                value: void 0,
                directed: !0
            }), l(e), s(e);
            var i, o = {},
                u = [],
                f = [],
                c = [],
                g = 0,
                h = [],
                v = [],
                m = .85;
            for (b(e, function(e) {
                    u.push(e.id), o[e.id] = u.length - 1, f.push([]), c.push({}), h.push(0)
                }), i = Math.max(100, 5 * Math.sqrt(u.length)), p(e, function(n, r, a, i) {
                    var u = o[a],
                        s = o[i],
                        l = t(n, e);
                    e.directed && !n.a2 && (n.a1 || n.a2) || (c[s][u] || (c[s][u] = 0, f[s].push(u)), c[s][u] += l, h[u] += l), e.directed && !n.a1 && (n.a1 || n.a2) || (c[u][s] || (c[u][s] = 0, f[u].push(s)), c[u][s] += l, h[s] += l), g++
                }), a = 0; a < u.length; a++) v.push(Math.random());
            var x = 1 / r(v);
            for (a = 0; a < u.length; a++) v[a] = v[a] * x;
            for (a = 0; i > a; a++) v = n(v);
            for (x = 1 / r(v), a = 0; a < u.length; a++) v[a] = v[a] * x;
            var y = {};
            for (a = 0; a < u.length; a++) y[u[a]] = v[a];
            return y
        }, g.neighbours = function(e, t) {
            function n(e, n, r) {
                var a = d.rawId(m[r][e][n]);
                !c[a] && M(r, e, a, t) && (i[a] = o[e] = c[a] = 1, e in g && (o[r] = 1), e in u || (s++, f[e] = 1))
            }

            function a() {
                for (var e in u)
                    if (e = d.rawId(e), h[e] && m[e])
                        for (var r in m[e]) {
                            r = d.rawId(r);
                            for (var a = 0; a < m[e][r].length; a++) n(r, a, e)
                        } else if (v[e]) {
                            var i = v[e];
                            !c[i.id2] && M(i.id2, i.id1, e, t) && (o[i.id2] = c[i.id2] = c[e] = 1, i.id2 in u || (s++, f[i.id2] = 1)), !c[i.id1] && M(i.id1, i.id2, e, t) && (o[i.id1] = c[i.id1] = c[e] = 1, i.id1 in u || (s++, f[i.id1] = 1))
                        }
            }
            e = d.ensureArray(e);
            var i = {},
                o = {},
                u = d.makeIdMap(e);
            if (t = d.defaults(t, {
                    all: !1,
                    direction: "any",
                    hops: 1
                }), t.hops < 1) throw new Error("neighbours error: hops must be a positive number");
            l(t);
            var s = 0,
                f = {},
                c = {},
                g = u,
                m = r(t).adjacency;
            for (var x in u) {
                if (x = d.rawId(x), !h[x] && !v[x]) throw new Error("neighbours error: ids must be present in the graph");
                s++
            }
            for (; t.hops-- && s;) s = 0, a(), u = f, f = {};
            return {
                nodes: d.flattenMap(o),
                links: d.flattenMap(i)
            }
        }, g.clusters = function(e) {
            function t(e, t) {
                var n = 1;
                return t.value && e.d && (n = Number(e.d[t.value])), n
            }

            function n(e) {
                c = Number(e), c = isNaN(c) ? 5 : c, c = Math.max(0, c), c = Math.min(10, c), c = c * c * c / 125
            }

            function r() {
                var e, t, n = [];
                for (e = 0; e < A.length; e++) {
                    var r = [];
                    for (t = 0; t < I[A[e]].length; t++) r.push(m[I[A[e]][t]]);
                    n.push(r)
                }
                return n
            }

            function a(e, t, n) {
                var r, a = B[t],
                    i = B[e],
                    o = g[n],
                    u = (-o + a - i) * o * c;
                for (u /= 2 * y, r = 0; r < h[n].length; r++) {
                    var s = h[n][r];
                    w[s] === t ? u -= v[n][s] : w[s] === e && (u += v[n][s])
                }
                return u / y
            }

            function i(e, t) {
                var n, r;
                if (t !== e && 0 !== I[e].length && 0 !== I[t].length) {
                    var a = B[e],
                        i = B[t],
                        u = 0;
                    for (u -= c * a * i / y, n = 0; n < I[e].length; n++) {
                        var s = I[e][n];
                        for (r = 0; r < h[s].length; r++) w[h[s][r]] === t && (u += v[s][h[s][r]])
                    }
                    if (u /= y, u > 0) {
                        var l = I[e].length;
                        for (n = 0; l > n; n++) o(I[e][0], t)
                    }
                }
            }

            function o(e, t) {
                var n, r = w[e];
                w[e] = t;
                var a = [];
                for (n = 0; n < I[r].length; n++) I[r][n] !== e && a.push(I[r][n]);
                0 === I[t].length && (A.push(t), C.splice(d.indexOf(C, t), 1)), I[r] = a, I[t].push(e), B[t] += g[e], B[r] -= g[e], 0 === I[r].length && (C.push(r), A.splice(d.indexOf(A, r), 1))
            }

            function u(e) {
                var t, n = I[e].length;
                if (1 !== n && C.length) {
                    var r = [];
                    for (t = 0; n > t; t++) r.push(I[e][t]);
                    for (t = 0; n > t && C.length; t++) o(r[t], C[0])
                }
            }
            var s;
            e = d.defaults(e, {
                all: !1,
                factor: 5
            }), l(e);
            var f, c, g = [],
                h = [],
                v = [],
                m = [],
                x = {},
                y = 0,
                I = [],
                w = [],
                B = [],
                C = [],
                A = [];
            if (b(e, function(e) {
                    m.push(e.id), h.push([]), v.push([]), x[e.id] = m.length - 1, I.push([m.length - 1]), w.push(m.length - 1), A.push(m.length - 1), g.push(0), B.push(0)
                }), p(e, function(n, r, a, i) {
                    var o = t(n, e);
                    y += o;
                    var u = x[a],
                        s = x[i];
                    v[u][s] || (v[u][s] = 0, v[s][u] = 0), v[u][s] += o, v[s][u] += o, g[u] += o, g[s] += o, B[u] += o, B[s] += o, h[u].push(s), h[s].push(u)
                }), f = Math.max(1e4, 50 * m.length), 0 === m.length) return [];
            for (0 === y && (f = 0), n(e.factor), s = 0; f > s; s++) {
                if (Math.random() > .95) {
                    var M = A[Math.floor(Math.random() * A.length)],
                        D = A[Math.floor(Math.random() * A.length)];
                    i(M, D)
                }
                if (.5 * f > s && Math.random() > .98) {
                    var L = A[Math.floor(Math.random() * A.length)];
                    u(L)
                }
                var Z = Math.floor(m.length * Math.random()),
                    R = h[Z][Math.floor(Math.random() * h[Z].length)],
                    G = w[R],
                    S = w[Z];
                if (G !== S) {
                    var T = a(G, S, Z);
                    T >= 0 && o(Z, G)
                }
            }
            return r()
        };
        var w = g.components = function(e) {
            var t, n, a = [],
                i = 0,
                o = [];
            e = d.defaults(e, {
                all: !1
            }), l(e);
            var u = r(e).vertices,
                s = r(e).edges;
            for (t in u)
                if (u[t].hasOwnProperty("c")) o[u[t].c] || (o[u[t].c] = {
                    nodes: [],
                    links: []
                });
                else {
                    for (a.push(t + ""); a.length > 0;) {
                        var f = a.shift();
                        if (!u[f].hasOwnProperty("c"))
                            for (u[f].c = i, n = 0; n < s.length; n++) s[n].hasOwnProperty("c") || (s[n].id1 === f ? (s[n].c = i, a.push(s[n].id2)) : s[n].id2 === f && (s[n].c = i, a.push(s[n].id1)))
                    }
                    o.push({
                        nodes: [],
                        links: []
                    }), i++
                }
            for (t in u) o[u[t].c].nodes.push(t + "");
            for (n = 0; n < s.length; n++) o[s[n].c].links = o[s[n].c].links.concat(s[n].ids);
            return o
        };
        g.internalMarkLinks = function(e) {
            var t = r().adjacency,
                n = 2,
                a = {};
            for (var i in e) {
                var o = t[i];
                if (o)
                    for (var u in o)
                        for (var s = o[u], l = 0; l < s.length; l++) {
                            var f = s[l];
                            a[f] = !0
                        }
            }
            for (var c in a) e[c] = n
        };
        var B = g.distances = function(e, t) {
                if (!h[e]) throw new Error("distances error: id(s) must be present in the graph");
                t = t || {}, s(t), l(t);
                var n = R(e, t),
                    r = {};
                for (var a in n.distance) {
                    var i = n.distance[a];
                    isFinite(i) && (r[a] = i)
                }
                return r
            },
            C = function(e, t, n) {
                var a, i = r(n).adjacency,
                    o = i[e][t],
                    u = 1 / 0,
                    s = L(n);
                if (o)
                    for (var l = 0; l < o.length; l++) {
                        var f = s(e, t, o[l], n);
                        isNaN(f) || (u > f && (a = [], u = f), f === u && u !== 1 / 0 && a.push(o[l]))
                    }
                return {
                    value: u,
                    links: a
                }
            },
            A = function(e, t) {
                return v[e].a2 && v[e].id2 === t || v[e].a1 && v[e].id1 === t
            },
            M = function(e, t, n, r) {
                return "any" !== r.direction ? "from" === r.direction && A(n, t) || "to" === r.direction && A(n, e) : !0
            },
            D = function(e, t, n, r) {
                var a = NaN;
                return M(e, t, n, r) && (r.value ? v[n].d && (a = Number(v[n].d[r.value])) : a = 1), a
            },
            L = function(e) {
                return f(e) ? Z : D
            },
            Z = function(e, t, n, r) {
                var a = D(e, t, n, r);
                return isNaN(a) ? a : 1 / a
            },
            R = function(e, t) {
                function n() {
                    for (var e, t = 1 / 0, n = 0; n < u.length; n++) {
                        var r = i[u[n]];
                        t >= r && (e = n, t = r)
                    }
                    return e
                }

                function a(e, t, n) {
                    o[e] || (o[e] = []), o[e].push({
                        id: t,
                        links: n
                    })
                }
                var i = {},
                    o = {},
                    u = [],
                    s = r(t).adjacency;
                for (b(t, function(e, t) {
                        i[t] = 1 / 0, o[t] = void 0, u.push(t)
                    }), i[e] = 0; u.length > 0;) {
                    var l = n(),
                        f = u[l];
                    if (i[f] === 1 / 0) break;
                    u.splice(l, 1);
                    for (var c in s[f]) {
                        c = d.rawId(c);
                        var g = C(f, c, t),
                            h = i[f] + g.value;
                        h < i[c] ? (i[c] = h, o[c] = [{
                            id: f,
                            links: g.links
                        }]) : h === i[c] && a(c, f, g.links)
                    }
                }
                return {
                    distance: i,
                    previous: o
                }
            };
        g.shortestPaths = function(e, t, n) {
            function r(e) {
                for (var t = [], n = 0; n < e.length; n++) i[e[n].id] && (t = t.concat(i[e[n].id]));
                return t
            }
            if (!h[e] || !h[t]) throw new Error("shortestPaths error: ids must be present in the graph");
            n = d.defaults(n, {
                all: !1
            }), s(n), l(n);
            var a = R(e, n),
                i = a.previous;
            if (a.distance[t] === 1 / 0) return {
                onePath: [],
                one: [],
                items: [],
                distance: 1 / 0
            };
            for (var o = [], u = [], f = t; i[f];) o.unshift(f), u.unshift(f), u.unshift(i[f][0].links[0]), f = i[f][0].id;
            o.unshift(f), u.unshift(f);
            var c = {};
            c[t] = 1;
            var g = [];
            g.push({
                id: t
            });
            for (var v = r(g); v.length > 0;) {
                for (var m = 0; m < v.length; m++) {
                    c[v[m].id] = 1;
                    for (var x = 0; x < v[m].links.length; x++) c[v[m].links[x]] = 1
                }
                v = r(v)
            }
            return {
                onePath: u,
                one: o,
                items: d.flattenMap(c),
                distance: a.distance[t]
            }
        };
        var G = g.degrees = function(e) {
            e = d.defaults(e, {
                multi: !0,
                all: !1
            }), s(e), l(e);
            var t = r(e).adjacency,
                n = {};
            return b(e, function(r, a) {
                n[a] = 0;
                for (var i in t[a]) {
                    i = d.rawId(i);
                    for (var o = 0; o < t[a][i].length; o++) {
                        var u = D(a, i, t[a][i][o], e);
                        if (!isNaN(u) && (n[a] += u, !e.multi)) break
                    }
                }
            }), n
        };
        return g.betweenness = function(e, t) {
            function n(e, t) {
                return U[e] - U[t]
            }

            function a(e) {
                return e.sort(n), e.shift()
            }

            function o(e, t) {
                e.sort(n), d.contains(e, t) || e.push(t)
            }

            function c(e, t) {
                var n = C(e, t, L).value;
                return isFinite(n) ? n : p() || isFinite(n) ? 1 / 0 : 1
            }

            function g(e) {
                return e.length > 0
            }

            function h() {
                F.length = 0, W.length = 0, b(e, function(e, t) {
                    for (var n = 0; n < X.length; n++) m(X[n].dict, t, X[n].value)
                })
            }

            function v(e, t, n) {
                b(e, function(e, r) {
                    m(t, r, n)
                })
            }

            function m(e, t, n) {
                d.isArray(n) && g(n) && e[t] ? e[t].length = 0 : e[t] = d.clone(n)
            }

            function x() {
                var t, n, r, a = w(e),
                    i = {};
                for (t = 0; t < a.length; t++)
                    for (r = a[t], n = 0; n < r.nodes.length; n++) i[r.nodes[n]] = r.nodes.length;
                return i
            }

            function y() {
                return !d.isNullOrUndefined(e.value)
            }

            function p() {
                return !!e.directed
            }

            function B() {
                return e.endpoints
            }
            e = d.defaults(e, {
                value: void 0,
                directed: !1,
                normalization: "component",
                endpoints: !1,
                weights: !1,
                all: !1
            }), i(e), l(e);
            var A = y() || p() ? "dijkstra" : "regular",
                M = B() ? "endpoints" : "regular",
                D = u(e),
                L = {
                    direction: p() ? "from" : "any",
                    value: e.value,
                    weights: f(e),
                    all: e.all
                };
            s(e);
            var Z, R, G, S, T, E, k = {},
                F = [],
                W = [],
                N = {},
                O = {},
                V = {},
                U = {},
                K = 0,
                X = [{
                    dict: O,
                    value: 0
                }, {
                    dict: U,
                    value: 1 / 0
                }, {
                    dict: N,
                    value: []
                }, {
                    dict: V,
                    value: 0
                }],
                H = r(e).adjacency,
                Y = {
                    regular: function(e) {
                        for (O[e] = 1, U[e] = 0, F.push(e); g(F);) {
                            Z = F.shift(), W.push(Z);
                            for (var t in H[Z]) t = d.rawId(t), isFinite(U[t]) || (U[t] = U[Z] + 1, F.push(t)), U[t] === U[Z] + 1 && (O[t] += O[Z], N[t].push(Z))
                        }
                    },
                    dijkstra: function(e) {
                        for (O[e] = 1, U[e] = 0, F.push(e); g(F);) {
                            Z = a(F), W.push(Z);
                            for (var t in H[Z]) t = d.rawId(t), U[t] > U[Z] + c(Z, t) && (U[t] = U[Z] + c(Z, t), o(F, t), O[t] = 0, N[t] = []), U[t] === U[Z] + c(Z, t) && (O[t] += O[Z], N[t].push(Z))
                        }
                    }
                },
                P = {
                    regular: function(e) {
                        for (; g(W);) {
                            for (R = W.pop(), T = (1 + V[R]) / O[R], S = 0; S < N[R].length; S++) Z = N[R][S], V[Z] += O[Z] * T;
                            R !== e && (k[R] += V[R])
                        }
                    },
                    endpoints: function(e) {
                        for (k[e] += W.length - 1; g(W);) {
                            for (R = W.pop(), T = (1 + V[R]) / O[R], S = 0; S < N[R].length; S++) Z = N[R][S], V[Z] += O[Z] * T;
                            R !== e && (k[R] += V[R] + 1)
                        }
                    }
                },
                z = {
                    chart: function() {
                        if (K > 2) {
                            G = 1 / ((K - 1) * (K - 2));
                            for (var e in k) e = d.rawId(e), k[e] *= G
                        }
                    },
                    component: function() {
                        E = x();
                        for (var e in k) e = d.rawId(e), E[e] > 2 && (G = 1 / ((E[e] - 1) * (E[e] - 2)), k[e] *= G)
                    },
                    unnormalized: function() {
                        if (!p()) {
                            G = .5;
                            for (var e in k) e = d.rawId(e), k[e] *= G
                        }
                    }
                },
                _ = function() {
                    Q(), d.invoke(t, k)
                },
                j = Y[A] || Y.regular,
                J = P[M] || P.regular,
                Q = z[D] || z.chart;
            b(e, function() {
                K++
            }), v(e, k, 0);
            var q = function(e, t) {
                h(), j(e.id), J(e.id), t()
            };
            I(e, "betweenness", q, _)
        }, g.closeness = function(e, t) {
            function n(e, t) {
                var n = 0;
                for (var r in t) r = d.rawId(r), n += isFinite(t[r]) ? 1 : 0;
                return n
            }

            function a(e) {
                for (var t in e)
                    if (t = d.rawId(t), e.hasOwnProperty(t)) return !1;
                return !0
            }

            function o(e) {
                return !d.isNullOrUndefined(e)
            }

            function f(e) {
                return !v[e].a1 && !v[e].a2
            }

            function g(e) {
                var t = {},
                    n = 0,
                    r = {},
                    i = {};
                for (i[e] = 1; !a(i);) {
                    r = i, i = {};
                    for (var u in r)
                        if (u = d.rawId(u), !o(t[u])) {
                            t[u] = n;
                            for (var s in A[u]) s = d.rawId(s), i[s] = 1
                        }
                    n += 1
                }
                return t
            }

            function h() {
                return "any" !== e.direction
            }

            function m() {
                if (e.mode = e.mode || "auto", !e.mode.match(/^(connected|disconnected|auto)$/)) throw new TypeError("Option mode property should be either 'connected', 'disconnected' or 'auto'")
            }

            function x() {
                return m(), "auto" === e.mode ? C() : e.mode
            }

            function y() {
                return !d.isNullOrUndefined(e.value)
            }

            function p() {
                return u(e).match(/chart/)
            }

            function b() {
                return u(e).match(/component/)
            }

            function C() {
                var t = "disconnected",
                    n = w(e);
                if (1 === n.length && (t = "connected", h()))
                    for (var r = 0; r < n[0].links.length; r++)
                        if (f(n[0].links[r])) return "disconnected";
                return t
            }
            e = d.defaults(e, {
                direction: "any",
                value: void 0,
                normalization: "component",
                mode: "auto",
                weights: !1,
                all: !1
            });
            var A = r(e).adjacency,
                M = s(e);
            i(e), l(e), M.all = e.all;
            var D, L, Z, R, G, S = y() || h() ? "dijkstra" : "regular",
                T = 0,
                E = {},
                k = x(),
                F = {
                    regular: function(e) {
                        return g(e)
                    },
                    dijkstra: function(e, t) {
                        return B(e, t)
                    }
                },
                W = {
                    connected: function(e, t) {
                        var n = 0;
                        for (var r in t) r = d.rawId(r), n += isFinite(t[r]) ? t[r] : 0;
                        return n ? 1 / n : 0
                    },
                    disconnected: function(e, t) {
                        var n = 0;
                        for (var r in t) r = d.rawId(r), n += e !== r ? 1 / t[r] : 0;
                        return n
                    }
                },
                N = function() {
                    d.invoke(t, E)
                };
            L = F[S] || F.regular, Z = W[k], T = c(e), E = {};
            var O = function(e, t) {
                R = L(e.id, M), D = n(e.id, R), E[e.id] = Z(e.id, R), T > 1 && (G = p() ? Math.pow(D - 1, 2) / (T - 1) : b() ? D - 1 : 1, E[e.id] *= G), t()
            };
            I(e, "closeness", O, N)
        }, g.kCores = function(e) {
            function t(e) {
                var t = 0;
                for (var n in e) n = d.rawId(n), t = Math.max(t, e[n]);
                return t
            }
            e = d.defaults(e, {
                all: !1
            }), l(e);
            var n = r(e).adjacency,
                a = function() {
                    var t, r, a, i, o, u, s, l, f, c, g, h = {},
                        v = {},
                        m = [],
                        x = [];
                    r = 0, h = G({
                        multi: !1,
                        all: e.all
                    });
                    for (var y in h) y = d.rawId(y), r = Math.max(r, h[y]);
                    for (g = d.flattenMap(h).length, t = 0; r + 1 > t; t++) x[t] = 0;
                    for (b(e, function(e, t) {
                            x[h[t]]++
                        }), o = 1, t = 0; r + 1 > t; t++) u = x[t], x[t] = o, o += u;
                    for (b(e, function(e, t) {
                            v[t] = x[h[t]], m[v[t]] = t, x[h[t]]++
                        }), t = r; t > 1; t--) x[t] = x[t - 1];
                    for (x[0] = 1, i = 0; g > i; i++) {
                        a = m[i];
                        for (var p in n[a]) p = d.rawId(p), h[p] > h[a] && (l = h[p], f = v[p], c = x[l], s = m[c], p !== s && (v[p] = c, m[f] = s, v[s] = f, m[c] = p), x[l]++, h[p]--)
                    }
                    return h
                },
                i = a();
            return {
                maximumK: t(i),
                values: i
            }
        }, g
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.create = function(t) {
        var n = {},
            r = {
                canvas: t,
                layersToDraw: [],
                textAtlas: null,
                fontIconAtlas: null,
                imageAtlases: [],
                allLayers: {},
                framebuffers: {}
            },
            a = e.Generator(r),
            i = e.Renderer(r);
        return n.clearView = function(e) {
            i.clearView(e)
        }, n.clearFramebuffers = function() {
            i.clearFramebuffers()
        }, n.updateTextures = function(t) {
            r.imageAtlases = e.ImageAtlas(t)
        }, n.build = function(e, t, n, r) {
            a.generate(e, t, n, r)
        }, n.draw = function(e, t) {
            var n = 0,
                a = 0,
                o = 1;
            e && (n = e.offsetXtoWebglPanX(), a = e.offsetYtoWebglPanY(), o = e.settings().zoom), i.render(n, a, o, r.textAtlas, r.fontIconAtlas, r.imageAtlases, t)
        }, n.drawShadowCanvas = function(e, t) {
            return i.drawShadowCanvas(e, t)
        }, n
    })
}(),
function() {
    function e(e, t) {
        var n, r = Object.getPrototypeOf(e).constructor;
        if (t) {
            n = new r;
            var a = i(Array.from(e));
            a.forEach(function(e) {
                n.add ? n.add(e) : n.set(e[0], e[1])
            })
        } else n = new r(e);
        return n
    }

    function t(e) {
        return null == r && KeyLines.es6CollectionSupport && (r = KeyLines.es6CollectionSupport()), r ? Map.prototype.isPrototypeOf(e) || Set.prototype.isPrototypeOf(e) : !1
    }

    function n(e) {
        return e.lastIndexOf(d())
    }
    var r, a = KeyLines.Util = {};
    a.shallowClone = function(n) {
        if (null === n) return null;
        switch (typeof n) {
            case "object":
                if (s(n)) return n.slice();
                if (t(n)) return e(n);
                if (n instanceof Date) return new Date(n.getTime());
                var r = {};
                for (var a in n) r[a] = n[a];
                return r;
            case "boolean":
            case "string":
            case "number":
                return n
        }
    };
    var i = a.clone = function(n, r) {
        function o(e, t) {
            r && "_" === t.charAt(0) || (u[t] = i(e, r))
        }
        if (null === n) return null;
        var u;
        switch (typeof n) {
            case "object":
                if (s(n)) {
                    for (var l = [], f = 0; f < n.length; f++) l.push(i(n[f], r));
                    return l
                }
                return t(n) ? e(n, !0) : n instanceof Date ? new Date(n.getTime()) : (u = {}, a.iterator(n, o), u);
            case "boolean":
            case "string":
            case "number":
                return n
        }
    };
    a.nextTick = function(e, t) {
        return o(e) ? setTimeout(e, t) : void 0
    }, a.setRegularTick = function(e, t) {
        return o(e) ? setInterval(e, t) : void 0
    }, a.isNumber = function(e) {
        return isFinite(e) && "number" == typeof e && !isNaN(e)
    };
    var o = a.isFunction = function(e) {
            if (e) {
                var t = {}.toString.call(e);
                return "[object Funct" === t.slice(0, 13)
            }
            return !1
        },
        u = a.isNormalObject = function(e) {
            return !("object" != typeof e || e instanceof Date || s(e) || c(e))
        },
        s = a.isArray = function(e) {
            return "[object Array]" === Object.prototype.toString.call(e)
        };
    a.invoke = function(e, t) {
        e && o(e) && e(t)
    }, a.tryCatch = function(e, t) {
        try {
            t(null, e())
        } catch (n) {
            t(n)
        }
    }, a.getItemsArray = function(e) {
        return e && !a.isArray(e) && e.items ? e.items : e
    };
    var l = a.ensureArray = function(e) {
            return s(e) ? e : [e]
        },
        f = !0;
    a.timerFn = function(e, t, n) {
        return function() {
            if (f) {
                var r = (new Date).getTime(),
                    a = e.apply(n || this, arguments),
                    i = (new Date).getTime();
                return console.log("fn " + t + " = " + (i - r) + "ms"), a
            }
            return e.apply(n || this, arguments)
        }
    }, a.defaults = function(e, t) {
        var n = i(e) || {};
        for (var r in t) n.hasOwnProperty(r) && !c(n[r]) || (n[r] = t[r]);
        return n
    }, a.defined = function(e) {
        return "undefined" != typeof e
    };
    var c = a.isNullOrUndefined = function(e) {
        return null == e
    };
    a.flattenMap = function(e) {
        var t = [];
        for (var n in e) t.push(n + "");
        return t
    };
    var d = a.idSep = function() {
            return "\u2704"
        },
        g = a.rawId = function(e) {
            if (c(e)) return null;
            var t = e + "",
                r = n(t);
            return -1 !== r && (t = t.substring(0, r)), t
        };
    a.subId = function(e) {
        if (c(e)) return null;
        var t = e + "",
            r = n(t);
        return -1 !== r ? t.substring(r + 1) : null
    };
    var h = a.dictToArray = function(e) {
            var t = [];
            for (var n in e) t.push({
                id: n,
                val: u(e[n]) ? h(e[n]) : e[n]
            });
            return t
        },
        v = a.arrayToDict = function(e) {
            for (var t = {}, n = 0; n < e.length; n++) t[e[n].id + ""] = s(e[n].val) ? v(e[n].val) : e[n].val;
            return t
        };
    a.dictValues = function(e) {
        var t = [];
        for (var n in e) t.push(e[n]);
        return t
    }, a.dictKeys = function(e) {
        var t = [];
        for (var n in e) t.push(n);
        return t
    }, a.hasAnyKeys = function(e) {
        for (var t in e) return !0;
        return !1
    }, a.makeIdMap = function(e) {
        var t = {};
        e = l(e);
        for (var n = 0; n < e.length; n++) t[g(e[n])] = 1;
        return t
    }, a.contains = function(e, t) {
        return -1 !== m(e, t)
    };
    var m = a.indexOf = function(e, t) {
        for (var n = 0; n < e.length; n++)
            if (e[n] === t) return n;
        return -1
    };
    a.merge = function(e, t) {
        if (e) {
            for (var n in t) e[n] = t[n];
            return e
        }
        return t
    }, a.len = function(e, t) {
        return Math.max(1e-4, Math.sqrt(e * e + t * t))
    }, a.numberSort = function(e, t) {
        var n, r, a, i = [];
        if (t)
            for (i = e, e = [], n = 0; n < i.length; n++) e.push(i[n][t]);
        for (var o, u, s, l, f = 0, c = e.length - 1, d = -1, g = [], h = 7;;)
            if (h >= c - f) {
                for (r = f + 1; c >= r; r++) {
                    for (o = e[r], s = i[r], n = r - 1; n >= f && e[n] > o;) e[n + 1] = e[n], i[n + 1] = i[n], n--;
                    e[n + 1] = o, i[n + 1] = s
                }
                if (-1 === d) break;
                c = g[d--], f = g[d--]
            } else {
                for (a = f + c >> 1, n = f + 1, r = c, o = e[a], s = i[a], e[a] = e[n], i[a] = i[n], e[n] = o, i[n] = s, e[f] > e[c] && (o = e[f], s = i[f], e[f] = e[c], i[f] = i[c], e[c] = o, i[c] = s), e[n] > e[c] && (o = e[n], s = i[n], e[n] = e[c], i[n] = i[c], e[c] = o, i[c] = s), e[f] > e[n] && (o = e[f], s = i[f], e[f] = e[n], i[f] = i[n], e[n] = o, i[n] = s), u = e[n], l = i[n];;) {
                    do n++; while (e[n] < u);
                    do r--; while (e[r] > u);
                    if (n > r) break;
                    o = e[n], s = i[n], e[n] = e[r], i[n] = i[r], e[r] = o, i[r] = s
                }
                e[f + 1] = e[r], i[f + 1] = i[r], e[r] = u, i[r] = l, c - n + 1 >= r - f ? (g[++d] = n, g[++d] = c, c = r - 1) : (g[++d] = f, g[++d] = r - 1, f = n)
            }
    }, a.asyncWhile = function(e, t, n) {
        function r() {
            return i || (i = t(), i || (i = !e())), i ? n() : a.nextTick(r, 0)
        }
        var i = !e();
        r()
    }, a.iterator = function(e, t) {
        if (!c(e) && o(t)) {
            var n;
            if (s(e))
                for (n = 0; n < e.length; n++) t(e[n], n);
            else
                for (n in e) t(e[n], n)
        }
    }, a.filter = function(e, t) {
        for (var n = [], r = 0; r < e.length; r++) {
            var a = e[r];
            t(a) && n.push(a)
        }
        return n
    }, a.ratelimit = function(e, t) {
        var n, r, i, o;
        return function() {
            function u() {
                r = (new Date).getTime(), e.apply(i, o)
            }
            var s = (new Date).getTime();
            if (i = this, o = arguments, !n)
                if (!r || s - r >= t) u();
                else {
                    var l = s - r,
                        f = t - l;
                    n = a.nextTick(function() {
                        n = null, u()
                    }, f)
                }
        }
    }
}(),
function() {
    var e = KeyLines.TimeBar.Config = {};
    e.create = function() {
        function e(e, t) {
            return o.format(e, t, f.options.locale)
        }

        function t(t) {
            var n = "dmy" === f.options.locale.order ? "d MMM yyyy" : "MMM d, yyyy";
            return e(t, n)
        }

        function n(t) {
            var n = f.options.locale.h12 ? "h:mm tt" : "HH:mm";
            return e(t, n)
        }

        function r(t) {
            var n = f.options.locale.h12 ? "h:mm:ss tt" : "HH:mm:ss";
            return e(t, n)
        }

        function a(e, t) {
            if ("histogram" === e) {
                if (!u.isNormalObject(t)) return !1;
                if (t.colour && !KeyLines.Rendering.validatergb(t.colour)) return !1;
                if (t.highlightColour && !KeyLines.Rendering.validatergb(t.highlightColour)) return !1
            }
            return u.contains(l.colours, e) ? KeyLines.Rendering.validatergb(t) : !0
        }

        function i(e) {
            e.showPlay ? f.playingExtend = !1 : e.showExtend ? f.playingExtend = !0 : f.playingExtend = null
        }
        var o = KeyLines.DateTime,
            u = KeyLines.Util,
            s = o.units,
            l = {
                colours: ["backColour", "fontColour", "sliderColour", "sliderLineColour"]
            },
            f = {
                displayTime: function(t, n) {
                    return u.isFunction(n) ? n(t) : e(t, n)
                },
                options: {
                    scale: {
                        highlightColour: "#F2F2F2",
                        showMajor: !0,
                        showMinor: !0
                    },
                    backColour: "#FFFFFF",
                    fontColour: KeyLines.Rendering.colours.grey,
                    showControlBar: !0,
                    showPlay: !0,
                    showExtend: !1,
                    sliderColour: "rgba(255, 255, 255, 0.6)",
                    sliderLineColour: "#bbb",
                    sliders: "fixed",
                    minRange: 3e3,
                    showSliders: !0,
                    showShadowCanvas: !1,
                    histogram: {
                        colour: "#d0d0d0",
                        highlightColour: "#a9a9a9"
                    },
                    heightChange: {
                        animate: !0,
                        time: 200
                    },
                    locale: u.defaults(o.defaultLocale, {
                        order: "mdy",
                        h12: !0
                    }),
                    playSpeed: 60
                },
                getOptions: function() {
                    return u.clone(f.options)
                },
                setOptions: function(e) {
                    if (u.isNormalObject(e)) {
                        i(e);
                        for (var t in e) f.options.hasOwnProperty(t) && (u.isNormalObject(f.options[t]) && a(t, e[t]) ? f.options[t] = u.defaults(e[t], f.options[t]) : f.options[t] = e[t])
                    }
                },
                hoverTime: 50,
                defaultAnimationTime: 200,
                sliderReleaseTime: 200,
                doubleClickZoomTime: 500,
                doubleClickZoomTimeFree: 250,
                playingExtend: !1,
                defaultPlaySpeed: 60,
                bars: {
                    major: {
                        height: 19,
                        textbase: 3,
                        fontsize: 13
                    },
                    minor: {
                        height: 17,
                        textbase: 0,
                        fontsize: 12
                    }
                },
                grip: {
                    lines: 2,
                    space: 4,
                    length: 14,
                    margin: 2
                },
                controlbar: {
                    height: 30,
                    space: 4,
                    colour: "#ddd"
                },
                fixedSideFactor: .5,
                freeSideFactor: .2,
                histogram: {
                    dy: 2,
                    topdy: 16,
                    bardx: 1,
                    minwidth: 11
                },
                selection: {
                    maxNumber: 3,
                    reducingFactor: .75,
                    dotsize: 3.5
                },
                rates: [{
                    max: 6,
                    major: {
                        units: s.sec,
                        by: 2,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 1,
                        f: r
                    }
                }, {
                    max: 15,
                    major: {
                        units: s.sec,
                        by: 5,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 1,
                        f: r
                    }
                }, {
                    max: 24,
                    major: {
                        units: s.sec,
                        by: 10,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 2,
                        f: r
                    }
                }, {
                    max: 60,
                    major: {
                        units: s.sec,
                        by: 15,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 5,
                        f: r
                    }
                }, {
                    max: 120,
                    major: {
                        units: s.sec,
                        by: 30,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 10,
                        f: r
                    }
                }, {
                    max: 360,
                    major: {
                        units: s.min,
                        by: 1,
                        f: t
                    },
                    minor: {
                        units: s.sec,
                        by: 30,
                        f: r
                    }
                }, {
                    max: 1e3,
                    major: {
                        units: s.min,
                        by: 5,
                        f: t
                    },
                    minor: {
                        units: s.min,
                        by: 1,
                        f: n
                    }
                }, {
                    max: 5e3,
                    major: {
                        units: s.min,
                        by: 15,
                        f: t
                    },
                    minor: {
                        units: s.min,
                        by: 5,
                        f: n
                    }
                }, {
                    max: 15e3,
                    major: {
                        units: s.hour,
                        by: 1,
                        f: t
                    },
                    minor: {
                        units: s.min,
                        by: 15,
                        f: n
                    }
                }, {
                    max: 6e4,
                    major: {
                        units: s.hour,
                        by: 6,
                        f: t
                    },
                    minor: {
                        units: s.hour,
                        by: 1,
                        f: n
                    }
                }, {
                    max: 12e4,
                    major: {
                        units: s.day,
                        by: 1,
                        f: t,
                        adjust: 432e5
                    },
                    minor: {
                        units: s.hour,
                        by: 2,
                        f: n
                    }
                }, {
                    max: 18e4,
                    major: {
                        units: s.day,
                        by: 1,
                        f: t,
                        adjust: 432e5
                    },
                    minor: {
                        units: s.hour,
                        by: 3,
                        f: n
                    }
                }, {
                    max: 36e4,
                    major: {
                        units: s.day,
                        by: 1,
                        f: t
                    },
                    minor: {
                        units: s.hour,
                        by: 6,
                        f: n
                    }
                }, {
                    max: 72e4,
                    major: {
                        units: s.day,
                        by: 1,
                        f: t
                    },
                    minor: {
                        units: s.hour,
                        by: 12,
                        f: "ttt"
                    }
                }, {
                    max: 5e6,
                    major: {
                        units: s.week,
                        by: 1,
                        f: function(t) {
                            var n, r, a = new Date(t),
                                i = new Date(o.add(t, s.day, 6)),
                                u = "dmy" === f.options.locale.order;
                            return a.getUTCFullYear() === i.getUTCFullYear() ? a.getUTCMonth() === i.getUTCMonth() ? (n = u ? "d - " : "MMM d - ", r = u ? "d MMM yyyy" : "d, yyyy") : (n = u ? "d MMM - " : "MMM d - ", r = u ? "d MMM yyyy" : "MMM d, yyyy") : (n = u ? "d MMM yyyy - " : "MMM d, yyyy - ", r = u ? "d MMM yyyy" : "MMM d, yyyy"), e(a, n) + e(i, r)
                        }
                    },
                    minor: {
                        units: s.day,
                        by: 1,
                        f: "d"
                    }
                }, {
                    max: 15e6,
                    major: {
                        units: s.month,
                        by: 1,
                        f: "MMMM yyyy"
                    },
                    minor: {
                        units: s.week,
                        by: 1,
                        f: function(t) {
                            var n = o.add(t, s.day, 6);
                            return e(t, "d - ") + e(n, "d")
                        }
                    }
                }, {
                    max: 3e7,
                    major: {
                        units: s.month,
                        by: 3,
                        f: "Q  yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 1,
                        f: "MMMM"
                    }
                }, {
                    max: 75e6,
                    major: {
                        units: s.month,
                        by: 3,
                        f: "Q  yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 1,
                        f: "MMM"
                    }
                }, {
                    max: 1e8,
                    major: {
                        units: s.month,
                        by: 3,
                        f: "Q  yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 1,
                        f: "MMMMM"
                    }
                }, {
                    max: 2e8,
                    major: {
                        units: s.month,
                        by: 6,
                        f: "QQ  yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 1,
                        f: "MMMMM"
                    }
                }, {
                    max: 36e7,
                    major: {
                        units: s.year,
                        by: 1,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 3,
                        f: "Q"
                    }
                }, {
                    max: 6e8,
                    major: {
                        units: s.year,
                        by: 1,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.month,
                        by: 6,
                        f: "QQ"
                    }
                }, {
                    max: 15e8,
                    major: {
                        units: s.year,
                        by: 10,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 1,
                        f: "'yy"
                    }
                }, {
                    max: 25e8,
                    major: {
                        units: s.year,
                        by: 10,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 2,
                        f: "'yy"
                    }
                }, {
                    max: 75e8,
                    major: {
                        units: s.year,
                        by: 10,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 5,
                        f: "'yy"
                    }
                }, {
                    max: 15e9,
                    major: {
                        units: s.year,
                        by: 50,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 10,
                        f: "'yy"
                    }
                }, {
                    max: 25e9,
                    major: {
                        units: s.year,
                        by: 50,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 25,
                        f: "'yy"
                    }
                }, {
                    max: 35e9,
                    major: {
                        units: s.year,
                        by: 100,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 25,
                        f: "'yy"
                    }
                }, {
                    max: 8e10,
                    major: {
                        units: s.year,
                        by: 100,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 50,
                        f: "'yy"
                    }
                }, {
                    max: 16e10,
                    major: {
                        units: s.year,
                        by: 200,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 100,
                        f: ""
                    }
                }, {
                    max: 4e11,
                    major: {
                        units: s.year,
                        by: 500,
                        f: "yyyy"
                    },
                    minor: {
                        units: s.year,
                        by: 100,
                        f: ""
                    }
                }, {
                    max: 1 / 0,
                    major: {
                        units: s.year,
                        by: 500,
                        f: ""
                    },
                    minor: {
                        units: s.year,
                        by: 100,
                        f: ""
                    }
                }]
            };
        return f
    }
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 6;
        KeyLines.WebGL.generateFontIcon = function(t, n, r, a) {
            var i = 0,
                o = t.wsc || [0, 0, 0, 0];
            if (r.alwaysUpdate || r.rebuildOptions.all)
                for (i = 0; e > i; i++) r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = o[0], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = o[1], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = o[2], r.triangleBuffers.shadowData[r.triangleBuffers.shadowIndex++] = o[3];
            if (r.rebuildOptions.colours || r.alwaysUpdate || r.rebuildOptions.all)
                for (i = 0; e > i; i++) r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = t.wc[0], r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = t.wc[1], r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = t.wc[2], r.triangleBuffers.colourData[r.triangleBuffers.colourIndex++] = t.wc[3];
            if (r.rebuildOptions.positions || r.alwaysUpdate || r.rebuildOptions.all) {
                var u = t.x1,
                    s = t.y1,
                    l = t.x2,
                    f = t.y2;
                a && (u += a.x, s += a.y, l += a.x, f += a.y), r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = u, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = u, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = u, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = s, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = l, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = f, r.triangleBuffers.positionData[r.triangleBuffers.positionIndex++] = 0
            }(r.rebuildOptions.textures || r.alwaysUpdate || r.rebuildOptions.all) && (r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.u2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.v2, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 1, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 3, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = n.colourComponentTexture, r.triangleBuffers.textureData[r.triangleBuffers.textureIndex++] = 0)
        }
    }
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["line-arc-vertex"] = "cHJlY2lzaW9uIGhpZ2hwIGZsb2F0OwphdHRyaWJ1dGUgdmVjMiBhX3Bvc2l0aW9uOwphdHRyaWJ1dGUgZmxvYXQgYV9yYWRpdXM7CmF0dHJpYnV0ZSBmbG9hdCBhX3N0YXJ0QW5nbGU7CmF0dHJpYnV0ZSBmbG9hdCBhX2VuZEFuZ2xlOwphdHRyaWJ1dGUgZmxvYXQgYV93aWR0aDsKYXR0cmlidXRlIGZsb2F0IGFfaW5kZXg7CmF0dHJpYnV0ZSBmbG9hdCBhX251bU9mU2VnbWVudHM7CmF0dHJpYnV0ZSB2ZWM0IGFfY29sb3VyOwphdHRyaWJ1dGUgZmxvYXQgYV9saW5lVHlwZTsKYXR0cmlidXRlIHZlYzQgYV9oaXRUZXN0Q29sb3VyOwoKdW5pZm9ybSBmbG9hdCB1X2hpdFRlc3Q7CnVuaWZvcm0gZmxvYXQgdV96b29tOwp1bmlmb3JtIHZlYzIgdV9yZXNvbHV0aW9uOwp1bmlmb3JtIG1hdDQgdV90cmFuc2Zvcm07Cgp2YXJ5aW5nIHZlYzQgdl9jb2xvdXI7CnZhcnlpbmcgZmxvYXQgdl9saW5lVHlwZTsKdmFyeWluZyBmbG9hdCB2X3V2Owp2YXJ5aW5nIGZsb2F0IHZfc2lnbjsKdmFyeWluZyB2ZWM0IHZfaGl0VGVzdENvbG91cjsKdmFyeWluZyBmbG9hdCB2X2xlbmd0aDsKdmFyeWluZyBmbG9hdCB2X3dpZHRoOwp2YXJ5aW5nIGZsb2F0IHZfZGFzaE9mZnNldDsKdmFyeWluZyBmbG9hdCB2X2RvdHNMZW5ndGg7CgoKZmxvYXQgcGkgPSAzLjE0MTU5MjY1MzU5OwpmbG9hdCBtaW5XaWR0aCA9IDEuMDsKICAgCnZlYzIgZ2V0TmV4dFBvc2l0aW9uKHZlYzIgcG9zaXRpb24sIGZsb2F0IGFfcmFkaXVzLCBmbG9hdCBhX3N0YXJ0QW5nbGUsIGZsb2F0IGFuZ2xlSW5jcmVtZW50LCBmbG9hdCBpbmRleCkgewogIHJldHVybiB2ZWMyKAogICAgICAgYV9wb3NpdGlvbi54ICsgYV9yYWRpdXMgKiBjb3MoYV9zdGFydEFuZ2xlICsgYW5nbGVJbmNyZW1lbnQgKiBpbmRleCksCiAgICAgICBhX3Bvc2l0aW9uLnkgKyBhX3JhZGl1cyAqIHNpbihhX3N0YXJ0QW5nbGUgKyBhbmdsZUluY3JlbWVudCAqIGluZGV4KQogICk7Cn0KCnZvaWQgbWFpbigpIHsKICAgZmxvYXQgZGFzaE9mZnNldCA9IDAuNTsKICAgaWYodV9oaXRUZXN0ID09IDEuMCkgewogICAgICBtaW5XaWR0aCA9IDguMDsKICAgfQogICBmbG9hdCByYWRpdXMgPSBhX3JhZGl1czsKICAgdmVjMiBwb3NpdGlvbjsKICAgLy8gRGlyZWN0aW9uIG9mIHRoZSBub3JtYWwgdmVjdG9yLCBlbmNvZGVkIGluIHRoZSBzaWduIG9mIG51bWJlck9mU2VnbWVudHMgZm9yIGVmZmljaWVuY3kKICAgZmxvYXQgZGlyZWN0aW9uID0gc2lnbihhX251bU9mU2VnbWVudHMpOwogICAKICAgLy8gV2hldGhlciB0aGUgdmVydGV4IGlzIGF0IHRoZSBzdGFydCBvciBlbmQgb2YgdGhlIHNlZ21lbnQsIGVuY29kZWQgaW4gdGhlIHNpZ24gb2YgdGhlIGluZGV4IHBhcmFtZXRlcgogICBib29sIGluZGV4RGlyZWN0aW9uID0gc2lnbihhX2luZGV4KSA9PSAxLjAgfHwgYV9pbmRleCA9PSAwLjA7CiAgIC8vIFRoZSByZWFsIGluZGV4IHZhbHVlCiAgIGZsb2F0IGluZGV4ID0gc2lnbihhX2luZGV4KSAqIGFfaW5kZXg7CiAgIC8vIFRoZSByZWFsIG51bU9mU2VnbWVudHMgdmFsdWUKICAgZmxvYXQgbnVtT2ZTZWdtZW50cyA9IHNpZ24oYV9udW1PZlNlZ21lbnRzKSAqIGFfbnVtT2ZTZWdtZW50czsKICAgLy8gSG93IG11Y2ggZWFjaCBzZWdtZW50IG5lZWRzIHRvIGN1cnZlCiAgIGZsb2F0IGFuZ2xlSW5jcmVtZW50ID0gKGFfZW5kQW5nbGUgLSAgYV9zdGFydEFuZ2xlKSAvIG51bU9mU2VnbWVudHM7CiAgCiAgIGZsb2F0IGZpbmFsV2lkdGggPSBtYXgoKGFfd2lkdGggKiAyLjApICogdV96b29tLCAobWluV2lkdGggKiAyLjApKSAvIHVfem9vbTsKICAgdmVjMiBkaXN0YW5jZVZlY3RvcjsKICAgZmxvYXQgbmV4dEluZGV4OwogICAKICAgaWYoZGlyZWN0aW9uID09IDEuMCkgewogICAgIHJhZGl1cyArPSBmaW5hbFdpZHRoLzIuMDsKICAgfSBlbHNlIHsKICAgICByYWRpdXMgLT0gZmluYWxXaWR0aC8yLjA7CiAgIH0KICAKICAgaWYoYV9lbmRBbmdsZSAtIGFfc3RhcnRBbmdsZSA9PSAyLjAqcGkpIHsKICAgICAgZGFzaE9mZnNldCA9IDAuMDsKICAgfQogIAogICBwb3NpdGlvbiA9IGdldE5leHRQb3NpdGlvbihhX3Bvc2l0aW9uLnh5LCByYWRpdXMsIGFfc3RhcnRBbmdsZSwgYW5nbGVJbmNyZW1lbnQsIGluZGV4KTsKICAKICAgLy8gY29udmVydCB0aGUgcmVjdGFuZ2xlIGZyb20gcGl4ZWxzIHRvIDAuMCB0byAxLjAKICAgdmVjMiB6ZXJvVG9PbmUgPSBwb3NpdGlvbiAvIHVfcmVzb2x1dGlvbjsKCiAgIC8vIGNvbnZlcnQgZnJvbSAwLT4xIHRvIDAtPjIKICAgdmVjMiB6ZXJvVG9Ud28gPSB6ZXJvVG9PbmUgKiAyLjA7CgogICAvLyBjb252ZXJ0IGZyb20gMC0+MiB0byAtMS0+KzEgKGNsaXBzcGFjZSkKICAgdmVjMiBjbGlwU3BhY2UgPSB6ZXJvVG9Ud28gLSAxLjA7CiAgIAogICBnbF9Qb3NpdGlvbiA9IHVfdHJhbnNmb3JtICogdmVjNChjbGlwU3BhY2UgKiB2ZWMyKDEsIC0xKSwgMCwgMSk7CiAgIHZfY29sb3VyID0gYV9jb2xvdXI7CiAgIAogICB2X3V2ID0gaW5kZXggLyAyNC4wOwogICB2X2xlbmd0aCA9IGFfcmFkaXVzICogKGFfZW5kQW5nbGUgLSBhX3N0YXJ0QW5nbGUpOwogICBpZihkaXJlY3Rpb24gPT0gLTEuMCkgewogICAgIHZfc2lnbiA9IDAuMDsKICAgfSBlbHNlIHsKICAgICB2X3NpZ24gPSAxLjA7CiAgIH0KICAgdl9oaXRUZXN0Q29sb3VyID0gYV9oaXRUZXN0Q29sb3VyOwogICB2X2xpbmVUeXBlID0gYV9saW5lVHlwZTsKICAgdl93aWR0aCA9IGFfd2lkdGg7CiAgIHZfZGFzaE9mZnNldCA9IGRhc2hPZmZzZXQ7CiAgIGlmKGFfbGluZVR5cGUgPT0gMy4wICkgewogICAgIGZsb2F0IGRhc2hMZW5ndGggPSB2X3dpZHRoICogMi4wOwogICAgIGZsb2F0IGdhcFdpZHRoID0gZGFzaExlbmd0aCAvIDIuMDsKICAgICBmbG9hdCBkYXNoZXMgPSBmbG9vcigodl9sZW5ndGggKyBnYXBXaWR0aCkgLyAoZGFzaExlbmd0aCApKTsKICAgICB2X3V2ID0gdl91diogKGRhc2hlcyk7CiAgIH0KfQ==");
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["line-fragment"] = "I2lmZGVmIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcwojZXh0ZW5zaW9uIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcyA6IGVuYWJsZQojZW5kaWYKcHJlY2lzaW9uIGhpZ2hwIGZsb2F0OwoKdW5pZm9ybSBmbG9hdCB1X2hpdFRlc3Q7CnVuaWZvcm0gZmxvYXQgdV9zaGFkb3dUeXBlOwoKdmFyeWluZyB2ZWM0IHZfY29sb3VyOwp2YXJ5aW5nIHZlYzQgdl9oaXRUZXN0Q29sb3VyOwp2YXJ5aW5nIGZsb2F0IHZfdXY7CnZhcnlpbmcgZmxvYXQgdl9zaWduOwp2YXJ5aW5nIGZsb2F0IHZfbGluZVR5cGU7CnZhcnlpbmcgZmxvYXQgdl9sZW5ndGg7CnZhcnlpbmcgZmxvYXQgdl93aWR0aDsKdmFyeWluZyBmbG9hdCB2X2RvdHNMZW5ndGg7CgpmbG9hdCBwaSA9IDMuMTQxNTkyNjUzNTk7CmZsb2F0IHNxcnRPZkhhbGYgPSAwLjcwNzEwNjc4MTE4NjU0NzU3OwovLyBQZXJmb3JtYW5jZSBhbnRpYWxpYXNpbmcgdXNpbmcgc3RhbmRhcmQgZGVyaXZhdGl2ZXMKLy8gU21vb3RocyB0aGUgZnJhZ21lbnQgb3ZlciAxIHBpeGVsIHdoZW4gdmFsdWUgaXMgbW9yZSB0aGFuIHRocmVzaG9sZAovLyBlc2VudGlhbGx5IGEgYmV0dGVyIHZlcnNpb24gb2Ygc3RlcCBhbmQgc21vb3Roc3RlcApmbG9hdCBhYXN0ZXAoZmxvYXQgdGhyZXNob2xkLCBmbG9hdCB2YWx1ZSkgewogICAgZmxvYXQgYWZ3aWR0aCA9IGxlbmd0aCh2ZWMyKGRGZHgodmFsdWUpLCBkRmR5KHZhbHVlKSkpICogc3FydE9mSGFsZjsKICAgIHJldHVybiBzbW9vdGhzdGVwKHRocmVzaG9sZC1hZndpZHRoLCB0aHJlc2hvbGQrYWZ3aWR0aCwgdmFsdWUpOwp9Cgp2b2lkIG1haW4oKSB7CmZsb2F0IGRhc2hUaHJlc2hvbGQgPSAtMC41OwogIGlmKHVfaGl0VGVzdCA+IDAuMCkgewogICAgLy8gRXhjbHVkZSBmcm9tIHNoYWRvdwogICAgaWYodV9zaGFkb3dUeXBlIDwgLTAuNSkgewogICAgICAgIGRpc2NhcmQ7CiAgICB9IGVsc2UgewogICAgICBnbF9GcmFnQ29sb3IgPSB2X2hpdFRlc3RDb2xvdXIgLyAyNTUuMDsKICAgIH0KICAvLyBzaG93IG9ubHkgb24gc2hhZG93CiAgfSBlbHNlIGlmKHVfc2hhZG93VHlwZSA+IDAuNSkgewogICAgZGlzY2FyZDsKICB9IGVsc2UgewogICAgZmxvYXQgZGlzdFRvRWRnZSA9IGRpc3RhbmNlKDAuNSwgdl9zaWduKTsKICAgIHZlYzQgY29sb3VyID0gdmVjNCgodl9jb2xvdXIucmdiLyAyNTUuMCkgKiAodl9jb2xvdXIuYSAvMjU1LjApLCAodl9jb2xvdXIuYSAvIDI1NS4wKSk7CiAgICBpZih2X2xpbmVUeXBlID4gMS4wKSB7CiAgICAgIC8vIGRvdHRlZCBsaW5lcwogICAgICBpZih2X2xpbmVUeXBlID4gMi41KSB7CiAgICAgICAgLy8gRGlzdGFuY2UgdG8gbmVhcmVzdCBwb2ludCBpbiBhIGdyaWQgb2YKICAgICAgICAvLyBwb2ludHMgb3ZlciB0aGUgdW5pdCBzcXVhcmUKICAgICAgICB2ZWMyIG5lYXJlc3QgPSAyLjAqZnJhY3QodmVjMih2X3V2LCB2X3NpZ24gKSkgLSAxLjA7CiAgICAgICAgZmxvYXQgZGlzdCA9IGxlbmd0aChuZWFyZXN0KTsKICAgICAgICBmbG9hdCByYWRpdXMgPSAwLjU7CiAgICAgICAgY29sb3VyICo9IGFhc3RlcChyYWRpdXMsIDEuMCAtIGRpc3QpOwogICAgICAvLyBkYXNoZXMgIAogICAgICB9IGVsc2UgewogICAgICAgIGZsb2F0IGRhc2hMZW5ndGggPSB2X3dpZHRoICogMi4wOwogICAgICAgIGZsb2F0IGdhcFdpZHRoID0gZGFzaExlbmd0aCAvIDIuMDsKICAgICAgICBmbG9hdCBkYXNoZXMgPSBmbG9vcigodl9sZW5ndGggKyBnYXBXaWR0aCkgLyAoZGFzaExlbmd0aCArIGdhcFdpZHRoKSkgKyAwLjU7CiAgICAgICAgZmxvYXQgZGFzaCA9IHNpbigoKHZfdXYgKiBkYXNoZXMpKSAqICgyLjAgKiBwaSkpOwogICAgICAgIC8vIHNtb290aCBhbmQgY3V0IG91dCBhIGdhcCB3aGVuIHRoZSBzaW5lIHdhdmUgaWYgcGFzdCB0aGUgZGFzaFRocmVzaG9sZAogICAgICAgIGNvbG91ciAqPSBhYXN0ZXAoZGFzaFRocmVzaG9sZCwgIGRhc2gpOwogICAgICAgIC8vIHNtb290aCBlZGdlIG9mIGxpbmUKICAgICAgICBjb2xvdXIgKj0gMS4wIC0gYWFzdGVwKDAuMjQ5LCBkaXN0VG9FZGdlKTsKICAgICAgfQogICAgfSBlbHNlIHsKICAgICAgLy8gc21vb3RoIGVkZ2Ugb2YgbGluZQogICAgICBjb2xvdXIgKj0gMS4wIC0gYWFzdGVwKDAuMjUxLCBkaXN0VG9FZGdlKTsKICAgIH0KICAgIC8vIGFzc2lnbiB0aGUgZmluYWwgY29sb3VyIHRvIHRoZSBmcmFnbWVudAogICAgZ2xfRnJhZ0NvbG9yID0gY29sb3VyOwogIH0KfQ==")
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["line-vertex"] = "cHJlY2lzaW9uIGhpZ2hwIGZsb2F0OwphdHRyaWJ1dGUgdmVjNCBhX3Bvc2l0aW9uOwphdHRyaWJ1dGUgZmxvYXQgYV93aWR0aDsKYXR0cmlidXRlIHZlYzQgYV92ZXJ0ZXhJbmZvOwphdHRyaWJ1dGUgdmVjNCBhX2NvbG91cjsKYXR0cmlidXRlIHZlYzQgYV9oaXRUZXN0Q29sb3VyOwoKdW5pZm9ybSBmbG9hdCB1X2hpdFRlc3Q7CnVuaWZvcm0gdmVjMiB1X3Jlc29sdXRpb247CnVuaWZvcm0gbWF0NCB1X3RyYW5zZm9ybTsKdW5pZm9ybSBmbG9hdCB1X3pvb207CnVuaWZvcm0gZmxvYXQgdV92aWV3SW5kZXBlbmRlbnQ7Cgp2YXJ5aW5nIHZlYzQgdl9jb2xvdXI7CnZhcnlpbmcgdmVjNCB2X3ZlcnRleEluZm87CnZhcnlpbmcgZmxvYXQgdl91djsKdmFyeWluZyB2ZWM0IHZfaGl0VGVzdENvbG91cjsKdmFyeWluZyBmbG9hdCB2X3NpZ247CnZhcnlpbmcgZmxvYXQgdl9hYURpc3RhbmNlOwp2YXJ5aW5nIGZsb2F0IHZfbGVuZ3RoOwp2YXJ5aW5nIGZsb2F0IHZfd2lkdGg7CnZhcnlpbmcgZmxvYXQgdl96b29tOwp2YXJ5aW5nIGZsb2F0IHZfbGluZVR5cGU7CnZhcnlpbmcgZmxvYXQgdl9kb3RzTGVuZ3RoOwoKdm9pZCBtYWluKCkgewogIGZsb2F0IG1pbldpZHRoID0gMS4wOwogIC8vIE1ha2UgaXQgd2lkZXIgdG8gYWxsb3cgdGhlIGFudGkgYWxpYXNpbmcgdG8gZWF0IGludG8gdGhlIGxpbmUgaW4gb3JkZXIgdG8gbWFrZSBpdCBzbW9vdGgKICBmbG9hdCBhYVdpZHRoQnVmZmVyID0gMi4wOwogIAogIHZfYWFEaXN0YW5jZSA9IDAuMjU7CiAgLy8gZGlzYWJsZSBhYSB3aGVuIHR5cGUgPT0gMQogIGlmKGFfdmVydGV4SW5mby53ID09IDEuMCkgewogICAgbWluV2lkdGggPSAxLjA7CiAgICAvL2Vuc3VyZSBhYSBuZXZlciBraWNrcyBpbiB3aGlsZSBhdm9pZGluZyBhbiBpZiBzdGF0ZW1lbnQgaW4gZnJhZ21lbnQgc2hhZGVyCiAgICB2X2FhRGlzdGFuY2UgPSAxLjA7CiAgfQogIAogIGlmKHVfaGl0VGVzdCA9PSAxLjApIHsKICAgIG1pbldpZHRoID0gNC4wOwogIH0KICB2ZWMyIHBvc2l0aW9uOwogIAogIC8vIERpcmVjdGlvbiBvZiB0aGUgbm9ybWFsIHZlY3RvcgogIGZsb2F0IG5vcm1hbFBvaW50c1Vwd2FyZCA9IGFfdmVydGV4SW5mby55IC0gMS4wOwogIAogIC8vIFRoZSBVViBjb29yZHMgZnJvbSAwIC0+IDEsIHVzZWQgZm9yIGRhc2hpbmcsIHRoZSBsaW5lIG51bWJlciBpcyBhZGRlZAogIC8vIGZvciB0aGUgY2FzZSB3aGVuIGxpbmVzIGFyZSBqb2luZWQKICBmbG9hdCB1diA9IGFfdmVydGV4SW5mby54ICsgYV92ZXJ0ZXhJbmZvLno7CiAgCiAgdmVjMiBkaXN0YW5jZVZlY3RvciA9IGFfcG9zaXRpb24uencgLSBhX3Bvc2l0aW9uLnh5OwogIAogIHZlYzIgbm9ybWFsID0gbm9ybWFsaXplKHZlYzIoLWRpc3RhbmNlVmVjdG9yLnksIGRpc3RhbmNlVmVjdG9yLngpKTsKICAKICAvLyBXaGV0aGVyIHRoZSB2ZXJ0ZXggaXMgYXQgdGhlIHN0YXJ0IG9yIGVuZCBvZiB0aGUgc2VnbWVudC4KICBpZihhX3ZlcnRleEluZm8ueCA9PSAwLjApIHsKICAgcG9zaXRpb24gPSBhX3Bvc2l0aW9uLnh5OwogIH0gZWxzZSB7CiAgIHBvc2l0aW9uID0gYV9wb3NpdGlvbi56dzsKICB9CiAgZmxvYXQgZmluYWxXaWR0aCA9IGFfd2lkdGggKiBhYVdpZHRoQnVmZmVyOwogIGlmKHVfdmlld0luZGVwZW5kZW50ID09IDAuMCkgewogICBmaW5hbFdpZHRoID0gbWF4KChmaW5hbFdpZHRoKSAqIHVfem9vbSwgbWluV2lkdGggKiBhYVdpZHRoQnVmZmVyKSAvIHVfem9vbTsKICB9CiAgdmVjMiBub3JtYWxWZWMgPSBub3JtYWwgKiAoZmluYWxXaWR0aC8yLjApICogbm9ybWFsUG9pbnRzVXB3YXJkOwogIHBvc2l0aW9uID0gcG9zaXRpb24gKyBub3JtYWxWZWM7CiAgCiAgLy8gY29udmVydCB0aGUgcmVjdGFuZ2xlIGZyb20gcGl4ZWxzIHRvIDAuMCB0byAxLjAKICB2ZWMyIHplcm9Ub09uZSA9IHBvc2l0aW9uIC8gdV9yZXNvbHV0aW9uOwogIAogIC8vIGNvbnZlcnQgZnJvbSAwLT4xIHRvIDAtPjIKICB2ZWMyIHplcm9Ub1R3byA9IHplcm9Ub09uZSAqIDIuMDsKICAKICAvLyBjb252ZXJ0IGZyb20gMC0+MiB0byAtMS0+KzEgKGNsaXBzcGFjZSkKICB2ZWMyIGNsaXBTcGFjZSA9IHplcm9Ub1R3byAtIDEuMDsKICAKICBnbF9Qb3NpdGlvbiA9IHVfdHJhbnNmb3JtICogdmVjNChjbGlwU3BhY2UgKiB2ZWMyKDEsIC0xKSwgMCwgMSk7CiAgCiAgdl9jb2xvdXIgPSBhX2NvbG91cjsKICB2X3V2ID0gdXY7CiAgaWYobm9ybWFsUG9pbnRzVXB3YXJkID09IDEuMCkgewogICB2X3NpZ24gPSAwLjA7CiAgfSBlbHNlIHsKICAgdl9zaWduID0gMS4wOwogIH0KICAKICB2X2xlbmd0aCA9IGRpc3RhbmNlKGFfcG9zaXRpb24ueHksIGFfcG9zaXRpb24uencpOwogIHZfaGl0VGVzdENvbG91ciA9IGFfaGl0VGVzdENvbG91cjsKICB2X3dpZHRoID0gYV93aWR0aDsKICB2X3pvb20gPSB1X3pvb207CiAgdl9saW5lVHlwZSA9IGFfdmVydGV4SW5mby53OwogIAogIGlmKGFfdmVydGV4SW5mby54ID09IDAuMCkgewogICAgdl9kb3RzTGVuZ3RoID0gMC4wOyAKICB9IGVsc2UgewogICAgdl9kb3RzTGVuZ3RoID0gbWF4KHZfbGVuZ3RoIC8gKGFfd2lkdGgqMi4wKSwxMDAwMC4wKTsKICB9CiAgCiAgaWYoYV92ZXJ0ZXhJbmZvLncgPT0gMy4wICkgewogICAgZmxvYXQgZGFzaExlbmd0aCA9IHZfd2lkdGggKiAyLjA7CiAgICBmbG9hdCBnYXBXaWR0aCA9IGRhc2hMZW5ndGggLyAyLjA7CiAgICBmbG9hdCBkYXNoZXMgPSBmbG9vcigodl9sZW5ndGggKyBnYXBXaWR0aCkgLyAoZGFzaExlbmd0aCApKTsKICAgIHZfdXYgPSB1diogKGRhc2hlcyk7CiAgfQp9")
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["triangle-fragment"] = "I2lmZGVmIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcwojZXh0ZW5zaW9uIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcyA6IGVuYWJsZQojZW5kaWYKCnByZWNpc2lvbiBoaWdocCBmbG9hdDsKCi8vIG91ciB0ZXh0dXJlCnVuaWZvcm0gc2FtcGxlcjJEIHVfaW1hZ2VbOF07CnVuaWZvcm0gbWVkaXVtcCBmbG9hdCB1X2hpdFRlc3Q7CnVuaWZvcm0gdmVjMiB1X3Jlc29sdXRpb247CnVuaWZvcm0gZmxvYXQgdV9zaGFkb3dUeXBlOwoKCi8vIHRoZSB0ZXhDb29yZHMgcGFzc2VkIGluIGZyb20gdGhlIHZlcnRleCBzaGFkZXIuCnZhcnlpbmcgdmVjNCB2X3RleENvb3JkOwp2YXJ5aW5nIHZlYzQgdl9jb2xvdXI7CnZhcnlpbmcgdmVjNCB2X2hpdFRlc3RDb2xvdXI7CnZhcnlpbmcgdmVjMiB2X3RleHR1cmVCYW5rOwp2YXJ5aW5nIGZsb2F0IHZfd2lkdGg7CnZhcnlpbmcgZmxvYXQgdl9pc0NpcmNsZTsKdmFyeWluZyBmbG9hdCB2X3BpeGVsOwp2YXJ5aW5nIGZsb2F0IHZfaFdpZHRoOwp2YXJ5aW5nIGZsb2F0IHZfdkhlaWdodDsKdmFyeWluZyBmbG9hdCB2X2JvcmRlcldpZHRoOwoKZmxvYXQgYWFzdGVwKGZsb2F0IHRocmVzaG9sZCwgZmxvYXQgdmFsdWUpIHsKICBmbG9hdCBhZndpZHRoID0gbGVuZ3RoKHZlYzIoZEZkeCh2YWx1ZSksIGRGZHkodmFsdWUpKSkgKiAwLjcwNzEwNjc4MTE4NjU0NzU3OwogIHJldHVybiBzbW9vdGhzdGVwKHRocmVzaG9sZC1hZndpZHRoLCB0aHJlc2hvbGQrYWZ3aWR0aCwgdmFsdWUpOwp9CgpmbG9hdCBlZGdlRmFjdG9yKHZlYzMgYmFyeWNlbnRyaWNDb29yZHMpewogIHZlYzMgZCA9IGZ3aWR0aChiYXJ5Y2VudHJpY0Nvb3Jkcyk7CiAgdmVjMyBhMyA9IHNtb290aHN0ZXAodmVjMygwLjApLCBkKjMuMCwgYmFyeWNlbnRyaWNDb29yZHMpOwogcmV0dXJuIG1pbihtaW4oYTMueCwgYTMueSksIGEzLnopOwp9CgovLyBSZWFkIHRoZSB0ZXh0dXJlIHZhbHVlIG9mIGEgZ2x5cGggZnJvbSB0aGUgc3BlY2lmaWVkIGNvbG91ciBjaGFubmVsCmZsb2F0IHJlYWRDb2xvdXJDb21wb25lbnRGcm9tVGV4dHVyZShmbG9hdCB0ZXh0dXJlQmFuaywgdmVjNCB0ZXhDb29yZCkgewogIGZsb2F0IHRleENvbXBvbmVudDsKICBpZih0ZXh0dXJlQmFuayA8IDAuNSkgewogICAgdGV4Q29tcG9uZW50ID0gdGV4Q29vcmQucjsKICB9IGVsc2UgaWYodGV4dHVyZUJhbmsgPCAxLjUpIHsKICAgIHRleENvbXBvbmVudCA9IHRleENvb3JkLmc7CiAgfSBlbHNlIHsKICAgIHRleENvbXBvbmVudCA9IHRleENvb3JkLmI7CiAgfQogIHJldHVybiB0ZXhDb21wb25lbnQ7Cn0KCi8vIFVzZWQgdG8gcmVjb25zdHJ1Y3QgYSBnbHlwaCBmcm9tIHRoZSBjb2xvdXJDb21wb25lbnQgdGV4dHVyZQp2ZWM0IGdldENvbG91ckZyb21Db21wb25lbnQoZmxvYXQgY29sb3VyQ29tcG9uZW50LCB2ZWM0IGNvbG91ck1vZGlmaWVyKSB7CiAgLy8gbXVsdGlwbHkgdGhlIGNvbG91ciB0byBoZWxwIHJlZHVjZSBjb2xvdXIgYmxlZWRpbmcgZm9yIHRleHQKICBmbG9hdCBjb2xvdXJCb29zdEZhY3RvclIgPSBtYXgoMS4yICogY29sb3VyTW9kaWZpZXIuciwgMS4wKTsKICBmbG9hdCBjb2xvdXJCb29zdEZhY3RvckcgPSBtYXgoMS4yICogY29sb3VyTW9kaWZpZXIuZywgMS4wKTsKICBmbG9hdCBjb2xvdXJCb29zdEZhY3RvckIgPSBtYXgoMS4yICogY29sb3VyTW9kaWZpZXIuYiwgMS4wKTsKICBmbG9hdCBhID0gbWluKGNvbG91ckNvbXBvbmVudCAqIGNvbG91ck1vZGlmaWVyLmEsIGNvbG91ck1vZGlmaWVyLmEpOwogIGZsb2F0IHIgPSBtaW4oY29sb3VyQ29tcG9uZW50ICogY29sb3VyTW9kaWZpZXIuciAqIGNvbG91ckJvb3N0RmFjdG9yUiwgY29sb3VyTW9kaWZpZXIucik7CiAgZmxvYXQgZyA9IG1pbihjb2xvdXJDb21wb25lbnQgKiBjb2xvdXJNb2RpZmllci5nICogY29sb3VyQm9vc3RGYWN0b3JHLCBjb2xvdXJNb2RpZmllci5nKTsKICBmbG9hdCBiID0gbWluKGNvbG91ckNvbXBvbmVudCAqIGNvbG91ck1vZGlmaWVyLmIgKiBjb2xvdXJCb29zdEZhY3RvckIsIGNvbG91ck1vZGlmaWVyLmIpOwogIHJldHVybiB2ZWM0KHIsZyxiLGEpOwp9Cgp2b2lkIG1haW4oKSB7CiAgdmVjNCBjb2xvckE7CiAgdmVjMiBjaXJjbGVDZW50cmUgPSB2ZWMyKDAuNSwgMC41KTsKICBpZih1X2hpdFRlc3QgPiAwLjApIHsKICAgIC8vIEV4Y2x1ZGUgZnJvbSBzaGFkb3cKICAgIGlmKHVfc2hhZG93VHlwZSA8IC0wLjUpewogICAgICAgIGRpc2NhcmQ7CiAgICB9IGVsc2UgewogICAgICB2ZWM0IGNvbG9yQSA9IHZfaGl0VGVzdENvbG91ciAvIDI1NS4wOwogICAgICBpZih2X2lzQ2lyY2xlID4gMC4wKSB7CiAgICAgICAgaWYodl90ZXh0dXJlQmFuay54ID4gLTEuNSAmJiB2X3RleHR1cmVCYW5rLnggPCAtMC41KSB7CiAgICAgICAgIC8vIGZvciBvZmZzZXQgY2lyY2xlcwogICAgICAgICBjaXJjbGVDZW50cmUgPSB2X3RleENvb3JkLnh5OwogICAgICAgIH0KICAgICAgICBmbG9hdCBkaXN0ID0gZGlzdGFuY2Uodl90ZXhDb29yZC56dywgY2lyY2xlQ2VudHJlKTsKICAgICAgICBmbG9hdCBib3JkZXIgPSAwLjUgLSB2X3dpZHRoOwogICAgICAgIGNvbG9yQSAqPSBzdGVwKDAuNSArIHZfcGl4ZWwvMi4wLCAgMS4wIC0gZGlzdCk7CiAgICAgICAgaWYodl93aWR0aCA+IDAuMCkgewogICAgICAgICAgY29sb3JBICo9IHN0ZXAoYm9yZGVyLCBkaXN0KTsKICAgICAgICB9CiAgICAgIH0KICAgICAgZ2xfRnJhZ0NvbG9yID0gY29sb3JBOwogICAgfQogIH0gZWxzZSBpZih1X3NoYWRvd1R5cGUgPiAwLjUpewogICAgZGlzY2FyZDsKICB9IGVsc2UgewogICAgICBjb2xvckEgPSB2X2NvbG91ciAvIDI1NS4wOwogICAgICAvLyBsb2FkIHRoZSBjb3JyZWN0IHRleHR1cmUgYW5kIHJlYWQgaXQKICAgICAgLy8gLTIgZmxhdCBzaGFkZWQgd2l0aCBhbnRpIGFsaWFzaW5nCiAgICAgIC8vIC0xIGZsYXQgc2hhZGVkCiAgICAgIC8vIDAuLjIgLT4gdGV4dAogICAgICAvLyAzIC0+IGZvbnRJY29ucwogICAgICAvLyA0Li4uNyAtPiBpbWFnZXMKICAgICAgaWYgKHZfdGV4dHVyZUJhbmsueCA+IC0wLjUpIHsKICAgICAgICAgIHZlYzQgdGV4Q29vcmQ7CiAgICAgICAgICAvL3RleHQgMAogICAgICAgICAgaWYodl90ZXh0dXJlQmFuay54IDwgMC41KSAgewogICAgICAgICAgIHRleENvb3JkID0gdGV4dHVyZTJEKHVfaW1hZ2VbMF0sIHZfdGV4Q29vcmQueHkpOwogICAgICAgICAgLy90ZXh0IDEKICAgICAgICAgIH0gZWxzZSBpZih2X3RleHR1cmVCYW5rLnggPCAxLjUpIHsKICAgICAgICAgICAgdGV4Q29vcmQgPSB0ZXh0dXJlMkQodV9pbWFnZVsxXSwgdl90ZXhDb29yZC54eSk7CiAgICAgICAgICAvLyB0ZXh0IDIKICAgICAgICAgIH0gZWxzZSBpZih2X3RleHR1cmVCYW5rLnggPCAyLjUpIHsKICAgICAgICAgICAgdGV4Q29vcmQgPSB0ZXh0dXJlMkQodV9pbWFnZVsyXSwgdl90ZXhDb29yZC54eSk7CiAgICAgICAgICAvLyBmb250SWNvbnMgMyAKICAgICAgICAgIH0gZWxzZSBpZih2X3RleHR1cmVCYW5rLnggPCAzLjUpIHsKICAgICAgICAgICAgdGV4Q29vcmQgPSB0ZXh0dXJlMkQodV9pbWFnZVszXSwgdl90ZXhDb29yZC54eSk7CiAgICAgICAgICAvLyBpbWFnZSA0ICAgCiAgICAgICAgICB9IGVsc2UgaWYodl90ZXh0dXJlQmFuay54IDwgNC41KSB7CiAgICAgICAgICAgIHRleENvb3JkID0gdGV4dHVyZTJEKHVfaW1hZ2VbNF0sIHZfdGV4Q29vcmQueHkpOwogICAgICAgICAgLy8gaW1hZ2UgNSAgIAogICAgICAgICAgfSBlbHNlIGlmKHZfdGV4dHVyZUJhbmsueCA8IDUuNSkgewogICAgICAgICAgICB0ZXhDb29yZCA9IHRleHR1cmUyRCh1X2ltYWdlWzVdLCB2X3RleENvb3JkLnh5KTsKICAgICAgICAgIC8vIGltYWdlIDYKICAgICAgICAgIH0gZWxzZSBpZih2X3RleHR1cmVCYW5rLnggPCA2LjUpIHsKICAgICAgICAgICAgdGV4Q29vcmQgPSB0ZXh0dXJlMkQodV9pbWFnZVs2XSwgdl90ZXhDb29yZC54eSk7ICAgICAKICAgICAgICAgIC8vIGltYWdlIDcgICAKICAgICAgICAgIH0gZWxzZSAgewogICAgICAgICAgICB0ZXhDb29yZCA9IHRleHR1cmUyRCh1X2ltYWdlWzddLCB2X3RleENvb3JkLnh5KTsKICAgICAgICAgIH0KICAgICAgICAgIGlmKHZfdGV4dHVyZUJhbmsueCA+IDMuNSkgewogICAgICAgICAgICBjb2xvckEgPSB2ZWM0KGNvbG9yQS5yZ2IgICogdGV4Q29vcmQucmdiLGNvbG9yQS5hICogdGV4Q29vcmQuYSk7CiAgICAgICAgICB9IGVsc2UgewogICAgICAgICAgICBmbG9hdCBjb2xvdXJDb21wb25lbnQgPSByZWFkQ29sb3VyQ29tcG9uZW50RnJvbVRleHR1cmUodl90ZXh0dXJlQmFuay55LCB0ZXhDb29yZCk7ICAgIAogICAgICAgICAgICBjb2xvckEgPSBnZXRDb2xvdXJGcm9tQ29tcG9uZW50KGNvbG91ckNvbXBvbmVudCwgY29sb3JBKTsKICAgICAgICAgIH0KICAgICAgfSBlbHNlIGlmICh2X3RleHR1cmVCYW5rLnggPCAtMS41KSB7CiAgICAgICAgLy8gVGV4dCBjb29yZHMgYXJlIGJhcnljZW50cmljLCB1c2VkIGZvciBhYSB0cmlhbmdsZXMKICAgICAgICBmbG9hdCBhbHBoYSA9IGNvbG9yQS5hOwogICAgICAgIGNvbG9yQSA9IHZlYzQoY29sb3JBLnJnYiAgKiBhbHBoYSwgYWxwaGEpOwogICAgICAgIGNvbG9yQSAqPSBhYXN0ZXAoMC4wNSwgZWRnZUZhY3Rvcih2X3RleENvb3JkLnh5eikpOyAKICAgICAgfSBlbHNlIHsKICAgICAgICAvLyBmbGF0IHNoYWRlZCByZWN0IG9yIHRyaWFuZ2xlLCBqdXN0IGFwcGx5IHRoZSBjb2xvdXIKICAgICAgICBmbG9hdCBhbHBoYSA9IGNvbG9yQS5hOwogICAgICAgIGNpcmNsZUNlbnRyZSA9IHZfdGV4Q29vcmQueHk7CiAgICAgICAgY29sb3JBID0gdmVjNChjb2xvckEucmdiICAqIGFscGhhLCBhbHBoYSk7CiAgICAgIH0KICAgICAgCiAgICAgIGlmKHZfaXNDaXJjbGUgPiAwLjApIHsKICAgICAgICAgZmxvYXQgZGlzdCA9IGRpc3RhbmNlKHZfdGV4Q29vcmQuencsIGNpcmNsZUNlbnRyZSk7CiAgICAgICAgIGNvbG9yQSAqPSBhYXN0ZXAoMC41LCAgMS4wIC0gZGlzdCk7CiAgICAgICAgIGlmKHZfd2lkdGggPiAwLjApIHsKICAgICAgICAgIGZsb2F0IGJvcmRlciA9IDAuNSAtIHZfd2lkdGg7CiAgICAgICAgICBjb2xvckEgKj0gYWFzdGVwKGJvcmRlciwgZGlzdCk7CiAgICAgICAgIH0KICAgICAgLy8gZHJhdyBhIHNxdWFyZSBib3JkZXIgIAogICAgICB9IGVsc2UgaWYodl9pc0NpcmNsZSA8IC0wLjUpIHsKICAgICAgICBmbG9hdCB0bCA9IG1heChzdGVwKHZfdGV4Q29vcmQueCwgdl9ib3JkZXJXaWR0aCksc3RlcCh2X3RleENvb3JkLnksIHZfYm9yZGVyV2lkdGgpKTsgCiAgICAgICAgZmxvYXQgYnIgPSBtYXgoc3RlcCh2X3RleENvb3JkLnogLSB2X3RleENvb3JkLngsdl9ib3JkZXJXaWR0aCksc3RlcCh2X3RleENvb3JkLncgLSB2X3RleENvb3JkLnksdl9ib3JkZXJXaWR0aCkpOyAKICAgICAgICBjb2xvckEgKj0gbWF4KHRsLGJyKTsKICAgICAgfQogICAgICBnbF9GcmFnQ29sb3IgPSBjb2xvckE7CiAgIH0KfQo=")
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["triangle-vertex"] = "cHJlY2lzaW9uIGhpZ2hwIGZsb2F0OwphdHRyaWJ1dGUgdmVjMiBhX3Bvc2l0aW9uOwphdHRyaWJ1dGUgdmVjNCBhX3RleENvb3JkOwphdHRyaWJ1dGUgdmVjNCBhX2hpdFRlc3RDb2xvdXI7CmF0dHJpYnV0ZSB2ZWMyIGFfdGV4dHVyZUJhbms7CmF0dHJpYnV0ZSB2ZWM0IGFfY29sb3VyOwphdHRyaWJ1dGUgZmxvYXQgYV9jaXJjbGVSYWRpdXM7CmF0dHJpYnV0ZSBmbG9hdCBhX3dpZHRoOwoKdW5pZm9ybSB2ZWMyIHVfcmVzb2x1dGlvbjsKdW5pZm9ybSBtYXQ0IHVfdHJhbnNmb3JtOwoKdmFyeWluZyBmbG9hdCB2X3dpZHRoOwp2YXJ5aW5nIGZsb2F0IHZfaXNDaXJjbGU7CnZhcnlpbmcgdmVjNCB2X3RleENvb3JkOwp2YXJ5aW5nIHZlYzQgdl9jb2xvdXI7CnZhcnlpbmcgdmVjNCB2X2hpdFRlc3RDb2xvdXI7CnZhcnlpbmcgdmVjMiB2X3RleHR1cmVCYW5rOwp2YXJ5aW5nIGZsb2F0IHZfcGl4ZWw7CnZhcnlpbmcgZmxvYXQgdl9ib3JkZXJXaWR0aDsKCgp2b2lkIG1haW4oKSB7CiAgIC8vIGNvbnZlcnQgdGhlIHJlY3RhbmdsZSBmcm9tIHBpeGVscyB0byAwLjAgdG8gMS4wCiAgIHZlYzIgemVyb1RvT25lID0gYV9wb3NpdGlvbiAvIHVfcmVzb2x1dGlvbjsKICAgLy8gY29udmVydCBmcm9tIDAtPjEgdG8gMC0+MgogICB2ZWMyIHplcm9Ub1R3byA9IHplcm9Ub09uZSAqIDIuMDsKCiAgIC8vIGNvbnZlcnQgZnJvbSAwLT4yIHRvIC0xLT4rMSAoY2xpcHNwYWNlKQogICB2ZWMyIGNsaXBTcGFjZSA9IHplcm9Ub1R3byAtIDEuMDsKCiAgIGdsX1Bvc2l0aW9uID0gdV90cmFuc2Zvcm0gKiB2ZWM0KGNsaXBTcGFjZSAqIHZlYzIoMSwgLTEpLCAwLCAxKTsKCiAgIC8vIHBhc3MgdGhlIHRleENvb3JkIHRvIHRoZSBmcmFnbWVudCBzaGFkZXIKICAgLy8gVGhlIEdQVSB3aWxsIGludGVycG9sYXRlIHRoaXMgdmFsdWUgYmV0d2VlbiBwb2ludHMuCiAgIHZfdGV4Q29vcmQgPSBhX3RleENvb3JkOwogICB2X2NvbG91ciA9IGFfY29sb3VyOwogICB2X2hpdFRlc3RDb2xvdXIgPSBhX2hpdFRlc3RDb2xvdXI7CiAgIHZfdGV4dHVyZUJhbmsgPSBhX3RleHR1cmVCYW5rOwogICBpZihhX2NpcmNsZVJhZGl1cyA+IDAuMCkgewogICAgIHZfaXNDaXJjbGUgPSAxLjA7CiAgIH0gZWxzZSB7CiAgICAgdl9pc0NpcmNsZSA9IGFfY2lyY2xlUmFkaXVzOwogICB9CiAgIHZfd2lkdGggPSBhX3dpZHRoIC8gKGFfY2lyY2xlUmFkaXVzICogMi4wKTsKICAgdl9waXhlbCA9IDEuMCAvIChhX2NpcmNsZVJhZGl1cyAqIDIuMCk7CiAgIHZfYm9yZGVyV2lkdGggPSBhX3dpZHRoOwp9Cg==")
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["renderToTexture-fragment"] = "I2lmZGVmIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcwojZXh0ZW5zaW9uIEdMX09FU19zdGFuZGFyZF9kZXJpdmF0aXZlcyA6IGVuYWJsZQojZW5kaWYKCnByZWNpc2lvbiBoaWdocCBmbG9hdDsKCi8vIG91ciB0ZXh0dXJlCnVuaWZvcm0gc2FtcGxlcjJEIHVfaW1hZ2U7CnVuaWZvcm0gbWVkaXVtcCBmbG9hdCB1X2dsb2JhbEFscGhhOwoKLy8gdGhlIHRleENvb3JkcyBwYXNzZWQgaW4gZnJvbSB0aGUgdmVydGV4IHNoYWRlci4KdmFyeWluZyB2ZWMyIHZfdGV4Q29vcmQ7Cgp2b2lkIG1haW4oKSB7CiAgZ2xfRnJhZ0NvbG9yID0gdGV4dHVyZTJEKHVfaW1hZ2UsIHZfdGV4Q29vcmQpICogdV9nbG9iYWxBbHBoYTsKfQo=")
}(),
function() {
    var e = KeyLines.WebGL;
    e && (e["renderToTexture-vertex"] = "cHJlY2lzaW9uIGhpZ2hwIGZsb2F0OwphdHRyaWJ1dGUgdmVjMiBhX3Bvc2l0aW9uOwphdHRyaWJ1dGUgdmVjMiBhX3RleENvb3JkOwoKdmFyeWluZyB2ZWMyIHZfdGV4Q29vcmQ7Cgp2b2lkIG1haW4oKSB7CiAgIGdsX1Bvc2l0aW9uID0gdmVjNChhX3Bvc2l0aW9uLCAwLjAsMS4wKTsKICAgdl90ZXhDb29yZCA9IGFfdGV4Q29vcmQ7Cn0K")
}(),
function() {
    var e = "undefined" != typeof L && L.version;
    "undefined" != typeof window && (window.L_PREFER_CANVAS = !0);
    var t = KeyLines.Map = {};
    t.create = function(t, n, r) {
        function a() {
            throw new Error("Leaflet was not found. Make sure you include leaflet.js and leaflet.css in your web page.")
        }

        function i() {
            throw new Error("Flash version does not support maps")
        }

        function o(e) {
            return Ye ? i : je ? e : a
        }

        function u(e) {
            je && !Ye && (e.map = W())
        }

        function s(e, t) {
            var n = De.latLngToContainerPoint([e, t]);
            return {
                x: n.x,
                y: n.y
            }
        }

        function l(e, t) {
            var n = De.containerPointToLatLng([e, t]);
            return {
                lat: n.lat,
                lng: n.lng
            }
        }

        function f(e, t, n) {
            if (We) {
                var r = De._container,
                    a = r.parentNode;
                r.style.height = t + "px", r.style.width = e + "px", a.style.height = t + "px", a.style.width = e + "px", Te.nextTick(function() {
                    ne(n)
                }, 0)
            } else Re = [e, t], Te.invoke(n)
        }

        function c(e) {
            t.privateEach({
                type: "node"
            }, function(t) {
                t.hi || e({
                    id: t.id,
                    x: t.x,
                    y: t.y
                })
            })
        }

        function d() {
            var e = [];
            return c(function(t) {
                e.push(t)
            }), e
        }

        function g() {
            var e = {};
            return c(function(t) {
                e[t.id] = t
            }), e
        }

        function h() {
            ze = "restore" === Xe.transition ? g() : null
        }

        function v(e) {
            if ("restore" === Xe.transition && ze) {
                var n = Te.dictValues(ze);
                ze = null, t.setProperties(n, !1, e)
            } else t.allowDraw(!1), t.layout("standard", {
                animate: !1
            }, function() {
                t.allowDraw(!0), Te.invoke(e)
            })
        }

        function m(e, t) {
            if (e) {
                for (var n in e) Xe[n] = e[n];
                "tiles" in e && De ? k(t) : Te.invoke(t)
            }
            return Te.clone(Xe)
        }

        function x(e) {
            function r() {
                T(), D(M(), !1), we(function() {
                    w(Xe.animate, a)
                })
            }

            function a() {
                We = !0, Oe = !1, n.trigger("map", "showend"), ce(Ne), Re && (f(Re[0], Re[1], function() {
                    t.zoom("fit")
                }), Re = null), Te.invoke(e)
            }
            if (De || Oe) return Te.invoke(e);
            Oe = !0, n.trigger("map", "showstart"), y(), t.options(Fe), Ee = {};
            var i = {};
            t.privateEach({
                type: "node"
            }, function(e) {
                e.hi || Ae(e) || (Ee[e.id] = !0, i[e.id] = !0)
            }), t.privateEach({
                type: "link"
            }, function(e) {
                e.hi || !i[e.id1] && !i[e.id2] || (Ee[e.id] = !0)
            }), h();
            var o = Te.dictKeys(Ee);
            o.length > 0 ? t.hide(o, {
                time: Xe.time,
                animate: Xe.animate
            }, r) : r()
        }

        function y() {
            var e = t.options();
            ke = {};
            for (var n in Fe) ke[n] = e[n]
        }

        function p() {
            t.options(ke), ke = {}
        }

        function b() {
            Ge.style.top = null, Ge.style.left = null, Ge.parentNode.style.position = null
        }

        function I(e, n, r) {
            e ? t.privateAnimateBackgroundOpacity(n, {
                time: Xe.time
            }, r) : (t.privateSetBackgroundOpacity(n), Te.invoke(r))
        }

        function w(e, t) {
            b(), F(), I(e, 0, t)
        }

        function B(e, t) {
            I(e, 1, t)
        }

        function C(t, n, r, a, i, o) {
            function u(e, t, n) {
                var r = e.width,
                    a = e.height,
                    i = t / n;
                return r / a !== i && (a > r / i ? a = r / i : r > a * i ? r = a * i : (r = t, a = n)), {
                    width: r,
                    height: a
                }
            }

            function s(e) {
                if (p.push(e), p.length >= y) {
                    for (var r = a(g.x, g.y), s = r.getContext("2d"), l = 0; l < p.length; l++) {
                        var f = p[l];
                        s.drawImage(f.img, Math.floor(f.pos.x), Math.floor(f.pos.y), f.size, f.size)
                    }
                    var c = a(t, n),
                        d = c.getContext("2d"),
                        h = u(r, t, n),
                        v = h.width,
                        m = h.height;
                    d.drawImage(r, 0, 0, v, m, 0, 0, t, n), d.drawImage(i, 0, 0);
                    var x;
                    try {
                        x = c.toDataURL()
                    } catch (b) {
                        throw new Error("[toDataURL] There is a problem to export the chart with tainted map tiles.")
                    }
                    Te.invoke(o, x)
                }
            }

            function l(e, t, n, r) {
                var a = new Image;
                a.crossOrigin = "", a.onload = function() {
                    return r({
                        img: a,
                        pos: t,
                        size: n
                    })
                }, a.onerror = function() {
                    return r({
                        img: b,
                        pos: t,
                        size: n
                    })
                }, a.src = e
            }
            if (!We || !De || "exact" !== r.fit) return Te.invoke(o, i.toDataURL());
            if (e) throw new Error("[toDataURL] The mapping library has to be loaded after KeyLines to make toDataURL work");
            var f = De.getPixelBounds(),
                c = De.getPixelOrigin(),
                d = De.getZoom(),
                g = De.getSize(),
                h = Ze.options.tileSize;
            if (d > Ze.options.maxZoom || d < Ze.options.minZoom) return Te.invoke(o, i.toDataURL());
            for (var v = L.bounds(f.min.divideBy(h)._floor(), f.max.divideBy(h)._floor()), m = v.max.y - v.min.y + 1 + (v.min.y > 0 ? 0 : v.min.y), x = v.max.x - v.min.x + 1 + (v.min.x > 0 ? 0 : v.min.x), y = m * x, p = [], b = a(1, 1), I = v.min.y; I <= v.max.y; I++)
                for (var w = v.min.x; w <= v.max.x; w++) {
                    var B = new L.Point(w, I);
                    B.z = d;
                    var C = Ze._getTilePos(B).subtract(f.min).add(c);
                    B.y >= 0 && B.x >= 0 && l(Ze.getTileUrl(B), C, h, s)
                }
        }

        function A(e) {
            function r() {
                Oe = !1, De = null, n.trigger("map", "hideend"), Te.invoke(e)
            }

            function a(e, n, a, i) {
                var o = Te.dictKeys(Ee);
                t.viewOptions(e, {
                    animate: !1
                }, function() {
                    t.hide(o, {
                        animate: !1
                    }, function() {
                        t.setProperties(n, !1, function() {
                            t.allowDraw(!0), t.viewOptions(a, {
                                animate: !0,
                                time: Xe.time
                            }), t.animateProperties(i, {
                                time: Xe.time
                            }, function() {
                                o.length > 0 ? t.show(o, !1, {
                                    animate: !0,
                                    time: Xe.time
                                }, r) : r()
                            })
                        })
                    })
                })
            }

            function i() {
                B(Xe.animate, function() {
                    G(), p();
                    var e, n;
                    Xe.animate && (e = t.viewOptions(), n = d()), t.show(Te.dictKeys(Ee), !1, {
                        animate: !1
                    }, function() {
                        We = !1, v(function() {
                            t.zoom("fit", {
                                animate: !1
                            }, function() {
                                if (Xe.animate) {
                                    var i = t.viewOptions(),
                                        o = d();
                                    a(e, n, i, o)
                                } else r()
                            })
                        })
                    })
                })
            }
            if (!De || Oe) return Te.invoke(e);
            Oe = !0, n.trigger("map", "hidestart");
            var o = M(),
                u = L.point([2 * Ue, 2 * Ue]);
            o.length && De.getZoom() !== De.getBoundsZoom(L.latLngBounds(o), !1, u) ? (De.once("zoomend", i), D(o, Xe.animate)) : i()
        }

        function M() {
            var e = [];
            return t.privateEach({
                type: "node"
            }, function(t) {
                Ae(t) && !t.hi && e.push(L.latLng(t.pos.lat, t.pos.lng))
            }), e
        }

        function D(e, t) {
            e.length ? De.fitBounds(L.latLngBounds(e), {
                animate: !!t,
                padding: [Ue, Ue]
            }) : De.setView(L.latLng(0, 0), 2, {
                animate: t
            })
        }

        function Z(e) {
            if (!e) return -1;
            for (var t = 0, n = e; n = n.previousSibling;) ++t;
            return t
        }

        function R(e, t) {
            var n = Se > 0 ? e.childNodes[Se + 1] : e.firstChild;
            e.insertBefore(t, n || null)
        }

        function G() {
            var e = De._container,
                t = e.parentNode,
                n = t.parentNode;
            De.removeControl(Le), De.remove(), n.removeChild(t), R(n, Ge), Ge.style.position = "", Ge.style["margin-top"] = null, Ge.style["margin-left"] = null
        }

        function S(e) {
            Ge.style.position = "absolute", Ge.style.top = 0, Ge.style.left = 0, Ge.style["margin-top"] = 0, Ge.style["margin-left"] = 0, Ge.style.zIndex = 2;
            var t = document.createElement("div");
            t.style.position = "relative", t.id = e + "-container";
            var n = document.createElement("div");
            n.id = e, n.style.width = t.style.width = Ge.clientWidth + "px", n.style.height = t.style.height = Ge.clientHeight + "px", n.style.position = "absolute", n.style.top = Ge.top, n.style.left = Ge.left;
            var r = Ge.parentNode;
            r.removeChild(Ge), t.appendChild(n), t.appendChild(Ge), R(r, t)
        }

        function T(e) {
            S(Pe), De = new L.Map(Pe, Te.merge(Ke, {
                boxZoom: !1,
                zoomAnimationThreshold: 20,
                zoomControl: !1
            })), E(e)
        }

        function E(e) {
            var t = "",
                n = {
                    noWrap: !0
                };
            "string" != typeof Xe.tiles ? (t = (Xe.tiles || He.tiles).url, n = L.extend(n, Xe.tiles)) : t = Xe.tiles, Ze = new L.TileLayer(t, n), e && Ze.once("load", e), De.addLayer(Ze)
        }

        function k(e) {
            De.removeLayer(Ze), E(e)
        }

        function F(e) {
            Le = Me(e), De.addControl(Le)
        }

        function W() {
            var e = {
                show: We
            };
            if (We) {
                var t = De.getCenter();
                e.lat = t.lat, e.lng = t.lng, e.zoom = De.getZoom(), e.bounds = De.getBounds().toBBoxString(), e.hiddenOnMap = Te.clone(Ee), e.restoreXY = Te.clone(ze)
            }
            return e
        }

        function N(e, n) {
            var r = !0;
            je && !Ye && (e = e || {}, e.show ? (Ee = Te.clone(e.hiddenOnMap) || [], ze = Te.clone(e.restoreXY) || null, We || (We = !0, y(), r = !1, T(function() {
                t.options(Fe), w(!1, n)
            })), De.setView([e.lat, e.lng], e.zoom, {
                reset: !0
            }), Ie()) : !e.show && We && B(!1, function() {
                We = !1, G(), p(), De = null
            })), r && Te.invoke(n)
        }

        function O(e) {
            e && (_e.options = Te.defaults(e, tt), De && X())
        }

        function V(e) {
            _e = {
                options: Te.clone(tt),
                zooming: null,
                zoomLevel: null,
                zoomAnimating: null
            }, O(e)
        }

        function U() {
            return Ge.parentNode.removeChild(Ge), Ge.style.top = 0, Ge.style.left = 0, H(), Ge
        }

        function K() {
            P(!1), ge(!1), Ge.removeAttribute("class")
        }

        function X() {
            De.off("zoomanim", _), _e.options.animZoom && De.on("zoomanim", _)
        }

        function H() {
            t.options({
                handMode: !0,
                hover: 5
            }), P(!0), ge(!0)
        }

        function Y(e) {
            return e = Te.ensureArray(e), {
                handler: function(t) {
                    Te.iterator(e, function(e) {
                        e(t)
                    })
                },
                add: function(t) {
                    e.push(t)
                }
            }
        }

        function P(e) {
            var n = e ? "on" : "off",
                r = {
                    resize: te,
                    dragend: J,
                    drag: J,
                    move: Q
                },
                a = ee(function(e, n) {
                    t.dblclick(n.x, n.y, e.ctrlKey, e.shiftKey)
                }),
                i = Y(a);
            r.dblclick = i.handler;
            var o = !1,
                u = ee(function(e, n) {
                    var r = t.mousedown(e.button, n.x, n.y, e.ctrlKey, e.shiftKey);
                    De && (De.doubleClickZoom.enabled() || o) && (Ne || r && "_" === r.charAt(0) ? (De.doubleClickZoom.disable(), o = !0) : (De.doubleClickZoom.enable(), o = !1))
                }),
                s = Y(u);
            if (L.Browser.mobile || L.Browser.pointer && L.Browser.touch) {
                var l = ee(function(e, n) {
                    t.mouseup(e.button, n.x, n.y, e.ctrlKey, e.shiftKey)
                });
                s.add(l), r.click = s.handler
            } else r.mousedown = s.handler;
            De[n](r), _e.zooming = !1, L.DomEvent.getWheelDelta = function(e) {
                var n;
                return n = void 0 !== e.wheelDelta ? e.wheelDelta : -e.detail, t.trigger("mousewheel", n, e.layerX, e.layerY), n > 0 ? .025 : 0 > n ? -.025 : 0
            }, _e.zoomLevel = De._zoom, De[n]("zoomstart", z), e && !_e.options.animZoom || De[n]("zoomanim", _), De[n]("zoomend", j)
        }

        function z() {
            _e.zooming = !0
        }

        function _(e) {
            re(e)
        }

        function j() {
            _e.zooming = !1, _e.options.animZoom && _e.options.zoomAnimating || Ie()
        }

        function J() {
            xe(!0)
        }

        function Q() {
            _e.zooming || Ie()
        }

        function q(e, t) {
            De.panBy(e, t)
        }

        function $(e, n, r, a) {
            var i = null,
                o = 100,
                u = {
                    animate: !!n,
                    duration: Te.isNumber(r) ? r / 1e3 : .25
                };
            if (Te.isFunction(a) && De.once("moveend", a), "selection" === e) {
                var s = [],
                    l = t.getItem(t.selection());
                if (l.length) {
                    for (var f, c = 0; c < l.length; c++) f = l[c], "node" === f.type && Ae(f) && !f.hi && s.push(L.latLng(f.pos.lat, f.pos.lng));
                    De.panInsideBounds(L.latLngBounds(s), u)
                }
            } else {
                switch (e) {
                    case "up":
                        i = [0, -o];
                        break;
                    case "down":
                        i = [0, o];
                        break;
                    case "left":
                        i = [-o, 0];
                        break;
                    case "right":
                        i = [o, 0]
                }
                i && De.panBy(i, u)
            }
        }

        function ee(e) {
            return function(t) {
                var n = t.originalEvent,
                    r = KeyLines.coords(n);
                return n.stopPropagation && n.stopPropagation(), n.shiftKey ? !0 : void e(n, r)
            }
        }

        function te(e) {
            Ve = !0, Ie(e)
        }

        function ne(e) {
            Ve ? Te.invoke(e) : (De.invalidateSize(!0), te(e)), Ve = !1
        }

        function re(e) {
            if (!_e.zoomAnimating) {
                _e.zoomAnimating = !0;
                var n = [];
                t.privateEach({
                    type: "node"
                }, function(r) {
                    if (Ae(r)) {
                        var a = De._latLngToNewLayerPoint(L.latLng(r.pos.lat, r.pos.lng), e.zoom, e.center),
                            i = De.layerPointToContainerPoint(a),
                            o = t.worldCoordinates(i.x, i.y);
                        n.push({
                            id: r.id,
                            x: o.x,
                            y: o.y
                        })
                    }
                }), t.animateProperties(n, {
                    time: 150,
                    queue: !1
                }, function() {
                    _e.zoomAnimating = !1, _e.zooming || Ie()
                })
            }
        }

        function ae() {
            var e = De.getZoom();
            e = Math.min(et, Math.max($e, e));
            var t = Qe + (qe - Qe) * (e - $e) / (et - $e);
            return t
        }

        function ie(e, t, n, r) {
            Te.isFunction(r) && De.once("moveend", r);
            var a = Math.round(e);
            De.setZoom(a, {
                animate: t
            })
        }

        function oe() {
            return De.getZoom()
        }

        function ue() {
            return !!De
        }

        function se() {
            return Oe
        }

        function le() {
            return De.getMaxZoom()
        }

        function fe(e, n, r, a) {
            var i = {
                animate: !!n,
                duration: Te.isNumber(r) ? r / 1e3 : .25
            };
            switch (Te.isFunction(a) && De.once("moveend", a), e) {
                case "in":
                    De.zoomIn(1, i);
                    break;
                case "out":
                    De.zoomOut(1, i);
                    break;
                case "fit":
                case "fitToModel":
                    D(M(), !!n);
                    break;
                case "selection":
                case "fitToSelection":
                    for (var o, u = [], s = t.getItem(t.selection()), l = 0; l < s.length; l++) o = s[l], Ae(o) && !o.hi && u.push(L.latLng(o.pos.lat, o.pos.lng));
                    u.length && D(u, !!n);
                    break;
                case "height":
                case "fitModelHeight":
                    var f = -(1 / 0),
                        c = -(1 / 0),
                        d = 1 / 0,
                        g = 1 / 0;
                    t.privateEach({
                        type: "node"
                    }, function(e) {
                        Ae(e) && !e.hi && (e.pos.lat > f && (f = e.pos.lat, c = e.pos.lng), e.pos.lat < d && (d = e.pos.lat, g = e.pos.lng))
                    }), isFinite(f) && isFinite(c) && isFinite(d) && isFinite(g) && D([
                        [f, c],
                        [d, g]
                    ], !!n)
            }
            return oe() / le()
        }

        function ce(e) {
            Ne = e;
            var t, n, r = e ? "disable" : "enable",
                a = ["dragging", "touchZoom", "doubleClickZoom", "scrollWheelZoom", "keyboard", "tap"];
            for (n = 0; n < a.length; n++) t = a[n], Ke[t] = !e, We && De[t] && De[t][r]()
        }

        function de(e) {
            De.dragging.enabled() !== !!e && (e ? De.dragging.enable() : De.dragging.disable())
        }

        function ge(e) {
            var n = e ? t.bind : t.unbind;
            n("dblclick", he), n("dragstart", me), n("keydown", ve)
        }

        function he() {
            return !0
        }

        function ve(e) {
            return e > 36 && 41 > e
        }

        function me(e) {
            return "move" === e
        }

        function xe(e, n) {
            var r = t.viewOptions(),
                a = De.getPixelBounds().min,
                i = De.getPixelOrigin();
            r.offsetX = i.x - a.x, r.offsetY = i.y - a.y;
            var o = ae();
            r.zoom !== o && (r.zoom = o);
            var u = {
                animate: e,
                time: e ? 100 : -1
            };
            t.viewOptions(r, u, n)
        }

        function ye(e) {
            var n = De.latLngToContainerPoint([e.pos.lat, e.pos.lng]);
            return t.worldCoordinates(n.x, n.y)
        }

        function pe() {
            var e = [];
            return t.privateEach({
                type: "node"
            }, function(t) {
                if (Ae(t)) {
                    var n = ye(t);
                    n.id = t.id, e.push(n)
                }
            }), e
        }

        function be(e) {
            var n, r, a, i;
            for (e = Te.ensureArray(e), r = 0; r < e.length; r++) n = e[r], n.hasOwnProperty("pos") && (a = {
                pos: Te.merge(t.getItem(n.id).pos, n.pos)
            }, i = ye(a), n.x = i.x, n.y = i.y)
        }

        function Ie(e) {
            xe(!1, function() {
                t.setProperties(pe(), !1, e)
            })
        }

        function we(e) {
            if (Xe.animate) {
                var n = t.viewOptions(),
                    r = ae();
                n.zoom === r ? t.animateProperties(pe(), {
                    time: Xe.time
                }, e) : t.viewOptions({
                    zoom: r
                }, {
                    animate: !1
                }, function() {
                    var a = pe();
                    t.viewOptions(n, {
                        animate: !1
                    }, function() {
                        function n() {
                            0 === --i && Te.invoke(e)
                        }
                        var i = 2;
                        t.viewOptions({
                            zoom: r
                        }, {
                            animate: !0,
                            time: Xe.time
                        }, n), t.animateProperties(a, {
                            animate: !0,
                            time: Xe.time
                        }, n)
                    })
                })
            } else Ie(e)
        }

        function Be(e) {
            if ("node" === e.type)
                if (ze && delete ze[e.id], delete Ee[e.id], Ae(e)) {
                    var t = e.x || 0,
                        n = e.y || 0;
                    ze && (ze[e.id] = {
                        id: e.id,
                        x: t,
                        y: n
                    });
                    var r = ye(e);
                    e.x = r.x, e.y = r.y
                } else e.hi || (e.hi = !0, Ee[e.id] = !0)
        }

        function Ce(e) {
            for (var n = [], r = 0; r < e.length; r++) {
                var a = e[r];
                if ("node" === a.type) {
                    var i = t.getItem(a.id);
                    if (i) {
                        var o = {};
                        Ee[a.id] ? o.hi = !1 : "hi" in i && (o.hi = i.hi), ze && ze[a.id] && (o.x = ze[a.id].x, o.y = ze[a.id].y), i.pos && (o.pos = i.pos), a = Te.merge(o, a)
                    }
                    Be(a)
                }
                n.push(a)
            }
            return n
        }

        function Ae(e) {
            return Te.isNormalObject(e.pos) && !Te.isNullOrUndefined(e.pos.lat) && !Te.isNullOrUndefined(e.pos.lng)
        }

        function Me() {
            var e = L.Control.extend({
                options: {
                    position: "topleft"
                },
                setOptions: O,
                initialize: V,
                onAdd: U,
                onRemove: K
            });
            return new e(Xe)
        }
        var De, Le, Ze, Re, Ge, Se, Te = KeyLines.Util,
            Ee = {},
            ke = {},
            Fe = {
                watermark: !1,
                overview: !1,
                gestures: !1
            },
            We = !1,
            Ne = !1,
            Oe = !1,
            Ve = !1,
            Ue = 5,
            Ke = {},
            Xe = {
                animate: !0,
                time: 800,
                tiles: {
                    url: "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png",
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
                    subdomains: "abcd",
                    minZoom: 0,
                    maxZoom: 20
                },
                transition: "restore"
            },
            He = Te.clone(Xe),
            Ye = "undefined" == typeof document,
            Pe = r + "-map";
        Ye || (Ge = document.getElementById(r), Se = Z(Ge));
        var ze, _e, je = "undefined" != typeof L && L.version,
            Je = {
                api: {
                    show: o(x),
                    hide: o(A),
                    viewCoordinates: o(s),
                    mapCoordinates: o(l),
                    isShown: function() {
                        return We
                    },
                    leafletMap: function() {
                        return De || null
                    },
                    options: o(m)
                },
                internalUse: {
                    appendMap: u,
                    convertPos: be,
                    loadUp: N,
                    setSize: f,
                    enableDragging: de,
                    lock: ce,
                    pan: $,
                    panBy: q,
                    zoom: fe,
                    setZoom: ie,
                    getMaxZoom: le,
                    getZoom: oe,
                    toDataURL: C,
                    isMapLoaded: ue,
                    preMerge: Ce,
                    preSetItem: Be,
                    isTransitioning: se
                }
            },
            Qe = .75,
            qe = 1.25,
            $e = 4,
            et = 15,
            tt = {
                animZoom: !0,
                scaleOnZoom: !0
            };
        return Je
    }
}(),
function() {
    var e = KeyLines.TimeBar.API = KeyLines.TimeBar.API || {};
    e.createAPI = function() {
        var e = KeyLines.Common,
            t = KeyLines.Util,
            n = {},
            r = KeyLines.Events.createEventBus(),
            a = KeyLines.TimeBar.Config.create(),
            i = KeyLines.TimeBar.Model.createModel(r, a),
            o = KeyLines.TimeBar.Generator.create(),
            u = KeyLines.TimeBar.Controller.createController(r, i, o, a);
        n.clear = function() {
            return i.load({
                items: []
            })
        }, n.getIds = function(e, t) {
            return i.getIds(e, t)
        }, n.load = function(e, t) {
            return i.load(e, t)
        }, n.options = function(e, t) {
            return u.options(e, t)
        }, n.pan = function(e, t, n) {
            return u.pan(e, t, n)
        }, n.pause = function() {
            return u.pause()
        }, n.play = function(e) {
            return u.play(e)
        }, n.range = function(e, t, n, r) {
            return 0 !== arguments.length ? u.range(e, t, n, r) : u.range()
        }, n.merge = function(e, n) {
            return i.merge(t.getItemsArray(e), n)
        }, n.selection = function(e) {
            return i.selection(e)
        }, n.zoom = function(e, n, r) {
            return e = e || "in", n = t.defaults(n, {
                animate: !0,
                time: 200,
                pad: 0
            }), u.zoom(e, n, r)
        }, n.privateInit = u.privateInit, n.privateSetSize = u.privateSetSize, n.getIdcounts = i.getIdcounts, n.privateAPIDefinitions = function() {
            return {
                getIdcounts: !0
            }
        }, e.appendActions(n, u), t.merge(n, r);
        var s = KeyLines.Common.frameManager(u.draw, u.animate);
        return t.merge(n, s), n.bind("redraw", s.redraw), n.getItems = i.getItems, n
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.Generator = function(t) {
        function n(e) {
            y.isNullOrUndefined(e.fontFamily) && (e.fontFamily = "sans-serif"), B === e.fontFamily && null !== B || (B = e.fontFamily, M.textAtlas = !0), A[B] = !0, w[B] = !0
        }

        function r(e) {
            e.iconFontFamily && C !== e.iconFontFamily && (C = e.iconFontFamily, M.fontIcons = !0)
        }

        function a(e, t, n) {
            e.fbAlpha = n, e.rebuildOptions = {
                all: t.all,
                colours: t.colours,
                textures: t.textures,
                shadow: t.shadow,
                positions: t.positions
            }
        }

        function i(e, t) {
            return e.all || e.textures || e.positions || e.shadow || e.colours || t
        }

        function o(e) {
            e.arcBuffers.numOfTriangles = 0, e.lineBuffers.numOfTriangles = 0, e.triangleBuffers.numOfTriangles = 0
        }

        function u(e, t, n) {
            for (var r, a = t.rebuildOptions, i = t.alwaysUpdate, o = 0; L > o; o++) r = e[o], V[r.p](t, r, n, i, a)
        }

        function s() {
            M.textAtlas && (t.textAtlas = e.TextAtlas(A), w = A)
        }

        function l() {
            !M.fontIcons && t.fontIconAtlas || (t.fontIconAtlas = e.FontIconAtlas(I, C))
        }

        function f(e, t, n) {
            function r(e) {
                "_overviewIcon" !== e.id && "_overviewIconRect" !== e.id && (n[L++] = e, v(e, a, i, o))
            }
            if (t.widgets) {
                L = 0;
                var a = e.lineBuffers,
                    i = e.arcBuffers,
                    o = e.triangleBuffers;
                t.widgets.each(r, t.channels, t.layers)
            }
        }

        function c(e) {
            d(e.lineBuffers, e), g(e), h(e)
        }

        function d(e, t) {
            var n = 8;
            e.elementIndexIndex = 0, e.elementMappingIndex = 0, e.elementDataIndex = 0, e.elementIntegerDataIndex = 0, (!e.elementData || e.elementData.length < e.numOfTriangles * n * 4) && (e.elementData = new Float32Array(e.numOfTriangles * n * 4), e.elementIntegerData = new Uint8Array(e.elementData.buffer, 0, e.elementData.length * Float32Array.BYTES_PER_ELEMENT)), (!e.elementIndex || e.elementIndex.length < 3 * e.numOfTriangles) && (e.elementIndex = new Uint32Array(3 * e.numOfTriangles), t.rebuildOptions.all = !0)
        }

        function g(e) {
            function t(e) {
                (!e.arcBuffers.positionData || e.arcBuffers.positionData.length < e.arcBuffers.totalNumOfVerticies * i) && (e.rebuildOptions.positions = !0, e.arcBuffers.positionData = new Float32Array(e.arcBuffers.totalNumOfVerticies * i))
            }

            function n(e) {
                (!e.arcBuffers.colourData || e.arcBuffers.colourData.length < e.arcBuffers.totalNumOfVerticies * u) && (e.rebuildOptions.colours = !0, e.arcBuffers.colourData = new Uint8Array(e.arcBuffers.totalNumOfVerticies * u))
            }

            function r(e) {
                (!e.arcBuffers.shadowData || e.arcBuffers.shadowData.length < e.arcBuffers.totalNumOfVerticies * o) && (e.rebuildOptions.shadow = !0, e.arcBuffers.shadowData = new Uint8Array(e.arcBuffers.totalNumOfVerticies * o))
            }

            function a(e) {
                (!e.arcBuffers.coreData || e.arcBuffers.coreData.length < e.arcBuffers.totalNumOfVerticies * s) && (e.rebuildOptions.all = !0, e.arcBuffers.coreData = new Float32Array(e.arcBuffers.totalNumOfVerticies * s))
            }
            var i = 6,
                o = 4,
                u = 4,
                s = 3;
            e.arcBuffers.positionIndex = 0, e.arcBuffers.colourIndex = 0, e.arcBuffers.shadowIndex = 0, e.arcBuffers.coreIndex = 0, e.arcBuffers.totalNumOfVerticies = 3 * e.arcBuffers.numOfTriangles, t(e), n(e), r(e), a(e)
        }

        function h(e) {
            function t(e) {
                (!e.triangleBuffers.positionData || e.triangleBuffers.positionData.length < e.triangleBuffers.totalNumOfVerticies * o) && (e.triangleBuffers.positionData = new Float32Array(e.triangleBuffers.totalNumOfVerticies * o), e.rebuildOptions.positions = !0)
            }

            function n(e) {
                (!e.triangleBuffers.colourData || e.triangleBuffers.colourData.length < e.triangleBuffers.totalNumOfVerticies * l) && (e.triangleBuffers.colourData = new Uint8Array(e.triangleBuffers.totalNumOfVerticies * l), e.rebuildOptions.colours = !0)
            }

            function r(e) {
                (!e.triangleBuffers.textureData || e.triangleBuffers.textureData.length < e.triangleBuffers.totalNumOfVerticies * s) && (e.triangleBuffers.textureData = new Float32Array(e.triangleBuffers.totalNumOfVerticies * s), e.rebuildOptions.textures = !0)
            }

            function a(e) {
                (!e.triangleBuffers.shadowData || e.triangleBuffers.shadowData.length < e.triangleBuffers.totalNumOfVerticies * u) && (e.triangleBuffers.shadowData = new Uint8Array(e.triangleBuffers.totalNumOfVerticies * u), e.rebuildOptions.shadow = !0)
            }
            var i = 3,
                o = 3,
                u = 4,
                s = 7,
                l = 4;
            e.triangleBuffers.positionIndex = 0, e.triangleBuffers.colourIndex = 0, e.triangleBuffers.textureIndex = 0, e.triangleBuffers.shadowIndex = 0, e.triangleBuffers.totalNumOfVerticies = e.triangleBuffers.numOfTriangles * i, t(e), n(e), r(e), a(e)
        }

        function v(e, t, n, r) {
            switch (e.p) {
                case "A":
                    n.numOfTriangles += 48;
                    break;
                case "L":
                    t.numOfTriangles += 2;
                    break;
                case "L3":
                    t.numOfTriangles += 6;
                    break;
                case "C":
                    if (e.bw > 0) {
                        var a = b[e.bs] || 0;
                        a > 1 ? n.numOfTriangles += 48 : r.numOfTriangles += 2
                    }
                    e.fi && (r.numOfTriangles += 2);
                    break;
                case "R":
                    if (e.fi && (r.numOfTriangles += 2), e.bw > 0) {
                        var i = b[e.bs] || 0;
                        i ? t.numOfTriangles += 8 : r.numOfTriangles += 2
                    }
                    break;
                case "RR":
                    var o = e.y2 - e.y1 > 2 * e.r,
                        u = e.x2 - e.x1 > 2 * e.r;
                    e.fi && (u && (r.numOfTriangles += 2), o ? r.numOfTriangles += 12 : r.numOfTriangles += 4), e.bw > 0 && (u && (r.numOfTriangles += 4), o ? r.numOfTriangles += 12 : r.numOfTriangles += 4);
                    break;
                case "T":
                    if (e.isfi) {
                        r.numOfTriangles += 2;
                        var s = I[e.t];
                        s || (M.fontIcons = !0, I[e.t] = {
                            t: e.t
                        })
                    } else e.ff && (A[e.ff] = !0, w[e.ff] || (M.textAtlas = !0, w[e.ff] = !0)), e.back && (r.numOfTriangles += 2), r.numOfTriangles += 2 * e.t.length;
                    break;
                case "PE":
                    e.bw > 0 && (t.numOfTriangles += 10), e.fi && (r.numOfTriangles += 3);
                    break;
                case "TR":
                    e.fi && r.numOfTriangles++, e.bw > 0 && r.numOfTriangles++;
                    break;
                case "I":
                    r.numOfTriangles += 2
            }
        }

        function m(e, n) {
            var r = t.allLayers[n.id];
            return r || (r = t.allLayers[n.id] = x(n)), r
        }

        function x(e) {
            return {
                triangleBuffers: {
                    numOfTriangles: 0
                },
                lineBuffers: {
                    numOfTriangles: 0
                },
                arcBuffers: {
                    numOfTriangles: 0
                },
                id: e.id,
                useFramebuffer: e.useFramebuffer,
                drawFramebuffer: e.drawFramebuffer,
                framebuffer: null,
                renderbuffer: null,
                framebufferTexture: null,
                fbAlpha: e.fbAlpha,
                useView: e.useView,
                shadowType: e.shadowType,
                channels: e.channels,
                rebuildOptions: {},
                alwaysUpdate: e.alwaysUpdate
            }
        }
        var y = KeyLines.Util,
            p = {},
            b = {
                dashed: 2,
                dotted: 3
            },
            I = {},
            w = {},
            B = null,
            C = null,
            A = {},
            M = {
                textAtlas: !1,
                fontIcons: !1
            },
            D = [],
            L = 0,
            Z = !1;
        p.generate = function(e, d, g, h) {
            t.layersToDraw.length = 0, M.textAtlas = !1, M.fontIcons = !1, Z = KeyLines.Rendering.useHiResImages();
            for (var v = 0, x = e.length; x > v; v++) {
                var y = e[v];
                L = 0;
                var b = m(t.allLayers, y);
                t.layersToDraw[v] = b, a(b, g, y.fbAlpha), i(g, y.alwaysUpdate) && (n(h), r(h), o(b), f(b, y, D), M.textAtlas || M.fontIcons || (c(b), u(D, b, d)))
            }
            s(), l(), (M.textAtlas || M.fontIcons) && (M.fontIcons = !1, M.textAtlas = !1, p.generate(e, d, {
                all: !0
            }, h)), A = {}
        };
        var R = function(t, n, r, a, i) {
                if (n.fi && (e.generateTriangle(n, n.x1, n.y1, n.x2, n.y2, n.x5, n.y5, n.fi ? n.wfc : n.wbc, -1, t, n.a), e.generateTriangle(n, n.x5, n.y5, n.x2, n.y2, n.x4, n.y4, n.fi ? n.wfc : n.wbc, -1, t, n.a), e.generateTriangle(n, n.x2, n.y2, n.x3, n.y3, n.x4, n.y4, n.fi ? n.wfc : n.wbc, -1, t, n.a)), n.bw > 0) {
                    var o = b[n.bs] || 0;
                    n.x1 < n.x2 ? (e.generateLine(n, n.x1, n.y1, n.x2, n.y2, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x2, n.y2, n.x3, n.y3, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x3, n.y3, n.x4 - n.bw / 2, n.y4, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x4, n.y4 - n.bw / 2, n.x5, n.y5 + n.bw / 2, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x5 - n.bw / 2, n.y5, n.x1, n.y1, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a)) : (e.generateLine(n, n.x1, n.y1, n.x2, n.y2, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x2, n.y2, n.x3, n.y3, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x3, n.y3, n.x4 + n.bw / 2, n.y4, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x4, n.y4 - n.bw / 2, n.x5, n.y5 + n.bw / 2, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x5 + n.bw / 2, n.y5, n.x1, n.y1, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a))
                }
            },
            G = function(n, r, a, i, o) {
                r.isfi ? e.generateFontIcon(r, I[r.t], n, r.a) : (r.back && e.generateRectangle(r, r.x1, r.y1, r.x2, r.y2, r.wbc, n, r.a), e.generateText(n.triangleBuffers, r, t.textAtlas.getGlyphsFrom(r.t, r.ff, B, r.bo, M), t.textAtlas.fontSize, n, r.a))
            },
            S = function(t, n, r, a, i) {
                var o = b[n.ls] || 0;
                e.generateArc(t.arcBuffers, n.x, n.y, n.r, n.sa, n.ea, n.wbc, n.wsc, n.bw, o, i, a, n.a)
            },
            T = function(t, n, r, a, i) {
                var o = b[n.ls] || 0;
                e.generateLine(n, n.x1, n.y1, n.x2, n.y2, n.wc, n.wsc, o, n.w, t.lineBuffers, a, i, n.a)
            },
            E = function(t, n, r, a, i) {
                var o = b[n.ls] || 0;
                e.generateLine(n, n.x1, n.y1, n.x2, n.y2, n.wc, n.wsc, o, n.w, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x2, n.y2, n.x3, n.y3, n.wc, n.wsc, o, n.w, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x3, n.y3, n.x4, n.y4, n.wc, n.wsc, o, n.w, t.lineBuffers, a, i, n.a)
            },
            k = function(t, n, r, a, i) {
                n.fi && e.generateTriangle(n, n.x1, n.y1, n.x2, n.y2, n.x3, n.y3, n.wfc, -2, t, n.a), n.bw > 0 && e.generateTriangle(n, n.x1, n.y1, n.x2, n.y2, n.x3, n.y3, n.wbc, -2, t, n.a)
            },
            F = function(t, n, r, a, i) {
                if (n.fi && e.generateRectangle(n, n.x1, n.y1, n.x2, n.y2, n.wfc, t, n.a), n.bw > 0) {
                    var o = b[n.bs] || 0,
                        u = Math.ceil(n.bw / 2);
                    o ? (e.generateLine(n, n.x1 - u, n.y1, n.x2 + u, n.y1, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x2, n.y1 - u, n.x2, n.y2 + u, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x2 + u, n.y2, n.x1 - u, n.y2, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a), e.generateLine(n, n.x1, n.y2 + u, n.x1, n.y1 - u, n.wbc, n.wsc, o, n.bw, t.lineBuffers, a, i, n.a)) : e.generateRectangle(n, n.x1 - u, n.y1 - u, n.x2 + u, n.y2 + u, n.wbc, t, n.a, n.bw, o)
                }
            },
            W = function(t, n, r, a, i) {
                var o = n.r,
                    u = n.bw,
                    s = n.bw / 2,
                    l = n.x1,
                    f = n.y1,
                    c = n.x2,
                    d = n.y2,
                    g = d - f > 2 * o,
                    h = c - l > 2 * o,
                    v = f + (d - f) / 2;
                if (n.fi)
                    if (h && e.generateRectangle(n, l + o, f, c - o, d, n.wfc, t, n.a), g) e.generateRectangle(n, l, f + o, l + o, d - o, n.wfc, t, n.a), e.generateRectangle(n, c - o, f + o, c, d - o, n.wfc, t, n.a), e.generateCircle(l, f, o, n.wfc, n.wsc, 0, 1, 1, t, n.a), e.generateCircle(c, f, o, n.wfc, n.wsc, 0, 0, 1, t, n.a), e.generateCircle(l, d, o, n.wfc, n.wsc, 0, 1, 0, t, n.a), e.generateCircle(c, d, o, n.wfc, n.wsc, 0, 0, 0, t, n.a);
                    else {
                        var v = f + (d - f) / 2;
                        e.generateCircle(l, v, o, n.wfc, n.wsc, 0, 1, .5, t, n.a), e.generateCircle(c, v, o, n.wfc, n.wsc, 0, 0, .5, t, n.a)
                    }
                if (u > 0) {
                    b[n.bs] || 0;
                    h && (e.generateRectangle(n, l + o - s, f - s, c - o + s, f + s, n.wbc, t, n.a), e.generateRectangle(n, l + o - s, d - s, c - o + s, d + s, n.wbc, t, n.a)), g ? (e.generateRectangle(n, c + s, f + o - s, c - s, d - o + s, n.wbc, t, n.a), e.generateRectangle(n, l - s, f + o - s, l + s, d - o + s, n.wbc, t, n.a), e.generateCircle(l - s, f - s, o + s, n.wbc, n.wsc, u, 1, 1, t, n.a), e.generateCircle(c + s, f - s, o + s, n.wbc, n.wsc, u, 0, 1, t, n.a), e.generateCircle(c + s, d + s, o + s, n.wbc, n.wsc, u, 0, 0, t, n.a), e.generateCircle(l - s, d + s, o + s, n.wbc, n.wsc, u, 1, 0, t, n.a)) : (e.generateCircle(l - s, v, o + s, n.wbc, n.wsc, u, 1, .5, t, n.a), e.generateCircle(c + s, v, o + s, n.wbc, n.wsc, u, 0, .5, t, n.a))
                }
            },
            N = function(t, n, r, a, i) {
                if (n.fi && e.generateCircle(n.x, n.y, n.r, n.wfc, n.wsc, 0, .5, .5, t, n.a), n.bw > 0) {
                    var o = b[n.bs] || 0;
                    o > 1 ? e.generateArc(t.arcBuffers, n.x, n.y, n.r, 0, 6.28318530718, n.wbc, n.wsc, n.bw, o, i, a, n.a) : e.generateCircle(n.x, n.y, n.r + n.bw / 2, n.wbc, n.wsc, n.bw, .5, .5, t, n.a)
                }
            },
            O = function(t, n, r, a, i) {
                "_background" === n.id && (n.wsc = [0, 0, 0, 0]);
                var o = r[n.u];
                if (n.hi && Z) {
                    var u = r[KeyLines.Rendering.hiResUrl(n.u)];
                    u && !u.missing && (o = u)
                }
                e.generateImage(n, "co" === n.s ? o.co : o.im, t, n.a)
            },
            V = {
                A: S,
                L: T,
                L3: E,
                I: O,
                TR: k,
                R: F,
                RR: W,
                C: N,
                T: G,
                PE: R
            };
        return p
    })
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 24,
            t = 6,
            n = function(n, r, a, i, o, u) {
                for (var s = 0; e > s; s++)
                    for (var l = 0; t > l; l++) n[r.shadowIndex++] = a, n[r.shadowIndex++] = i, n[r.shadowIndex++] = o, n[r.shadowIndex++] = u
            },
            r = function(t, n, r) {
                for (var a = 0; e > a; a++) t[n.coreIndex++] = r, t[n.coreIndex++] = a, t[n.coreIndex++] = e, t[n.coreIndex++] = r, t[n.coreIndex++] = -(a + 1), t[n.coreIndex++] = e, t[n.coreIndex++] = r, t[n.coreIndex++] = a, t[n.coreIndex++] = -e, t[n.coreIndex++] = r, t[n.coreIndex++] = a, t[n.coreIndex++] = -e, t[n.coreIndex++] = r, t[n.coreIndex++] = -(a + 1), t[n.coreIndex++] = e, t[n.coreIndex++] = r, t[n.coreIndex++] = -(a + 1), t[n.coreIndex++] = -e
            },
            a = function(n, r, a, i, o, u, s, l, f) {
                var c = a,
                    d = i;
                if (f) var c = a + f.x,
                    d = i + f.y;
                for (var g = 0; e > g; g++)
                    for (var h = 0; t > h; h++) n[r.positionIndex++] = c, n[r.positionIndex++] = d, n[r.positionIndex++] = o, n[r.positionIndex++] = u, n[r.positionIndex++] = s, n[r.positionIndex++] = l
            },
            i = function(n, r, a, i, o, u) {
                for (var s = 0; e > s; s++)
                    for (var l = 0; t > l; l++) n[r.colourIndex++] = a, n[r.colourIndex++] = i, n[r.colourIndex++] = o, n[r.colourIndex++] = u
            };
        KeyLines.WebGL.generateArc = function(e, t, o, u, s, l, f, c, d, g, h, v, m) {
            c = c || [0, 0, 0, 0], (h.shadow || v || h.all) && n(e.shadowData, e, c[0], c[1], c[2], c[3]), (h.positions || v || h.all) && a(e.positionData, e, t, o, u, s, l, d, m), (h.colours || v || h.all) && i(e.colourData, e, f[0], f[1], f[2], f[3]), (v || h.all) && r(e.coreData, e, g)
        }
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        var t = 2048,
            n = 244;
        e.FontIconAtlas = function(e, a, i) {
            var o = Object.keys(e),
                u = document.createElement("canvas");
            if (u.id = "webglFontIconDebugCanvas", u.width = 1, u.height = 1, o.length) {
                var s = r(o.length, t),
                    l = u.getContext("2d");
                u.width = s, u.height = s;
                var f = document.getElementById("webglFontIconDebugCanvas");
                i && (f ? f.parentNode.replaceChild(u, f) : document.getElementsByTagName("body")[0].appendChild(u));
                var c = 4,
                    d = 2 * c + n / 2,
                    g = c,
                    h = 1 / s;
                l.fillStyle = "rgb(0,0,0)", l.textAlign = "center";
                var v = 0,
                    m = ["rgba(255,0,0,1)", "rgba(0,255,0,1)", "rgba(0,0,255,1)", "rgba(0,0,0,0)"];
                l.fillStyle = "black", l.fillRect(0, 0, s, s), l.fillStyle = m[0], l.globalCompositeOperation = "lighter";
                for (var x = 0, y = o.length; y > x; x++) {
                    var p = e[o[x]];
                    if (d + n > s + 128 && (d = c + n / 2, g += c + n), g + n >= s) {
                        if (d = c + n / 2, g = 0, v++, v > 2) throw new Error("Maximum number of icons exceeded!");
                        l.fillStyle = m[v]
                    }
                    p.colourComponentTexture = v, p.u1 = (d - 128) * h, p.v1 = g * h, p.u2 = (d - 128 + n) * h, p.v2 = (g + n) * h, l.textBaseline = "top", l.font = n + "px " + a;
                    var b = l.measureText(o[x]).width,
                        I = n * (n / b) - 2 * c;
                    if (n > I) {
                        var w = n - I;
                        l.font = I + "px " + a, l.fillText(o[x], d - c, g + w / 4)
                    } else l.fillText(o[x], d - c, g, 236);
                    d += 2 * c + n
                }
            }
            return {
                img: u,
                texture: null
            }
        };
        var r = function(e, t) {
            for (var n = 512, r = 12; t > n;) {
                if (r >= e) return n;
                n *= 2, r *= 4
            }
            return t
        }
    }
}(),
function() {
    "use strict";
    var e = KeyLines.Generator = {};
    e.create = function(e) {
        function t() {
            return ue || ve.colours.white
        }

        function n(e) {
            return Math.abs(e) < 1e-5
        }

        function r(e, t, n, r, a, i, o) {
            return {
                x: ((i - r) * n - (o - a) * t) / e,
                y: ((i - r) * t + (o - a) * n) / e
            }
        }

        function a(e) {
            return e.hasOwnProperty("t") && e.t
        }

        function i(e, t, a, i, o, u, s, l, f, c) {
            var d = r(t, a, i, o, u, s, l),
                g = r(t, a, i, o, u, f, c);
            e /= t;
            var h = e / 2;
            if (d.y > h && g.y > h || d.y < -h && g.y < -h) return NaN;
            if (n(g.y - d.y)) return Math.max(d.x, g.x);
            var v, m = d.y > g.y ? d : g,
                x = d.y > g.y ? g : d;
            v = h < m.y ? m.x - (m.y - h) * (m.x - x.x) / (m.y - x.y) : m.x;
            var y;
            return y = x.y < -h ? x.x - (x.y - -h) * (x.x - m.x) / (x.y - m.y) : x.x, Math.max(v, y)
        }

        function o(e, t, n, r, a, o, u, s) {
            for (var l = -(1 / 0), f = u.length, c = 0; f > c; c += 2) {
                var d = (c + 2) % f,
                    g = i(e, t, n, r, a, o, u[c] + s.x, u[c + 1] + s.y, u[d] + s.x, u[d + 1] + s.y);
                isNaN(g) || (l = Math.max(l, g))
            }
            return l
        }

        function u(e, t, n, r, a, i, u, s) {
            return o(e, t, n, r, a, i, [u.x1, u.y1, u.x2, u.y1, u.x2, u.y2, u.x1, u.y2], s)
        }

        function s(e, t, n, r, a, i, o, u) {
            var s = -(1 / 0),
                l = o.cx + u.x - a,
                f = o.cy + u.y - i,
                c = t * t,
                d = -2 * t * (l * r - f * n),
                g = l * l + f * f - o.r * o.r,
                h = d * d - 4 * c * g;
            return h >= 0 && (s = (-d + Math.sqrt(h)) / (2 * c)), s
        }

        function l(e, t, n, r, a, i) {
            for (var l, f = -(1 / 0), c = Math.sin(-n), d = Math.cos(-n), g = i._cl, h = 0; h < g.length; h++) {
                var v = g[h];
                l = v.length ? o(e, t, c, d, r, a, v, i) : v.r ? s(e, t, c, d, r, a, v, i) : u(e, t, c, d, r, a, v, i), f = Math.max(f, l)
            }
            return f
        }

        function f(e, t, n, r, a, i) {
            var o = 0;
            return r._c && (o = r._c / t), r._cl && (o = Math.max(o, l(e, t, n, a, i, r))), o
        }

        function c(e, t, r, a, i, o, u, s, l, c, d) {
            var g = {},
                h = r - e,
                v = a - t,
                m = Math.sqrt(h * h + v * v);
            if (n(m)) g.skip = !0;
            else {
                var x = Math.atan2(v, h),
                    y = f(ie && s ? 0 : i, m, x, o, e, t),
                    p = f(ie && l ? 0 : i, m, x + Math.PI, u, r, a);
                c = c || 0, d = d || 0, y = Math.max(c, y), p = Math.max(d, p);
                var b = 1 - (y + p),
                    I = 0;
                if (s || l) {
                    var w = R(i) * Math.cos(Te);
                    I = w / m, s && l && (I = 1.5 * I)
                }
                g.skip = b < Math.max(.1, I), g.skip || (g.cx1 = e + y * h, g.cy1 = t + y * v, g.cx2 = r + p * -h, g.cy2 = a + p * -v)
            }
            return g
        }

        function d(e, t, n, r, a, i, o) {
            var u = [],
                s = i - r,
                l = o - a,
                f = r - e,
                c = a - t,
                d = s * s + l * l,
                g = 2 * (s * f + l * c),
                h = f * f + c * c - n * n,
                v = g * g - 4 * d * h;
            if (v >= 0) {
                var m = Math.sqrt(v),
                    x = (-g - m) / (2 * d),
                    y = (-g + m) / (2 * d);
                if (x >= 0 && 1 >= x) {
                    var p = r + x * s,
                        b = a + x * l;
                    u.push(Math.atan2(b - t, p - e))
                }
                if (y !== x && y >= 0 && 1 >= y) {
                    var I = r + y * s,
                        w = a + y * l;
                    u.push(Math.atan2(w - t, I - e))
                }
            }
            return u
        }

        function g(e, t, n, r, a, i) {
            var o = r - t;
            if (Math.abs(o) > n) return [];
            var u = Math.max(a, i),
                s = Math.min(a, i);
            if (e - n > u || s > e + n) return [];
            var l = Math.asin(o / n),
                f = n * Math.cos(l),
                c = [];
            return e - f >= s && u >= e - f && c.push(o > 0 ? Math.PI - l : -Math.PI - l), e + f >= s && u >= e + f && c.push(l), c
        }

        function h(e, t, n, r, a, i) {
            var o = r - e;
            if (Math.abs(o) > n) return [];
            var u = Math.max(a, i),
                s = Math.min(a, i);
            if (t - n > u || s > t + n) return [];
            var l = Math.acos(o / n),
                f = n * Math.sin(l),
                c = [];
            return t - f >= s && u >= t - f && c.push(-l), t + f >= s && u >= t + f && c.push(l), c
        }

        function v(e, t, n, r, a, i) {
            for (var o = [], u = 0; u < r.length; u += 2) {
                var s = (u + 2) % r.length,
                    l = d(e, t, n, r[u] + a, r[u + 1] + i, r[s] + a, r[s + 1] + i);
                o = o.concat(l)
            }
            return o
        }

        function m(e, t, n, r, a, i) {
            var o = g(e, t, n, r.y2 + i, r.x1 + a, r.x2 + a),
                u = g(e, t, n, r.y1 + i, r.x1 + a, r.x2 + a),
                s = h(e, t, n, r.x1 + a, r.y1 + i, r.y2 + i),
                l = h(e, t, n, r.x2 + a, r.y1 + i, r.y2 + i);
            return o.concat(u, l, s)
        }

        function x(e, t, n, r, a, i, o) {
            var u = Math.PI,
                s = o ? r : a;
            if (i._c && (s += (o ? 1 : -1) * i._c / n), i._cl && i._cl.length > 0)
                for (var l = 0; l < i._cl.length; l++) {
                    var f, c = i._cl[l],
                        d = i.x,
                        g = i.y;
                    f = c.length ? v(e, t, n, c, d, g) : c.r ? Le(e, t, n, c, d, g) : m(e, t, n, c, d, g);
                    for (var h = 0; h < f.length; h++) f[h] + 2 * Math.PI < a && (f[h] += 2 * Math.PI), r < f[h] && f[h] < a && (o ? f[h] - s < u && (s = Math.max(s, f[h])) : s - f[h] < u && (s = Math.min(s, f[h])))
                }
            return s
        }

        function y(e, t, n, r, a, i, o, u) {
            var s = x(t, n, r, a, i, e > 0 ? o : u, !0),
                l = x(t, n, r, s, i, e > 0 ? u : o, !1);
            return {
                startangle: s,
                endangle: l
            }
        }

        function p(e, t) {
            var n = t(e.id),
                r = De(e),
                a = K("b", e, n),
                i = K("c", e, n);
            return !(!a && !i || r)
        }

        function b(e, t, n, r, a, i, o, u, s, l, f, c, d, g, h, v, m) {
            var x = (l + c) / 2,
                b = (f + d) / 2,
                I = c - l,
                w = d - f;
            i = Number(i);
            var B = i > 0,
                C = Math.max(.001, w * w + I * I),
                A = Math.sqrt(C),
                M = (C / 4 + i * i) / (2 * Math.abs(i)),
                D = M - Math.abs(i),
                L = B ? 1 : -1,
                Z = x - w * L * D / A,
                R = b + I * L * D / A,
                S = Math.atan2(f - R, l - Z),
                T = Math.atan2(d - R, c - Z),
                E = B ? S : T,
                k = B ? T : S;
            E > k && (k += 2 * Math.PI), r = r || 0, a = a || 0;
            var F = E + (B ? r : a) * (k - E),
                W = E + (B ? 1 - a : 1 - r) * (k - E),
                N = y(i, Z, R, M, E, k, u, s);
            if (N.startangle = Math.max(F, N.startangle), N.endangle = Math.min(W, N.endangle), !(N.startangle >= N.endangle)) {
                var O = (N.startangle + N.endangle) / 2,
                    V = {};
                V.lx = Z + M * Math.cos(O), V.ly = R + M * Math.sin(O);
                var U, K, X, H, Y, P, z = 1.5 * h / M,
                    _ = -.25;
                return t ? (U = B ? N.startangle : N.endangle, K = Z + M * Math.cos(U), X = R + M * Math.sin(U), B ? N.startangle += z : N.endangle -= z) : ie && p(u, m) && (B ? N.startangle += _ * z : N.endangle -= _ * z), n ? (H = B ? N.endangle : N.startangle, Y = Z + M * Math.cos(H), P = R + M * Math.sin(H), B ? N.endangle -= z : N.startangle += z) : ie && p(s, m) && (B ? N.endangle -= _ * z : N.startangle += _ * z), N.startangle < N.endangle && (M > 1e5 ? e.line(l, f, c, d, g, h, o, v) : e.arc(Z, R, M, N.startangle, N.endangle, g, h, o, v), t && G(e, K, X, h, +L * z * .66 + U - L * Math.PI / 2, g, o + pe + "a1"), n && G(e, Y, P, h, -L * z * .66 + H + Math.PI - L * Math.PI / 2, g, o + pe + "a2")), V
            }
        }

        function I(e, t, n, r) {
            return Math.sqrt((n - e) * (n - e) + (r - t) * (r - t))
        }

        function w(e, t) {
            var n, r = Fe,
                a = (e.e || 1) * Pe;
            return n = t.x > e.x && "e" === e.sh ? e.x + a * r : t.x < e.x && "w" === e.sh ? e.x - a * r : e.x
        }

        function B(e, t, n, r, a, i, o, u, s, l, c, d, g, h) {
            var v, m, x = w(u, s),
                y = w(s, u),
                p = i || 0;
            y > x ? (v = x + se, m = y - se, v > m && (v = m = (x + y) / 2)) : (v = x - se, m = y + se, m > v && (v = m = (x + y) / 2));
            var b = I(x, l, v, p),
                B = I(v, p, m, p),
                C = I(m, p, y, c),
                A = b + B + C,
                M = Math.atan2(p - l, v - x),
                D = Math.atan2(p - c, m - y),
                L = 0 === b ? 0 : f(g, b, M, u, x, l),
                Z = 0 === C ? 0 : f(g, C, D, s, y, c);
            r = 0 === b ? 0 : (r || 0) * A / b, a = 0 === C ? 0 : (a || 0) * A / C, L = Math.min(1, Math.max(r, L)), Z = Math.min(1, Math.max(a, Z));
            var R, S, T, E, k, F, W, N, O = x + L * (v - x),
                V = l + L * (p - l),
                U = y + Z * (m - y),
                K = c + Z * (p - c);
            t && (k = O, F = V, R = v - O, S = p - V, T = Math.max(.001, S * S + R * R), E = Math.sqrt(T), O += 1.5 * g * R / E, V += 1.5 * g * S / E), n && (W = U, N = K, R = m - U, S = p - K, T = Math.max(.001, S * S + R * R), E = Math.sqrt(T), U += 1.5 * g * R / E, K += 1.5 * g * S / E), e.line3(O, V, v, p, m, p, U, K, d, g, o, h), t && G(e, k, F, g, M + Math.PI, d, o + pe + "a1"), n && G(e, W, N, g, D + Math.PI, d, o + pe + "a2");
            var X = {
                above: !0,
                lx: (v + m) / 2,
                ly: (p + p) / 2
            };
            return X
        }

        function C(e, t, n, r, a, i, o, u, s, l, f, d, g, h, v, m) {
            var x = c(s, l, f, d, h, o, u, t, n, r, a);
            if (!x.skip) {
                var y, b, I, w, B, C, A = x.cx1,
                    M = x.cy1,
                    D = x.cx2,
                    L = x.cy2,
                    Z = D - A,
                    R = L - M,
                    S = Math.max(.001, R * R + Z * Z),
                    T = Math.sqrt(S),
                    E = .25;
                t ? (y = Math.atan2(R, Z), I = A, w = M, A += 1.5 * h * Z / T, M += 1.5 * h * R / T) : ie && p(o, m) && (A -= E * h * Z / T, M -= E * h * R / T), n ? (b = Math.atan2(R, Z), B = D, C = L, D -= 1.5 * h * Z / T, L -= 1.5 * h * R / T) : ie && p(u, m) && (D += E * h * Z / T, L += E * h * R / T), e.line(A, M, D, L, g, h, i, v), t && G(e, I, w, h, y + Math.PI, g, i + pe + "a1"), n && G(e, B, C, h, b, g, i + pe + "a2");
                var k = {};
                return k.lx = (A + D) / 2, k.ly = (M + L) / 2, k
            }
        }

        function A(e, t, n, r, a, i, o, u, s, l, f) {
            function c(e) {
                for (var t = n.length - 1, a = r; t >= 0; t--) e(n[t], a), t > 0 && (a -= Z(n[t - 1], n[t]) * i)
            }
            if (o) {
                for (var d = 0, g = 0; g < n.length - 1; g++) d += Z(n[g], n[g + 1]) * i;
                r += d / 2
            } else r -= L(n[n.length - 1]) * i;
            o && u && c(function(t, n) {
                e.circle(n, a, D(t) * i * 1.25, ve.colours.white, -1, Ae(), !0, l)
            }), c(function(n, r) {
                Q(e, t, l + pe + g, r, a, i, n, s, f)
            })
        }

        function M(e, n, r, i, o, u, s) {
            function l(e) {
                return d && de && ce.hasOwnProperty(e) ? ce[e] : r[e]
            }
            if (!r.hi) {
                r._edit = null;
                var f, c = r.id,
                    d = s(c),
                    g = d && !de,
                    h = l("b1"),
                    v = l("b2"),
                    m = l("c"),
                    x = l("fb"),
                    y = l("fbc"),
                    p = l("fc"),
                    I = l("ff"),
                    w = l("fs"),
                    M = l("ls"),
                    D = l("w"),
                    L = i.x,
                    Z = i.y,
                    R = o.x,
                    G = o.y,
                    T = (D || 1) * Pe,
                    E = m || ve.colours.midgrey,
                    W = 0;
                f = "h" === r.st ? B(n, r.a1, r.a2, h, v, r.y, c, i, o, Z, G, E, T, M) : r.off ? b(n, r.a1, r.a2, h, v, r.off, c, i, o, L, Z, R, G, E, T, M, s) : C(n, r.a1, r.a2, h, v, c, i, o, L, Z, R, G, E, T, M, s);
                var N = f ? f.lx : (L + R) / 2,
                    V = f ? f.ly : (Z + G) / 2;
                if (r._x = N, r._y = V, f) {
                    var U = f.lx,
                        K = f.ly,
                        X = U,
                        H = K,
                        Y = Me(r);
                    w = Number(w || xe) * Pe;
                    var P = !1,
                        z = p || ve.colours.black,
                        _ = y || ve.colours.textback;
                    g && (z = t(), _ = Ae());
                    var j = k(r),
                        J = F(j, e, w, x, I),
                        Q = w + 3 * Pe;
                    W = Q * j.length, j.length > 0 && (X -= J[0] / 2);
                    for (var q = (j.length - 1) * Q, $ = f.above ? K - (q + Q / 2 + T / 2) : K - q / 2, te = 0; te < j.length; te++) {
                        0 === te && (H = $);
                        var re = O(e, n, j[te], U, $, c, !0, z, _, x, r, !1, !0, 99, J[te], g, w, I || ne);
                        P = P || re, $ += Q
                    }
                    var ae = !a(r) || !P;
                    ae && g && !Y && n.circle(U, K, w / 2, ve.colours.white, -1, Ae(), !0, c), Y && A(n, e, r.g, X, H, 1, ae, g, u, c), r.bu && ee(n, e, c, U, K, W / 2 + 4, 1, r.bu, u)
                }
                S(i, "id1", c, L, Z, n, T, E), S(o, "id2", c, R, G, n, T, E)
            }
        }

        function D(e) {
            var t = Ge;
            return J(e) && (t += Se / 2), e.e && (t *= e.e), t
        }

        function L(e) {
            return D(e) + Ze
        }

        function Z(e, t) {
            return D(e) + Re + D(t)
        }

        function R(e) {
            return 1.7 * e + Pe * ("small" === te ? 6 : 10)
        }

        function G(e, t, n, r, a, i, o) {
            var u = R(r),
                s = a + Math.PI;
            e.triangle(t + u * Math.cos(s + Te), n + u * Math.sin(s + Te), t + u * Math.cos(s - Te), n + u * Math.sin(s - Te), t, n, i, -1, i, !0, o)
        }

        function S(e, t, n, r, a, i, o, u) {
            e.du && i.circle(r, a, 1.5 * o, u, o, ve.colours.white, !0, n + pe + t)
        }

        function T(e) {
            return Be.showFullLabel && Be.showFullLabel === e
        }

        function E(e) {
            return ge && ("node" === e.type || "link" === e.type) && e.t.length > ge && !T(e.id)
        }

        function k(e) {
            if (e.t) {
                var t = e.t + "";
                return E(e) && (t = t.substring(0, ge - 3) + "..."), t.split(/\r\n|\r|\n/)
            }
            return [""]
        }

        function F(e, t, n, r, a) {
            for (var i = [], o = 0; o < e.length; o++) i[o] = ve.measureText(t, e[o], n, r, a || ne).width;
            return i
        }

        function W(e) {
            for (var t = 0, n = 0; n < e.length; n++) t = Math.max(t, e[n]);
            return t
        }

        function N(e, t, n, r, a, i, o, u, s, l, f, c, d, g, h, v, m) {
            var x = ve.measureText(e, n, v, l, m).width;
            return O(e, t, n, r, a, i, o, u, s, l, f, c, d, g, x, h, v, m)
        }

        function O(e, t, n, r, a, i, o, u, s, l, f, c, d, g, h, v, m, x) {
            var y = !0;
            0 === n.length && (y = !1);
            var p = i + pe + "t";
            if (c && h > g) return N(e, t, n, r, a, i, o, u, s, l, f, c, d, g, v, .85 * m, x);
            var b, I = 4 * Pe,
                w = r - h / 2;
            b = d ? a + 1 + m / 2 : a + (1 + m) / 2;
            var B = {
                x1: w - I,
                y1: b - m,
                x2: w + h + I,
                y2: b
            };
            if (y) {
                var C = d ? we : 0;
                t.text(r, b + C, n, u, m, l, p, x, !0, h, !1, o, B.x1, B.y1, B.x2, B.y2, s, v)
            }
            return f && "node" === f.type && (B.x1 = B.x1 - f.x, B.y1 = B.y1 - f.y, B.x2 = B.x2 - f.x, B.y2 = B.y2 - f.y), f && V(f, B, m, y), y
        }

        function V(e, t, n, r) {
            e._edit ? (e._edit.x1 = Math.min(e._edit.x1, t.x1), e._edit.x2 = Math.max(e._edit.x2, t.x2), e._edit.y1 = Math.min(e._edit.y1, t.y1), e._edit.y2 = Math.max(e._edit.y2, t.y2)) : (e._edit = t, e._edit.fs = n), oe && r && e._cl && e._cl.push(t)
        }

        function U(e, t) {
            var n = e.getLayer(),
                r = ve.selMap[n];
            r && e.setLayer(r), t(), r && e.setLayer(n)
        }

        function K(e, t, n) {
            return n && fe && le.hasOwnProperty(e) ? le[e] : t[e]
        }

        function X(e, t) {
            return t ? Ve[e.sh] ? e.sh : "box" : e.sh in Ve ? e.sh : e.sq ? "box" : "circle"
        }

        function H(e, t, n, r, a, i, o, u, s) {
            var l = r + i,
                f = a + i;
            if ("circle" === e) {
                var c = Math.min(l, f);
                o(c)
            } else {
                var d = t - l,
                    g = n - f,
                    h = t + l,
                    v = n + f;
                if ("box" === e) u(d, g, h, v);
                else {
                    var m = i * Oe,
                        x = r * Fe / ke + i * Ne;
                    "e" === e ? (h -= m, s(h, g, t + x, n, h, v, d, v, d, g)) : (d += m, s(d, v, t - x, n, d, g, h, g, h, v))
                }
            }
        }

        function Y(e, t, n, r, a, i, o, u, s, l, f, c, d) {
            H(d, t, n, r, a, i, function(r) {
                e.circle(t, n, r, o, u, l, f, c, s)
            }, function(t, n, r, a) {
                e.rect(t, n, r, a, o, u, l, f, c, s)
            }, function(t, n, r, a, i, d, g, h, v, m) {
                e.pentagon(t, n, r, a, i, d, g, h, v, m, o, u, l, f, c, s)
            })
        }

        function P(e, t, n, r, a, i, o) {
            H(o, t, n, r, a, i, function(t) {
                e._c = t
            }, function(r, a, i, o) {
                e._cl.push({
                    x1: r - t,
                    y1: a - n,
                    x2: i - t,
                    y2: o - n
                })
            }, function(r, a, i, o, u, s, l, f, c, d) {
                e._cl.push([r - t, a - n, i - t, o - n, u - t, s - n, l - t, f - n, c - t, d - n])
            })
        }

        function z(e, t, n, r, a) {
            function i(e) {
                return K(e, n, l)
            }
            if (!n.hi && !n.du) {
                var o = n.id,
                    u = De(n),
                    s = X(n, u),
                    l = a(o),
                    f = l && !fe,
                    c = i("b"),
                    d = i("bs"),
                    g = i("c"),
                    h = (u ? 1 : i("e") || 1) * Pe,
                    v = u ? i("re") : !1,
                    m = i("u"),
                    x = i("fi");
                n._c = 0, n._cl = [], n._edit = null;
                var y, p, b = !!c,
                    I = !!g,
                    w = !!m || !(!x || !x.t),
                    B = b ? c : ve.colours.white,
                    C = b ? h * (i("bw") || Ee) : 0,
                    A = I ? g : ve.colours.white,
                    M = n.x,
                    D = n.y;
                u ? (y = n.w / 2, p = n.h / 2) : y = p = h * ke;
                var L, Z = y + C / 2,
                    R = p + C / 2,
                    G = b ? C : h * Ee,
                    S = 3 * G / 4;
                if (L = ie ? b || I ? C / 2 : S - G / 2 : S + G / 2, (b || I) && Y(t, M, D, Z, R, 0, B, C || -1, d, A, I, o, s), (w || b || I) && (P(n, M, D, Z, R, L, s), l && (v && !w && (He = t.cloneLast()), f))) {
                    var T = Ae();
                    U(t, function() {
                        Y(t, M, D, Z, R, S, T, G, null, T, !1, o, s), v && (Xe = t.getLast())
                    })
                }
                if (w) {
                    if (m) t.image(M - y, D - p, M + y, D + p, r + m, o, n.ci ? "ci" : void 0, 1);
                    else {
                        var E = u ? Math.min(y, p) : y;
                        q(t, e, o, M, D, E, x.t, x.c || ve.colours.black, re)
                    }
                    l && v && (He = t.cloneLast())
                }
                _(t, e, M, D, C, y, p, n, u, s, b, I, w, f, h, o, r, i)
            }
        }

        function _(e, n, r, i, o, u, s, l, f, c, d, g, h, v, m, x, y, p) {
            var b, I = r,
                w = r,
                B = i,
                C = Me(l),
                M = !1,
                D = p("fb"),
                Z = p("fbc"),
                R = p("fc"),
                G = p("ff"),
                S = (p("fs") || xe) * m,
                T = 3 * m + S / 2,
                E = R ? R : ve.colours.black,
                W = Z ? Z : ve.colours.textback;
            v && (E = t(), W = Ae());
            var N = i + s + T,
                V = k(l),
                U = F(V, n, S, D, G),
                K = S + 3,
                X = K,
                H = K * V.length;
            V.length > 0 && (w -= U[0] / 2);
            var Y = f || h || d || g,
                P = me.defined(l.tc) ? !!l.tc : !h && !f,
                z = (V.length - 1) * X / 2;
            if (Y) P && (N = i - z);
            else {
                if (C) {
                    var _ = L(l.g[l.g.length - 1]) * m;
                    w = r + _, r += U[0] / 2 + _
                }
                N = i, i += z
            }
            var j = r;
            if (("e" === c || "w" === c) && (d || g)) {
                var J = 8 * m;
                j += "e" === c ? J : -J
            }
            V.length > 0 && (B = N), d && !P && (N += o - 3 * m);
            for (var q = 0; q < V.length; q++) {
                var te = O(n, e, V[q], j, N, x, !0, E, W, D, l, !1, !0, 99, U[q], v, S, G || ne);
                M = M || te, N += X
            }
            var re = !a(l) || !M;
            if (C)
                if (h || d || g) {
                    var ae = 3,
                        ie = h ? 11 : 3;
                    if (!f && !h && "circle" === c) {
                        var oe = 2;
                        ae += oe, ie += oe
                    }
                    f && (ae++, ie++);
                    var ue = ae * m,
                        se = ie * m;
                    if (l.g.length)
                        for (b = 0; b < l.g.length; b++) {
                            var le, fe = l.g[b],
                                ce = null,
                                de = !1;
                            switch (fe.p) {
                                case "ne":
                                    ce = u - ue, le = -s + se;
                                    break;
                                case "nw":
                                    ce = -u + ue, le = -s + se, de = !0;
                                    break;
                                case "sw":
                                    ce = -u + ue, le = s - se, de = !0;
                                    break;
                                case "se":
                                    ce = u - ue, le = s - se
                            }
                            null !== ce && Q(e, n, x + pe + fe.p, l.x + ce, l.y + le, m, fe, y, l, de, !0)
                        }
                } else A(e, n, l.g, re ? I : w, B, m, re, v, y, x, l);
            if (f || $(e, x, r, i, m, p), l.bu) {
                var ge = 0;
                Y ? ge = s : H > 0 ? ge = H / 2 + 4 : Me(l) && (ge = 13 * m), ee(e, n, x, r, i, ge, m, l.bu, y)
            }
        }

        function j(e, t) {
            var n = 0,
                r = 0;
            if (a(e)) {
                var i = (e.fs ? e.fs : xe) * (e.e ? e.e : 1),
                    o = k(e),
                    u = F(o, t, i, e.fb, e.ff);
                n = W(u), r = i * (o.length + 1), e.u || (n /= 10)
            }
            return {
                width: n,
                height: r
            }
        }

        function J(e) {
            return "b" in e ? e.b : ve.colours.keyline
        }

        function Q(e, t, n, r, a, i, o, u, s, l, f) {
            if (o) {
                o.e && (i *= o.e);
                var c = o.a ? Ke : 1,
                    d = Ge * i * c,
                    g = J(o),
                    h = g ? Se * i : -1,
                    v = d + (g ? h / 2 : 0),
                    m = !!(o.u || o.fi && o.fi.t);
                if (o.c) {
                    if (o.t) {
                        var x, y, p, b, I = o.hasOwnProperty("fb") ? o.fb : !0,
                            w = o.fc || ve.colours.white,
                            B = o.ff || ne;
                        if (o.w && f) {
                            x = i * ye[1] * c, p = (o.t + "").substring(0, 25), y = ve.measureText(t, p, x, I, B).width, b = Math.max(y, d);
                            var C, A, M = d;
                            l ? (C = r - (b - M) - d, A = r + d, r -= (b - M) / 2) : (C = r - d, A = r + (b - M) + d, r += (b - M) / 2);
                            var D = a - d,
                                L = a + d;
                            e.round(C, D, A, L, d, g, h, o.c, !0, n), s && (s._cl.push({
                                cx: C + d - s.x,
                                cy: a - s.y,
                                r: v
                            }), s._cl.push({
                                cx: A - d - s.x,
                                cy: a - s.y,
                                r: v
                            }), s._cl.push({
                                x1: C + d - s.x,
                                x2: A - d - s.x,
                                y1: D - s.y - h / 2,
                                y2: L - s.y + h / 2
                            }))
                        } else e.circle(r, a, d, g, h, o.c, !0, n), p = (o.t + "").substring(0, 4), x = i * ye[p.length - 1] * c, y = ve.measureText(t, p, x, I, B).width, b = 1.7 * d, s && s._cl.push({
                            cx: r - s.x,
                            cy: a - s.y,
                            r: v
                        });
                        O(t, e, p, r, a, n, !1, w, ve.colours.white, I, null, !0, !1, b, y, !1, x, B)
                    } else if (e.circle(r, a, d, g, h, o.c, !0, n), s && s._cl.push({
                            cx: r - s.x,
                            cy: a - s.y,
                            r: v
                        }), m) {
                        var Z = d / 1.41421;
                        o.u ? e.image(r - Z, a - Z, r + Z, a + Z, u + o.u, n) : q(e, t, n, r, a, Z, o.fi.t, o.fi.c || ve.colours.black, re)
                    }
                } else m && (s && s._cl.push({
                    cx: r - s.x,
                    cy: a - s.y,
                    r: d
                }), o.u ? e.image(r - d, a - d, r + d, a + d, u + o.u, n) : q(e, t, n, r, a, d, o.fi.t, o.fi.c || ve.colours.black, re));
                o.a && (Ye = Ye || {}, Ye[me.rawId(n)] = !0)
            }
        }

        function q(e, t, n, r, a, i, o, u, s) {
            e.rect(r - i, a - i, r + i, a + i, ve.colours.transparent, -1, ve.colours.transparent, !0, n);
            var l = 2 * i,
                f = ve.measureText(t, o, l, !1, s || ne).width,
                c = f - l,
                d = l - (c > 0 ? 5 * c / 4 : 0);
            f = ve.measureText(t, o, d, !1, s || ne).width, e.text(r, a + d / 2, o, u, d, !1, n, s, !0, f, !0, n, r - i, a - i, r + i, a + i)
        }

        function $(e, t, n, r, a, i) {
            var o = e.getLayer();
            e.setLayer(ve.layers.HALOS);
            for (var u = 0; u < Ue.length; u++) {
                var s = Ue[u],
                    l = i(s);
                if (l) {
                    var f = t + pe + s,
                        c = l.r * a,
                        d = l.w * a;
                    d / 2 >= c ? e.circle(n, r, c + d / 2, ve.colours.white, -1, l.c, !0, f, null, !0) : e.circle(n, r, c, l.c, d, ve.colours.white, !1, f, null, !0)
                }
            }
            e.setLayer(o)
        }

        function ee(e, t, n, r, a, i, o, u, s) {
            if (u.t) {
                var l = e.getLayer();
                e.setLayer(ve.layers.BUBBLES);
                var f = u.p ? "" + u.p : "ne",
                    c = "w" === f.substring(1, 2),
                    d = "s" === f.substring(0, 1),
                    g = n + pe + "bu",
                    h = Number(u.fs ? u.fs : xe) * Pe,
                    v = u.b ? u.b : ve.colours.keyline,
                    m = u.c ? u.c : ve.colours.white,
                    x = u.fc ? u.fc : ve.colours.black,
                    y = k(u),
                    p = h + 3 * Pe,
                    b = p * y.length,
                    I = F(y, t, h, u.fb, u.ff),
                    w = W(I),
                    B = 13 * Pe,
                    C = 6 * Pe,
                    A = 2 * o,
                    M = 9 * Pe,
                    D = 13 * Pe,
                    L = 10 * Pe;
                i += D;
                var Z = b + 2 * C,
                    R = 2 * Pe,
                    G = Math.max(R / 2, 1),
                    S = 3 * Pe;
                u.g && delete u.g.e;
                var T = u.g ? 17 * Pe : 0;
                c && (r -= w + 2 * A + 2 * B + T), d && (a += (h + 3) * y.length + 2 * i + 2 * C);
                var E = r + A + .5,
                    N = r + A + 2 * B + w + T + .5,
                    O = a - i - Z + .5,
                    V = a - i + .5;
                e.round(E, O, N, V, 8 * Pe, v, R, m, !0, g), Q(e, t, g, r + T, (O + V) / 2, Pe, u.g, s);
                var U, K = O - .5 + C + p,
                    X = E + B + T - .5;
                for (U = 0; U < y.length; U++) {
                    var H = I[U];
                    e.text(X, K, y[U], x, h, u.fb, g, u.ff || ne, !1, H, !1, !1, X - 1.5, K - h, N, V), K += p
                }
                c && (E = N, M = -M, L = -L, G = -G), d && (V = O, D = -D, S = -S), e.triangle(E + M, V, E + M + L, V, E + M, V + D, v, R, m, !0, g), e.triangle(E + M + G, V - S, E + M + L + G, V - S, E + M + G, V + D - S, v, -1, m, !0, g), e.setLayer(l)
            }
        }
        var te, ne, re, ae, ie, oe, ue, se, le, fe, ce, de, ge, he = {},
            ve = KeyLines.Rendering,
            me = KeyLines.Util,
            xe = he.fontSize = 14,
            ye = [12, 9, 7, 7],
            pe = me.idSep(),
            be = 1,
            Ie = 2,
            we = 0,
            Be = {};
        he.setSpecialOptions = function(e, t) {
            Be[e] = t
        };
        var Ce = !1;
        he.setDashSupport = function(e) {
            Ce = !!e
        };
        var Ae = he.editColour = function() {
            return ae || ve.colours.edit
        };
        he.setOptions = function(e) {
            te = e.arrows, me.isNumber(e.labelOffset) && (we = e.labelOffset), ge = e.truncateLabels && e.truncateLabels.maxLength && me.isNumber(e.truncateLabels.maxLength) ? e.truncateLabels.maxLength : !1, e.selectionColour && (ae = ve.validatergb(e.selectionColour) ? e.selectionColour : void 0), se = e.fanLength, ne = e.fontFamily, re = e.iconFontFamily, oe = e.linkEnds.avoidLabels, ie = "loose" !== e.linkEnds.spacing, e.selectionFontColour && (ue = ve.validatergb(e.selectionFontColour) ? e.selectionFontColour : void 0),
                le = e.selectedNode, fe = me.isNormalObject(e.selectedNode), ce = e.selectedLink, de = me.isNormalObject(e.selectedLink)
        };
        var Me = he.itemHasGlyph = function(e) {
                return e.hasOwnProperty("g") && me.isArray(e.g) && e.g.length > 0
            },
            De = he.shapeTest = function(e) {
                return isFinite(e.w) && isFinite(e.h)
            },
            Le = function(e, t, n, r, a, i) {
                var o = r.cx + a - e,
                    u = r.cy + i - t,
                    s = Math.sqrt(o * o + u * u);
                if (s > n + r.r || s < Math.abs(n - r.r) || 0 === s) return [];
                var l = (n * n - r.r * r.r + s * s) / (2 * s),
                    f = Math.sqrt(n * n - l * l),
                    c = e + l * o / s,
                    d = t + l * u / s,
                    g = c + f * u / s,
                    h = c - f * u / s,
                    v = d - f * o / s,
                    m = d + f * o / s;
                return [Math.atan2(v - t, g - e), Math.atan2(m - t, h - e)]
            },
            Ze = 3,
            Re = 1,
            Ge = 9,
            Se = 2,
            Te = Math.PI / 7;
        he.labelPosition = function(e, t) {
            if (!e || e.hi) return null;
            var n = e._edit,
                r = n.x1,
                a = n.y1,
                i = n.x2,
                o = n.y2;
            return "node" === e.type && (r += e.x, a += e.y, i += e.x, o += e.y), {
                x1: t.x(r),
                y1: t.y(a),
                x2: t.x(i),
                y2: t.y(o),
                fs: t.dim(n.fs)
            }
        };
        var Ee = 4,
            ke = 27,
            Fe = 50,
            We = Fe - ke,
            Ne = Math.sqrt(ke * ke + We * We) / ke,
            Oe = 1 - Math.sin(Math.atan2(ke, We) / 2),
            Ve = {
                box: !0,
                circle: !0,
                e: !1,
                w: !1
            };
        he.nodeSize = function(e, t, n) {
            if (e.w && e.h) return me.len(e.w, e.h) / 2;
            if (e.du) return 10;
            var r = ke * (e.e ? e.e : 1),
                a = j(e, t);
            if (n) {
                var i = r + a.height / 2,
                    o = a.width / 2;
                return me.len(o, i)
            }
            return r + a.height / 2
        };
        var Ue = ["ha0", "ha1", "ha2", "ha3", "ha4", "ha5", "ha6", "ha7", "ha8", "ha9"];
        he.contains = function(e, t) {
            function n(e, t) {
                e && i.push(t)
            }
            var r, a, i = [];
            if (e = me.defaults(e, {
                    sh: "box",
                    x: 0,
                    y: 0
                }), De(e))
                if ("circle" === e.sh) {
                    var o = Math.min(e.w, e.h) / 2;
                    for (a in t) r = t[a], De(r) ? "circle" === r.sh ? n(ve.insideCircle(e.x, e.y, o, r.x, r.y, Math.min(r.w, r.h) / 2), r.id) : n(ve.circleContains(e.x, e.y, o, r.x - r.w / 2, r.y - r.h / 2, r.x + r.w / 2, r.y + r.h / 2), r.id) : n(ve.insideCircle(e.x, e.y, o, r.x, r.y), r.id)
                } else {
                    var u = {
                        x1: e.x - e.w / 2,
                        y1: e.y - e.h / 2,
                        x2: e.x + e.w / 2,
                        y2: e.y + e.h / 2
                    };
                    for (a in t)
                        if (r = t[a], De(r))
                            if ("circle" === r.sh) {
                                var s = Math.min(r.w, r.h) / 2;
                                n(ve.rectContains(u, r.x - s, r.y - s, r.x + s, r.y + s), r.id)
                            } else n(ve.rectContains(u, r.x - r.w / 2, r.y - r.h / 2, r.x + r.w / 2, r.y + r.h / 2), r.id);
                    else n(ve.inside(u, r.x, r.y), r.id)
                }
            return i
        };
        var Ke, Xe, He, Ye, Pe = 1;
        return he.getResizePrimitive = function() {
            return He
        }, he.animated = function() {
            return !!Ye
        }, he.generate = function(t, n, r, a, i, o, u, s, l, f) {
            function c(e) {
                i.resetItem(e, e.x, e.y), i.setLayer(De(e) ? ve.layers.SHAPES : ve.layers.NODES), z(o, i, e, l, f)
            }

            function d(e) {
                i.resetItem(e), i.setLayer(ve.layers.LINKS), M(o, i, e, t[e.id1], t[e.id2], l, f)
            }
            if (He && (r || a[He.id] === Ie) && (Xe = null, He = null), Ye = null, Ke = e.atanEasing((new Date).getTime() % 1e3 / 1e3), Pe = u.getUnzoom(), r) {
                i.removeAllItems();
                for (var g in t) c(t[g]);
                for (var h in n) d(n[h])
            } else {
                var v;
                for (v in a) {
                    var m = t[v];
                    m ? a[v] === be ? i.moveItem(m, m.x, m.y) : c(m) : n[v] || i.removeItem(v)
                }
                for (v in a) n[v] && d(n[v])
            }
            return i.assembleItems(), Ye
        }, he.generateHandles = function(e, t, n, r) {
            e.extras(), e.setLayer(ve.layers.HANDLES), Xe && 1 === r.length && ve.getHandles(e, t, n, Xe, 1, Xe.id, Ae(), pe)
        }, he
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.LineShader = function(t) {
        function n(e, t, n, r, a, i) {
            c.uniform2f(h.resolution, t, n), c.uniformMatrix4fv(h.transform, !1, e.get()), c.uniform1f(h.hitTest, 0 | r), c.uniform1f(h.zoom, e.getZoomLevel()), c.uniform1f(h.shadowType, a), c.uniform1f(h.viewIndependent, 0 | !i)
        }

        function r(e) {
            e.elementBuffer || (e.elementBuffer = c.createBuffer()), e.indexBuffer || (e.indexBuffer = c.createBuffer())
        }

        function a(e) {
            c.bindBuffer(c.ARRAY_BUFFER, e.elementBuffer), c.enableVertexAttribArray(h.position), c.enableVertexAttribArray(h.width), c.enableVertexAttribArray(h.vertexInfo), c.enableVertexAttribArray(h.colour), c.enableVertexAttribArray(h.shadowColour), c.vertexAttribPointer(h.position, 4, c.FLOAT, !1, 8 * Float32Array.BYTES_PER_ELEMENT, 0), c.vertexAttribPointer(h.width, 1, c.FLOAT, !1, 8 * Float32Array.BYTES_PER_ELEMENT, 16), c.vertexAttribPointer(h.vertexInfo, 4, c.UNSIGNED_BYTE, !1, 8 * Float32Array.BYTES_PER_ELEMENT, 20), c.vertexAttribPointer(h.colour, 4, c.UNSIGNED_BYTE, !1, 8 * Float32Array.BYTES_PER_ELEMENT, 24), c.vertexAttribPointer(h.shadowColour, 4, c.UNSIGNED_BYTE, !1, 8 * Float32Array.BYTES_PER_ELEMENT, 28)
        }

        function i(e, t) {
            (e.alwaysUpdate || e.rebuildOptions.all || e.rebuildOptions.positions || e.rebuildOptions.colours || e.rebuildOptions.textures || e.rebuildOptions.shadow) && (c.bindBuffer(c.ARRAY_BUFFER, t.elementBuffer), c.bufferData(c.ARRAY_BUFFER, t.elementData, c.DYNAMIC_DRAW), c.bindBuffer(c.ARRAY_BUFFER, null), c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, t.indexBuffer), c.bufferData(c.ELEMENT_ARRAY_BUFFER, t.elementIndex, c.DYNAMIC_DRAW), c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, null))
        }

        function o(e) {
            c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, e.indexBuffer), c.drawElements(c.TRIANGLES, 3 * e.numOfTriangles, c.UNSIGNED_INT, 0)
        }

        function u() {
            c.bindBuffer(c.ELEMENT_ARRAY_BUFFER, null), c.bindBuffer(c.ARRAY_BUFFER, null)
        }
        var s = e.Utils,
            l = e["line-vertex"],
            f = e["line-fragment"],
            c = t,
            d = {},
            g = s.createProgram(c, atob(l), atob(f));
        c.getExtension("OES_standard_derivatives"), c.getExtension("OES_element_index_uint");
        var h = {
            hitTest: c.getUniformLocation(g, "u_hitTest"),
            zoom: c.getUniformLocation(g, "u_zoom"),
            transform: c.getUniformLocation(g, "u_transform"),
            resolution: c.getUniformLocation(g, "u_resolution"),
            shadowType: c.getUniformLocation(g, "u_shadowType"),
            viewIndependent: c.getUniformLocation(g, "u_viewIndependent"),
            position: c.getAttribLocation(g, "a_position"),
            colour: c.getAttribLocation(g, "a_colour"),
            width: c.getAttribLocation(g, "a_width"),
            vertexInfo: c.getAttribLocation(g, "a_vertexInfo"),
            shadowColour: c.getAttribLocation(g, "a_hitTestColour")
        };
        return d.drawItems = function(e, t, s, l, f, d) {
            c.useProgram(g), u(), n(s, l, f, d, 0 | e.shadowType, e.useView), r(t), i(e, t), a(t), o(t), u()
        }, d
    })
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.Renderer = function(t) {
        function n() {
            var e = w.createTexture();
            return w.bindTexture(w.TEXTURE_2D, e), w.texParameteri(w.TEXTURE_2D, w.TEXTURE_WRAP_S, w.CLAMP_TO_EDGE), w.texParameteri(w.TEXTURE_2D, w.TEXTURE_WRAP_T, w.CLAMP_TO_EDGE), w.texParameteri(w.TEXTURE_2D, w.TEXTURE_MIN_FILTER, w.NEAREST), w.texParameteri(w.TEXTURE_2D, w.TEXTURE_MAG_FILTER, w.NEAREST), e
        }

        function r(e) {
            if (e) {
                var t = e.framebuffer,
                    n = e.renderBuffer,
                    r = e.texture;
                w.bindFramebuffer(w.FRAMEBUFFER, t), w.bindRenderbuffer(w.RENDERBUFFER, n), w.bindTexture(w.TEXTURE_2D, r)
            } else d();
            w.viewport(0, 0, m, x)
        }

        function a(e) {
            var t = I[e] || null;
            if (e && !t) {
                t = I[e] = {
                    framebuffer: w.createFramebuffer(),
                    renderbuffer: w.createRenderbuffer(),
                    texture: n()
                }, w.texImage2D(w.TEXTURE_2D, 0, w.RGBA, w.drawingBufferWidth, w.drawingBufferHeight, 0, w.RGBA, w.UNSIGNED_BYTE, null), w.bindFramebuffer(w.FRAMEBUFFER, t.framebuffer), w.bindRenderbuffer(w.RENDERBUFFER, t.renderbuffer), w.clearColor(0, 0, 0, 0), w.renderbufferStorage(w.RENDERBUFFER, w.DEPTH_COMPONENT16, w.drawingBufferWidth, w.drawingBufferHeight), w.framebufferTexture2D(w.FRAMEBUFFER, w.COLOR_ATTACHMENT0, w.TEXTURE_2D, t.texture, 0), w.framebufferRenderbuffer(w.FRAMEBUFFER, w.DEPTH_ATTACHMENT, w.RENDERBUFFER, t.renderbuffer), w.viewport(0, 0, w.drawingBufferWidth, w.drawingBufferHeight);
                var a = w.checkFramebufferStatus(w.FRAMEBUFFER);
                if (w.FRAMEBUFFER_COMPLETE !== a) throw new Error(a.toString())
            }
            r(t)
        }

        function i() {
            return w = e.Utils.create3DContext(b, {
                antialias: !1,
                preferLowPowerToHighPerformance: !1
            }), h = b.width, v = b.height, m = w.drawingBufferWidth, x = w.drawingBufferHeight, w.getExtension("OES_standard_derivatives"), w.getExtension("OES_element_index_uint"), w.viewport(0, 0, m, x), w.blendFunc(w.ONE, w.ONE_MINUS_SRC_ALPHA), w.enable(w.BLEND), w.clearColor(0, 0, 0, 0), I = t.framebuffers = {}, L = new Uint8Array(4), w
        }

        function o(e, t) {
            if (I[e]) {
                var n = I[e].texture;
                M.drawItems(t, n)
            }
        }

        function u() {
            w.viewport(0, 0, w.drawingBufferWidth, w.drawingBufferHeight)
        }

        function s(e, t, n, r) {
            D = y.createMatrix(), e ? (D.pan(t, n), D.zoom(r)) : (D.pan(0, 0), D.zoom(1))
        }

        function l(e, t, n, r, a, i, o) {
            n.numOfTriangles > 0 && e.drawItems(t, n, r, a, i, o)
        }

        function f(e, t, n, r, a, i) {
            t.arcBuffers.numOfTriangles > 0 && e.drawItems(t, n, r, a, i)
        }

        function c(e, t, n, r, a, i, o, u, s) {
            t.triangleBuffers.numOfTriangles > 0 && e.drawItems(t, n, r, a, i, o, u, s)
        }

        function d() {
            w.bindFramebuffer(w.FRAMEBUFFER, null), w.bindTexture(w.TEXTURE_2D, null), w.bindRenderbuffer(w.RENDERBUFFER, null)
        }

        function g(e) {
            e.rebuildOptions.colours = !1, e.rebuildOptions.positions = !1, e.rebuildOptions.textures = !1, e.rebuildOptions.shadow = !1, e.rebuildOptions.all = !1
        }
        var h, v, m, x, y = e.Utils,
            p = {},
            b = t.canvas,
            I = t.framebuffers,
            w = i(),
            B = e.LineShader(w),
            C = e.ArcShader(w),
            A = e.TriangleShader(w),
            M = e.RenderToTextureShader(w),
            D = null,
            L = [0, 0, 0, 0];
        return p.clearView = function(e) {
            e && (w = i()), u(), w.clear(w.COLOR_BUFFER_BIT)
        }, p.clearFramebuffers = function(e) {
            I = t.framebuffers = {}
        }, p.render = function(e, n, r, i, u, y, p) {
            h = b.clientWidth, v = b.clientHeight, m = w.drawingBufferWidth, x = w.drawingBufferHeight;
            var I = t.layersToDraw;
            t.panX = e, t.panY = n, t.zoom = r;
            for (var M = 0, L = I.length; L > M; M++) {
                var Z = I[M];
                s(Z.useView, e, n, r), a(Z.drawFramebuffer), l(B, Z, Z.lineBuffers, D, h, v, p.showShadowCanvas), f(C, Z, D, h, v, p.showShadowCanvas), c(A, Z, D, i, u, y, h, v, p.showShadowCanvas), Z.useFramebuffer && (d(), o(Z.useFramebuffer, Z.fbAlpha), a(Z.useFramebuffer), w.clear(w.COLOR_BUFFER_BIT), d()), g(Z)
            }
        }, p.drawShadowCanvas = function(e, n) {
            if (h = b.clientWidth, v = b.clientHeight, m = w.drawingBufferWidth, x = w.drawingBufferHeight, t.layersToDraw.length > 0) {
                a("shadow");
                for (var r = 0, i = t.layersToDraw.length; i > r; r++) {
                    var o = t.layersToDraw[r];
                    s(o.useView, t.panX, t.panY, t.zoom), l(B, o, o.lineBuffers, D, h, v, !0), f(C, o, D, h, v, !0), c(A, o, D, t.textAtlas, t.fontIconAtlas, t.imageAtlases, h, v, !0)
                }
                var u = m / b.clientWidth,
                    d = e * u,
                    g = x - n * u;
                d >= 0 && g >= 0 && m > d && x > g && w.readPixels(d, g, 1, 1, w.RGBA, w.UNSIGNED_BYTE, L)
            }
            return L
        }, p
    })
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 6;
        KeyLines.WebGL.generateRectangle = function(t, n, r, a, i, o, u, s, l, f) {
            var c = t.wsc || [0, 0, 0, 0];
            if (l = l || 0, f = f || 0, u.alwaysUpdate || u.rebuildOptions.all || u.rebuildOptions.shadow)
                for (var d = 0; e > d; d++) u.triangleBuffers.shadowData[u.triangleBuffers.shadowIndex++] = c[0], u.triangleBuffers.shadowData[u.triangleBuffers.shadowIndex++] = c[1], u.triangleBuffers.shadowData[u.triangleBuffers.shadowIndex++] = c[2], u.triangleBuffers.shadowData[u.triangleBuffers.shadowIndex++] = c[3];
            if (u.rebuildOptions.colours || u.alwaysUpdate || u.rebuildOptions.all)
                for (d = 0; e > d; d++) u.triangleBuffers.colourData[u.triangleBuffers.colourIndex++] = o[0], u.triangleBuffers.colourData[u.triangleBuffers.colourIndex++] = o[1], u.triangleBuffers.colourData[u.triangleBuffers.colourIndex++] = o[2], u.triangleBuffers.colourData[u.triangleBuffers.colourIndex++] = o[3];
            if (u.rebuildOptions.positions || u.alwaysUpdate || u.rebuildOptions.all) {
                var g = n,
                    h = r,
                    v = a,
                    m = i;
                s && (g = n + s.x, h = r + s.y, v = a + s.x, m = i + s.y), u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = g, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = h, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = v, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = h, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = g, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = m, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = g, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = m, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = v, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = h, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = v, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = m, u.triangleBuffers.positionData[u.triangleBuffers.positionIndex++] = l
            }(u.rebuildOptions.textures || u.alwaysUpdate || u.rebuildOptions.all) && (u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = v - g, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = m - h, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -1, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = 0, u.triangleBuffers.textureData[u.triangleBuffers.textureIndex++] = -l)
        }
    }
}(),
function() {
    var e = KeyLines.Model = {};
    e.createModel = function(e, t, n) {
        function r(e, t) {
            switch (e) {
                case "x1":
                    return he[t.id1].x;
                case "y1":
                    return he[t.id1].y;
                case "x2":
                    return he[t.id2].x;
                case "y2":
                    return he[t.id2].y
            }
        }

        function a(e) {
            pe = !1, ye = e || {}, be = 0
        }

        function i(e) {
            ye[e] || (be++, ye[e] = me)
        }

        function o() {
            if (pe) return !0;
            for (var e in ye) return !0;
            return !1
        }

        function u(e) {
            "node" === e.type && (e.x || (e.x = 0), e.y || (e.y = 0))
        }

        function s(e, t) {
            var n = {},
                r = {};
            if (e)
                for (var a = 0; a < e.length; a++) {
                    var i = e[a];
                    if (i.id && !(i.id in n || i.id in r)) {
                        var o = null;
                        if ("node" === i.type) {
                            o = n;
                            var s = !0;
                            t && he[i.id] && (s = !1), s && u(i)
                        }
                        "link" === i.type && (o = r), o && (o[i.id] = de.clone(i))
                    }
                }
            return {
                nodes: n,
                links: r
            }
        }

        function l() {
            function e(e) {
                var n = de.clone(e, !0);
                n.id = de.rawId(e.id), t.push(n)
            }
            var t = [];
            return de.iterator(ve, e), de.iterator(he, e), t
        }

        function f(e) {
            Le(e.items)
        }

        function c(e) {
            for (var t in e) de.isNullOrUndefined(e[t]) && delete e[t]
        }

        function d(e, t) {
            var n = e[t],
                r = !0;
            if (de.isNormalObject(n))
                for (var a in n) g(n, a), r &= !isNaN(n[a]);
            r || delete e[t]
        }

        function g(e, t) {
            var n = Number(e[t]);
            0 > n && !Fe[t] && (n = null), e[t] = n
        }

        function h(e, t) {
            de.isNullOrUndefined(e[t]) || (e[t] = e[t] + "", We[t] && !We[t][e[t]] && delete e[t])
        }

        function v(e, t) {
            e[t] && (e[t] = ce.validatergb(e[t]) ? e[t] : void 0)
        }

        function m(e, t) {
            Ge[t] ? h(e, t) : Re[t] ? g(e, t) : Se[t] ? v(e, t) : ke[t] && d(e, t)
        }

        function x(e, t, n) {
            n[t] && delete e[t]
        }

        function y(e, t) {
            for (var n in e) m(e, n), x(e, n, Ee), t && x(e, n, Te)
        }

        function p(e, t) {
            var n = {};
            if (e && de.isArray(e)) {
                for (var r = [], a = 0; a < e.length; a++) {
                    var i = e[a].p,
                        o = null;
                    t ? o = e[a] : i && 2 === i.length && /(ne|nw|se|sw)/.test(i) && (n[i] || (o = e[a], n[i] = 1)), o && (b(o), r.push(o))
                }
                e = r
            }
            return e
        }

        function b(e) {
            if (de.isNormalObject(e.fi)) {
                var t = e.fi;
                de.isNullOrUndefined(t.t) || (t.t = "string" == typeof t.t ? t.t.charAt(0) : String.fromCharCode(t.t)), t.c && (t.c = ce.validatergb(t.c) ? t.c : void 0)
            } else null !== e.fi && delete e.fi
        }

        function I(e, t) {
            for (var n = 0; n < Ne.length; n++) {
                var r = Ne[n],
                    a = e[r];
                a && (t ? a.hasOwnProperty("c") || a.hasOwnProperty("r") || a.hasOwnProperty("w") || delete e[r] : a.hasOwnProperty("c") && a.hasOwnProperty("r") && a.hasOwnProperty("w") || delete e[r])
            }
        }

        function w(e) {
            if (de.isArray(e.bu)) delete e.bu;
            else {
                e.bu.p || (e.bu.p = "ne");
                var t = e.bu.p;
                /(ne|nw|se|sw)/.test(t) || 2 === t.length || delete e.bu
            }
        }

        function B(e, t) {
            var n = {};
            for (var r in e) e[r].id1 !== e[r].id2 && C(e[r], t) && (n[r] = e[r]);
            return n
        }

        function C(e, t) {
            return t = t || {}, (he[e.id1] || t[e.id1]) && (he[e.id2] || t[e.id2])
        }

        function A(e, t) {
            for (var n in ve) {
                var r = ve[n];
                he[r.id1][e] !== t && he[r.id2][e] !== t || r[e] !== t && (r[e] = t, Me(n))
            }
        }

        function M(e, t) {
            e[t] && _e(e.id)
        }

        function D(e) {
            for (var t in ve) M(ve[t], e);
            for (t in he) M(he[t], e)
        }

        function L() {
            Z("hi")
        }

        function Z(e, t) {
            A(e, !0), t || D(e), "hi" === e && U()
        }

        function R(e, t, n) {
            for (var r = "x" === t || "y" === t ? i : Me, a = 0; a < e.length; a++) {
                var o = e[a],
                    u = he[o];
                if (u) u[t] = n, r(o);
                else {
                    var s = ve[o];
                    s && (s[t] = n, Me(o))
                }
            }
        }

        function G(e, t) {
            var n = !1;
            return he[e] && (n = T(he[e], t)), ve[e] && (n = T(ve[e], t)), n
        }

        function S(e) {
            return (he[e] || ve[e]) && !(G(e, "hi") || G(e, "gh"))
        }

        function T(e, t) {
            return !!e[t]
        }

        function E(e, t, n) {
            e = de.ensureArray(e), R(e, t, !1);
            var r;
            if (Z(t), n) {
                var a = {};
                for (r = 0; r < e.length; r++) a[e[r]] = 1;
                for (r in ve) T(ve[r], t) && (T(he[ve[r].id1], t) || T(he[ve[r].id2], t) || (a[ve[r].id1] || a[ve[r].id2]) && (ve[r][t] = !1))
            }
        }

        function k(e) {
            var t, n = [];
            for (t in he) t = de.rawId(t), he[t][e] && n.push(t);
            for (t in ve) t = de.rawId(t), ve[t][e] && n.push(t);
            return n
        }

        function F(e, t) {
            e = de.ensureArray(e), R(e, t, !0), Z(t)
        }

        function W(e) {
            var t = {};
            for (var n in e) ve[n] && "undefined" == typeof ve[n].off && (t[n] = ve[n]);
            return t
        }

        function N() {
            var e = O(he);
            return e = O(ve) || e
        }

        function O(e) {
            var t = !1;
            for (var r in e) {
                var a = e[r];
                if (t = V(a) || t, n.itemHasGlyph(a))
                    for (var i = 0; i < a.g.length; i++) t = V(a.g[i]) || t;
                a.bu && (t = V(a.bu.g) || t)
            }
            return t
        }

        function V(e) {
            if (e) {
                var t = e.u;
                if (t && !(t in Xe)) return Xe[t] = 1, !0
            }
        }

        function U() {
            oe = ue = null
        }

        function K(e) {
            var t = {};
            for (var n in e) {
                var r = e[n];
                r.hi || (t[n] = r)
            }
            return t
        }

        function X() {
            oe || (oe = K(he), ue = K(ve))
        }

        function H() {
            pe || (be > 100 ? Y() : Ce().internalMarkLinks(ye))
        }

        function Y() {
            for (var e in ue)
                if (!ye[e]) {
                    var t = ve[e];
                    (ye[t.id1] || ye[t.id2]) && (ye[e] = xe)
                }
        }

        function P(e) {
            for (var t in e) ze(t, !0)
        }

        function z(t) {
            var n = KeyLines.Graph.create(e);
            return n.privateSetProperties(he, t), n
        }

        function _() {
            return Ce().connections({
                all: !1
            })
        }

        function j(e, t) {
            var n = [],
                r = {};
            if (de.hasAnyKeys(e)) {
                var a = z(e).findAdjacency;
                n = _();
                for (var i = 0; i < n.length; i++) {
                    var o = ve[n[i][0]];
                    a({
                        all: !0
                    }, o.id1, o.id2) && (r[i] = 1)
                }
            }
            t(n, r)
        }

        function J(e) {
            for (var t = {}, n = 0; n < e.length; n++) {
                var r = e[n],
                    a = ve[r];
                a && (t[r] = a)
            }
            return t
        }

        function Q() {
            ee(_())
        }

        function q() {
            for (var e in ve) {
                var t = ve[e];
                delete t.st
            }
        }

        function $(e, t, n, r) {
            function a(e, t) {
                return i(ve[e]) - i(ve[t])
            }

            function i(e) {
                var t = de.defined(e.off) ? e.off : s;
                return e.id1 >= e.id2 && (t = -t), t
            }

            function o(e, t) {
                r(e, e.id1 < e.id2 ? t : -t)
            }
            var u, s = 1e6,
                l = t || e,
                f = 40;
            de.iterator(l, function(t, r) {
                u = e[+r], n || (u = de.filter(u, function(e) {
                    return !ve[e].gh
                })), u.sort(a);
                for (var i = u.length, s = Math.floor(i / 2), l = (i + 1) % 2 / 2, c = 0; i > c; c++) {
                    var d = ve[u[c]];
                    o(d, (c - s + l) * f)
                }
            })
        }

        function ee(e, t) {
            function n(e, t) {
                e.off = t, Me(e.id)
            }
            $(e, t, !0, n)
        }

        function te(e, t, n) {
            function r(e, t) {
                a.push({
                    id: e.id,
                    off: t
                })
            }
            var a = [];
            return $(e, t, n, r), a
        }

        function ne() {
            for (var e = _(), t = {}, n = 0; n < e.length; n++) {
                var r = !1,
                    a = e[n];
                if (a.length > 1) {
                    for (var i = 0; i < a.length; i++) {
                        var o = ve[a[i]];
                        o && (r = r || de.defined(o.off))
                    }
                    r || (t[n] = 1)
                }
            }
            ee(e, t)
        }

        function re(e) {
            return (e + "").match(et)
        }

        function ae(e) {
            var t = re(e);
            return tt[t[2]]
        }

        function ie(e) {
            var t = re(e);
            return t ? t[1] : null
        }
        var oe, ue, se, le, fe = KeyLines.Common,
            ce = KeyLines.Rendering,
            de = KeyLines.Util,
            ge = {},
            he = {},
            ve = {},
            me = 1,
            xe = 2,
            ye = {},
            pe = !1,
            be = 0,
            Ie = de.idSep(),
            we = null,
            Be = KeyLines.Layouts.create(),
            Ce = ge.graph = function() {
                return we || (we = KeyLines.Graph.create(e), we.privateSetProperties(he, ve)), we
            },
            Ae = ge.setAllDirty = function() {
                pe = !0
            },
            Me = ge.setDirty = function(e) {
                ye[e] || be++, ye[e] = xe
            },
            De = ge.moveNode = function(e, t, n) {
                var r = he[e];
                r && (r.x = t, r.y = n, i(e))
            };
        ge.moveLink = function(e, t) {
            var n = ve[e];
            n && (n.y = t, Me(e))
        }, ge.setLinkOffset = function(e, t) {
            var n = ve[e];
            n && (n.off = t, Me(e))
        }, ge.load = function(e) {
            if (e) {
                he = {}, ve = {}, Je(!0), f(e);
                var t = s(e.items);
                he = t.nodes, ve = B(t.links), L(), we = null, ne(), Ae()
            }
            N()
        };
        var Le = ge.ensureTypesInArray = function(e, t) {
                if (e)
                    for (var n = 0; n < e.length; n++) Ze(e[n], t)
            },
            Ze = ge.ensureObject = function(e, t) {
                y(e, t), b(e), e.g && (de.isArray(e.g) ? (Le(e.g), e.g = p(e.g, "link" === e.type || ve[e.id])) : delete e.g), I(e, t), e.bu && w(e)
            };
        ge.defaultValueFor = function(e) {
            switch (e) {
                case "e":
                    return 1;
                case "fs":
                    return n.fontSize;
                case "off":
                case "b1":
                case "b2":
                    return 0;
                case "x":
                case "y":
                    return 0;
                case "w":
                case "h":
                    return 1;
                case "bw":
                    return 4;
                case "c":
                case "b":
                    return ce.colours.midgrey;
                case "fc":
                    return ce.colours.black;
                case "fbc":
                    return ce.colours.textback
            }
        };
        var Re = {
                x: 1,
                y: 1,
                w: 1,
                h: 1,
                e: 1,
                fs: 1,
                off: 1,
                b1: 1,
                b2: 1,
                bw: 1
            },
            Ge = {
                t: 1,
                u: 1,
                id: 1,
                id1: 1,
                id2: 1,
                ff: 1,
                sh: 1,
                p: 1,
                ls: 1,
                bs: 1
            },
            Se = {
                c: 1,
                b: 1,
                fc: 1,
                fbc: 1
            },
            Te = {
                hi: 1,
                bg: 1,
                ci: 1,
                du: 1,
                fb: 1,
                a1: 1,
                a2: 1,
                re: 1,
                tc: 1,
                a: 1
            },
            Ee = {
                gh: 1
            },
            ke = {
                pos: 1
            },
            Fe = {
                x: 1,
                y: 1,
                off: 1,
                lat: 1,
                lng: 1
            },
            We = {
                ls: {
                    solid: !0,
                    dashed: !0,
                    dotted: !0
                },
                bs: {
                    solid: !0,
                    dashed: !0
                }
            },
            Ne = ["ha0", "ha1", "ha2", "ha3", "ha4", "ha5", "ha6", "ha7", "ha8", "ha9"];
        ge.bothEndsShown = function(e) {
            return !(he[e.id1].hi || he[e.id2].hi)
        }, ge.ensureBackgroundConsistency = function() {
            Z("bg", !0)
        }, ge.resurrect = function(e, t) {
            E(e, "gh", t)
        }, ge.ghost = function(e) {
            F(e, "gh")
        }, ge.show = function(e, t) {
            E(e, "hi", t), He()
        }, ge.hidden = function() {
            return k("hi")
        }, ge.hide = function(e) {
            F(e, "hi"), He()
        }, ge.each = function(e, t) {
            if (e.type.match(/^(node|all)$/))
                for (var n in he) t(he[n]);
            if (e.type.match(/^(link|all)$/))
                for (var r in ve) t(ve[r])
        }, ge.setItem = function(e) {
            Ze(e);
            var t = null;
            return "node" === e.type && (t = he), "link" !== e.type || (t = ve, C(e)) ? (u(e), e.id && (t[e.id] = de.clone(e), e.hi ? L() : U(), He(), Me(e.id)), N()) : !1
        };
        var Oe = ge.type = function() {
            return "LinkChart"
        };
        ge.setOptions = function(e) {
            le = e.marqueeLinkSelection, t.setOptions(e), n.setOptions(e)
        };
        var Ve;
        ge.setLastHoverId = function(e) {
            Ve = e
        }, ge.showFullLabelOnHover = function(e) {
            Ve && Me(Ve), e ? (Me(e), n.setSpecialOptions("showFullLabel", e)) : n.setSpecialOptions("showFullLabel", null)
        }, ge.serialize = function() {
            return {
                type: Oe(),
                items: l()
            }
        };
        var Ue = ge.applyChanges = function(e, t) {
            var n, r, a, o, u, s, l;
            for (n = 0; n < e.length; n++) {
                if (r = de.rawId(e[n].id))
                    if (s = [], t) {
                        l = new RegExp(r, "im");
                        for (a in he) l.test(a) && s.push(he[a]);
                        for (o in ve) l.test(o) && s.push(ve[o])
                    } else he[r] && s.push(he[r]), ve[r] && s.push(ve[r]);
                var f = de.clone(e[n]);
                for (delete f.id, delete f.type, u = 0; u < s.length; u++) {
                    var d = !0,
                        g = s[u];
                    for (var h in f) g[h] = f[h], "x" !== h && "y" !== h && (d = !1);
                    c(g), d ? i(g.id) : Me(g.id)
                }
            }
        };
        ge.unsafeApplyChanges = function(e) {
            var t, n, r, a;
            for (t = 0; t < e.length; t++)
                for (n = de.rawId(e[t].id), n && (a = [], he[n] && a.push(he[n]), ve[n] && a.push(ve[n])), r = 0; r < a.length; r++) {
                    var o = !0,
                        u = a[r];
                    de.merge(u, e[t]);
                    for (var s in e[t]) "id" !== s && "x" !== s && "y" !== s && (o = !1);
                    c(u), o ? i(u.id) : Me(u.id)
                }
        }, ge.setProperties = function(e, t) {
            e = de.ensureArray(e), Le(e), Ue(e, t), L(), He();
            var n = N();
            return n
        }, ge.merge = function(e) {
            Le(e);
            var t = s(e, !0);
            t.links = B(t.links, t.nodes);
            for (var n in t.nodes) he[n] = de.merge(he[n], t.nodes[n]), Me(n);
            for (var r in t.links) ve[r] = de.merge(ve[r], t.links[r]), Me(r);
            He();
            var a = W(t.links);
            qe(a), L();
            var i = N();
            return i
        }, ge.getItem = function(e) {
            var t = Ke(e);
            return de.clone(t, !0)
        };
        var Ke = ge.getByID = function(e) {
                return e = de.rawId(e), he[e] || ve[e]
            },
            Xe = {};
        ge.imageList = function(e) {
            var t = {
                updated: !1,
                _coList: {}
            };
            for (var n in Xe) t[e + n] = 1;
            return t
        };
        var He = ge.clearGraphCache = function() {
            we && we.clearCache()
        };
        ge.generate = function(e, t, r, i, u) {
            var s = r.getUnzoom();
            if (s !== se && Ae(), o()) {
                X(), H();
                var l = n.generate(oe, ue, pe, ye, e, t, r, i, u, je);
                a(l), se = s
            }
            return n.generateHandles(e, t, r, Pe), n.animated()
        }, ge.contains = function(e) {
            return n.contains(e, he)
        }, ge.animated = function() {
            return n.animated()
        };
        var Ye = {},
            Pe = [];
        ge.setSelection = function(e, t, n) {
            e = de.rawId(e), t || n || Je(e), Ye[e] ? t && _e(e) : ze(e)
        };
        var ze = ge.addToSelection = function(t, n) {
                t = de.rawId(t), t && (Ye[t] || S(t) && (Ye[t] = 1, Pe.push(t), Me(t), n || e.trigger("selectionchange")))
            },
            _e = ge.removeFromSelection = function(t, n) {
                if (t = de.rawId(t), Ye[t]) {
                    Me(t), delete Ye[t];
                    var r = de.indexOf(Pe, t);
                    r > -1 && Pe.splice(r, 1), n || e.trigger("selectionchange")
                }
            };
        ge.hasSelection = function() {
            return Pe.length > 0
        };
        var je = ge.isSelected = function(e) {
                return e = de.rawId(e), e in Ye
            },
            Je = ge.clearSelection = function(t) {
                for (var n = 0; n < Pe.length; n++) Me(Pe[n]);
                Ye = {}, Pe = [], t || e.trigger("selectionchange")
            };
        ge.selectAll = function() {
            P(he), P(ve), e.trigger("selectionchange")
        };
        var Qe = ge.doSelection = function(e) {
            if (e) {
                Je(!0);
                for (var t = 0; t < e.length; t++) ze(e[t], !0)
            }
            return de.clone(Pe)
        };
        ge.privateSelection = function() {
            return Ye
        }, ge.moveSelection = function(e, t) {
            var n = [];
            if (Ye) {
                for (var r in he) je(r) && n.push(r);
                for (var a in ve) je(a) && (he[ve[a].id1].du && n.push(ve[a].id1), he[ve[a].id2].du && n.push(ve[a].id2))
            }
            for (var o = 0; o < n.length; o++) {
                var u = he[n[o]];
                u.x += e, u.y += t, i(u.id)
            }
        }, ge.removeItem = function(e) {
            var t;
            e = de.ensureArray(e);
            for (var n = {}, r = {}, a = 0; a < e.length; a++) t = e[a], ve[t] && (n[t] = ve[t]), he[t] && (r[t] = 1);
            for (t in ve)(r[ve[t].id1] || r[ve[t].id2]) && (n[t] = ve[t]);
            for (t in n) _e(t, !0), Me(t), delete ve[t];
            for (t in r) _e(t, !0), Me(t), delete he[t];
            He(), qe(n), U()
        }, ge.selectionPlusDummyNodes = function() {
            for (var e = Qe(), t = 0; t < e.length; t++) {
                var n = ve[e[t]];
                n && (he[n.id1].du && e.push(n.id1), he[n.id2].du && e.push(n.id2))
            }
            return e
        };
        var qe = ge.resetConnectionOffsets = function(e) {
                j(e, ee)
            },
            $e = function(e, t) {
                var n;
                return j(e, function(e, r) {
                    n = te(e, r, t)
                }), n
            };
        ge.showHideBendLinks = function(e) {
            var t = J(e);
            qe(t)
        }, ge.showHideGetLinksToBend = function(e, t) {
            for (var n = J(e), r = $e(n, t), a = [], i = 0; i < r.length; i++) {
                var o = r[i];
                ve[o.id].off !== o.off && a.push(o)
            }
            return a
        }, ge.getCursor = function(e, t) {
            var n = ie(e);
            return n ? n : e ? 'url("' + t + 'openhand.cur"), pointer' : "auto"
        }, ge.getResizePrimitive = function() {
            return n.getResizePrimitive()
        };
        var et = new RegExp(Ie + "((\\S+)(?:\\-resize))$"),
            tt = {
                nw: ["x1", "y1"],
                n: ["y1"],
                ne: ["x2", "y1"],
                e: ["x2"],
                se: ["x2", "y2"],
                s: ["y2"],
                sw: ["x1", "y2"],
                w: ["x1"]
            },
            nt = new RegExp(Ie + "(id[1|2])$");
        ge.startDragger = function(e, n, r, a, i, o) {
            var u = (o + "").match(nt),
                s = de.rawId(r);
            return t.createLinkEndDragger(e, ge, n, he, ve[s], u[1], a, i, s)
        }, ge.createDragger = function(n, a, i, o, u, s, l, f) {
            var c = l.handMode,
                d = de.rawId(o),
                g = re(o);
            if (g) {
                var h = ae(o),
                    v = f;
                return t.primitiveMover(ce, a, i, d, v, !0, h, u, s, function(t) {
                    e.trigger("prechange", "resize");
                    var n = he[de.rawId(o)],
                        r = t.a,
                        a = r ? r.x : 0,
                        i = r ? r.y : 0;
                    if ("C" === t.p) n.x = a + t.x, n.y = i + t.y, n.w = n.h = 2 * t.r;
                    else {
                        var u = a + t.x1,
                            s = i + t.y1,
                            l = a + t.x2,
                            f = i + t.y2;
                        n.x = u + Math.floor((l - u) / 2), n.y = s + Math.floor((f - s) / 2), n.w = l - u, n.h = f - s
                    }
                    Me(n.id)
                })
            }
            if (null !== d && ve[d] && "h" !== ve[d].st) {
                var m = (o + "").match(nt);
                return m ? t.createLinkEndDragger(n, ge, i, he, ve[d], m[1], u, s, d) : t.createLinkDragger(r, a, i, ge, ve[d], u, s, d)
            }
            return c && null === o ? t.createHandDragger(i, u, s, Je) : null !== d ? (X(), t.createDefaultDragger(ge, oe, ue, je, i, o, u, s)) : rt(i, u, s)
        };
        var rt = function(t, n, r) {
            function a(e, n) {
                l.x1 = Math.min(t.worldToViewX(f), e), l.y1 = Math.min(t.worldToViewY(c), n), l.x2 = Math.max(t.worldToViewX(f), e), l.y2 = Math.max(t.worldToViewY(c), n), d.x1 = t.viewToWorldX(l.x1), d.y1 = t.viewToWorldY(l.y1), d.x2 = t.viewToWorldX(l.x2), d.y2 = t.viewToWorldY(l.y2)
            }

            function i(e, t, n) {
                for (var r in e) e[r] ? ze(r, !0) : t || n ? u[r] || _e(r, !0) : _e(r, !0)
            }

            function o(e, t, n, r) {
                X(), a(e, t);
                var o = {};
                for (var u in oe) o[u] = ce.inside(d, oe[u].x, oe[u].y);
                if ("off" !== le)
                    for (var s in ue)
                        if ("centre" === le) o[s] = ce.inside(d, ue[s]._x, ue[s]._y);
                        else if ("ends" === le) {
                    var l = oe[ue[s].id1],
                        f = oe[ue[s].id2];
                    o[s] = ce.inside(d, l.x, l.y) && ce.inside(d, f.x, f.y)
                }
                i(o, n, r)
            }
            if (!e.trigger("dragstart", "area", null, t.unscale(n), t.unscale(r), null)) {
                var u = {};
                for (var s in Ye) u[s] = 1;
                var l = {
                        p: ce.primitives.rect,
                        x1: n,
                        y1: r,
                        x2: n,
                        y2: r,
                        bc: ce.colours.midgrey,
                        bw: 1,
                        fc: ce.colours.blue,
                        fi: !1
                    },
                    f = t.viewToWorldX(n),
                    c = t.viewToWorldY(r),
                    d = {
                        x1: f,
                        y1: c,
                        x2: f,
                        y2: c
                    };
                l.wbc = ce.webglColour(l.bc), l.wfc = ce.webglColour(l.fc);
                var g = {
                    scrollable: !0,
                    getCursor: function() {
                        return "crosshair"
                    },
                    generate: function(e) {
                        var t = e.getLayer();
                        e.setLayer(ce.layers.HANDLES), e.copyPrimitive(l), e.setLayer(t)
                    },
                    dragMove: function(e, t, n, r) {
                        o(e, t, n, r)
                    },
                    endDrag: function(n, r, a) {
                        t.cancelScroll();
                        var i = e.trigger("dragend", "area", null, t.unscale(r), t.unscale(a));
                        i || (n ? e.trigger("selectionchange") : Je()), e.trigger("dragcomplete", "area", null, t.unscale(r), t.unscale(a))
                    }
                };
                return g
            }
        };
        return ge.createGestureDragger = function(t, n, r) {
            function a(e, n, r) {
                var a = 2 * -e * Math.PI / 360;
                a *= 2;
                var i = Math.sin(a),
                    o = Math.cos(a);
                for (var u in he) {
                    var s = (d[u].x - f) * o + (d[u].y - c) * i + f,
                        l = -(d[u].x - f) * i + (d[u].y - c) * o + c;
                    De(u, s, l)
                }
                var g = t.viewToWorldX(n),
                    h = t.viewToWorldY(r);
                for (var v in he) De(v, he[v].x + g - f, he[v].y + h - c)
            }

            function i(e) {
                e = Math.pow(e, 2), t.zoomToPosition(e * l, n, r)
            }

            function o(e, t, n, r) {
                h = e, v = t, m = n, x = r
            }

            function u() {
                i(h), y = y || Math.abs(v) > 12, b = b || Math.abs(m - f) > p || Math.abs(x - c) > p, a(y ? v : 0, b ? m : f, b ? x : c)
            }

            function s() {
                for (var e in he) De(e, d[e].x, d[e].y)
            }
            var l = t.zoom(),
                f = t.viewToWorldX(n),
                c = t.viewToWorldY(r),
                d = {};
            for (var g in he) d[g] = {
                x: he[g].x,
                y: he[g].y
            };
            var h, v, m, x;
            o(1, 0, f, c);
            var y = !1,
                p = 60,
                b = !1,
                I = {
                    animate: function() {
                        u()
                    },
                    getCursor: function() {
                        return "move"
                    },
                    dragMove: function(e, t, n, r) {
                        o(e, t, n, r)
                    },
                    endDrag: function(t, n, r, a, i) {
                        o(n, r, a, i), t ? (s(), e.trigger("prechange", "move"), u()) : s()
                    }
                };
            return I
        }, ge.labelPosition = function(e, t) {
            var r = Ke(e);
            return n.labelPosition(r, t)
        }, ge.extractStructure = function(e, t, r) {
            return t.straighten && Q(), q(), Be.extractStructure(n, e, he, ve, Ce, t.fixed, r)
        }, ge.layout = function(e, t, n, r, a, i) {
            Be.layout(e, t, n, Ce, r, a, i)
        }, ge.arrange = function(e, t, n, r, a, i) {
            Be.arrange(e, t, n, r, a, i)
        }, ge.applyVertices = function(e) {
            for (var t in he) e[t] && De(t, e[t].x, e[t].y)
        }, ge.makeAnimator = function(e, t, n) {
            var r = t,
                a = {};
            for (var i in he) a[i] = {
                x: he[i].x,
                y: he[i].y
            };
            return fe.buildAnimatorInstance(function(i) {
                r -= i;
                var o = n(Math.max(0, (t - r) / t));
                for (var u in he)
                    if (e[u]) {
                        var s = a[u].x + o * (e[u].x - a[u].x),
                            l = a[u].y + o * (e[u].y - a[u].y);
                        De(u, s, l)
                    }
                return r > 0
            }, {
                all: !0
            })
        }, ge
    }
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = KeyLines.WebGL.Utils = {};
        e.create3DContext = function(e, t) {
            for (var n = ["webgl", "webgl2", "experimental-webgl", "webkit-3d", "moz-webgl"], r = null, a = 0; a < n.length; ++a) {
                try {
                    r = e.getContext(n[a], t)
                } catch (i) {}
                if (r) break
            }
            return r
        }, e.createProgram = function(e, r, a) {
            var i = t(e, r, e.VERTEX_SHADER),
                o = t(e, a, e.FRAGMENT_SHADER);
            return n(e, [i, o])
        }, e.createMatrix = function() {
            var e = {},
                t = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]),
                n = 1;
            return e.zoom = function(e) {
                n = e, t[0] *= e, t[4] *= e, t[8] *= 1, t[1] *= e, t[5] *= e, t[9] *= 1, t[2] *= e, t[6] *= e, t[10] *= 1, t[3] *= e, t[7] *= e, t[11] *= 1
            }, e.getZoomLevel = function() {
                return n
            }, e.pan = function(e, n) {
                t[12] += t[0] * e + t[4] * n, t[13] += t[1] * e + t[5] * n, t[14] += t[2] * e + t[6] * n, t[15] += t[3] * e + t[7] * n
            }, e.get = function() {
                return t
            }, e
        };
        var t = function(e, t, n) {
                var r = e.createShader(n);
                e.shaderSource(r, t), e.compileShader(r);
                var a = e.getShaderParameter(r, e.COMPILE_STATUS);
                if (!a) {
                    var i = e.getShaderInfoLog(r);
                    throw e.deleteShader(r), new Error("Error in shader: " + i)
                }
                return r
            },
            n = function(e, t) {
                for (var n = e.createProgram(), r = 0; r < t.length; ++r) e.attachShader(n, t[r]);
                e.linkProgram(n);
                var a = e.getProgramParameter(n, e.LINK_STATUS);
                if (!a) {
                    var i = e.getProgramInfoLog(n);
                    throw e.deleteProgram(n), new Error("Error in program linking:" + i)
                }
                return n
            }
    }
}(),
function() {
    var e = KeyLines.TimeBar.Model = {};
    e.createModel = function(e, t) {
        function n() {
            e.trigger("redraw")
        }

        function r(e, t, r, a) {
            N.t1 = e, N.t2 = t, O.t1 = r, O.t2 = a, Z && (i(), n()), ne = !0
        }

        function a(e) {
            var n = _[e.units] * e.by,
                r = n / R;
            return r >= t.histogram.minwidth
        }

        function i() {
            R = (O.t2 - O.t1) / (Z.x2 - Z.x1);
            var e = l(R);
            for (H.major = k[e].major, H.minor = k[e].minor; e > 0 && a(k[e - 1].minor);) e--;
            H.histogram = k[e].minor
        }

        function o() {
            z = null;
            for (var e = 0; e < P.length; e++) Y[e] = null, P[e] = null;
            Y.length = 0, P.length = 0
        }

        function u(e) {
            z = e;
            var n, r;
            for (n = 0; n < e.length; n++) {
                var a = e[n].index || 0;
                if (a < t.selection.maxNumber && e[n].id) {
                    var i = S.ensureArray(e[n].id);
                    if (i.length) {
                        P[a] = S.makeIdMap(i), Y[a] = [];
                        var o = 0;
                        for (r = 0; r < F.length; r++) Y[a][r] = o, F[r].id in P[a] && (o += F[r].v);
                        Y[a][r] = o, s(a, e[n].c)
                    } else !i.length && Y[a] && (Y[a] = null, P[a] = null)
                }
            }
        }

        function s(e, n) {
            var r = {
                    colour0: "rgb(114, 179, 0)",
                    colour1: "rgb(255, 153, 0)",
                    colour2: "rgb(178, 38, 9)"
                },
                a = r["colour" + e];
            n && T.validatergb(n) && (a = n), t.selection["colour" + e] = a
        }

        function l(e) {
            for (var t = 0; k[t].max < e;) t++;
            return t
        }

        function f(e) {
            for (var t = 0, n = F.length; n > t;) {
                var r = t + n >>> 1;
                F[r].t < e ? t = r + 1 : n = r
            }
            return t
        }

        function c(e, t) {
            for (var n = f(e), r = f(t), a = {}, i = n; r > i; i++) a[F[i].id] || (a[F[i].id] = 1);
            return S.flattenMap(a)
        }

        function d(e, t, n) {
            for (var r = f(e), a = f(t), i = r; a > i; i++) {
                var o = F[i].id;
                V[o] = (V[o] || 0) + n
            }
        }

        function g() {
            var e = N.t1,
                t = N.t2,
                n = U.t1,
                r = U.t2;
            n > e ? d(e, Math.min(n, t), 1) : e > n && d(n, Math.min(e, r), -1), t > r ? d(Math.max(e, r), t, 1) : r > t && d(Math.max(n, t), r, -1), U.t1 = e, U.t2 = t
        }

        function h() {
            var e = $(),
                n = e ? 2 * (e.t2 - e.t1) : K.max,
                r = K.min;
            return t.options.minRange && (r = Math.max(r, t.options.minRange)), n = Math.max(n, Math.floor(4 * r)), r = Math.min(r, Math.floor(n / 4)), {
                max: n,
                min: r
            }
        }

        function v() {
            var e = 0,
                n = D,
                r = t.options.showControlBar ? t.controlbar.height : 0,
                a = t.options.scale.showMinor ? t.bars.minor.height : 0,
                i = t.options.scale.showMajor ? t.bars.major.height : 0,
                o = L - r,
                u = o - i,
                s = u - a,
                l = s - t.histogram.dy,
                f = l - t.histogram.topdy,
                c = f * t.selection.reducingFactor;
            Z = {
                bars: {
                    major: {
                        y1: u,
                        y2: o
                    },
                    minor: {
                        y1: s,
                        y2: u
                    }
                },
                histogram: {
                    ybase: l,
                    maxbarh: f,
                    maxselh: c
                },
                x1: e,
                y1: s,
                x2: n,
                y2: o,
                width: D,
                height: L
            }
        }

        function m() {
            var e, n, r, a = t.selection.maxNumber,
                i = H.histogram,
                o = E.clear(O.t1, i.units, i.by),
                u = f(o),
                s = 0,
                l = 0,
                c = 0;
            for (n = 0; a > n; n++) te.hasSel[n] = !!Y[n], te.selvalues[n] = te.selvalues[n] || [];
            do {
                r = o > O.t2, te.x[s] = J(o), o = E.add(o, i.units, i.by), e = f(o);
                var d = W[e] - W[u];
                for (te.value[s] = d, d > l && !r && (l = d), n = 0; a > n; n++) {
                    var g = Y[n];
                    if (g) {
                        var h = g[e] - g[u];
                        te.selvalues[n][s] = h, h > c && !r && (c = h)
                    }
                }
                s++, u = e
            } while (!r);
            te.count = s - 1, te.maxvalue = l, te.maxselvalue = c, ne = !1
        }

        function x(t) {
            var n = $();
            n && N.t1 >= n.t1 && t <= n.t1 && e.trigger("start", "data")
        }

        function y(t) {
            var n = $();
            n && N.t2 <= n.t2 && t >= n.t2 && e.trigger("end", "data")
        }

        function p(e) {
            var t = [];
            e = S.ensureArray(e);
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                if (r.id && r.dt)
                    for (var a = r.id + "", i = S.ensureArray(r.dt), o = S.ensureArray(r.v), u = 0; u < i.length; u++) {
                        var s = j(i[u]);
                        if (!S.isNumber(s)) throw new Error("load: invalid date in dt property. Value was: " + i[u]);
                        var l = o[u] || 1;
                        if (!S.isNumber(l)) throw new Error("load: invalid value in v property - must be numeric. Value was: " + l);
                        t.push({
                            id: a,
                            t: s,
                            v: l
                        })
                    }
            }
            return t
        }

        function b() {
            W = [0], V = {}, ne = !0;
            for (var t = 0; t < F.length; t++) {
                var r = F[t];
                r.t >= N.t1 && r.t < N.t2 && (V[r.id] = (V[r.id] || 0) + 1), W[t + 1] = W[t] + r.v
            }
            U.t1 = N.t1, U.t2 = N.t2, e.trigger("datachange"), n()
        }

        function I(e) {
            S.numberSort(e, "t")
        }

        function w(e, t) {
            return e.id === t.id ? e.v - t.v : e.id > t.id ? 1 : -1
        }

        function B(e, t, n) {
            t.sort(w), n.sort(w);
            for (var r = 0, a = 0; r < t.length && a < n.length;) {
                var i = w(t[r], n[a]);
                0 > i ? (e.push(t[r]), r++) : i > 0 ? (e.push(n[a]), a++) : (e.push(t[r]), r++, a++)
            }
            for (; r < t.length;) e.push(t[r]), r++;
            for (; a < n.length;) e.push(n[a]), a++
        }

        function C(e, t) {
            for (var n = [], r = 0, a = 0; r < e.length && a < t.length;) {
                var i = e[r].t,
                    o = t[a].t;
                if (o > i) n.push(e[r]), r++;
                else if (i > o) n.push(t[a]), a++;
                else {
                    var u = r,
                        s = a;
                    do r++; while (r < e.length && e[r].t === o);
                    do a++; while (a < t.length && t[a].t === o);
                    r === u + 1 && a === s + 1 ? (n.push(e[u]), e[u].id === t[s].id && e[u].v === t[s].v || n.push(t[s])) : B(n, e.slice(u, r), t.slice(s, a))
                }
            }
            for (; r < e.length;) n.push(e[r]), r++;
            for (; a < t.length;) n.push(t[a]), a++;
            return n
        }

        function A(e, t) {
            var n = Math.floor((e.t2 - e.t1) * t);
            return {
                t1: e.t1 - n,
                t2: e.t2 + n
            }
        }

        function M() {
            var e = N.t1,
                t = N.t2;
            ie(N.t1, N.t2), e === N.t1 && t === N.t2 || oe()
        }
        var D, L, Z, R, G = {},
            S = KeyLines.Util,
            T = KeyLines.Rendering,
            E = KeyLines.DateTime,
            k = t.rates,
            F = [],
            W = [],
            N = {
                t1: 0,
                t2: 0
            },
            O = {
                t1: 0,
                t2: 0
            },
            V = {},
            U = {
                t1: 0,
                t2: 0
            },
            K = {
                max: 315576e7,
                min: 3e3
            },
            X = 250,
            H = {},
            Y = [],
            P = [],
            z = null,
            _ = [1, 1e3, 6e4, 36e5, 864e5, 6048e5, 2592e6, 31536e6],
            j = G.extractUTC = function(e) {
                var t;
                return e instanceof Date ? t = E.UTCify(e) : "number" == typeof e && (t = e), t
            },
            J = G.ttox = function(e) {
                return Z.x1 + (e - O.t1) / R
            },
            Q = G.xtot = function(e) {
                return O.t1 + (e - Z.x1) * R
            },
            q = G.getClickedBar = function(e) {
                return e >= Z.bars.major.y1 ? "major" : e >= Z.bars.minor.y1 ? "minor" : "histogram"
            };
        G.findRange = function(e, t, n) {
            var r, a = n || q(t),
                i = Q(e);
            return ee(a, function(e, t) {
                return t > i ? (r = {
                    t1: e,
                    t2: t,
                    bar: a
                }, !0) : void 0
            }), r
        };
        var $ = G.dataRange = function() {
                if (F.length > 0) {
                    var e = F[0].t,
                        t = F[F.length - 1].t + 1;
                    return {
                        t1: e,
                        t2: t
                    }
                }
            },
            ee = G.eachTick = function(e, t) {
                var n, r = H[e],
                    a = E.clear(O.t1, r.units, r.by),
                    i = !1;
                do n = E.add(a, r.units, r.by), i = t(a, n, J(a), J(n), r.f), a = n; while (!i && a <= O.t2)
            },
            te = {
                count: 0,
                x: [],
                value: [],
                hasSel: [],
                selvalues: [],
                maxvalue: 0,
                maxselvalue: 0
            },
            ne = !0;
        G.getHistogram = function() {
            return ne && m(), te
        }, G.getBounds = function() {
            return Z
        }, G.getInnerRange = function() {
            return N
        }, G.getOuterRange = function() {
            return O
        }, G.getTimeRate = function() {
            return R
        }, G.nextPage = function(e) {
            var t = N.t1,
                n = N.t2,
                r = "back" === e ? -1 : 1,
                a = E.units.month;
            if (t === E.clear(t, a, 1) && n === E.clear(n, a, 1)) {
                var i = new Date(t),
                    o = new Date(n),
                    u = 12 * (o.getUTCFullYear() - i.getUTCFullYear()) + o.getUTCMonth() - i.getUTCMonth();
                t = E.add(t, a, r * u), n = E.add(n, a, r * u)
            } else {
                var s = r * (n - t);
                t += s, n += s
            }
            return {
                t1: t,
                t2: n
            }
        }, G.getItems = function() {
            return F
        }, G.setInnerRangeSide = function(e, t) {
            "t1" === e ? x(t) : y(t), N[e] = t
        }, G.getSideLimits = function(e) {
            var n, r;
            if ("t1" === e ? (n = O.t1, r = N.t2) : (n = N.t1, r = O.t2), "fixed" === t.options.sliders) {
                var a = r - n,
                    i = Math.floor(.1 * a);
                n += i, r -= i
            }
            var o = h();
            "t1" === e ? (n = Math.max(n, N.t2 - o.max), r = Math.min(r, N.t2 - o.min)) : (n = Math.max(n, N.t1 + o.min), r = Math.min(r, N.t1 + o.max));
            var u = $();
            return u && ("t1" === e ? r = Math.min(r, u.t2) : n = Math.max(n, u.t1)), {
                min: n,
                max: r
            }
        };
        var re = G.hasItems = function() {
                return F.length > 0
            },
            ae = G.limitInnerRange = function(e, t, n) {
                var r, a = $(),
                    i = n ? (e + t) / 2 : (N.t1 + N.t2) / 2,
                    o = h(),
                    u = t - e;
                return u > o.max ? (e = Math.ceil(i - o.max / 2), t = Math.floor(i + o.max / 2)) : u < o.min && (e = Math.floor(i - o.min / 2), t = Math.ceil(i + o.min / 2)), a && (t < a.t1 ? (r = a.t1 - t, e += r, t += r) : e > a.t2 && (r = e - a.t2, e -= r, t -= r)), {
                    t1: e,
                    t2: t
                }
            };
        G.inMainBarOrHistogram = function(e, t) {
            return e >= Z.x1 && e <= Z.x2 && t >= 0 && t <= Z.y2
        }, G.load = function(e, t) {
            var n = (e || {}).items,
                r = p(n);
            I(r), F = r, o(), b(), S.invoke(t)
        }, G.merge = function(e, t) {
            var n = p(e);
            n.length > 0 && (I(n), F = 0 === F.length ? n : n[0].t > F[F.length - 1].t ? F.concat(n) : n[n.length - 1].t < F[0].t ? n.concat(F) : C(F, n), z && u(z), b()), S.invoke(t)
        }, G.offsetRanges = function(e, t) {
            var n = N.t1,
                r = N.t2;
            return ie(n + (t ? 0 : e), r + e), n !== N.t1 || r !== N.t2
        }, G.selection = function(e) {
            e = S.isNullOrUndefined(e) ? e : S.ensureArray(e), e && (e.length ? u(e) : o()), ne = !0, n();
            for (var t = [], r = 0; r < P.length; r++) t[r] = P[r] ? S.flattenMap(P[r]) : null;
            return t
        };
        var ie = G.setInner = function(n, a) {
            var i = ae(n, a),
                o = $();
            if (o && N.t1 && N.t2) {
                var u = ae(n + 1, a + 1),
                    s = ae(n - 1, a - 1);
                x(n), i.t1 < N.t1 && s.t1 === i.t1 && e.trigger("start", "range"), y(a), i.t2 > N.t2 && u.t2 === i.t2 && e.trigger("end", "range")
            }
            var l;
            if ("none" === t.options.sliders) l = i;
            else if ("free" === t.options.sliders)
                if (re()) {
                    var f = h();
                    o.t2 - o.t1 < f.min && (o.t1 = o.t1 - f.min / 2, o.t2 = o.t2 + f.min / 2), l = A(o, t.freeSideFactor)
                } else l = O;
            else l = A(i, t.fixedSideFactor);
            r(i.t1, i.t2, l.t1, l.t2)
        };
        G.setOuter = function(e, t) {
            r(N.t1, N.t2, e, t)
        }, G.updateOptions = function() {
            M(), v()
        }, G.setSize = function(e, t) {
            D = e, L = t, v()
        };
        var oe = G.triggerTimeBarChanged = S.ratelimit(function() {
            e.trigger("change")
        }, X);
        return G.getIdcounts = function() {
            return U.t1 === N.t1 && U.t2 === N.t2 || g(), V
        }, G.getRangeForIds = function(e) {
            e = S.ensureArray(e);
            var t, n, r = 1 / 0,
                a = -(1 / 0),
                i = {};
            for (t = 0; t < e.length; t++) i[e[t]] = !0;
            for (t = 0; t < F.length; t++) n = F[t], i[n.id] && (n.t < r && (r = n.t), n.t > a && (a = n.t));
            return r !== 1 / 0 ? {
                t1: r,
                t2: a
            } : void 0
        }, G.getIds = function(e, t) {
            if (S.isNullOrUndefined(e) || S.isNullOrUndefined(t)) throw new Error("getIds error: both dt1 and dt2 dates must be passed.");
            var n = j(e),
                r = j(t);
            return c(n, r)
        }, G
    }
}(),
function() {
    var e = KeyLines.Layouts = KeyLines.Layouts || {};
    e.create = function() {
        function t(e, t, r, a, i, o, u, s, l) {
            var f = i[u];
            if (!(f in r)) {
                if (!(f in a)) throw "link has end which is not in nodes list";
                var c = a[f];
                if (c.hi) throw "link has end which is hidden";
                r[f] = n(e, t, c, f, s, l)
            }
            return f
        }

        function n(e, t, n, r, a, i) {
            return {
                id: r,
                x: n.x,
                y: n.y,
                size: e.nodeSize(n, t, i),
                fixed: a ? r in a : !1,
                lc: 0,
                d: n.d
            }
        }

        function r(e, t) {
            return e.fixed && !t.fixed ? -1 : t.fixed && !e.fixed ? 1 : 0
        }

        function a(e, r, a, i, o, u, s) {
            for (var l = o().connections({
                    all: !1
                }), f = {}, c = [], d = 0; d < l.length; d++) {
                var g = l[d][0],
                    h = i[g],
                    v = t(e, r, f, a, h, g, "id1", u, s),
                    m = t(e, r, f, a, h, g, "id2", u, s);
                null !== v && null !== m && (c.push({
                    id1: v,
                    id2: m,
                    id: g
                }), f[v].lc += 1, f[m].lc += 1)
            }
            var x;
            for (x in a)
                if (x += "", !a[x].hi && !f[x]) {
                    var y = n(e, r, a[x], x, u, s);
                    f[x] = y
                }
            var p = 0;
            for (x in f) p++;
            return {
                v: f,
                e: c,
                vcount: p
            }
        }

        function i(e, t) {
            var n, r, a, i, u = 0,
                s = [];
            for (n in e)
                if (n += "", !e[n].hasOwnProperty("c")) {
                    var l = u++;
                    for (s.push(n + ""); s.length > 0;) {
                        var f = s.shift();
                        if (!e[f].hasOwnProperty("c"))
                            for (e[f].c = l, r = 0; r < t.length; r++) a = t[r].id1 + "", i = t[r].id2 + "", a === f && (t[r].c = l, s.push(i)), i === f && (t[r].c = l, s.push(a))
                    }
                }
            for (var c = new Array(u), d = 0; d < c.length; d++) c[d] = {
                verts: {},
                edges: []
            };
            for (n in e) n += "", c[e[n].c].verts[n] = e[n];
            for (r = 0; r < t.length; r++) c[t[r].c].edges.push(t[r]);
            for (var g = 0; g < c.length; g++) c[g].extents = o(c[g].verts);
            return c
        }

        function o(e) {
            var t, n, r, a, i = 0;
            for (var o in e) {
                var u = e[o + ""];
                t = t ? Math.min(t, u.x - u.size) : u.x - u.size, n = n ? Math.min(n, u.y - u.size) : u.y - u.size, r = r ? Math.max(r, u.x + u.size) : u.x + u.size, a = a ? Math.max(a, u.y + u.size) : u.y + u.size, i++
            }
            return {
                x1: t,
                y1: n,
                x2: r,
                y2: a,
                c: i
            }
        }

        function u(e, t, n, r, a) {
            function i(t) {
                var n = t.origId || t.id,
                    r = {
                        id: ae + n + "-" + k++,
                        level: t.level - 1,
                        origId: n,
                        x: 0,
                        y: 0,
                        c: t.c,
                        size: 10,
                        fixed: !1,
                        lc: t.lc,
                        du: !0
                    };
                return e[r.id] = r, a[t.c].verts[r.id] = r, F[r.id] = 1, r
            }

            function o(e, n, r) {
                var i = {
                    id: ae + n.id + "-" + r.id,
                    id1: n.id,
                    id2: r.id,
                    c: e.c
                };
                return "level" in n && "level" in r && (i.level = Math.max(n.level, r.level)), t.push(i), a[e.c].edges.push(i), i
            }

            function u(e, t, n) {
                var r = i(e),
                    a = n.id1 === t.id;
                o(n, a ? r : e, a ? e : r), r.level === t.level + 1 ? o(n, a ? t : r, a ? r : t) : u(r, t, n)
            }
            for (var s, l, f, c = [], d = {}, g = {}, h = {}, v = r(), m = 0; m < a.length; m++) {
                d[m] = {}, g[m] = [];
                var x = [],
                    y = 1 / 0;
                for (l in a[m].verts) l = ne.rawId(l), s = a[m].verts[l], s.d && ne.isNumber(s.d[n]) && (f = s.d[n], d[m][f] || (d[m][f] = [], g[m].push(f)), d[m][f].push(l), h[l] = f, y > f ? (y = f, x = [l]) : f === y && x.push(l));
                if (0 === x.length)
                    for (l in a[m].verts) {
                        l += "", x.push(l), h[l] = 0, d[m][0] = [l], g[m].push(0);
                        break
                    }
                g[m].sort(function(e, t) {
                    return e - t
                });
                var p, b = [];
                for (p = 0; p < g[m].length; p++) {
                    var I = g[m][p];
                    b = [];
                    var w, B, C = v.neighbours(d[m][I]),
                        A = g[m][p + 1] || I + 1,
                        M = (A + I) / 2;
                    for (B = 0; B < C.nodes.length; B++) w = C.nodes[B], ne.isNullOrUndefined(h[w]) && (b.push(w), h[w] = M);
                    b.length && (g[m].splice(p + 1, 0, M), d[m][M] = b)
                }
                for (p = 0; p < g[m].length; p++) d[m][g[m][p]] = p;
                for (p = 0; p < x.length; p++) e[x[p]].level = 0;
                c = c.concat(x)
            }
            for (var D, L, Z, R, G, S, T, E, k = 0, F = {}, W = 0, N = t.length; N > W; W++) {
                D = t[W], L = ne.rawId(D.id1), Z = ne.rawId(D.id2), R = e[L], G = e[Z];
                var O = R.level = d[R.c][h[L]],
                    V = G.level = d[G.c][h[Z]];
                ne.isNumber(O) && ne.isNumber(V) && (D.level = Math.max(O, V), V > O ? (F[G.id] = 1, E = R, T = G) : O > V && (F[R.id] = 1, E = G, T = R)), S = Math.abs(O - V), S > 1 && u(T, E, D)
            }
            var U = ne.makeIdMap(c);
            for (l in e)
                if (l = ne.rawId(l), s = e[l], !F[l] && !U[l]) {
                    for (var K, X = 1 / 0, H = 0; H < c.length; H++) {
                        var Y = v.shortestPaths(l, c[H]);
                        Y.distance < X && (K = Y, X = Y.distance)
                    }
                    for (var P = 0; P < K.onePath.length; P++) {
                        var z = K.onePath[P],
                            _ = e[z];
                        if (_ && "level" in _ && _.level < s.level) {
                            var j = o(s, s, _);
                            s.level - _.level > 1 && u(s, _, j);
                            break
                        }
                    }
                }
            return t
        }

        function s(e, t, n, r) {
            for (var a, i, o, u, s = r(), l = 0; l < n.length; l++) {
                var f = s.distances(n[l]);
                for (var c in f) c += "", e[c].hasOwnProperty("level") || (e[c].level = 1 / 0), e[c].level = Math.min(e[c].level, f[c])
            }
            for (var d = 0; d < t.length; d++) a = t[d].id1 + "", i = t[d].id2 + "", o = e[a], u = e[i], o.hasOwnProperty("level") && u.hasOwnProperty("level") && (t[d].level = Math.max(o.level, u.level))
        }

        function l(e, t) {
            function n(e, t, n, a) {
                r[e] = r[e] || [], r[e][t] = r[e][t] || {}, r[e][t][n] = r[e][t][n] || [], r[e][t][n].push(a)
            }
            var r = {};
            for (var a in e) a += "", e[a].hasOwnProperty("level") && n(e[a].c, e[a].level, "vertices", a);
            for (var i = 0; i < t.length; i++) t[i].hasOwnProperty("level") && n(t[i].c, t[i].level, "edges", t[i]);
            return r
        }

        function f(e, t) {
            for (var n = [], r = 0; r < e.length; r++) t[e[r]] && n.push(e[r]);
            return n
        }

        function c(e) {
            var t = Number(e);
            return t = isNaN(t) ? 5 : t, t = Math.max(0, t), t = Math.min(10, t)
        }

        function d(e) {
            return 10 * (5 - c(e))
        }

        function g(e) {
            var t, n = 0,
                r = 0,
                a = new RegExp("^" + ae);
            for (var i in e) i += "", t = e[i], "level" in t && !a.test(t.id) && (n += t.size, r += 1);
            return n / r
        }

        function h(e) {
            var t = 0;
            for (var n in e) t = Math.max(t, e[n].length);
            return t
        }

        function v(e, t, n) {
            for (var r = [], a = h(e), i = 0; a > i; i++) {
                var o = 0,
                    u = 0,
                    s = 0,
                    l = 0,
                    f = 0;
                for (var c in e)
                    if (e[c][i]) {
                        for (var d = e[c][i].vertices, g = 0; g < d.length; g++) {
                            var v = d[g],
                                m = t[v].size;
                            u += m, o = Math.max(o, m)
                        }
                        l += d.length
                    }
                s = u / l;
                var x = r[i - 1];
                x && o < x.max + x.yOffset && (f = (x.max + x.yOffset) / 2), n && (f = Math.max(o, f)), r[i] = {
                    max: o,
                    average: s,
                    sum: u,
                    yOffset: f
                }
            }
            return r
        }

        function m(e, t, n, r, a, o) {
            function c() {
                var e, t, n, r = 0,
                    a = 0;
                for (t in G)
                    for (var i = G[t].length - 1; i >= 0; i--)
                        for (var o = G[t][i].vertices, u = [], s = 0; s < o.length; s++) {
                            n = o[s] + "";
                            var l = x(G, M, n);
                            if (M[n].children = [], M[n].subChildrenCount = 0, 0 === l.length) r++;
                            else {
                                var f = [];
                                for (e = 0; e < l.length; e++) ne.indexOf(u, l[e]) < 0 && f.push(l[e]);
                                if (f.length > 0)
                                    for (M[n].children = f, M[n].subChildrenCount = h(f), e = 0; e < f.length; e++) u.push(f[e]), M[f[e]].parent = n
                            }
                            a++
                        }
            }

            function h(e) {
                for (var t = 0, n = 0; n < e.length; n++) {
                    var r = M[e[n]].subChildrenCount;
                    t += 0 === r ? 1 : r
                }
                return t
            }

            function m(e, t, n) {
                n || (e.x = t);
                var r = t - e.subChildrenCount * k / 2,
                    a = e.children;
                if (a)
                    for (var i = 0; i < a.length; i++) {
                        var o = M[a[i]],
                            u = o.subChildrenCount;
                        0 === u && (u = 1);
                        var s = r + u * k;
                        m(o, (s + r) / 2, !1), r = s
                    }
            }

            function y(e, t) {
                for (var n, r = t.miny, a = 0; a < G[e].length; a++) {
                    for (var i = G[e][a].vertices, o = E[a].yOffset, u = E[a].average, s = u > S ? u : S, l = r + 2 * (s + 12) + o, f = 0; f < i.length; f++) {
                        n = i[f] + "";
                        var c = M[n];
                        c.y = l
                    }
                    r = l
                }
            }

            function p(e, t) {
                for (var n, r = {
                        up: function(e) {
                            e.y = -e.y, e.x = e.x
                        },
                        down: function(e) {},
                        right: function(e) {
                            var t = e.y;
                            e.y = e.x, e.x = t
                        },
                        left: function(e) {
                            var t = e.y;
                            e.y = e.x, e.x = -t
                        }
                    }, a = r[t] || r.down, i = 0; i < G[e].length; i++)
                    for (var o = G[e][i].vertices, u = 0; u < o.length; u++) n = o[u] + "", a(M[n])
            }
            r = ne.defaults(r, {
                top: [],
                orientation: "down",
                stable: !0
            });
            var b = r.level,
                I = o ? 1 : d(r.tightness),
                w = r.top,
                A = r.tidy,
                M = t.v,
                D = t.e,
                L = t.extents,
                Z = i(M, D);
            b ? D = u(M, D, b, n, Z) : (w = f(w, M), s(M, D, w, n));
            var R = /^(left|right)$/.test(r.orientation),
                G = l(M, D),
                S = g(M),
                E = v(G, M, R),
                k = 2 * S + 24 + I;
            c();
            var F, W = {},
                N = {};
            for (F in G) {
                var O = Number(F);
                W[O] = T(Z[O].verts), N[O] = {
                    children: G[F][0].vertices,
                    subChildrenCount: h(G[F][0].vertices)
                }
            }
            for (F in G) m(N[F], W[F].x, !0), y(F, W[F]);
            for (F in G) o || p(F, r.orientation);
            o && o(M, G, Z, W, S, r.tightness);
            var V = [];
            A ? B(e, M, Z, 0, 1, L, V, r, a) : C(e, M, L, V, r, a)
        }

        function x(e, t, n) {
            var r, a, i, o, u = t[n].c,
                s = t[n].level + 1,
                l = e[u][s],
                f = {},
                c = [];
            if (l) {
                for (i = 0; i < l.edges.length; i++) {
                    var d = l.edges[i];
                    r = d.id1 + "", a = d.id2 + "", r === n && (f[a] = 1), a === n && (f[r] = 1)
                }
                var g = l.vertices;
                for (o = 0; o < g.length; o++) r = g[o] + "", f[r] && c.push(r)
            }
            return c
        }

        function y(e, t, n, r, a, i) {
            function o(e, t) {
                return (e.r + t.r) / (e.a - t.a)
            }

            function u(e, t, n) {
                var r = t > 0 ? e + n : e + n / 2;
                return r > 0 ? r : e
            }

            function s(e, t, n, r) {
                if (r && 1 === e.length) return 0;
                var a = t,
                    i = a;
                for (l = 1; l < e.length; l++) i = Math.max(o(g[e[l]], g[e[l - 1]]), i);
                return u(i, i - a, n)
            }
            var l, f, c, g;
            i = d(i);
            for (var h in t) {
                var v = T(n[Number(h)].verts),
                    m = 0,
                    x = m,
                    y = 4 * a,
                    p = y,
                    b = 2 * a + 24;
                for (f = 0; f < t[h].length; f++) {
                    var I, w = t[h][f].vertices;
                    for (g = [], l = 0; l < w.length; l++) {
                        c = w[l] + "", I = e[c];
                        var B = I.x - v.minx,
                            C = v.maxx - v.minx + b;
                        if (B *= 2 * Math.PI / C, 0 > B || B > 2 * Math.PI) throw new Error("[Radial] Theta angle should be in the range [0; 2*PI]");
                        g[c] = {
                            a: B,
                            r: I.size
                        }
                    }
                    for (x = m, 0 === f ? w.length > 1 && (m += y / 2) : m += y, m = s(w, m, i, 1 > f), m - x > p && (p = m - x + 1, y = Math.max(y, p / 2), 0 === f && w.length < 3 && (m /= 2)), l = 0; l < w.length; l++) I = e[w[l] + ""], I.x = r[Number(h)].x + m * Math.cos(g[I.id].a), I.y = r[Number(h)].y + m * Math.sin(g[I.id].a)
                }
            }
        }

        function p(e, t) {
            var n = e.extents.c,
                r = Math.max(e.extents.x2 - e.extents.x1, e.extents.y2 - e.extents.y1),
                a = 120 * Math.sqrt(n),
                i = Math.max(r, a) / 2,
                o = 0;
            n > 1 && (o = Math.min(Math.pow(n, 2.5), 100));
            var u = o * n * Math.log(n),
                s = 1 / b(t);
            return {
                repulsionFactor: 40 * Math.max(1, Math.pow(s, 2)),
                coeff: 1,
                naturalDistance: 20 * s,
                iterations: o,
                chunk: 20,
                temperature: i,
                temp: i,
                estTime: u
            }
        }

        function b(e) {
            var t = c(e.tightness);
            return Math.pow(2, 2 * (t - 5) / 5)
        }

        function I(e, t, n, r) {
            function a(t) {
                var n = f[t],
                    r = y[t];
                Z(e, n, r, r.start, r.stop, function() {
                    G(n.verts), t++, t < f.length ? ne.nextTick(function() {
                        a(t)
                    }, 0) : o()
                })
            }

            function o() {
                g ? B(e, u, f, x, 1, c, d, n, r) : C(e, u, c, d, n, r)
            }
            var u = t.v,
                s = t.vcount,
                l = t.e,
                f = i(u, l),
                c = t.extents,
                d = n.fixed || [],
                g = n.tidy;
            S(u);
            var h = f.length,
                v = s / (s + h),
                m = 0,
                x = g ? v : 1;
            e.trigger("progress", "layout", 0);
            for (var y = [], b = 0, I = 0; I < f.length; I++) {
                var w = p(f[I], n);
                b += w.estTime, y.push(w)
            }
            var A = 0;
            for (I = 0; I < f.length; I++) {
                y[I].start = A;
                var M = A + (x - m) * y[I].estTime / b;
                y[I].stop = M, A = M
            }
            f.length > 0 ? a(0) : C(e, u, c, d, n, r)
        }

        function w(e) {
            for (var t in e)
                if (t += "", e[t].fixed) return !0;
            return !1
        }

        function B(e, t, n, r, a, i, o, u, s) {
            for (var l = {}, f = [], c = {}, d = 1 / 0, g = 1 / 0, h = 0; h < n.length; h++) {
                var v = E(n[h].verts);
                l[h] = v;
                var m = o.length > 0 && w(n[h].verts);
                f.push({
                    x: v.x,
                    y: v.y,
                    width: v.width,
                    height: v.height,
                    id: h,
                    fixed: m
                }), c[h] = {
                    x: v.x,
                    y: v.y
                }, d = Math.min(d, c[h].x), g = Math.min(g, c[h].y)
            }
            u = ne.defaults(u, {
                strategy: "rect",
                ratio: 4 / 3
            }), J(f, {
                strategy: u.strategy,
                step: 30,
                max: 2,
                sort: !1,
                ratio: u.ratio,
                tightness: u.tightness
            }, function(n) {
                d = "rect" === u.strategy ? d : 0, g = "rect" === u.strategy ? g : 0;
                for (var r = {}, a = 0; a < n.length; a++) r[n[a].id] = n[a], r[n[a].id].x += d, r[n[a].id].y += g;
                for (var l in t) {
                    var f = t[l + ""],
                        h = r[f.c].x - c[f.c].x,
                        v = r[f.c].y - c[f.c].y;
                    f.x += h, f.y += v
                }
                C(e, t, i, o, u, s)
            })
        }

        function C(e, t, n, r, a, i) {
            var u = o(t);
            0 === r.length && (u = R(t, n, u)), isNaN(u.x1) && (u = null), e.trigger("progress", "layout", 1), i({
                vertices: t,
                extents: u
            }, a)
        }

        function A(e, t, n) {
            var r = e[t.id1],
                a = e[t.id2];
            M(r, a, n)
        }

        function M(e, t, n) {
            if (!e.fixed || !t.fixed) {
                var r = 2 * Math.random() * Math.PI,
                    a = (e.x + t.x) / 2,
                    i = (e.y + t.y) / 2,
                    o = 1.3 * k(e, t, n) / 2;
                e.x = a + o * Math.cos(r), e.y = i + o * Math.sin(r), r += Math.PI, t.x = a + o * Math.cos(r), t.y = i + o * Math.sin(r)
            }
        }

        function D(e, t, n, r) {
            var a = {
                    temperature: 10,
                    i: 0
                },
                i = {
                    verts: t.v,
                    edges: t.e
                },
                u = p(t, n);
            u.chunk = u.iterations = 5, a.temperature = Math.pow(u.temperature, .72), L(a, i, u);
            var s = o(i.verts);
            isNaN(s.x1) && (s = null), e.trigger("progress", "layout", 1), r({
                vertices: i.verts,
                extents: s
            }, n, !1)
        }

        function L(e, t, n) {
            for (var r = Math.min(e.i + n.chunk, n.iterations); e.i < r;) {
                var a = t.verts,
                    i = t.edges,
                    o = 0;
                for (var u in a) u += "", a[u].dx = a[u].dy = 0, o++;
                if (1 === o) return void(e.i = r);
                if (2 === o) {
                    if (i[0]) A(a, i[0], n.naturalDistance);
                    else {
                        var s = ne.dictValues(a);
                        M(s[0], s[1], n.naturalDistance)
                    }
                    return void(e.i = r)
                }
                N(a, n.repulsionFactor);
                for (var l = 0; l < i.length; l++) F(i[l], a, n.naturalDistance, n.coeff);
                var f = e.temperature * (1 - e.i / n.iterations);
                for (var c in a) c += "", W(a[c], f);
                e.i++
            }
        }

        function Z(e, t, n, r, a, i) {
            function o() {
                return r + (a - r) * f.i / n.iterations
            }

            function u() {
                return f.i >= n.iterations
            }

            function s() {
                L(f, t, n)
            }

            function l() {
                s(), u() ? i() : (e.trigger("progress", "layout", o()), ne.nextTick(l, 0))
            }
            var f = {
                i: 0,
                temperature: n.temperature
            };
            l()
        }

        function R(e, t, n) {
            var r = (n.x1 + n.x2) / 2 - (t.x1 + t.x2) / 2,
                a = (n.y1 + n.y2) / 2 - (t.y1 + t.y2) / 2;
            for (var i in e) i += "", e[i].x -= r, e[i].y -= a;
            return {
                x1: n.x1 - r,
                y1: n.y1 - a,
                x2: n.x2 - r,
                y2: n.y2 - a
            }
        }

        function G(e) {
            var t, n, r, a, i = 0,
                o = 0,
                u = 0,
                s = !1,
                l = 0,
                f = 0;
            for (a in e) a += "", i += e[a].x, o += e[a].y, u += 1, s |= e[a].fixed;
            if (!(3 > u || s)) {
                i /= u, o /= u;
                for (a in e) a += "", t = Math.sqrt((e[a].x - i) * (e[a].x - i) + (e[a].y - o) * (e[a].y - o)), t > f && (f = t, l = Math.atan2(e[a].y - o, e[a].x - i));
                l = -l;
                var c = Math.sin(l),
                    d = Math.cos(l);
                for (a in e) n = i + (e[a].x - i) * d - (e[a].y - o) * c, r = o + (e[a].x - i) * c + (e[a].y - o) * d, e[a].x = n, e[a].y = r
            }
        }

        function S(e) {
            var t = 30;
            for (var n in e) n += "", e[n].fixed || (e[n].x += t * (Math.random() - .5), e[n].y += t * (Math.random() - .5))
        }

        function T(e) {
            var t, n = 0,
                r = 0,
                a = 0,
                i = 1 / 0,
                o = 1 / 0,
                u = -(1 / 0),
                s = -(1 / 0);
            for (t in e) t += "", n += 1, r += e[t].x, a += e[t].y, o = Math.min(o, e[t].x), i = Math.min(i, e[t].y), u = Math.max(u, e[t].x), s = Math.max(s, e[t].y);
            r /= n, a /= n;
            var l = 0,
                f = !1;
            for (t in e) {
                t += "";
                var c = e[t].x - r,
                    d = e[t].y - a,
                    g = Math.sqrt(c * c + d * d);
                l = Math.max(g + 1.5 * e[t].size, l), f = f || e[t].fixed
            }
            return {
                x: r,
                y: a,
                sum: n,
                size: l,
                fixed: f,
                miny: i,
                minx: o,
                maxy: s,
                maxx: u
            }
        }

        function E(e, t) {
            ne.isNullOrUndefined(t) && (t = {});
            var n, r = 20,
                a = 1 / 0,
                i = 1 / 0,
                o = -(1 / 0),
                u = -(1 / 0);
            for (n in e) n += "", n in t || (i = Math.min(i, e[n].x - e[n].size), a = Math.min(a, e[n].y - e[n].size), o = Math.max(o, e[n].x + e[n].size), u = Math.max(u, e[n].y + e[n].size));
            return i -= r, a -= r, o += r, u += r, {
                x: i,
                y: a,
                width: o - i,
                height: u - a
            }
        }

        function k(e, t, n) {
            return e.size + t.size + n * (1 + ((e.lc ? e.lc : 1) + (t.lc ? t.lc : 1)) / 6)
        }

        function F(e, t, n, r) {
            var a = t[e.id1],
                i = t[e.id2],
                o = a.x - i.x,
                u = a.y - i.y,
                s = k(a, i, n),
                l = ne.len(o, u);
            if (l > .02) {
                var f = r * (l - s) / l,
                    c = o * f,
                    d = u * f;
                a.dx -= c, a.dy -= d, i.dx += c, i.dy += d
            }
        }

        function W(e, t) {
            if (!e.fixed) {
                var n = ne.len(e.dx, e.dy),
                    r = e.dx / n * Math.min(n, t),
                    a = e.dy / n * Math.min(n, t);
                e.x += r, e.y += a
            }
        }

        function N(e, t) {
            function n(e, t, n, r) {
                return {
                    leaf: !0,
                    nodes: {},
                    point: null,
                    x1: e,
                    y1: t,
                    x2: n,
                    y2: r
                }
            }

            function r(e, t) {
                var r = e.x1,
                    i = e.y1,
                    o = e.x2,
                    u = e.y2,
                    s = .5 * (r + o),
                    l = .5 * (i + u),
                    f = t.x >= s,
                    c = t.y >= l,
                    d = (c << 1) + f;
                e.leaf = !1, f ? r = s : o = s, c ? i = l : u = l, e = e.nodes[d] || (e.nodes[d] = n(r, i, o, u)), a(e, t)
            }

            function a(e, t) {
                if (e.leaf) {
                    var n = e.point;
                    n ? Math.abs(n.x - t.x) + Math.abs(n.y - t.y) < .01 ? r(e, t) : (e.point = null, r(e, n), r(e, t)) : e.point = t
                } else r(e, t)
            }

            function i(e, t) {
                if (!e(t))
                    for (var n = t.nodes, r = 0; 4 > r; r++) n[r] && i(e, n[r])
            }

            function o(e) {
                var t = 0,
                    n = 0;
                if (e.charge = 0, e.size = 0, !e.leaf)
                    for (var r = e.nodes, a = 0; 4 > a; a++) {
                        var i = r[a];
                        i && (o(i), e.charge += i.charge, e.size += i.size, t += i.charge * i.cx, n += i.charge * i.cy)
                    }
                if (e.point) {
                    var u = l(e.point);
                    e.charge += e.pointCharge = u, e.size += e.point.size, t += u * e.point.x, n += u * e.point.y
                }
                e.cx = e.charge ? t / e.charge : 0, e.cy = e.charge ? n / e.charge : 0
            }

            function u(e) {
                if (e.point !== p) {
                    var t = e.cx - p.x,
                        n = e.cy - p.y,
                        r = 1 / ne.len(t, n);
                    if ((e.x2 - e.x1) * r < b) return s(p, e, e.charge, r, t, n), w++, !0;
                    e.point && (s(p, e.point, e.pointCharge, r, t, n), I++)
                }
                return !e.charge
            }

            function s(e, t, n, r, a, i) {
                r > B && (r = B), f(e, n, a, i, r)
            }

            function l(e) {
                return e.size * (1 + (e.lc ? e.lc : 1) / 3)
            }

            function f(e, n, r, a, i) {
                if (!(.001 > i)) {
                    var o = t * l(e) * n * i * i * i;
                    e.dx -= r * o, e.dy -= a * o
                }
            }

            function c() {
                for (var t in e) t += "", p = e[t], i(u, C)
            }
            var d, g, h, v, m = T(e);
            d = m.minx, h = m.miny, g = m.maxx, v = m.maxy;
            var x = g - d,
                y = v - h;
            x > y ? v = h + x : g = d + y;
            var p, b = .5,
                I = 0,
                w = 0,
                B = 1e5,
                C = n(d, h, g, v);
            for (var A in e) A += "", a(C, e[A]);
            return o(C), c(), C
        }

        function O(e, t, n, r, a, i) {
            r ? e[t] = n[t] : e[t] = {
                x: a,
                y: i
            }
        }

        function V(e, t, n, r) {
            var a = 1.25 - (c(n.tightness) - 5) / 10,
                i = K(t.v, t.e),
                o = U(i, t.v, a);
            I(e, o, n, function(e, n, a) {
                var o = {};
                for (var u in e.vertices)
                    if (u += "", i.rev[u])
                        for (var s = e.vertices[u].fixed, l = 0; l < i.rev[u].length; l++) {
                            var f = i.rev[u][l],
                                c = e.vertices[u].x,
                                d = e.vertices[u].y;
                            e.vertices[u].jit.positions && (c += e.vertices[u].jit.positions[f].x, d += e.vertices[u].jit.positions[f].y), O(o, f, t.v, s, c, d)
                        } else O(o, u, t.v, t.v[u].fixed, e.vertices[u].x, e.vertices[u].y);
                var g = {
                    vertices: o,
                    extents: e.extents
                };
                r(g, n, a)
            })
        }

        function U(e, t, n) {
            var r = {},
                a = [],
                i = 0;
            for (var u in e.rev) {
                u += "";
                var s = {};
                s.x = s.y = s.size = 0;
                for (var l = e.rev[u].length, f = !1, c = 0; l > c; c++) {
                    var d = t[e.rev[u][c]];
                    s.x += d.x, s.y += d.y, s.size += d.size
                }
                s.x = s.x / l, s.y = s.y / l;
                var g = z(t, e.rev[u], s.size / l, n);
                r[u] = {
                    x: s.x,
                    y: s.y,
                    size: g.radius,
                    fixed: f,
                    jit: g,
                    lc: 0
                }, i++;
                for (var h = e.rev[u][0], v = {}, m = e.index[h], x = 0; x < m.length; x++) {
                    var y = m[x];
                    v[e.ids[y]] = 1
                }
                for (var p in v) p += "", p > u && a.push({
                    id1: u,
                    id2: p
                })
            }
            for (var b, I, w = 0; w < a.length; w++) b = a[w].id1 + "", I = a[w].id2 + "", r[b].lc++, r[I].lc++;
            for (var B in t)
                if (B += "", !e.ids[B]) {
                    var C = t[B];
                    r[B] = {
                        x: C.x,
                        y: C.y,
                        size: C.size,
                        lc: 0,
                        fixed: C.fixed,
                        jit: {
                            radius: C.size,
                            position: {
                                x: 0,
                                y: 0
                            }
                        }
                    }, i++
                }
            return {
                v: r,
                e: a,
                vcount: i,
                extents: o(r)
            }
        }

        function K(e, t) {
            var n = Y(t);
            return P(n)
        }

        function X(e, t, n) {
            e[n] || (e[n] = []), e[n].push(t)
        }

        function H(e, t) {
            X(e, t.id1, t.id2), X(e, t.id2, t.id1)
        }

        function Y(e) {
            for (var t = {}, n = 0; n < e.length; n++) H(t, e[n]);
            return t
        }

        function P(e) {
            var t, n = {},
                r = {};
            for (var a in e) {
                a += "", e[a].sort(), t = "";
                for (var i = 0; i < e[a].length; i++) t += e[a][i];
                n[t] || (n[t] = []), n[t].push(a), r[a] = t
            }
            return {
                rev: n,
                ids: r,
                index: e
            }
        }

        function z(e, t, n, r) {
            var a, i = {},
                o = 0,
                u = t.length - 1;
            do a = _(e, t, n, u, i, r), o = Math.max(a.radius, o), u = a.index; while (u >= 0);
            var s;
            switch (t.length) {
                case 4:
                    s = 2;
                    break;
                case 3:
                    s = 2.5;
                    break;
                case 2:
                    s = 3;
                    break;
                case 1:
                    s = 1;
                    break;
                default:
                    s = 2
            }
            return {
                positions: i,
                radius: o * s
            }
        }

        function _(e, t, n, r, a, i) {
            if (0 === r) return a[t[0]] = {
                x: 0,
                y: 0
            }, {
                radius: n,
                index: -1
            };
            var o = (r + 1) * n,
                u = n,
                s = Math.sqrt(o / (Math.PI * u)),
                l = s * u,
                f = Math.floor(2 * Math.PI * l / n);
            f = Math.min(f, r + 1);
            var c = 2 * Math.PI / f,
                d = 0;
            switch (f) {
                case 2:
                    d = Math.PI / 2, l = .75 * l;
                    break;
                case 3:
                    d = Math.PI / 3, l = .85 * l;
                    break;
                case 4:
                    d = 0, l = .85 * l;
                    break;
                case 5:
                    d = 0, l = .9 * l;
                    break;
                default:
                    d = 0
            }
            l *= i;
            for (var g = 0; f > g;) a[t[r - g]] = {
                x: l * Math.cos(d),
                y: l * Math.sin(d)
            }, d += c, g++;
            return r -= f, {
                radius: l,
                index: r
            }
        }

        function j(e, t, n, r) {
            var a, i = 0,
                o = {};
            for (a = 0; a < t.length; a++) i += e[t[a]].size;
            if (1 === t.length) o[t[0]] = {
                x: 0,
                y: 0
            };
            else {
                var u = Math.PI / 4;
                2 === t.length && (u = Math.PI / 2), 4 === t.length && (u = 0), t.length < 4 && (i = 1.5 * i);
                var s = 2 * Math.PI / t.length,
                    l = r * i / 4;
                for (a = 0; a < t.length; a++) o[t[a]] = {
                    x: l * Math.cos(u),
                    y: l * Math.sin(u)
                }, u += s
            }
            return {
                positions: o,
                radius: i
            }
        }

        function J(e, t, n) {
            function a() {
                return 2 * Math.random() - 1 + (2 * Math.random() - 1) + (2 * Math.random() - 1)
            }

            function i(e, t, n) {
                return (t - e) * n + e
            }

            function o(e, t, n) {
                return (e - n) / (t - n)
            }

            function u(e) {
                var n = function() {
                        for (var t = 0, n = -(1 / 0), r = 0; r < e.length; r++) t += e[r].width * e[r].height, n = Math.max(n, e[r].width);
                        return {
                            area: t,
                            maxWidth: n
                        }
                    },
                    r = 1.1,
                    a = 1.03,
                    i = 10,
                    o = n(),
                    u = Math.sqrt(o.area * r / t.ratio) * a,
                    s = u * t.ratio;
                return s < o.maxWidth + i && (s = o.maxWidth + i), {
                    x: 0,
                    y: 0,
                    width: s,
                    height: u
                }
            }

            function s(e, t, n, r, a, u) {
                var s = o(t, n, r);
                return s = Math.pow(s, e), i(u, a, s)
            }

            function l(e, t) {
                if (!t) return !1;
                var n = e.x < m.x + m.width,
                    r = e.x + e.width > m.x,
                    a = e.y < m.y + m.height,
                    i = e.y + e.height > m.y;
                return n || r && a || i
            }

            function f(e, t) {
                function n(e, t) {
                    var n = e.y < t.y + t.height,
                        r = e.y + e.height > t.y;
                    return n && r
                }

                function r(e, t) {
                    var n = e.x < t.x + t.width,
                        r = e.x + e.width > t.x;
                    return n && r
                }
                if (t.length < 1) return !1;
                var a, i;
                for (a = 0; a < t.length; a++)
                    if (i = t[a], r(e, i) && n(e, i)) return !0;
                return !1
            }
            var c, d, g, h = {
                    spiral: function(e) {
                        var t = s(.6, e, 100, 0, p, 1),
                            n = s(1, e, 600, 0, .3, .5),
                            r = n * e,
                            a = Math.cos(r) * t,
                            i = Math.sin(r) * t;
                        return {
                            x: a,
                            y: i
                        }
                    },
                    controlled: function(e, t) {
                        for (var n; !l(t, n);) n = h.random(e);
                        return n
                    },
                    rect: function(e) {
                        return Q(e, t, n)
                    },
                    random: function(e) {
                        var t = .5 * e;
                        return {
                            x: a() * t,
                            y: a() * t
                        }
                    }
                },
                v = 0,
                m = u(e),
                x = [],
                y = h[t.strategy] || h.rect,
                p = 0;
            for (c = 0; c < e.length; c++) d = e[c], p += d.width;
            if (p /= 2 * e.length, "rect" === t.strategy) return y(e);
            for (e.sort(r), c = 0; c < e.length; c++) {
                for (d = e[c], d.x = d.fixed ? d.x : 0, d.y = d.fixed ? d.y : 0; f(d, x) && !d.fixed;) g = y(v, d), d.x = g.x, d.y = g.y, v++;
                x.push(d)
            }
            return n(x)
        }

        function Q(e, t, n) {
            function a() {
                for (var t = 0, n = 0; n < e.length; n++) t += e[n].width * e[n].height;
                return t
            }

            function i(e, t) {
                var n = r(e, t);
                return n ? n : t.height - e.height
            }

            function o(e) {
                for (var t = 0, n = 0; n < e.length; n++) t < e[n].width && (t = e[n].width);
                return t
            }

            function u(e) {
                for (var n = ne.clone(e), r = 0; r < n.length; r++) {
                    var a = n[r].width % t.step,
                        i = n[r].height % t.step;
                    0 !== a && (n[r].width = n[r].width + (t.step - a)), 0 !== i && (n[r].height = n[r].height + (t.step - i))
                }
                return n
            }

            function s(e) {
                return h ? e.ratio() >= t.ratio - .2 && e.ratio() <= t.ratio : !0
            }

            function l(e) {
                for (var n = d(t.tightness), r = 0; r < e.length; r++) e[r].width += n, e[r].height += n
            }

            function f(e, n) {
                var r = 1.1,
                    a = 1.03,
                    i = 10,
                    o = Math.sqrt(e.area() * r / t.ratio) * a,
                    u = o * t.ratio;
                return u < e.maxWidth + i && (u = e.maxWidth + i), {
                    width: u,
                    height: o,
                    lowestFreeHeightDeficit: 0,
                    ratio: function() {
                        return m.width / m.height
                    }
                }
            }
            t = ne.defaults(t, {
                ratio: 4 / 3,
                max: 10,
                sort: !0,
                step: 10,
                tightness: 5
            }), t.sort && e.sort(i), l(e);
            var c, g = u(e),
                h = !0;
            if (0 === e.length) return c = $(), n(c.comps);
            var v = {
                    area: a,
                    maxWidth: o(g),
                    maxHeight: g[0].height
                },
                m = f(v, g),
                x = 0;
            ne.asyncWhile(function() {
                return m.width >= v.maxWidth
            }, function() {
                var e = q(g, m);
                if (0 === e.comps.length) m.width *= 1.03, m.height *= 1.03;
                else {
                    if (x++, (!c || s(e)) && (c = e, c.efficiency = v.area() / e.area(), s(e))) return !0;
                    if (x >= t.max) return !0
                }
            }, function() {
                return n(c.comps)
            })
        }

        function q(e, t) {
            var n, r = $();
            t.canvas || (t.canvas = ee());
            for (var a = t.canvas.setDimensions(t.width, t.height), i = 0, o = 0, u = 0; u < e.length; u++) {
                var s = e[u];
                if (n = a.addRect(s.width, s.height), !n.placed) {
                    r.width = 0, r.height = 0, r.comps.length = 0, r.lowestHeightFlushedRightItem = 0;
                    break
                }
                s.x = n.offsetX, s.y = n.offsetY, r.comps.push(s), s.y + s.height > r.height && (r.height = s.y + s.height), s.x + s.width > r.width && (r.width = s.x + s.width);
                var l = s.width + s.x;
                (l > o || l === o && s.height > i) && (r.lowestHeightFlushedRightItem = n.lfhd, i = s.height, o = l)
            }
            return t.lowestFreeHeightDeficit = a.lfhDeficitSinceLastRedim(), r
        }

        function $() {
            var e = {
                width: 0,
                height: 0,
                area: function() {
                    return e.width * e.height
                },
                comps: [],
                lowestHeightFlushedRightItem: 0,
                ratio: function() {
                    return 0 === e.width ? 1 / 0 : e.width / e.height
                }
            };
            return e
        }

        function ee() {
            function e(e, t) {
                var n = f.height - e;
                return t - n
            }

            function t(e, t, n) {
                if (n.leftOverW > 0) {
                    f.cells += s.rows();
                    var r = e + n.nRequiredCellsH - 1;
                    s.insertCol(r, n.leftOverW)
                }
                if (n.leftOverH > 0) {
                    f.cells += s.cols();
                    var a = t + n.nRequiredCellsV - 1;
                    s.insertRow(a, n.leftOverH)
                }
                for (var i = e + n.nRequiredCellsH - 1; i >= e; i--)
                    for (var o = t + n.nRequiredCellsV - 1; o >= t; o--) s.setItem(i, o, !0)
            }

            function n(e, t, n, r) {
                r.nRequiredCellsH = 0, r.nRequiredCellsV = 0, r.leftOverW = 0, r.leftOverH = 0;
                for (var a, i = 0, o = 0, u = t; o < n.height;) {
                    for (a = e, i = 0; i < n.width;) {
                        if (s.item(a, u)) return !1;
                        i += s.colWidth(a), a++
                    }
                    o += s.rowHeight(u), u++
                }
                return r.nRequiredCellsH = a - e, r.nRequiredCellsV = u - t, r.leftOverW = i - n.width, r.leftOverH = o - n.height, !0
            }

            function r(e, t, n) {
                var r = 0,
                    a = 0,
                    i = 0,
                    o = 0,
                    u = 0;
                for (i = 0; e > i; i++) r += s.colWidth(i);
                for (i = 0; t > i; i++) a += s.rowHeight(i);
                if (o = n.x - r, u = n.y - a, 0 > o || 0 > u) return !1;
                if (o > 0) {
                    f.cells += s.rows();
                    var l = s.colWidth(e) - o;
                    s.insertCol(e, l)
                }
                if (u > 0) {
                    f.cells += s.cols();
                    var c = s.rowHeight(t) - u;
                    s.insertRow(t, c)
                }
                return !0
            }

            function a(e, t) {
                return f.width = e, f.height = t, f.area = o(), s.init(e, t, !1), f
            }

            function i(a, i, o) {
                var u = {
                        offsetX: 0,
                        offsetY: 0,
                        width: a,
                        height: i,
                        placed: !1
                    },
                    c = 0,
                    d = 0,
                    g = 0,
                    h = 0,
                    v = {};
                if (o) {
                    for (; c < s.cols() && g < o.x;) g += s.colWidth(c), c++;
                    for (; d < s.rows() && h < o.y;) h += s.rowHeight(d), d++;
                    r(c - 1, d - 1, o), g -= s.colWidth(c), h -= s.rowHeight(d)
                }
                f.addAttempts++;
                for (var m = ie;;) {
                    for (v = {}; d < s.rows() && s.item(c, d);) h += s.rowHeight(d), d++;
                    if (d < s.rows() && e(h, u.height) <= 0) {
                        if (n(c, d, u, v)) {
                            t(c, d, v), u.offsetX = g, u.offsetY = h, u.placed = !0;
                            break
                        }
                        h += s.rowHeight(d), d++
                    }
                    var x = e(h, u.height);
                    if (x > 0 && (h = 0, d = 0, g += s.colWidth(c), c++, f.lfhDeficitSinceLastRedim() > x && (l = x)), f.width - g < u.width) {
                        u.placed = !1;
                        break
                    }
                }
                return m = l, {
                    offsetX: u.offsetX,
                    offsetY: u.offsetY,
                    lfhd: m,
                    placed: u.placed
                }
            }

            function o() {
                return f.width * f.height
            }
            var u = 0,
                s = te(),
                l = ie,
                f = {
                    matrix: s,
                    cells: u,
                    addAttempts: 0,
                    addRect: i,
                    lfhDeficitSinceLastRedim: function() {
                        return l
                    },
                    setDimensions: a
                };
            return f
        }

        function te() {
            function e() {
                (i.length > 0 || o.length > 0) && (i.length = o.length = u.length = 0)
            }

            function t(t, n, r) {
                if (0 >= t) throw "[Matrix] Column size cannot be 0 or similar";
                if (0 >= n) throw "[Matrix] Row size cannot be 0 or similar";
                return s = 1, l = 1, e(), i.push({
                    index: 0,
                    size: t
                }), o.push({
                    index: 0,
                    size: n
                }), u[0] = [], u[0][0] = r, f
            }

            function n(e, t) {
                for (var n = o[e].index, r = 0; l > r; r++) u[r][s] = u[r][n];
                return s - 1 > e && o.splice(e + 1, 0, {
                    index: s,
                    size: t
                }), o[e + 1] = {
                    index: s,
                    size: t
                }, o[e].size -= t, s++, f
            }

            function r(e, t) {
                var n = (u[0].length, i[e].index);
                return u[l] = ne.clone(u[n]), l - 1 > e && i.splice(e + 1, 0, {
                    index: l,
                    size: t
                }), i[e + 1] = {
                    index: l,
                    size: t
                }, i[e].size -= t, l++, f
            }

            function a() {
                var e, t, n, r = "\n     ",
                    a = 0,
                    u = 0;
                for (a = 0; l > a; a++) e = Math.floor(i[a].size / 100) % 10 === 0 ? " " : "" + Math.floor(i[a].size / 100) % 10, t = Math.floor(i[a].size / 10) % 10 === 0 && " " === e ? " " : "" + Math.floor(i[a].size / 10) % 10, n = Math.floor(i[a].size) % 10 === 0 && " " === e && " " === t ? " " : "" + Math.floor(i[a].size) % 10, r += i[a].size > 999 ? " " + i[a].size + " " : " " + e + t + n + " ";
                for (r += "\n", a = 0; s > a; a++) {
                    for (e = Math.floor(o[a].size / 100) % 10 === 0 ? " " : "" + Math.floor(o[a].size / 100) % 10, t = Math.floor(o[a].size / 10) % 10 === 0 && " " === e ? " " : "" + Math.floor(o[a].size / 10) % 10, n = Math.floor(o[a].size) % 10 === 0 && " " === e && " " === t ? " " : "" + Math.floor(o[a].size) % 10, r += o[a].size > 999 ? " " + o[a].size + ": " : " " + e + t + n + ": ", u = 0; l > u; u++) r += f.item(u, a) ? "  X  " : "  .  ";
                    r += "\n"
                }
                return r
            }
            var i = [],
                o = [],
                u = [],
                s = 0,
                l = 0,
                f = {
                    init: t,
                    cols: function() {
                        return l
                    },
                    rows: function() {
                        return s
                    },
                    item: function(e, t) {
                        return u[i[e].index][o[t].index]
                    },
                    setItem: function(e, t, n) {
                        u[i[e].index][o[t].index] = n
                    },
                    insertRow: n,
                    insertCol: r,
                    colWidth: function(e) {
                        return i[e].size
                    },
                    rowHeight: function(e) {
                        return o[e].size
                    },
                    inspect: a
                };
            return f
        }
        var ne = KeyLines.Util;
        e.createLens(c);
        var re = {},
            ae = (re.extractStructure = function(e, t, n, r, i, u, s) {
                var l = null;
                if (u) {
                    l = {};
                    for (var f = 0; f < u.length; f++) l[u[f]] = 1
                }
                var c = a(e, t, n, r, i, l, s);
                return c.extents = o(c.v), c
            }, "_dummy");
        re.layout = function(t, n, r, a, i, o) {
            n.trigger("prechange", "layout");
            var u = "^(standard|hierarchy|lens";
            u += "|radial|structural|tweak", u += ")$";
            var s = new RegExp(u);
            if (!s.test(t)) throw new Error("The layout name is not valid.");
            "standard" === t && I(n, r, i, o), "hierarchy" === t && m(n, r, a, i, o), "lens" === t && e.lens(n, r, i, C, o), "structural" === t && V(n, r, i, o), "radial" === t && m(n, r, a, i, o, y), "tweak" === t && D(n, r, i, o)
        };
        re.arrange = function(e, t, n, r, a, o) {
            function u(e, t) {
                var n, r = [];
                for (n = 0; n < t.length; n++) e[t[n]] && r.push(t[n]);
                return r
            }

            function s(e, t, n, r, a) {
                var i;
                for (i = 0; i < e.length; i++)
                    if (e[i].id === t) {
                        e[i].x = n - e[i].width / 2, e[i].y = r - e[i].height / 2;
                        break
                    }
                return a(e)
            }

            function l(e, t, n, r, i) {
                var o = {},
                    u = [],
                    s = {};
                for (c = 0; c < e.length; c++) s = E(e[c].verts, t), isFinite(s.width) && (u.push({
                    x: s.x,
                    y: s.y,
                    width: s.width,
                    height: s.height,
                    id: c
                }), o[c] = {
                    x: s.x,
                    y: s.y
                });
                s = E(t), u.push({
                    x: s.x,
                    y: s.y,
                    width: s.width,
                    height: s.height,
                    id: x
                }), o[x] = {
                    x: s.x,
                    y: s.y
                }, b(u, {
                    ratio: a.ratio,
                    centre: n,
                    absolute: r,
                    specialId: x
                }, function(e) {
                    return i({
                        packed: e,
                        original: o
                    })
                })
            }

            function f(e, t, n) {
                var r, a;
                for (var i in e) {
                    var o = e[i + ""];
                    o.arranged ? (r = n[x].x - t[x].x, a = n[x].y - t[x].y) : (r = n[o.c].x - t[o.c].x, a = n[o.c].y - t[o.c].y), o.x += r, o.y += a
                }
            }
            var c, g, h, v = {
                    tidy: function(e, t, n) {
                        return Q(e, {
                            step: 30,
                            max: 2,
                            sort: !1,
                            ratio: t.ratio
                        }, n)
                    },
                    average: function(e, t, n) {
                        return s(e, t.specialId, t.centre.x, t.centre.y, n)
                    },
                    absolute: function(e, t, n) {
                        return s(e, t.specialId, t.absolute.x, t.absolute.y, n)
                    }
                },
                m = {
                    grid: function(e, t, n, r) {
                        var a = 2 * n + 24 + r,
                            i = 0,
                            o = 0,
                            u = Math.floor(Math.sqrt(e.length)),
                            s = Math.ceil(e.length / u),
                            l = {};
                        for (c = 0; u > c; c++) {
                            for (g = 0; s > g; g++) {
                                var f = s * c + g;
                                e[f] && (l[e[f]] = {
                                    x: i,
                                    y: o
                                }), i += a
                            }
                            i = 0, o += a
                        }
                        return l
                    },
                    circle: function(e, t, n, r) {
                        var a = 1.25 - (5 - r) / 50;
                        return j(t, e, n, a).positions
                    },
                    radial: function(e, t, n, r) {
                        var a = 1.25 - (5 - r) / 50;
                        return z(t, e, n, a).positions
                    }
                },
                x = "#arranged#";
            a = ne.defaults(a, {
                tightness: 5,
                position: "average",
                x: 0,
                y: 0
            }), t.trigger("prechange", "arrange");
            var y = r.v,
                p = d(a.tightness),
                b = v[a.position] || v.average,
                I = m[e] || m.grid,
                w = {
                    x: Number(a.x) || 0,
                    y: Number(a.y) || 0
                },
                B = {
                    x: 0,
                    y: 0
                };
            if (n = ne.ensureArray(n), n = u(y, n), !n.length) return o();
            for (h = 0, c = 0; c < n.length; c++) h += y[n[c]].size, B.x += y[n[c]].x, B.y += y[n[c]].y;
            h /= n.length, B.x /= n.length, B.y /= n.length;
            var A = I(n, y, h, p),
                M = {};
            for (var D in A) D += "", y[D].x = A[D].x, y[D].y = A[D].y, M[D] = y[D], M[D].arranged = 1;
            var L = i(y, r.e);
            l(L, M, B, w, function(e) {
                for (var n = e.packed, i = e.original, u = {}, s = 0; s < n.length; s++) u[n[s].id] = n[s];
                f(y, i, u);
                var l = [],
                    c = r.extents;
                C(t, y, c, l, a, o)
            })
        };
        var ie = 16e6;
        return re
    }
}(),
function() {
    var e = KeyLines.Draggers = {};
    e.createDraggers = function(e) {
        var t, n = KeyLines.Util,
            r = {};
        return r.setOptions = function(e) {
            t = e.fanLength
        }, r.primitiveMover = function(t, r, a, i, o, u, s, l, f, c) {
            function d(e, t) {
                return {
                    dx: a.undim(e - l),
                    dy: a.undim(t - f)
                }
            }

            function g(e, t, r, a) {
                var i, o = {};
                if (s.length > 0) {
                    o.x1 = o.x2 = o.y1 = o.y2 = 0;
                    for (var u = 0; u < s.length; u++) {
                        i = s[u];
                        var l = "x" === i.charAt(0) ? e : t;
                        o[i] = l
                    }
                    r && (0 !== o.x1 && (o.x2 = -o.x1), 0 !== o.x2 && (o.x1 = -o.x2), 0 !== o.y1 && (o.y2 = -o.y1), 0 !== o.y2 && (o.y1 = -o.y2))
                } else a && (e = Math.abs(e) > Math.abs(t) ? e : 0, t = 0 !== e ? 0 : t), o.x1 = o.x2 = e, o.y1 = o.y2 = t;
                for (var f = {}, c = ["x1", "y1", "x2", "y2"], d = 0; d < c.length; d++) i = c[d], f[i] = v[i] + o[i];
                if (a) {
                    var g = v.x2 - v.x1,
                        h = v.y2 - v.y1;
                    if (Math.abs(g) > 0 && Math.abs(h) > 0) {
                        var m, x = h / g,
                            y = f.x2 - f.x1,
                            p = f.y2 - f.y1,
                            b = Math.abs(e) > Math.abs(t);
                        if (b) {
                            var I = Math.floor(x * (g + y) - h) - p;
                            r || !n.contains(s, "y1") && !n.contains(s, "y2") ? (m = Math.floor(I / 2), f.y1 -= m, f.y2 += m) : (n.contains(s, "y1") && (f.y1 -= I), n.contains(s, "y2") && (f.y2 += I))
                        } else {
                            var w = Math.floor((h + p) / x - g) - y;
                            r || !n.contains(s, "x1") && !n.contains(s, "x2") ? (m = Math.floor(w / 2), f.x1 -= m, f.x2 += m) : (n.contains(s, "x1") && (f.x1 -= w), n.contains(s, "x2") && (f.x2 += w))
                        }
                    }
                }
                return f
            }

            function h(e, t, n, r) {
                var a = d(e, t);
                return g(a.dx, a.dy, n, r)
            }
            if (!e.trigger("dragstart", "resize", i, a.unscale(l), a.unscale(f), null)) {
                var v = t.extents(r, o),
                    m = {};
                for (var x in o) m[x] = o[x];
                t.setAlpha(m, .5);
                var y = {
                    scrollable: !0,
                    getCursor: function() {
                        return u ? "crosshair" : "move"
                    },
                    generate: function(e) {
                        e.copyPrimitive(m)
                    },
                    dragMove: function(e, n, a, i) {
                        var o = h(e, n, a, i);
                        t.setExtents(m, o.x1, o.y1, o.x2, o.y2, r)
                    },
                    endDrag: function(n, u, s, l, f) {
                        a.cancelScroll();
                        var d = e.trigger("dragend", "resize", i, a.unscale(u), a.unscale(s));
                        if (!d && n) {
                            var g = h(u, s, l, f);
                            t.setExtents(o, g.x1, g.y1, g.x2, g.y2, r), c(o)
                        }
                        e.trigger("dragcomplete", "resize", i, a.unscale(u), a.unscale(s))
                    }
                };
                return y
            }
        }, r.createHandDragger = function(t, r, a, i) {
            function o(e, t) {
                g = g || Math.abs(r - e) > 10 || Math.abs(a - t) > 10, c = e - r, d = t - a, s && (s.x || (c = 0), s.y || (d = 0))
            }

            function u() {
                g && (t.offsetX(l + c), t.offsetY(f + d), e.trigger("viewchange"))
            }
            var s = {
                    x: !0,
                    y: !0
                },
                l = t.offsetX(),
                f = t.offsetY(),
                c = 0,
                d = 0,
                g = !1,
                h = "hand",
                v = e.trigger("dragstart", h, null, t.unscale(r), t.unscale(a));
            if (v) {
                if (!n.isNormalObject(v)) return void i();
                for (var m in v) s[m] = v[m]
            }
            return {
                rebuild: {
                    drawOnly: !0
                },
                animate: u,
                direction: s,
                dragMove: o,
                getCursor: function() {
                    return "move"
                },
                endDrag: function(n, r, a) {
                    var s = e.trigger("dragend", h, null, t.unscale(r), t.unscale(a));
                    g ? (!s && n ? o(r, a) : c = d = 0, u()) : i(), e.trigger("dragcomplete", h, null, t.unscale(r), t.unscale(a))
                }
            }
        }, r.createDefaultDragger = function(t, r, a, i, o, u, s, l) {
            function f(e) {
                return h.add && h.add[e]
            }

            function c() {
                for (var e in r)(i(e) || f(e)) && I[e] && t.moveNode(e, I[e].x + D.dx, I[e].y + D.dy);
                for (var n in B)(i(n) || f(n)) && t.moveLink(n, B[n].y + D.dy)
            }

            function d() {
                for (var e in r) I[e] && t.moveNode(e, I[e].x, I[e].y);
                for (var n in B) t.moveLink(n, B[n].y)
            }

            function g(e, t) {
                return L = L || Math.abs(s - e) > 10 || Math.abs(l - t) > 10, e = o ? o.viewToWorldX(e) : e, t = o ? o.viewToWorldY(t) : t, h && (h.x || (e = p), h.y || (t = b)), {
                    dx: L ? e - p : 0,
                    dy: L ? t - b : 0
                }
            }
            var h = {
                    x: !0,
                    y: !0,
                    add: null
                },
                v = n.rawId(u),
                m = "move",
                x = e.trigger("dragstart", m, v, o.unscale(s), o.unscale(l), n.subId(u));
            if (x)
                if (n.isArray(x)) h.add = n.makeIdMap(x);
                else {
                    if (!n.isNormalObject(x)) return;
                    for (var y in x) h[y] = x[y];
                    n.isArray(h.add) && (h.add = n.makeIdMap(h.add))
                }
            var p = o ? o.viewToWorldX(s) : s,
                b = o ? o.viewToWorldY(l) : l,
                I = {};
            for (var w in r) I[w] = {
                x: r[w].x,
                y: r[w].y
            };
            var B = {};
            for (var C in a) "h" === a[C].st && (B[C] = {
                y: a[C].y || 0
            });
            var A, M, D = g(s, l),
                L = !1,
                Z = {
                    rebuild: {
                        all: !0
                    },
                    scrollable: !0,
                    direction: h,
                    animate: c,
                    generate: function(e) {
                        e.excludeHit(v)
                    },
                    getCursor: function() {
                        return "move"
                    },
                    dragMove: function(e, t) {
                        D = g(e, t)
                    },
                    mouseMoved: function(e) {
                        return e && (A = !0), A
                    },
                    afterDraw: function(t, r, a) {
                        A = !1;
                        var i = n.rawId(t);
                        v !== i && (void 0 === M ? M = i : i !== M && (e.trigger("dragover", i, o.unscale(r), o.unscale(a), n.subId(t)), M = i))
                    },
                    endDrag: function(r, a, i) {
                        o.cancelScroll();
                        var s = e.trigger("dragend", m, n.rawId(u), o.unscale(a), o.unscale(i));
                        s ? d() : r && L ? (d(), D = g(a, i), e.trigger("prechange", "move"), c()) : d(), t.setDirty(v), e.trigger("dragcomplete", m, n.rawId(u), o.unscale(a), o.unscale(i))
                    }
                };
            return Z
        }, r.createLinkEndDragger = function(t, r, a, i, o, u, s, l, f) {
            function c(e, t) {
                r.moveNode(o[u], e, t)
            }

            function d(e, t) {
                return e = a ? a.viewToWorldX(e) : e, t = a ? a.viewToWorldY(t) : t, {
                    dx: e - m,
                    dy: t - x
                }
            }

            function g() {
                return o["id1" === u ? "id2" : "id1"]
            }
            if (e.trigger("dragstart", "dummy", f, a.unscale(s), a.unscale(l), null)) return !1;
            var h, v, m = a ? a.viewToWorldX(s) : s,
                x = a ? a.viewToWorldY(l) : l,
                y = i[o[u]].x,
                p = i[o[u]].y,
                b = d(s, l),
                I = s,
                w = l,
                B = null,
                C = {
                    rebuild: {
                        all: !0
                    },
                    scrollable: !0,
                    animate: function() {
                        c(y + b.dx, p + b.dy)
                    },
                    getCursor: function() {
                        return "move"
                    },
                    dragMove: function(e, n) {
                        b = d(e, n), I = e, w = n, B = t.hittest(e, n, a)
                    },
                    mouseMoved: function(e) {
                        return e && (v = !0), v
                    },
                    afterDraw: function(t, r, i) {
                        v = !1;
                        var o = n.rawId(t),
                            u = n.subId(t);
                        f === o && (o = null, u = null), void 0 === h ? h = o : o !== h && (e.trigger("dragover", o, a.unscale(r), a.unscale(i), u), h = o)
                    },
                    endDrag: function(t, s, l) {
                        a.cancelScroll();
                        var f = e.trigger("dragend", "dummy", B, a.unscale(s), a.unscale(l));
                        if (!f)
                            if (t)
                                if (c(y, p), e.trigger("prechange", "move"), B = n.rawId(B), i[B] && B !== g()) {
                                    delete i[o[u]], o[u] = B;
                                    var h = {};
                                    h[o.id] = o, r.clearGraphCache(), r.resetConnectionOffsets(h)
                                } else b = d(s, l), c(y + b.dx, p + b.dy), B = null;
                        else c(y, p);
                        e.trigger("dragcomplete", "dummy", B, a.unscale(s), a.unscale(l))
                    }
                };
            return C
        }, r.createLinkDragger = function(t, r, a, i, o, u, s, l) {
            function f(e, t, n, r, a, i) {
                return ((n - e) * (t - i) - (e - a) * (r - t)) / Math.sqrt((n - e) * (n - e) + (r - t) * (r - t))
            }

            function c(e, n) {
                var r = a.viewToWorldX(e),
                    i = a.viewToWorldY(n),
                    u = t("x1", o),
                    s = t("y1", o),
                    l = t("x2", o),
                    c = t("y2", o),
                    d = f(u, s, l, c, r, i);
                v && Math.abs(d - g) > 8 && (v = !1), v || (Math.abs(d) < 12 && (d = 0), h = d)
            }

            function d() {
                i.setLinkOffset(o.id, h)
            }
            if (e.trigger("dragstart", "offset", l, a.unscale(u), a.unscale(s), n.subId(l))) return !1;
            var g = o.off ? o.off : 0,
                h = g,
                v = !0,
                m = !1,
                x = {
                    rebuild: {
                        all: !0
                    },
                    scrollable: !0,
                    animate: function() {
                        d()
                    },
                    getCursor: function() {
                        return "move"
                    },
                    dragMove: function(e, t) {
                        c(e, t), m = !0
                    },
                    endDrag: function(t, n, r) {
                        a.cancelScroll();
                        var i = e.trigger("dragend", "offset", l, a.unscale(n), a.unscale(r));
                        i || (t && m ? (o.off = g, e.trigger("prechange", "offset"), c(n, r), d()) : o.off = g), e.trigger("dragcomplete", "offset", l, a.unscale(n), a.unscale(r))
                    }
                };
            return x
        }, r
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.TriangleShader = function(t) {
        function n(e, t, n) {
            for (var r = 0, a = e.atlases.length; a > r; r++) u(e.atlases[r], r);
            t && u(t, 3);
            for (var r = 0, a = n.length; a > r; r++) u(n[r], r + 4)
        }

        function r(e) {
            c.bindBuffer(c.ARRAY_BUFFER, e.triangleBuffers.shadowBuffer), (e.alwaysUpdate || e.rebuildOptions.all || e.rebuildOptions.shadow) && c.bufferData(c.ARRAY_BUFFER, e.triangleBuffers.shadowData, c.STATIC_DRAW), c.enableVertexAttribArray(m.shadowColour), c.vertexAttribPointer(m.shadowColour, 4, c.UNSIGNED_BYTE, !1, 4 * Uint8Array.BYTES_PER_ELEMENT, 0)
        }

        function a(e) {
            c.bindBuffer(c.ARRAY_BUFFER, e.triangleBuffers.textureBuffer), (e.rebuildOptions.textures || e.alwaysUpdate || e.rebuildOptions.all) && c.bufferData(c.ARRAY_BUFFER, e.triangleBuffers.textureData, c.STATIC_DRAW), c.enableVertexAttribArray(m.textCoord), c.enableVertexAttribArray(m.textureBank), c.enableVertexAttribArray(m.circleRadius), c.vertexAttribPointer(m.textCoord, 4, c.FLOAT, !1, 7 * Float32Array.BYTES_PER_ELEMENT, 0), c.vertexAttribPointer(m.textureBank, 2, c.FLOAT, !1, 7 * Float32Array.BYTES_PER_ELEMENT, 16), c.vertexAttribPointer(m.circleRadius, 1, c.FLOAT, !1, 7 * Float32Array.BYTES_PER_ELEMENT, 24)
        }

        function i(e) {
            c.bindBuffer(c.ARRAY_BUFFER, e.triangleBuffers.colourBuffer), (e.rebuildOptions.colours || e.alwaysUpdate || e.rebuildOptions.all) && c.bufferData(c.ARRAY_BUFFER, e.triangleBuffers.colourData, c.STATIC_DRAW), c.enableVertexAttribArray(m.colour), c.vertexAttribPointer(m.colour, 4, c.UNSIGNED_BYTE, !1, 4 * Uint8Array.BYTES_PER_ELEMENT, 0)
        }

        function o(e) {
            c.bindBuffer(c.ARRAY_BUFFER, e.triangleBuffers.positionBuffer), (e.rebuildOptions.positions || e.rebuildOptions.all || e.alwaysUpdate) && c.bufferData(c.ARRAY_BUFFER, e.triangleBuffers.positionData, c.STATIC_DRAW), c.enableVertexAttribArray(m.position), c.enableVertexAttribArray(m.width), c.vertexAttribPointer(m.position, 2, c.FLOAT, !1, 3 * Float32Array.BYTES_PER_ELEMENT, 0), c.vertexAttribPointer(m.width, 1, c.FLOAT, !1, 3 * Float32Array.BYTES_PER_ELEMENT, 8)
        }

        function u(e, t) {
            c.activeTexture(c["TEXTURE" + t]), e.texture ? c.bindTexture(c.TEXTURE_2D, e.texture) : (e.texture = c.createTexture(), c.bindTexture(c.TEXTURE_2D, e.texture), c.pixelStorei(c.UNPACK_PREMULTIPLY_ALPHA_WEBGL, !0), c.texImage2D(c.TEXTURE_2D, 0, c.RGBA, c.RGBA, c.UNSIGNED_BYTE, e.img), x(e.img.width) && x(e.img.height) ? (c.generateMipmap(c.TEXTURE_2D), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_MIN_FILTER, c.LINEAR_MIPMAP_LINEAR), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_MAG_FILTER, c.LINEAR), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_WRAP_S, c.CLAMP_TO_EDGE), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_WRAP_T, c.CLAMP_TO_EDGE)) : (c.texParameteri(c.TEXTURE_2D, c.TEXTURE_WRAP_S, c.CLAMP_TO_EDGE), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_WRAP_T, c.CLAMP_TO_EDGE), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_MIN_FILTER, c.LINEAR), c.texParameteri(c.TEXTURE_2D, c.TEXTURE_MAG_FILTER, c.LINEAR)), e.img = null)
        }

        function s(e) {
            e.triangleBuffers.positionBuffer || (e.triangleBuffers.positionBuffer = c.createBuffer()), e.triangleBuffers.textureBuffer || (e.triangleBuffers.textureBuffer = c.createBuffer()), e.triangleBuffers.colourBuffer || (e.triangleBuffers.colourBuffer = c.createBuffer()), e.triangleBuffers.shadowBuffer || (e.triangleBuffers.shadowBuffer = c.createBuffer())
        }

        function l(e, t, n, r, a) {
            c.uniform2f(m.resolution, t, n), c.uniform1f(m.hitTest, 0 | r), c.uniform1iv(m.textures, [0, 1, 2, 3, 4, 5, 6, 7]), c.uniformMatrix4fv(m.transform, !1, e.get()), c.uniform1f(m.shadowType, a)
        }
        var f = {},
            c = t,
            d = e.Utils,
            g = e["triangle-vertex"],
            h = e["triangle-fragment"],
            v = (c.getExtension("OES_standard_derivatives"), d.createProgram(c, atob(g), atob(h))),
            m = {
                transform: c.getUniformLocation(v, "u_transform"),
                textures: c.getUniformLocation(v, "u_image[0]"),
                resolution: c.getUniformLocation(v, "u_resolution"),
                hitTest: c.getUniformLocation(v, "u_hitTest"),
                shadowType: c.getUniformLocation(v, "u_shadowType"),
                position: c.getAttribLocation(v, "a_position"),
                circleRadius: c.getAttribLocation(v, "a_circleRadius"),
                width: c.getAttribLocation(v, "a_width"),
                colour: c.getAttribLocation(v, "a_colour"),
                textCoord: c.getAttribLocation(v, "a_texCoord"),
                shadowColour: c.getAttribLocation(v, "a_hitTestColour"),
                textureBank: c.getAttribLocation(v, "a_textureBank")
            };
        f.drawItems = function(e, t, u, f, d, g, h, m) {
            c.useProgram(v), l(t, g, h, m, 0 | e.shadowType), s(e), r(e), a(e), i(e), o(e), n(u, f, d), c.drawArrays(c.TRIANGLES, 0, 3 * e.triangleBuffers.numOfTriangles)
        };
        var x = function(e) {
            return 0 == (e & e - 1)
        };
        return f
    })
}(),
function() {
    function e(e, t, n) {
        return function() {
            if (!n || !n()) {
                var r, a = t[e];
                return a && (r = a.apply(null, arguments)), r
            }
        }
    }
    var t = KeyLines.Common = {};
    t.frameManager = function(e, t) {
        function n() {
            return {
                all: !0,
                colours: !0,
                textures: !0,
                positions: !0,
                shadow: !0
            }
        }

        function r() {
            s.all = !1, s.colours = !1, s.textures = !1, s.positions = !1, s.shadow = !1
        }
        var a = (new Date).getTime(),
            i = a >> 10,
            o = 0,
            u = !1,
            s = n(),
            l = !0,
            f = 0;
        return {
            frameRate: function() {
                return f
            },
            update: function() {
                var n = (new Date).getTime(),
                    c = n >> 10;
                c > i ? (f = Math.round(o / (c - i)), o = 0, i = c) : o++;
                var d = t(n - a);
                a = n, u = u || d, u && (u = !1, l && (e(null, s), r()))
            },
            redraw: function(e) {
                u = !0, e = e || n(), s.all || (s.all = e.all), s.positions || (s.positions = e.positions), s.shadow || (s.shadow = e.shadow), s.textures || (s.textures = e.textures), s.colours || (s.colours = e.colours)
            },
            allowDraw: function(e) {
                e && !l && (u = !0), l = e
            }
        }
    }, t.buildAnimatorInstance = function(e, t, n) {
        var r = !0;
        return {
            rebuild: t,
            animate: function(t) {
                var a = t;
                r && !n && (a = 16);
                var i = e(a, r);
                return r = !1, i
            }
        }
    }, t.animator = function(e) {
        var t = [],
            n = [];
        return {
            push: function(e, r) {
                e && (e.queue = r, r ? n.push(e) : t.push(e))
            },
            cancel: function() {
                t = [], n = [], e()
            },
            animate: function(r, a, i) {
                var o = !1,
                    u = !1;
                if (t.length > 0) {
                    var s = t;
                    t = [];
                    for (var l = 0; l < s.length; l++) s[l].animate(r) && (u = u || s[l].queue, t.push(s[l])), e(s[l].rebuild);
                    o = !0
                }
                return u || n.length > 0 && (t.push(n.shift()), o = !0), a && a.animate && (a.animate(r), o = !0), i && (o = o || i.animated()), o
            }
        }
    }, t.makeValueAnimator = function(e, n) {
        var r, a, i, o;
        return function(u, s, l) {
            var f = n();
            f === u || o && a === u || (r = f, a = u, i = 0, o || (o = t.buildAnimatorInstance(function(e) {
                i = Math.min(s, i + e);
                var t = 0 >= s ? a : r + (a - r) * i / s;
                n(t);
                var u = s > i;
                return u || (o = null, KeyLines.Util.invoke(l)), u
            }), e.push(o, !1)))
        }
    };
    var n = ["mousemove", "mousedown", "mouseup", "mouseleave", "mousewheel", "dblclick", "touchstart", "touchmove", "touchend", "touchcancel", "keyup", "keydown", "gesturestart", "gesturechange", "gestureend", "dragover"];
    t.appendActions = function(t, r, a) {
        for (var i = 0; i < n.length; i++) t[n[i]] = e(n[i], r, a)
    }, t.hitTester = function(e, t, n, r) {
        function a(e, a) {
            o.clear(e, t, n), o.backFill(e, u, 1, t, n), r.drawShadowCanvas(e, a), s = !1
        }
        var i, o = KeyLines.Rendering,
            u = o.randomColour(),
            s = !0,
            l = 5,
            f = function() {
                var e = {},
                    t = {};
                return {
                    getColour: function(n) {
                        if (n in e) return e[n];
                        var r = o.randomColour(t);
                        return e[n] = r, t[r] = n, r
                    },
                    getId: function(e) {
                        return t[e] || null
                    }
                }
            }();
        r.setShadowColourMap(f);
        var c = {};
        return c.test = function(t, n, r, a, i) {
            var d = [0, 0, 0];
            i && i.webgl ? d = i.webglCanvas.renderer.drawShadowCanvas(Math.floor(t), Math.floor(n)) : (s && c.draw(r), d = e.getImageData(Math.floor(t), Math.floor(n), 1, 1).data);
            var g = o.rgbtostring(d[0], d[1], d[2]);
            if (g === u) return null;
            var h = f.getId(g);
            return h ? h : i && i.webgl ? null : (a = a || 1, a > l ? null : t + a > l && n + a > l ? c.test(t - 1, n - 1, r, ++a, i) : c.test(t + 1, n + 1, r, ++a, i))
        }, c.dirty = function() {
            s = !0
        }, c.debugCanvas = function(e) {
            i = e
        }, c.draw = function(t) {
            a(e, t), i && a(i, t)
        }, c
    }
}(),
function() {
    var e = KeyLines.TimeBar.API = KeyLines.TimeBar.API || {};
    e.appendExtras = function(e) {
        var t, n = KeyLines.Util;
        e.bind("change", function() {
            t = null
        }), e.bind("datachange", function() {
            t = null
        }), e.inRange = function(r) {
            if (n.isNullOrUndefined(r)) return !1;
            t = t || e.getIdcounts();
            var a = (n.isNormalObject(r) ? r.id : r) + "";
            return !!t[a]
        }
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    if (e) {
        var t = function(e, t, n, r, a, i, o) {
                e[t.positionIndex++] = n, e[t.positionIndex++] = r, e[t.positionIndex++] = 0, e[t.positionIndex++] = a, e[t.positionIndex++] = r, e[t.positionIndex++] = 0, e[t.positionIndex++] = n, e[t.positionIndex++] = i, e[t.positionIndex++] = 0, e[t.positionIndex++] = n, e[t.positionIndex++] = i, e[t.positionIndex++] = 0, e[t.positionIndex++] = a, e[t.positionIndex++] = r, e[t.positionIndex++] = 0, e[t.positionIndex++] = a, e[t.positionIndex++] = i, e[t.positionIndex++] = 0
            },
            n = function(e, t, n, r, a, i, o, u) {
                e[t.textureIndex++] = n, e[t.textureIndex++] = r, e[t.textureIndex++] = 0, e[t.textureIndex++] = 0, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0, e[t.textureIndex++] = a, e[t.textureIndex++] = r, e[t.textureIndex++] = 1, e[t.textureIndex++] = 0, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0, e[t.textureIndex++] = n, e[t.textureIndex++] = i, e[t.textureIndex++] = 0, e[t.textureIndex++] = 1, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0, e[t.textureIndex++] = n, e[t.textureIndex++] = i, e[t.textureIndex++] = 0, e[t.textureIndex++] = 1, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0, e[t.textureIndex++] = a, e[t.textureIndex++] = r, e[t.textureIndex++] = 1, e[t.textureIndex++] = 0, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0, e[t.textureIndex++] = a, e[t.textureIndex++] = i, e[t.textureIndex++] = 1, e[t.textureIndex++] = 1, e[t.textureIndex++] = u, e[t.textureIndex++] = o, e[t.textureIndex++] = 0
            };
        e.generateText = function(e, r, a, i, o, u) {
            if (o.rebuildOptions.all || o.rebuildOptions.positions || o.alwaysUpdate) {
                var s = 14 / i * (r.fs / 14);
                i *= s;
                var l = (r.w, 0)
            }
            var f = r.wc[0],
                c = r.wc[1],
                d = r.wc[2],
                g = r.wc[3],
                h = e.colourData,
                v = e.positionData,
                m = e.textureData,
                x = e.shadowData,
                y = r.x1,
                p = r.y1,
                b = r.x2,
                I = r.y2;
            u && (y += u.x, p += u.y, b += u.x, I += u.y);
            for (var w = 0, B = a.length; B > w; w++) {
                var C = a[w];
                if (o.rebuildOptions.all || o.rebuildOptions.shadow || o.alwaysUpdate)
                    for (var A = 0; 6 > A; A++) x[e.shadowIndex++] = 0, x[e.shadowIndex++] = 0, x[e.shadowIndex++] = 0, x[e.shadowIndex++] = 0;
                if (o.rebuildOptions.all || o.rebuildOptions.colours || o.alwaysUpdate)
                    for (var A = 0; 6 > A; A++) h[e.colourIndex++] = f, h[e.colourIndex++] = c, h[e.colourIndex++] = d, h[e.colourIndex++] = g;
                if (o.rebuildOptions.all || o.rebuildOptions.positions || o.alwaysUpdate) {
                    var M = C.width * s;
                    t(v, e, y + l + 4, p + i, y + (l + M) + 4, p, u)
                }(o.rebuildOptions.all || o.rebuildOptions.textures || o.alwaysUpdate) && n(m, e, C.u1, C.v1, C.u2, C.v2, C.colourComponent, C.textureBank), l += M
            }
        }
    }
}(),
function() {
    "use strict";
    if (KeyLines.WebGL) {
        var e = 6,
            t = new Float32Array(6);
        KeyLines.WebGL.generateCircle = function(n, r, a, i, o, u, s, l, f, c) {
            var d;
            if (f.rebuildOptions.all || f.rebuildOptions.shadow || f.alwaysUpdate)
                for (o = o || [0, 0, 0, 0], d = 0; e > d; d++) f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = o[0], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = o[1], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = o[2], f.triangleBuffers.shadowData[f.triangleBuffers.shadowIndex++] = o[3];
            if (f.rebuildOptions.all || f.rebuildOptions.colours || f.alwaysUpdate)
                for (d = 0; e > d; d++) f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = i[0], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = i[1], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = i[2], f.triangleBuffers.colourData[f.triangleBuffers.colourIndex++] = i[3];
            (f.rebuildOptions.all || f.rebuildOptions.positions || f.alwaysUpdate) && (t[0] = 2 * a, t[1] = 0, t[2] = n - a, t[3] = r - a, t[4] = t[2] + t[0], t[5] = t[3] + t[0], c && (t[2] = t[2] + c.x, t[3] = t[3] + c.y, t[4] = t[4] + c.x, t[5] = t[5] + c.y), f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[2], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[3], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[4], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[3], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[2], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[5], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[2], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[5], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[4], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[3], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u, f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[4], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = t[5], f.triangleBuffers.positionData[f.triangleBuffers.positionIndex++] = u), (f.rebuildOptions.all || f.rebuildOptions.textures || f.alwaysUpdate) && (f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = s, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = l, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = -1, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = 0, f.triangleBuffers.textureData[f.triangleBuffers.textureIndex++] = a)
        }
    }
}(),
function() {
    var e = KeyLines.DateTime = {},
        t = e.defaultLocale = {
            longMonths: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            longDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            ampm: ["AM", "PM"],
            quarter: "Q",
            half: "H"
        },
        n = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        r = e.units = {
            milli: 0,
            sec: 1,
            min: 2,
            hour: 3,
            day: 4,
            week: 5,
            month: 6,
            year: 7
        },
        a = function(e, t) {
            return t ? e - e % t : 0
        },
        i = function(e) {
            if (e < r.milli || r.year < e) throw new Error("bad unit range")
        },
        o = function(e, t) {
            return 1 === e && u(t) ? 29 : n[e]
        },
        u = function(e) {
            return 1 === new Date(e, 1, 29).getMonth()
        };
    e.UTCify = function(e) {
        return Date.UTC(e.getFullYear(), e.getMonth(), e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds(), e.getMilliseconds())
    }, e.unUTCify = function(e) {
        var t = new Date(e);
        return new Date(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate(), t.getUTCHours(), t.getUTCMinutes(), t.getUTCSeconds(), t.getUTCMilliseconds())
    };
    var s = (e.clear = function(e, t, n) {
            i(t);
            var o = new Date(e);
            switch (t > r.milli && o.setUTCMilliseconds(0), t > r.sec && o.setUTCSeconds(0), t > r.min && o.setUTCMinutes(0), t > r.hour && o.setUTCHours(0), t >= r.month && o.setUTCDate(1), t >= r.year && o.setUTCMonth(0), t) {
                case r.milli:
                    o.setUTCMilliseconds(a(o.getUTCMilliseconds(), n));
                    break;
                case r.sec:
                    o.setUTCSeconds(a(o.getUTCSeconds(), n));
                    break;
                case r.min:
                    o.setUTCMinutes(a(o.getUTCMinutes(), n));
                    break;
                case r.hour:
                    o.setUTCHours(a(o.getUTCHours(), n));
                    break;
                case r.day:
                    break;
                case r.week:
                    var u = o.getUTCDay(),
                        s = u >= 1 ? u - 1 : 6;
                    o.setTime(o.getTime() - 24 * s * 60 * 60 * 1e3);
                    break;
                case r.month:
                    o.setUTCMonth(a(o.getUTCMonth(), n));
                    break;
                case r.year:
                    o.setUTCFullYear(a(o.getUTCFullYear(), n))
            }
            return o.getTime()
        }, e.add = function(e, t, n) {
            if (i(t), t <= r.week) {
                var a = n;
                return t > r.milli && (a *= 1e3), t > r.sec && (a *= 60), t > r.min && (a *= 60), t > r.hour && (a *= 24), t > r.day && (a *= 7), e + a
            }
            var l = new Date(e);
            switch (t) {
                case r.month:
                    var f = l.getUTCMonth() + n,
                        c = f % 12,
                        d = Math.floor(f / 12);
                    0 > c && (c += 12);
                    var g = l.getUTCDate();
                    g = Math.min(g, o(c, l.getUTCFullYear() + d)), l.setUTCDate(g), l.setUTCMonth(c), 0 !== d && l.setTime(s(l.getTime(), r.year, d));
                    break;
                case r.year:
                    if (1 === l.getUTCMonth() && 29 === l.getUTCDate()) {
                        var h = l.getUTCFullYear() + n;
                        u(h) || l.setTime(s(l.getTime(), r.day, -1))
                    }
                    l.setUTCFullYear(l.getUTCFullYear() + n)
            }
            return l.getTime()
        }),
        l = function(e) {
            for (var t = e + ""; t.length < 2;) t = "0" + t;
            return t
        },
        f = new RegExp("(f?f?f)|(s?s)|(m?m)|(h?h)|(H?H)|(t?t?t)|(d?d?d?d)|(M?M?M?M?M)|(Q?Q)|yyyy|yy", "gm"),
        c = {
            f: function(e) {
                return Math.floor(e.getUTCMilliseconds() / 100)
            },
            ff: function(e) {
                return l(Math.floor(e.getUTCMilliseconds() / 10))
            },
            fff: function(e) {
                return l(Math.floor(e.getUTCMilliseconds()))
            },
            s: function(e) {
                return e.getUTCSeconds()
            },
            ss: function(e) {
                return l(e.getUTCSeconds())
            },
            m: function(e) {
                return e.getUTCMinutes()
            },
            mm: function(e) {
                return l(e.getUTCMinutes())
            },
            h: function(e) {
                return e.getUTCHours() % 12 || 12
            },
            hh: function(e) {
                return l(e.getUTCHours() % 12 || 12)
            },
            H: function(e) {
                return e.getUTCHours()
            },
            HH: function(e) {
                return l(e.getUTCHours())
            },
            t: function(e, t) {
                return t.ampm[Math.floor(e.getUTCHours() / 12)].charAt(0)
            },
            tt: function(e, t) {
                return t.ampm[Math.floor(e.getUTCHours() / 12)]
            },
            ttt: function(e, t) {
                var n = Math.floor(e.getUTCHours() / 12);
                return t.ampm[n + 2] || t.ampm[n]
            },
            d: function(e) {
                return e.getUTCDate()
            },
            dd: function(e) {
                return l(e.getUTCDate())
            },
            ddd: function(e, t) {
                return t.shortDays[e.getUTCDay()]
            },
            dddd: function(e, t) {
                return t.longDays[e.getUTCDay()]
            },
            M: function(e) {
                return e.getUTCMonth() + 1
            },
            MM: function(e) {
                return l(e.getUTCMonth() + 1)
            },
            MMM: function(e, t) {
                return t.shortMonths[e.getUTCMonth()]
            },
            MMMM: function(e, t) {
                return t.longMonths[e.getUTCMonth()]
            },
            MMMMM: function(e, t) {
                return t.shortMonths[e.getUTCMonth()].substr(0, 1)
            },
            Q: function(e, t) {
                var n = e.getUTCMonth(),
                    r = Math.floor(n / 3) + 1;
                return t.quarter + r
            },
            QQ: function(e, t) {
                var n = e.getUTCMonth(),
                    r = Math.floor(n / 6) + 1;
                return t.half + r
            },
            yyyy: function(e) {
                return e.getUTCFullYear()
            },
            yy: function(e) {
                return l(e.getUTCFullYear() % 100)
            }
        },
        d = (e.format = function(e, n, r) {
            return "number" == typeof e && (e = new Date(e)), r = r || t, n.replace(f, function(t) {
                return c[t](e, r)
            })
        }, "(?::)?"),
        g = "(?:-|$)",
        h = "(?:T|$)",
        v = function(e, t) {
            return e[t] ? parseInt(e[t], 10) : void 0
        },
        m = function(e, t, n, r) {
            var a = v(e, t);
            return x(a) ? a >= n && r >= a ? a : void 0 : n
        },
        x = function(e) {
            return "undefined" != typeof e
        },
        y = new RegExp("^(\\d{4})" + g + "(\\d{2})?" + g + "(\\d{2})?" + h + "(\\d{2})?" + d + "(\\d{2})?" + d + "(\\d{2})?(?:Z)?(\\+|-)?(\\d{2})?" + d + "(\\d{2})?");
    e.parseISO8601 = function(e) {
        var t = y.exec(e);
        if (t) {
            var n = v(t, 1),
                r = m(t, 2, 1, 12),
                a = m(t, 3, 1, 31),
                i = m(t, 4, 0, 23),
                u = m(t, 5, 0, 59),
                s = m(t, 6, 0, 59),
                l = "-" === t[7] ? -1 : 1,
                f = m(t, 8, 0, 12),
                c = m(t, 9, 0, 59);
            if (!(x(n) && x(r) && x(a) && x(i) && x(u) && x(s) && x(f) && x(c))) return;
            if (a > o(r - 1, n)) return;
            var d = l * (60 * f + c) * 60 * 1e3;
            return new Date(Date.UTC(n, r - 1, a, i, u, s) - d)
        }
    }
}(),
function() {
    var e = KeyLines.Combo = {};
    e.create = function(e) {
        function t(t, n) {
            function r(e) {
                t[e.id] && a.push(e.id)
            }
            var a = [];
            return e.privateEach({
                type: n
            }, r), a
        }

        function n(e) {
            for (var t = {}, n = 0; n < e.length; n++) t[e[n].id] = e[n];
            return t
        }

        function r(e) {
            var t = [];
            for (var n in e) n = U.rawId(n), t.push(e[n]);
            return t
        }

        function a(e) {
            return e + Math.floor(1e7 * Math.random())
        }

        function i(e, t) {
            var n = a(t);
            return e[n] ? i(e) : n
        }

        function o(e, t) {
            return e.substring(0, t.length) === t
        }

        function u(e) {
            return o(e, K) && !!X[e]
        }

        function s(e) {
            var t = [];
            if (u(e))
                for (var n = X[e].nodes, r = 0; r < n.length; r++) u(n[r].id) ? t = t.concat(s(n[r].id)) : t.push(n[r]);
            return t
        }

        function l(e) {
            var t = [];
            if (u(e))
                for (var n = X[e].nodes, r = 0; r < n.length; r++) u(n[r].id) && (t.push(n[r].id), t = t.concat(l(n[r].id)));
            return t
        }

        function f(e, t) {
            var n = H[e.id];
            if ("undefined" == typeof n) return 0;
            var r = X[n],
                a = +e[t] - r[t];
            return a + f(r, t)
        }

        function c(e, t, n) {
            var r = i(X, K);
            return X[r] = {
                id: r,
                nodes: e,
                x: t,
                y: n
            }, r
        }

        function d(e) {
            var t = X[e];
            return delete X[e], t
        }

        function g() {
            var e = {};
            for (var t in X) {
                t = U.rawId(t);
                for (var n = 0; n < X[t].nodes.length; n++) e[X[t].nodes[n].id] = t
            }
            return e
        }

        function h() {
            H = g()
        }

        function v(e) {
            for (var t; e;) t = e, e = H[e];
            return t
        }

        function m(e) {
            return e !== v(e)
        }

        function x(e) {
            return o(e, Y)
        }

        function y() {
            var t = a(Y);
            return e.getItem(t) ? y() : t
        }

        function p(e, t, n) {
            var r = v(e.id1),
                a = v(e.id2);
            return r === t && a === n || r === n && a === t
        }

        function b(t) {
            if (x(t)) {
                var n = e.getItem(t);
                if (n) {
                    var r = [];
                    for (var a in P) a = U.rawId(a), p(P[a], n.id1, n.id2) && r.push(P[a]);
                    return r
                }
            }
            return null
        }

        function I(e) {
            for (var t = [], n = {}, r = 0; r < e.length; r++) n[e[r].id] = 1;
            for (var a in P) {
                a = U.rawId(a);
                var i = P[a];
                n[i.id1] && n[i.id2] && t.push(i)
            }
            return t
        }

        function w() {
            function t(e, t, n) {
                var r = t > e ? e : t,
                    a = e > t ? e : t;
                f[r] || (f[r] = {}), f[r][a] || (f[r][a] = []), f[r][a].push(n)
            }

            function n(e) {
                for (var t = 0; t < e.length; t++)
                    if (!e[t].hi) return !0;
                return !1
            }

            function r(e) {
                return !(o(e, K) && !X[e])
            }

            function a(e) {
                for (var t = {}, n = s(e), r = 0; r < n.length; r++) t[n[r].id + ""] = 1;
                return t
            }

            function i(e, t, n, r) {
                var i, o, s, l;
                u(t) ? s = a(t) : (s = {}, s[t + ""] = 1), u(n) ? l = a(n) : (l = {}, l[n + ""] = 1);
                for (var f = 0;
                    (!i || !o) && f < r.length; f++) {
                    var c = r[f];
                    i = i || c.a1 && c.id1 in s, i = i || c.a2 && c.id2 in s, o = o || c.a2 && c.id2 in l, o = o || c.a1 && c.id1 in l
                }
                e.a1 = i, e.a2 = o
            }
            var l = {};
            e.privateEach({
                type: "node"
            }, function(e) {
                l[e.id] = {}
            }), e.privateEach({
                type: "link"
            }, function(e) {
                l[e.id1][e.id2] = l[e.id2][e.id1] = !0
            });
            var f = {};
            for (var c in P) {
                c = U.rawId(c);
                var d = P[c],
                    g = v(d.id1),
                    h = v(d.id2),
                    m = r(g) && r(h);
                m && g !== h && (l[g][h] || t(g, h, d))
            }
            var x = [];
            for (var p in f) {
                p = U.rawId(p);
                for (var b in f[p])
                    if (b = U.rawId(b), u(p) || u(b)) {
                        var I = f[p][b],
                            w = U.clone(I[0]);
                        w.id1 = p, w.id2 = b, w.id = y(), w.hi && n(I) && delete w.hi, w.off = void 0, i(w, p, b, I), x.push(w)
                    } else
                        for (var B = f[p][b], C = 0; C < B.length; C++) x.push(B[C]), delete P[B[C].id]
            }
            return x
        }

        function B(t, n, r) {
            for (var a = e.privateGraph().shortestPaths(n, r, {
                    all: !0
                }).items, i = 0; i < a.length; i++)
                if (a[i] !== n && a[i] !== r && x(a[i]))
                    for (var o = b(a[i]), u = 0; u < o.length; u++)
                        if (t === o[u].id) return a[i]
        }

        function C(e) {
            var t, n = P[e],
                r = v(n.id1),
                a = v(n.id2),
                i = r && a && r !== a,
                o = r && r === a;
            return t = i ? B(e, r, a) : o ? r : r || a
        }

        function A(e) {
            e = U.ensureArray(e);
            for (var t = [], n = 0; n < e.length; n++) {
                var r = null;
                if (e[n]) {
                    r = v(e[n]);
                    var a = u(r),
                        i = e[n] in P;
                    a || (r = i ? C(e[n]) : null)
                }
                t.push(r)
            }
            return t
        }

        function M(e) {
            if (U.isNullOrUndefined(e)) throw new Error("[Find] Undefined argument passed");
            return U.isArray(e) ? A(e) : A(e)[0]
        }

        function D(e, t) {
            if (U.isNullOrUndefined(t.label)) {
                for (var n = "", r = 0; r < e.length; r++) U.isNullOrUndefined(e[r].t) || (n += e[r].t, r < e.length - 1 && (n += "\n"));
                return n
            }
            return t.label
        }

        function L(e, t, n) {
            var r, a = 0,
                i = 0;
            for (r = 0; r < e.length; r++) a += e[r].x, i += e[r].y;
            a /= e.length, i /= e.length;
            var o = "average" === t.position ? a : e[0].x,
                u = "average" === t.position ? i : e[0].y,
                s = U.isNumber(t.dx) ? t.dx : 0,
                l = U.isNumber(t.dy) ? t.dy : 0;
            for (r = 0; r < e.length; r++) n.push({
                id: e[r].id,
                x: o,
                y: u
            }), e[r].x += s, e[r].y += l;
            return {
                x: o,
                y: u
            }
        }

        function Z(e, t, n) {
            var r = U.isNormalObject(t.style) ? t.style : U.clone(e[0]);
            delete r.id;
            var a = {
                    p: "ne",
                    c: "rgb(255, 0, 0)",
                    t: n
                },
                i = [];
            return U.isNormalObject(t.glyph) && (a = U.merge(a, t.glyph), t.glyph.c || t.glyph.u || (a.c = "rgb(255, 0, 0)")), null !== t.glyph && i.push(a), U.isArray(r.g) && (i = i.concat(r.g)), r.g = i, U.isNormalObject(t.d) && (r.d = U.clone(t.d)), r.type = "node", r
        }

        function R(e) {
            for (var t = [], n = 0; n < e.length; n++) t.push(e[n].id);
            return t
        }

        function G(e) {
            for (var t, n = [], r = {}, a = 0; a < e.length; a++)
                for (var i = 0; i < e[a].ids.length; i++) {
                    if (t = e[a].ids[i], r[t]) throw new Error("[Combine] nodes should be only in one combo");
                    r[t] = 1, n.push(t)
                }
            return n
        }

        function S(t) {
            for (var n, r, a = {}, i = [], o = e.getItem(t), u = 0; u < t.length; u++)
                if (n = t[u], !a[n]) {
                    if (r = o[u], !r || "node" !== r.type) throw new Error("[Combine] function must be passed valid ids");
                    "node" === r.type && (i.push(r), a[n] = 1)
                }
            if (i.length < 1) throw new Error("[Combine] function must be passed at least one node id");
            return i
        }

        function T(e, t, n) {
            var r = c(e, n.x, n.y),
                a = s(r),
                i = Z(e, t, a.length),
                o = U.merge(i, {
                    id: r,
                    t: D(e, t),
                    x: n.x,
                    y: n.y
                });
            return o
        }

        function E(t, n, r) {
            function a() {
                for (var a = [], i = 0; i < t.length; i++) {
                    var o = T(l[i], t[i], g[i]);
                    a.push(o)
                }
                h(), e.merge(a, function() {
                    e.privateRemoveItem(f);
                    var t = w();
                    e.privateMerge(t, function() {
                        var t = R(a);
                        n.select && (e.selection(t), e.trigger("selectionchange")), U.invoke(r, t)
                    })
                })
            }
            var i, o, u, s, l = [];
            for (n = U.defaults(n, {
                    animate: !0,
                    select: !0,
                    time: 250
                }), t = U.ensureArray(t), i = 0; i < t.length; i++) s = t[i], t[i] = U.defaults(s, {
                position: "average",
                dx: 0,
                dy: 0,
                label: null,
                style: null,
                d: null
            }), t[i].ids = U.ensureArray(t[i].ids), u = S(t[i].ids), l.push(u);
            var f;
            f = G(t);
            var c, d = [],
                g = [];
            for (i = 0; i < t.length; i++) c = L(l[i], t[i], d), g.push(c);
            var v = e.privateGraph().neighbours(f, {
                all: !0
            });
            for (o = 0; o < v.links.length; o++) {
                var m = v.links[o];
                x(m) || (P[m] = e.getItem(m))
            }
            e.trigger("prechange", "combine");
            var y = d.length ? n.time : 0;
            n.animate ? e.animateProperties(d, {
                time: y
            }, a) : e.setProperties(d, !1, a)
        }

        function k(t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.hi = !e.privateBothEndsShown(r)
            }
        }

        function F(t, n, r) {
            function a() {
                n.select && (e.selection(y), e.trigger("selectionchange")), U.invoke(r)
            }
            e.trigger("prechange", "uncombine"), n = U.defaults(n, {
                animate: !0,
                time: 250,
                full: !1,
                select: !0
            }), h();
            for (var i, o, c = U.ensureArray(t), g = {}, v = 0, m = {}, x = [], y = [], p = [], b = 0; b < c.length; b++)
                if (B = c[b], u(B)) {
                    var I = e.getItem(B);
                    I && (v++, g[B] = I, n.full ? (m[B] = s(B), x = x.concat(l(B))) : m[B] = X[B].nodes)
                }
            if (0 === v) return void U.invoke(r);
            for (var B in g)
                for (B = U.rawId(B), o = 0; o < m[B].length; o++) i = m[B][o], y.push(i.id), p.push({
                    id: i.id,
                    x: g[B].x + f(i, "x"),
                    y: g[B].y + f(i, "y"),
                    pos: i.pos
                });
            for (var C = 0; C < x.length; C++) d(x[C]);
            var A = [],
                M = [];
            for (var D in g) {
                D = U.rawId(D);
                for (var L = m[D], Z = 0; Z < L.length; Z++) {
                    var R = L[Z];
                    R.x = g[D].x, R.y = g[D].y
                }
                A = A.concat(L), M.push(D)
            }
            for (e.privateMerge(A), o = 0; o < M.length; o++) d(M[o]);
            e.privateRemoveItem(M), h();
            var G = w();
            k(G), e.privateMerge(G, function() {
                n.animate ? e.animateProperties(p, {
                    time: n.time
                }, a) : e.setProperties(p, !1, a)
            })
        }

        function W(e) {
            if (u(e)) {
                var t = s(e);
                return {
                    nodes: t,
                    links: I(t)
                }
            }
            return x(e) ? {
                nodes: null,
                links: b(e)
            } : null
        }

        function N(e, t) {
            var n = U.ensureArray(e);
            if (t = U.defaults(t, {
                    type: "all"
                }), !t.type.match(/^(node|link|all)$/)) throw new TypeError("[isCombo] opts type property should be either 'node', 'link' or 'all'");
            for (var r = 0; r < n.length; r++) {
                if (u(n[r]) && "link" !== t.type) return !0;
                if (x(n[r]) && "node" !== t.type) return !0
            }
            return !1
        }

        function O(e) {
            for (var t = ["id", "id1", "id2"], n = 0; n < t.length; n++) t[n] in e && (e[t[n]] += "")
        }

        function V(e) {
            e && (X = n(e.nodes), P = n(e.links));
            var t = U.clone(r(X)),
                a = U.clone(r(P));
            return {
                nodes: t,
                links: a
            }
        }
        var U = KeyLines.Util,
            K = "_combo_",
            X = {},
            H = {},
            Y = "_combolink_",
            P = {},
            z = {
                api: {
                    combine: E,
                    uncombine: F,
                    info: W,
                    isCombo: N,
                    find: M
                },
                internalUse: {
                    preMerge: function(e) {
                        for (var t = [], n = 0; n < e.length; n++) {
                            var r = e[n];
                            if (O(r), "node" === r.type) m(r.id) || t.push(r);
                            else {
                                if ("link" !== r.type) throw new Error("The type of the item should be specified.");
                                m(r.id1) || m(r.id2) ? P[r.id] = r : t.push(r)
                            }
                        }
                        return t
                    },
                    postMerge: function(t) {
                        var n = w();
                        e.privateMerge(n, t)
                    },
                    preDelete: function(n) {
                        var r, a;
                        n = U.ensureArray(n);
                        var i = U.makeIdMap(n),
                            o = t(i, "node"),
                            s = t(i, "link"),
                            l = e.privateGraph().neighbours(o, {
                                all: !0
                            }).links,
                            f = U.makeIdMap(l.concat(s));
                        for (var c in P)
                            if (c in f) {
                                if (x(c)) {
                                    var g = b(c);
                                    for (r = 0; r < g.length; r++) P[g[r].id] && delete P[g[r].id]
                                }
                            } else {
                                var m = P[c],
                                    y = v(m.id1),
                                    p = v(m.id2);
                                (y in i || p in i) && delete P[c]
                            }
                        var I;
                        for (r = 0; r < s.length; r++)
                            if (I = s[r], x(I)) {
                                var g = b(I);
                                for (a = 0; a < g.length; a++) delete P[g[r].id]
                            }
                        for (r = 0; r < o.length; r++) I = o[r], u(I) && d(I);
                        h()
                    },
                    serialize: V,
                    loadUp: function(e) {
                        e ? V(e) : (X = {}, P = {}), h()
                    }
                }
            };
        return z
    }
}(),
function() {
    var e = KeyLines.View = {},
        t = e.createView = function(e, n, r, a, i) {
            function o() {
                return i && i.api.isShown()
            }

            function u() {
                return W(d * L)
            }

            function s() {
                return W(d / L)
            }

            function l(e, t, n, r, a, i, o, u, s, l, f) {
                t.generate(n, r, p, a, s);
                var c = n.itemExtents(e, r);
                if (c) {
                    var d = O(c);
                    return f && (d.x1 = d.x2 = p.width() / 2), V(d, i, !0, !0, !0, o, !1, u, l)
                }
                return x.invoke(u), null
            }
            var f, c, d, g, h, v, m, x = KeyLines.Util,
                y = KeyLines.Common,
                p = {},
                b = n,
                I = r,
                w = a ? a : 1;
            p.scaleFactor = function(e) {
                return e && (w = e), w
            };
            var B = p.scale = function(e) {
                return Math.floor(w * e)
            };
            p.unscale = function(e) {
                return Math.floor(e / w)
            }, p.settings = function() {
                return {
                    width: b,
                    height: I,
                    zoom: d,
                    offsetX: g,
                    offsetY: h
                }
            }, p.fromSettingsDirect = function(e, t, n, r) {
                return z(d, W(e.zoom), e.offsetX - g, e.offsetY - h, t, n, r)
            }, p.fromOldSettings = function(n, r, a, i) {
                if (r) {
                    var o = t(e, n.width, n.height);
                    o.setZoom(n.zoom, !1), o.offsetX = n.offsetX, o.offsetY = n.offsetY;
                    var u = {
                        x1: o.viewToWorldX(0),
                        y1: o.viewToWorldY(0),
                        x2: o.viewToWorldX(o.width()),
                        y2: o.viewToWorldY(o.height())
                    };
                    return V(u, a, !1, !1, !1, i)
                }
                N(n.zoom, !1), g = n.offsetX, h = n.offsetY
            }, p.width = function(e) {
                return e && (b = e), b
            }, p.height = function(e) {
                return e && (I = e), I
            };
            var C = p.x = p.worldToViewX = function(e) {
                    return Math.floor(((e - f) * d + g) * w)
                },
                A = p.y = p.worldToViewY = function(e) {
                    return Math.floor(((e - c) * d + h) * w)
                },
                M = p.viewToWorldX = function(e) {
                    return Math.floor((e / w - g) / d + f)
                },
                D = p.viewToWorldY = function(e) {
                    return Math.floor((e / w - h) / d + c)
                };
            p.w = function(e) {
                return -1 === e ? -1 : Math.max(1, Math.floor(e * d * w))
            }, p.sw = function(e) {
                return -1 === e ? -1 : p.w(e) + 7
            }, p.dim = function(e) {
                return Math.floor(e * d * w)
            }, p.sdim = function(e) {
                return e && e * d * w
            }, p.undim = function(e) {
                return Math.floor(e / (d * w))
            }, p.sundim = function(e) {
                return e / (d * w)
            };
            var L = 1.4,
                Z = 4,
                R = .05,
                G = R,
                S = .01,
                T = 1,
                E = 200,
                k = 1 / 0,
                F = 100;
            p.getMaxZoom = function() {
                return Z
            }, p.getMinZoom = function() {
                return G
            }, p.setMinZoom = function(e) {
                G = Math.min(T, Math.max(S, e || R))
            }, p.setMaxItemZoom = function(e) {
                k = e ? e : 1 / 0
            }, p.getMaxItemZoom = function() {
                return k
            }, p.zoomIn = function(e, t, n) {
                var r = u();
                return U(r, w * b / 2, w * I / 2, e, t, n)
            }, p.zoomOut = function(e, t, n) {
                var r = s();
                return U(r, w * b / 2, w * I / 2, e, t, n)
            };
            var W = function(e) {
                    return Math.min(Z, Math.max(e, G))
                },
                N = p.setZoom = function(e, t, n, r) {
                    var a = W(e);
                    return U(a, w * b / 2, w * I / 2, t, n, r)
                };
            p.getUnzoom = function() {
                return d > k ? k / d : 1
            }, p.wheelToPosition = function(e, t, n) {
                var r = e > 0 ? u() : s();
                return U(r, t, n, !0, 200)
            };
            var O = p.worldExtentsToView = function(e) {
                return e ? {
                    x1: C(e.x1),
                    y1: A(e.y1),
                    x2: C(e.x2),
                    y2: A(e.y2)
                } : null
            };
            p.fitWorldExtents = function(e, t, n, r, a, i, o) {
                if (!e) return x.invoke(o), null;
                var u = O(e);
                return V(u, t, n, r, a, i, !1, o)
            };
            var V = p.fitExtents = function(e, t, n, r, a, i, o, u, s) {
                var l, v = (e.y2 - e.y1) / (e.x2 - e.x1),
                    m = I / b;
                if (v > m) {
                    var x = a ? I / 8 : 0;
                    l = d * (I - x) / (e.y2 - e.y1)
                } else {
                    var y = a ? b / 8 : 0;
                    l = d * (b - y) / (e.x2 - e.x1)
                }
                l *= w, n && (l = W(l)), r && l > 1 && (l = 1), s && (l = d);
                var p = (e.x1 + e.x2) / 2,
                    B = (e.y1 + e.y2) / 2,
                    C = M(p),
                    A = D(B),
                    L = b / 2 - (C - f) * l,
                    Z = I / 2 - (A - c) * l;
                return z(d, l, L - g, Z - h, t, i, u, o)
            };
            p.fitToSelection = function(e, t, n, r, a, i, o, u, s) {
                return l(e.privateSelection(), e, t, n, r, a, i, o, u, s)
            }, p.fitToModel = function(e, t, n, r, a, i, o, u) {
                return l(null, e, t, n, r, a, i, o, u)
            }, p.fitModelHeight = function(e, t, n, r, a, i, o, u) {
                return l(null, e, t, n, r, a, i, o, u, !1, !0)
            };
            var U = p.zoomToPosition = function(e, t, n, r, a, i) {
                    e = W(e);
                    var o = M(t),
                        u = D(n),
                        s = t / w - (o - f) * e,
                        l = n / w - (u - c) * e;
                    return z(d, e, s - g, l - h, r, a, i)
                },
                K = p.reset = function(e) {
                    f = 421, c = 298, e ? z(d, 1, 421 - g, 298 - h, !0) : (g = 421, h = 298, d = 1)
                };
            p.zoom = function() {
                return o() ? i.internalUse.getZoom() : d
            }, p.offsetXtoWebglPanX = function() {
                var e = b / 2;
                return -(1 - p.offsetX() / e) + (e - f) / e * d
            }, p.offsetYtoWebglPanY = function() {
                var e = I / 2;
                return 1 - p.offsetY() / e - (e - c) / e * d
            }, p.offsetX = function(e) {
                return x.defined(e) && (g = e), g
            }, p.offsetY = function(e) {
                return x.defined(e) && (h = e), h
            }, p.diffX = function() {
                return v
            }, p.diffY = function() {
                return m
            }, p.pan = function(e, t, n, r) {
                var a = 0,
                    i = 0;
                switch (e) {
                    case "up":
                        i = 1;
                        break;
                    case "down":
                        i = -1;
                        break;
                    case "left":
                        a = 1;
                        break;
                    case "right":
                        a = -1
                }
                return a *= F, i *= F, P(a, i, t, !1, n, r)
            };
            var X, H, Y, P = p.panBy = function(e, t, n, r, a, i) {
                    return z(d, d, e, t, n, a, i, r)
                },
                z = function(t, n, r, a, i, o, u, s) {
                    return v = r, m = a, i ? _(t, n, r, a, o, u) : (g += Math.floor(r), h += Math.floor(a), d = n, e && !s && e.trigger("viewchange"), x.invoke(u), void 0)
                },
                _ = function(t, n, r, a, i, o) {
                    i || (i = E);
                    var u = i,
                        s = g,
                        l = h;
                    return y.buildAnimatorInstance(function(f) {
                        u -= f;
                        var c = e.atanEasing(Math.max(0, Math.min(1, (i - u) / i)));
                        g = s + c * r, h = l + c * a, d = W(t + c * (n - t));
                        var v = u > 0;
                        return v || (e.trigger("viewchange"), x.invoke(o)), v
                    }, {
                        drawOnly: !0
                    }, !0)
                },
                j = function(t, n, r, a, u) {
                    var s = o(),
                        l = s ? -3 : .1;
                    return y.buildAnimatorInstance(function(o) {
                        var f = 0 === H && 0 === Y;
                        return s ? i.internalUse.panBy([H * l, Y * l], {
                            animate: !1
                        }) : (g += o * l * H, h += o * l * Y), X = !f, r && r.dragMove(t, n, a, u), f || e.trigger("viewchange"), !f
                    })
                };
            return p.cancelScroll = function() {
                H = Y = 0
            }, p.maybeScroll = function(e, t, n, r, a) {
                var i = B(50);
                return H = 0, Y = 0, i > e && (H = 1), e > B(b) - i && (H = -1), i > t && (Y = 1), t > B(I) - i && (Y = -1), n.direction && (n.direction.x || (H = 0), n.direction.y || (Y = 0)), 0 === H && 0 === Y || X ? void 0 : (X = !0, j(e, t, n, r, a))
            }, K(), p
        }
}(),
function() {
    var e = KeyLines.Canvas = {},
        t = 8888,
        n = e.getDevicePixelRatio = function() {
            return "undefined" != typeof window ? window.devicePixelRatio || 1 : void 0
        };
    e.create = function(e, r, a, i, o, u, s) {
        function l(e, t) {
            var n, r = document.createElement("canvas");
            return n = s ? document.createElement("canvas").getContext("2d") : r.getContext("2d"), r.context2d = n, r.height = t, r.width = e, s || g(r), r
        }

        function f(e, t) {
            var r = n(),
                a = t.webkitBackingStorePixelRatio || t.mozBackingStorePixelRatio || t.msBackingStorePixelRatio || t.oBackingStorePixelRatio || t.backingStorePixelRatio || 1,
                i = r / a;
            if (r !== a) {
                var o = e.width,
                    u = e.height;
                e.width = Math.round(o * i), e.height = Math.round(u * i), e.style.width = o + "px", e.style.height = u + "px", s || t.scale(i, i)
            }
        }

        function c() {
            if (!KeyLines.Rendering.rtlBugChecked) {
                var e = 100,
                    t = 30,
                    n = l(e, t);
                document.body.appendChild(n), n.setAttribute("dir", "rtl");
                var r = n.context2d;
                r.fillStyle = "#000", r.fillRect(0, 0, e, t), r.textAlign = "center", r.fillStyle = "#fff", r.font = "20px sans-serif", r.fillText("\u2588", e / 2, t);
                var a = r.getImageData(e / 2, t - 1, 1, 1).data;
                KeyLines.Rendering.rtlBugPresent = 0 === a[0], document.body.removeChild(n), KeyLines.Rendering.rtlbugChecked = !0
            }
        }

        function d() {
            if (!KeyLines.Rendering.minimumFontChecked) {
                for (var e = "Hello", t = l(100, 100).context2d, n = !1, r = function(n) {
                        return KeyLines.Rendering.setFont(t, n), t.measureText(e).width
                    }, a = r(1), i = 2; !n && 30 > i;) {
                    var o = r(i);
                    n = a !== o, n && (KeyLines.Rendering.browserMinimumFontSize = i - 1), i++
                }
                KeyLines.Rendering.minimumFontChecked = !0
            }
        }

        function g(e) {
            function t(e) {
                n.setLineDash = function(t) {
                    n[e] = t
                }
            }
            var n = e.getContext("2d");
            n.setLineDash || ("undefined" != typeof n.webkitLineDash ? t("webkitLineDash") : "undefined" != typeof n.mozDash && t("mozDash"))
        }

        function h() {
            return new Image
        }

        function v() {
            w.update(), C(v)
        }
        var m = l(a, i),
            x = {},
            y = m.context2d,
            p = document.getElementById(r);
        p.style.display = "block", p.parentNode.replaceChild(m, p), m.setAttribute("id", r), s && KeyLines.webGLSupport() && "chart" === e && (x.renderer = m.renderer = KeyLines.WebGL.create(m), m.webgl = !0), m.canvas = !0;
        var b = m;
        m.webgl ? (y.webgl = !0, y.webglCanvas = m) : y.webgl = !1, f(m, y), d(), c();
        var I = b.getAttribute("style") || "";
        I += "-ms-touch-action: none; touch-action: none; outline: none;", b.setAttribute("style", I), b.setAttribute("tabindex", "" + t++);
        var w = "chart" === e ? KeyLines.API.createAPI(r) : KeyLines.TimeBar.API.createAPI(r);
        for (var B in w) - 1 === B.indexOf("private") && (x[B] = w[B]);
        w.privateInit(y, a, i, l, function(e) {
            document.getElementById(r).style.cursor = e
        }, h, o, void 0, u), x.internalSetSize = function(e, t, n) {
            w.privateMapSetSize && w.privateMapSetSize(e, t), b.width = e, b.height = t, f(b, y), w.privateSetSize(y, e, t, n, !0)
        };
        var C = function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e) {
                KeyLines.Util.nextTick(e, 1e3 / 60)
            }
        }();
        return C(v), x
    }
}(),
function() {
    var e = KeyLines.TimeBar.Controller = {};
    e.createController = function(e, t, n, r) {
        function a() {
            e.trigger("redraw")
        }

        function i(e, n, r) {
            _ = e, Y = n, P = r, t.setSize(n, r);
            var a = z(Y, P);
            ie = KeyLines.Common.hitTester(a.getContext("2d"), n, r, te)
        }

        function o(e) {
            var t = {},
                n = $.useHiResImages();
            for (var r in me) {
                var i = me[r];
                t[i] = !0, n && (t[$.hiResUrl(i)] = !0)
            }
            $.imageLoader(e, j, t, function(e, t) {
                he = {};
                for (var n in t) he[n] = {
                    im: t[n]
                };
                a()
            })
        }

        function u() {
            pe._tbLeft = pe._tbRight = 'url("' + j + 'openhand.cur"), pointer';
            for (var e in ge) pe[e] = "pointer"
        }

        function s() {
            var e = null,
                t = q.makeValueAnimator(ne, function(t) {
                    return 0 === arguments.length ? e : void(e = t)
                });
            return function(n) {
                return r.options.heightChange.animate && null !== e ? t(n, r.options.heightChange.time) : e = n, e
            }
        }

        function l(e, t) {
            return e && t ? e.t1 !== t.t1 || e.t2 !== t.t2 : !0
        }

        function f(e, n) {
            for (var r = t.getHistogram(), a = "_tbBar" === ue, i = 0; r.x[i + 1] < n && i < r.count;) i++;
            var o = a ? r.value[i] : r.selvalues[e][i],
                u = a ? r.maxvalue : r.maxselvalue,
                s = (r.x[i] + r.x[i + 1]) / 2,
                l = t.getBounds().histogram,
                f = l.ybase,
                c = a ? l.maxbarh : l.maxselh,
                d = Math.floor(c * o / u),
                g = f - d;
            return a || (g -= 5), {
                x: s,
                y: g,
                v: o
            }
        }

        function c(n, r, a) {
            ue = ie.test(n, r), x(n, r) && (se = t.findRange(n, r));
            var i = l(se, de.lastRange);
            if ("hover" !== a || ue !== de.lastId || i) {
                var o, u, s, c, d = n,
                    g = r,
                    h = null,
                    v = se && "histogram" === se.bar;
                if (x(n, r) && (o = ee.unUTCify(se.t1), u = ee.unUTCify(se.t2), h = v ? null : "scale"), y()) {
                    h = "bar", "_tbBar" !== ue && (h = "selection", s = +ue.match(xe)[1], v || (se = t.findRange(n, r, "histogram"), o = ee.unUTCify(se.t1), u = ee.unUTCify(se.t2)));
                    var m = f(s, n);
                    d = m.x, g = m.y, c = m.v
                }
                return !h && ue && (h = O(ue)), de.lastId = ue, de.lastRange = se, e.trigger(a, h, s, c, d, g, o, u)
            }
        }

        function d(n) {
            r.playingExtend = !!n.extend, J || (J = !0, ne.push(KeyLines.Common.buildAnimatorInstance(function(n) {
                var a = t.offsetRanges(Math.floor(t.getTimeRate() * (r.options.playSpeed || r.defaultPlaySpeed) * n / 1e3), r.playingExtend);
                return a || e.trigger("pause"), J && a ? (t.triggerTimeBarChanged(), !0) : (J = !1, !1)
            })))
        }

        function g(e, n, a, i, o, u) {
            var s = e.t1,
                l = e.t2,
                f = a - s,
                c = i - l;
            o = o || {};
            var d = o.time || r.defaultAnimationTime,
                g = d;
            ne.push(KeyLines.Common.buildAnimatorInstance(function(e) {
                g = Math.max(0, g - e);
                var r = 1 - g / d,
                    a = s + f * r,
                    i = l + c * r;
                return n(a, i), 0 >= g && (t.triggerTimeBarChanged(), Q.invoke(u)), g > 0
            }))
        }

        function h(e, n, r, a) {
            var i = t.limitInnerRange(e, n);
            g(t.getInnerRange(), t.setInner, i.t1, i.t2, r, a)
        }

        function v(e, n, r, a) {
            r.animate ? h(e, n, r, a) : (t.setInner(e, n), t.triggerTimeBarChanged(), Q.invoke(a))
        }

        function m(e, n, r, a) {
            g(t.getOuterRange(), t.setOuter, e, n, r, a)
        }

        function x(e, n) {
            return t.inMainBarOrHistogram(e, n) || xe.test(ue)
        }

        function y() {
            return "_tbBar" === ue || xe.test(ue)
        }

        function p(e, t) {
            return Math.min(t.max, Math.max(t.min, e))
        }

        function b(e) {
            return function(n) {
                var a = t.getInnerRange()[e],
                    i = t.getSideLimits(e);
                return {
                    getCursor: function() {
                        return "ew-resize"
                    },
                    dragMove: function(r) {
                        t.setInnerRangeSide(e, p(a + (r - n) * t.getTimeRate(), i)), t.triggerTimeBarChanged()
                    },
                    endDrag: function(n) {
                        if (n && t.setInnerRangeSide(e, a), "fixed" === r.options.sliders) {
                            var i = t.getInnerRange(),
                                o = (i.t2 - i.t1) * r.fixedSideFactor;
                            m(i.t1 - o, i.t2 + o, {
                                time: r.sliderReleaseTime
                            })
                        }
                    }
                }
            }
        }

        function I(e, t) {
            le.x === e && le.y === t || (le.x = e, le.y = t, a())
        }

        function w() {
            I(-1, -1)
        }

        function B() {
            (ue in ge || de.lastId in ge) && a()
        }

        function C(e, n, r) {
            var a = t.nextPage(e);
            v(a.t1, a.t2, n, r)
        }

        function A(e) {
            var n = t.getInnerRange().t1;
            return {
                getCursor: function() {
                    return "move"
                },
                dragMove: function(a) {
                    var i = Math.floor((e - a) * t.getTimeRate());
                    "free" === r.options.sliders && (i = -1 * i), t.offsetRanges(i + (n - t.getInnerRange().t1)), t.triggerTimeBarChanged()
                },
                endDrag: function(e) {
                    e && t.offsetRanges(n - t.getInnerRange().t1)
                }
            }
        }

        function M(e) {
            return e[0].x > e[1].x ? {
                xmin: e[1].x,
                xmax: e[0].x
            } : {
                xmin: e[0].x,
                xmax: e[1].x
            }
        }

        function D(e) {
            var n = M(e),
                a = t.xtot(n.xmin),
                i = t.xtot(n.xmax),
                o = i - a,
                u = t.getInnerRange(),
                s = t.ttox(u.t1),
                l = t.ttox(u.t2);
            return {
                pinchMove: function(e) {
                    var u = M(e);
                    if ("free" === r.options.sliders) {
                        var f = s + (u.xmin - n.xmin),
                            c = l + (u.xmax - n.xmax);
                        t.setInner(t.xtot(f), t.xtot(c))
                    } else if (u.xmax !== u.xmin) {
                        var d = o / (u.xmax - u.xmin),
                            g = Math.floor(a - d * (u.xmin - s)),
                            h = Math.floor(i + d * (l - u.xmax));
                        t.setInner(g, h), t.triggerTimeBarChanged()
                    }
                },
                endPinch: function() {}
            }
        }

        function L(e) {
            var t = N(ie.test(e.x, e.y));
            return Ie[t]
        }

        function Z(t, n, r, a, i) {
            fe && (fe.endDrag(e.trigger("dragend", fe.type, n, r)), e.trigger("dragcomplete", fe.type, n, r), fe = null, ae("auto"))
        }

        function R() {
            ce && (ce = null)
        }

        function G(e, t, n, r) {
            w(), Z(!1, e, t, n, r)
        }

        function S(e, n, r, a, i) {
            a = Q.defaults(a, {
                animate: !0,
                time: 200
            }), r = r || .5;
            var o = e > 0 ? r : 1 / r,
                u = t.getInnerRange(),
                s = u.t1,
                l = u.t2,
                f = l - s,
                c = f * o,
                d = (n - u.t1) / (u.t2 - u.t1);
            s = Math.floor(s - (c - f) * d), l = Math.floor(l + (c - f) * (1 - d)), v(s, l, a, i)
        }

        function T(e, n) {
            var r;
            if (r = e && e.id ? t.getRangeForIds(e.id) : t.dataRange()) {
                var a = e && Q.isNumber(e.pad) ? e.pad : 0;
                if (a = Math.max(a, 0), 0 !== a) {
                    var i = Math.floor((r.t2 - r.t1) * a / 100);
                    r.t1 -= i, r.t2 += i
                }
                re.range(r.t1, r.t2, e, n)
            } else Q.invoke(n)
        }

        function E() {
            var e = t.getInnerRange();
            return (e.t1 + e.t2) / 2
        }

        function k(e, t, n) {
            S(e, E(), .5, t, n)
        }

        function F(e, t) {
            k(1, e, t)
        }

        function W(e, t) {
            k(-1, e, t)
        }

        function N(e) {
            return xe.test(e) && (e = "_tbLine"), e
        }

        function O(e) {
            var t = 0 === e.indexOf("_tb") ? e.substr(3) : e;
            return t.toLowerCase()
        }

        function V(t, n, a) {
            var i = ie.test(t, n);
            if (a || ("_tbplay" === i ? (r.playingExtend = !1, re.play()) : "_tbplayextend" === i ? (r.playingExtend = !0, re.play({
                    extend: !0
                })) : "_tbpause" === i ? re.pause() : "_tbnext" === i ? re.pan("forward") : "_tbprev" === i ? re.pan("back") : "_tbfit" === i ? T() : "_tbzoomin" === i ? "free" === r.options.sliders ? W() : F() : "_tbzoomout" === i && ("free" === r.options.sliders ? F() : W())), i = N(i), be[i]) {
                var o = "hand";
                /left|right/i.test(i) && (o = O(i)), e.trigger("dragstart", o, t, n) || (fe = be[i](t, n), fe.type = o, ae(fe.getCursor(t, n)))
            }
        }

        function U(e, t) {
            if (1 === e.length) V(e[0].x, e[0].y, !t), c(e[0].x, e[0].y, "touchdown");
            else if (2 === e.length) {
                var n = L(e[0]),
                    r = L(e[1]);
                n && n === r && (ce = n(e))
            }
        }

        function K(e, n) {
            if (t.inMainBarOrHistogram(e, n)) {
                if (!c(e, n, "dblclick")) {
                    var a = t.findRange(e, n);
                    a = t.limitInnerRange(a.t1, a.t2, !0);
                    var i = "free" === r.options.sliders ? r.doubleClickZoomTimeFree : r.doubleClickZoomTime;
                    h(a.t1, a.t2, {
                        time: i
                    })
                }
            } else c(e, n, "dblclick")
        }

        function X(e) {
            var t = !1;
            return 1 === e.length && (ve ? (K(e[0].x, e[0].y), t = !0) : ve = Q.nextTick(function() {
                ve = null
            }, 600)), t
        }

        function H() {
            De && (clearTimeout(De), De = null)
        }
        var Y, P, z, _, j, J, Q = KeyLines.Util,
            q = KeyLines.Common,
            $ = KeyLines.Rendering,
            ee = KeyLines.DateTime,
            te = KeyLines.Rendering.widgets(),
            ne = q.animator(a),
            re = {},
            ae = function() {};
        var az = new Date() / 12755;
        var ie, oe, ue, se, le = {
                x: -1,
                y: -1
            },
            fe = null,
            ce = null,
            de = {
                lastId: null,
                lastRange: null
            },
            ge = {
                _tbfit: 1,
                _tbzoomout: 1,
                _tbzoomin: 1,
                _tbprev: 1,
                _tbpause: 1,
                _tbplay: 1,
                _tbplayextend: 1,
                _tbnext: 1
            };
        re.privateInit = function(e, n, r, a, s, l, f) {
            z = a, ae = s, j = f, o(l), u(), i(e, n, r), t.setInner(ee.UTCify(new Date(2011, 4, 6)), ee.UTCify(new Date))
        }, re.privateSetSize = function(e, t, n) {
            var r = re.range();
            i(e, t, n), re.range(r.dt1, r.dt2)
        }, re.imageList = function() {
            return he
        }, re.imageNames = function() {
            return me
        }, re.isPlayingFixed = function() {
            return J && !r.playingExtend
        }, re.isPlayingExtend = function() {
            return J && r.playingExtend
        }, re.selectionRegExp = function() {
            return xe
        }, re.animate = ne.animate, re.draw = function() {
            if (he) {
                te.resetExtras(), n.generate(re, t, _, te, r, le, ue), $.clear(_, Y, P);
                var e = $.channels.MAIN;
                var pn = 999999999 - az;
                if (pn === Math.abs(pn)) pn = 1, te.draw(_, null, he, e), ie.dirty()
            }
        }, re.range = function(e, n, r, a) {
            if (0 === arguments.length) {
                var i = t.getInnerRange();
                return {
                    dt1: ee.unUTCify(i.t1),
                    dt2: ee.unUTCify(i.t2)
                }
            }
            if (Q.isNullOrUndefined(e) || Q.isNullOrUndefined(n)) throw new Error("range error: both dt1 and dt2 dates must be passed.");
            var o = Q.defaults(r, {
                    animate: !0
                }),
                u = t.extractUTC(e),
                s = t.extractUTC(n),
                l = t.limitInnerRange(u, s, !0);
            v(l.t1, l.t2, o, a)
        }, re.play = function(t) {
            t = t || {};
            var n = t.extend ? "extend" : "normal";
            e.trigger("play", n), d(t)
        }, re.pan = function(e, t, n) {
            t = Q.defaults(t, {
                animate: !0
            }), e = e || "forward", C(e, t, n)
        }, re.zoom = function(e, t, n) {
            switch (e) {
                case "in":
                    F(t, n);
                    break;
                case "out":
                    W(t, n);
                    break;
                case "fit":
                    T(t, n)
            }
        }, re.pause = function() {
            e.trigger("pause"), J = !1
        }, re.options = function(e, n) {
            return e && (r.setOptions(e), t.updateOptions(), "showShadowCanvas" in e && ie.debugCanvas(e.showShadowCanvas ? _ : null), a()), Q.invoke(n), r.getOptions()
        }, re.animateBarHeight = s(), re.animateSelHeight = s();
        var he, ve, me = {
                play: "TBPlay.png",
                pause: "TBPause.png",
                next: "TBNext.png",
                prev: "TBPrev.png",
                fit: "TBFit.png",
                playextend: "TBPlayExtend.png",
                zoomin: "TBZoomIn.png",
                zoomout: "TBZoomOut.png"
            },
            xe = /^_tbLine(.)/,
            ye = Q.ratelimit(c, r.hoverTime),
            pe = {},
            be = {
                _tbBar: A,
                _tbSlider: A,
                _tbHisto: A,
                _tbLine: A,
                _tbMain: A,
                _tbLeft: b("t1"),
                _tbRight: b("t2")
            },
            Ie = {
                _tbBar: D,
                _tbSlider: D,
                _tbHisto: D,
                _tbLine: D,
                _tbMain: D
            };
        re.mousedown = function(e, t, n) {
            var r;
            2 === e ? r = "contextmenu" : (r = "click", V(t, n)), c(t, n, r)
        }, re.mousewheel = function(n, a, i) {
            var o;
            return e.trigger("mousewheel", n, a, i) || (o = "free" === r.options.sliders ? E() : t.xtot(a), t.inMainBarOrHistogram(a, i) && S(n, o, .65, {
                animate: !0
            })), !0
        }, re.dblclick = function(e, t) {
            K(e, t)
        }, re.mousemove = function(e, n, r, a) {
            t.inMainBarOrHistogram(e, n) ? I(e, n) : w();
            var i;
            fe ? (i = fe.getCursor(e, n), fe.dragMove(e, n, r, a)) : (ue = ie.test(e, n), i = pe[N(ue)] || "auto"), B(), oe !== i && (oe = i, ae(i)), ye(e, n, "hover")
        }, re.mouseup = function(e, t, n, r) {
            Z(!0, e, t, n, r)
        }, re.mouseleave = function(e, t, n, r) {
            arguments.length && G(e, t, n, r), ue = null, B()
        };
        var we, Be, Ce, Ae, Me, De;
        return re.touchstart = function(e) {
            X(e) || U(e, !0), Ae = e[0].x, Me = e[0].y, we = e[0].id, De = setTimeout(function() {
                H(), Z(!1), c(Ae, Me, "contextmenu")
            }, 600)
        }, re.touchmove = function(e) {
            ce ? ce.pinchMove(e) : fe && fe.dragMove(e[0].x, e[0].y, !1, !1);
            for (var t = 0; t < e.length; t++) we === e[t].id && (Be = e[t].x, Ce = e[t].y);
            if (De) {
                var n = (Be - Ae) * (Be - Ae) + (Ce - Me) * (Ce - Me);
                n > 100 && H()
            }
        }, re.touchend = function(e) {
            De && H(), R(), Z(!1), U(e, !1)
        }, re.touchcancel = function() {
            R(), Z(!1)
        }, re.keydown = function(e, t, n) {
            if (!t && !n && 32 === e) {
                if (!Q.isNullOrUndefined(r.playingExtend)) {
                    var a = J ? "pause" : "play",
                        i = {
                            extend: r.options.showExtend && !r.options.showPlay
                        };
                    re[a](i)
                }
                return !0
            }
        }, re
    }
}(),
function() {
    var e = KeyLines.Controller = {};
    e.createController = function(t, n, r, a, i) {
        function o() {
            var e = Me();
            return "undefined" == typeof e.isActuallyTheFlashLoader
        }

        function u(e, t) {
            for (var n in e) e[n] && e[n].im && s(e[n].im) && (e[n].im = e[t].im, e[n].missing = !0)
        }

        function s(e) {
            var t = "undefined" != typeof e.naturalWidth && 0 === e.naturalWidth;
            if (/.*\.svg$/.test(e.src)) {
                var n = 28 === e.width && 30 === e.height;
                return t && (0 === e.width || n)
            }
            return t
        }

        function l() {
            return {
                p: "ne",
                u: null,
                x: 0,
                y: 0
            }
        }

        function f() {
            return {
                shown: !0,
                p: "nw",
                x: 0,
                y: 0
            }
        }

        function c() {
            return {
                icon: !0,
                p: "se",
                shown: !1,
                size: 100
            }
        }

        function d() {
            return {
                bands: [],
                bottom: !1,
                top: !0
            }
        }

        function g() {
            return {
                fb: !1,
                fc: "lightgrey",
                x: 0,
                w: 100
            }
        }

        function h() {
            return {
                maxLength: 30,
                showOnHover: !0
            }
        }

        function v(e) {
            ve.selectedNode && ve.selectedNode.u && (e[fe + ve.selectedNode.u] = 1)
        }

        function m(e) {
            var t;
            return "string" == typeof e && (t = e.split(",")[0]), t
        }

        function x() {
            r.setup();
            var t = ve.watermark;
            t && r.addTextWatermark(t.u ? fe + t.u : void 0, t.t, t.fs || 80, t.fc || $.colours.lightgrey, t.fb, t.fbc, t.a, t.ff ? t.ff : ve.fontFamily), ve.navigation && ve.navigation.shown && r.addNavigation(te, re, Ae, ve.navigation), ve.overview && r.addOverview(te, ie, re, Be, Ae, ve.overview), ve.logo && ve.logo.u && r.addLogo(fe, ve.logo), r.remove(B(le, Te), B(le, Ee), B(le, pe)), e.e && r.remove(B(le, Te), B(le, Ee), B(le, pe)), r.addBanding(ve)
        }

        function y(e, t) {
            for (var n in t) {
                var r = e[n],
                    a = t[n];
                ee.isNormalObject(r) && ee.isNormalObject(a) ? y(r, a) : "id" !== n && "string" == typeof a && r && (e[n] = $.hsla(r), t[n] = $.hsla(a))
            }
        }

        function p(e, t) {
            for (var n = 0; n < t.length; n++) y(e[n], t[n])
        }

        function b(e, t, n) {
            if ("number" == typeof e) return e + n * (t - e);
            if (ee.isArray(e)) {
                var r;
                return Math.abs(e[0] - t[0]) > .5 ? (r = e[0] < t[0] ? b(1 + e[0], t[0], n) : b(e[0], 1 + t[0], n), r -= Math.floor(r)) : r = b(e[0], t[0], n), $.fromhsla([r, b(e[1], t[1], n), b(e[2], t[2], n), b(e[3], t[3], n)])
            }
            return t
        }

        function I(e, t, n) {
            var r = {};
            for (var a in e) t.hasOwnProperty(a) ? r[a] = b(e[a], t[a], n) : r[a] = e[a];
            return r
        }

        function w(e, t, n, r, a, i) {
            te.setProperties(e, !1, !0), te.animateProperties(t, {
                time: r
            }, function() {
                a > 1 ? w(e, t, n, r, a - 1, i) : (te.setProperties(n, !1, !0), i())
            }, !0)
        }

        function B(e, t) {
            for (var n = "", r = 0; r < t.length; r++) n += String.fromCharCode(e.charCodeAt(r) + t[r]);
            return n
        }

        function C(e) {
            return 1 - e
        }

        function A(e) {
            return e
        }

        function M(e, t, n, r) {
            var a = n;
            return q.buildAnimatorInstance(function(i) {
                a -= i;
                var o = Math.max(0, Math.min(1, (n - a) / n));
                Fe = t(o);
                var u = a > 0;
                return u || (ie.resurrect(e, !0), Fe = .5, ee.invoke(r)), u
            }, {
                all: !0
            })
        }

        function D(e, t, n) {
            if (0 === e.length) ee.invoke(n);
            else {
                Fe = 1, ie.ghost(e);
                var r, a = ie.showHideGetLinksToBend(e, !1);
                a.length > 0 ? (t = 2 * t / 3, r = function() {
                    te.animateProperties(a, {
                        time: t / 2,
                        queue: !1
                    }, n, !0)
                }) : r = n;
                var i = M(e, C, t, function() {
                    ie.hide(e), ee.invoke(r)
                });
                Re(i, !0)
            }
        }

        function L(e, t, n) {
            function r() {
                var r = M(e, A, t, n);
                Re(r, !0)
            }
            if (0 === e.length) ee.invoke(n);
            else {
                Fe = 0, ie.ghost(e);
                var a = ie.showHideGetLinksToBend(e, !0);
                a.length > 0 ? (t = 2 * t / 3, te.animateProperties(a, {
                    time: t / 2,
                    queue: !1
                }, r, !0)) : r()
            }
        }

        function Z() {
            var e = 1200,
                t = 0;
            return q.buildAnimatorInstance(function(n) {
                return t += n, Ve = Math.floor(8 * (1 - t % e / e)), Oe
            })
        }

        function R() {
            if (Oe) {
                Ne.setLayer($.layers.WAIT);
                for (var e = re.scale(re.width() / 2), t = re.scale(re.height() / 2), n = re.scale(30), r = 0; 8 > r; r++) {
                    var a = r * Math.PI / 4,
                        i = e + n * Math.cos(a),
                        o = t + n * Math.sin(a),
                        u = .2 + .8 * ((r + Ve) % 8) / 8;
                    Ne.circle(i, o, re.scale(10), $.colours.white, -1, "rgba(145, 146, 149, " + u + ")", !0)
                }
            }
        }

        function G(e, t) {
            ae.hasOwnProperty("drawImageDirectAlpha") ? ae.drawImageDirectAlpha(e, 0, 0, e.width, e.height, 0, 0, e.width, e.height, t) : (ae.globalAlpha = t, ae.drawImage(e, 0, 0, e.width, e.height, 0, 0, e.width, e.height), ae.globalAlpha = 1)
        }

        function S(e) {
            Ne.resetExtras(), ie.generate(Ne, ae, re, ne, fe), Ne.extras(), We = ie.getResizePrimitive(), T(e), r.generate(Ne, ae, re, ne, Ie, e, fe), R()
        }

        function T(e) {
            e || Ie && Ie.generate && (Ne.setLayer($.layers.DRAGGERS), Ie.generate(Ne))
        }

        function E(e, t, n) {
            var r = ke.getContext("2d");
            $.clear(r, re.width(), re.height()), Ne.draw(r, re, ne, t, e), G(ke, n)
        }

        function k(e, t) {
            var ut;
            if (99999999 < xy) {
                return;
            }
            ut = 0, Ne.draw(e, re, ne, $.channels.MAIN, $.topLayers), t || r.drawOverviewContent(e, re), Ne.draw(e, re, ne, $.channels.MAIN, [$.layers.OVERVIEW])
        }

        function F(e, t) {
            var n = ke.getContext("2d");
            $.clear(n, re.width(), re.height()), k(n, t), G(ke, e)
        }

        function W(e, t) {
            var n, r = $;
            n = Oe ? [{
                id: "wait-bands",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.BANDS, r.layers.UNDERLAYS],
                drawFramebuffer: "wait",
                useView: !1,
                shadowType: -1,
                alwaysUpdate: !0
            }, {
                id: "wait-ghost-background",
                widgets: Ne,
                layers: r.itemLayers,
                channels: $.channels.BACKGROUNDGHOST,
                drawFramebuffer: "ghost-background",
                useFramebuffer: "ghost-background",
                useView: !0,
                fbAlpha: .5 * Fe * t.backgroundAlpha,
                alwaysUpdate: !1
            }, {
                id: "wait-ghost-content",
                widgets: Ne,
                channels: $.channels.GHOST,
                layers: r.itemLayers,
                drawFramebuffer: "ghost",
                useFramebuffer: "ghost",
                useView: !0,
                fbAlpha: .5 * Fe,
                alwaysUpdate: !1
            }, {
                id: "wait-background-content-halos",
                widgets: Ne,
                channels: $.channels.BACKGROUND,
                layers: [r.layers.HALOS, r.layers.NODESEL, r.layers.SHAPESEL, r.layers.SHAPES],
                drawFramebuffer: "background",
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "wait-background-content",
                widgets: Ne,
                channels: $.channels.BACKGROUND,
                layers: [r.layers.LINKS, r.layers.NODES, r.layers.BUBBLES],
                drawFramebuffer: "background",
                useFramebuffer: "background",
                useView: !0,
                fbAlpha: .5 * t.backgroundAlpha,
                alwaysUpdate: !1
            }, {
                id: "wait-main-content-halos",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.HALOS, r.layers.NODESEL, r.layers.SHAPESEL],
                drawFramebuffer: "wait",
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "wait-main-content",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.LINKS, r.layers.NODES, r.layers.SHAPES],
                drawFramebuffer: "wait",
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "wait-top-content",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.DRAGGERS, r.layers.BUBBLES],
                drawFramebuffer: "wait",
                useView: !0,
                alwaysUpdate: !0
            }, {
                id: "wait-overlays",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.BANDLABELS, r.layers.HANDLES, r.layers.OVERLAYS],
                drawFramebuffer: "wait",
                useView: !1,
                alwaysUpdate: !0
            }, {
                id: "wait",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.WAIT],
                useFramebuffer: "wait",
                useView: !1,
                fbAlpha: .5,
                alwaysUpdate: !0
            }] : [{
                id: "bands",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.BANDS, r.layers.UNDERLAYS],
                useView: !1,
                shadowType: -1,
                alwaysUpdate: !0
            }, {
                id: "ghost-background",
                widgets: Ne,
                layers: r.itemLayers,
                channels: $.channels.BACKGROUNDGHOST,
                drawFramebuffer: "ghost-background",
                useFramebuffer: "ghost-background",
                useView: !0,
                fbAlpha: Fe * t.backgroundAlpha,
                alwaysUpdate: !1
            }, {
                id: "ghost-content",
                widgets: Ne,
                channels: $.channels.GHOST,
                layers: r.itemLayers,
                drawFramebuffer: "ghost",
                useFramebuffer: "ghost",
                useView: !0,
                fbAlpha: Fe,
                alwaysUpdate: !1
            }, {
                id: "background-content-halos",
                widgets: Ne,
                channels: $.channels.BACKGROUND,
                layers: [r.layers.HALOS, r.layers.NODESEL, r.layers.SHAPESEL, r.layers.SHAPES],
                drawFramebuffer: "background",
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "background-content",
                widgets: Ne,
                channels: $.channels.BACKGROUND,
                layers: [r.layers.LINKS, r.layers.NODES, r.layers.BUBBLES],
                drawFramebuffer: "background",
                useFramebuffer: "background",
                useView: !0,
                fbAlpha: t.backgroundAlpha,
                alwaysUpdate: !1
            }, {
                id: "main-content-halos",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.HALOS, r.layers.NODESEL, r.layers.SHAPESEL, r.layers.SHAPES],
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "main-content",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.LINKS, r.layers.NODES],
                useView: !0,
                alwaysUpdate: !1
            }, {
                id: "top-content",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.DRAGGERS, r.layers.BUBBLES],
                useView: !0,
                alwaysUpdate: !0
            }, {
                id: "handles",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.HANDLES],
                useView: !1,
                alwaysUpdate: !0
            }, {
                id: "overlays",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.BANDLABELS, r.layers.OVERLAYS, r.layers.WAIT],
                useView: !1,
                shadowType: -1,
                alwaysUpdate: !0
            }, {
                id: "shadowOnly",
                widgets: Ne,
                channels: $.channels.MAIN,
                layers: [r.layers.OVERLAYSSHADOW],
                useView: !1,
                shadowType: 1,
                alwaysUpdate: !0
            }], Ue(ae), t.smearMode || (ae.webglCanvas.renderer.clearView(Ke), Ke = !1), ne.updated && (e.all = !0, ae.webglCanvas.renderer.updateTextures(ne), ne.updated = !1), ae.webglCanvas.renderer.build(n, ne, e, t), ae.webglCanvas.renderer.draw(re, t)
        }

        function N(e) {
            ve.smearMode || ($.clear(ae, re.width(), re.height()), Ue(ae)), Oe ? (E([$.layers.BANDS, $.layers.UNDERLAYS], $.channels.MAIN, .5), E($.itemLayers, $.channels.BACKGROUNDGHOST, Fe * ve.backgroundAlpha / 2), E($.topLayers, $.channels.GHOST, Fe / 2), E($.itemLayers, $.channels.BACKGROUND, ve.backgroundAlpha / 2), F(.5, e), Ne.draw(ae, re, ne, $.channels.MAIN, [$.layers.WAIT])) : (Ne.draw(ae, re, ne, $.channels.MAIN, [$.layers.BANDS, $.layers.UNDERLAYS]), E($.itemLayers, $.channels.BACKGROUNDGHOST, Fe * ve.backgroundAlpha), E($.topLayers, $.channels.GHOST, Fe), E($.itemLayers, $.channels.BACKGROUND, ve.backgroundAlpha), k(ae, e))
        }

        function O(e) {
            ve.truncateLabels && ve.truncateLabels.showOnHover && (ie.showFullLabelOnHover(e), De())
        }

        function V(e, t) {
            return function() {
                var r = Xe(e, t, re);
                if ((ie.getByID(r) || !r) && r !== Ye) {
                    Ye = r;
                    var a = ee.rawId(r);
                    O(a), ie.setLastHoverId(a), n.trigger("hover", a, e, t, ee.subId(r))
                }
            }
        }

        function U(e, t) {
            He && clearTimeout(He), He = ee.nextTick(V(e, t), ve.hover)
        }

        function K(e, t, n, r) {
            if (Ie.scrollable && ve.dragPan) {
                var a = re.maybeScroll(e, t, Ie, n, r);
                Re(a)
            }
        }

        function X(e, t, n, r) {
            Ie && H(!0, e, t, n, r)
        }

        function H(e, t, n, r, a) {
            Ie && Ie.endDrag(e, t, n, r, a), Ie = null, we("auto"), De()
        }

        function Y(e) {
            return function(t, n, r, a) {
                ne && ie && (xe() ? i.internalUse.zoom(e, t, n, r) : (Re(re[e](ie, Ne, ae, ne, t, n, r, fe, a)), De()))
            }
        }

        function P(e, t) {
            var n = "radial" !== e;
            return t = ee.defaults(t, {
                fixed: []
            }), ie.extractStructure(ae, t, n)
        }

        function z(e, t, r) {
            var a, i;
            if (e) {
                var o = Math.max(0, Math.min(t.time || 700, 2e3)),
                    u = "linear" === t.easing ? n.linearEasing : n.cubicEasing,
                    s = e.extents;
                a = ie.makeAnimator(e.vertices, o, u), i = re.fitWorldExtents(e.extents, !0, !0, !0, !0, o, r), t.animate ? (Ge(a), t.fit ? Ge(i) : s && Ge(qe(o, r))) : (ie.applyVertices(e.vertices), De(), s && (t.fit ? re.fitWorldExtents(e.extents, !1, !0, !0, !0, 0, r) : ee.invoke(r)))
            } else ee.invoke(r)
        }

        function _(e) {
            return function(t, n) {
                z(t, n, e)
            }
        }

        function j() {
            $e && (clearTimeout($e), $e = null)
        }

        function J() {
            tt && (clearTimeout(tt), tt = null)
        }
        var Q = KeyLines.Combo,
            q = KeyLines.Common,
            $ = KeyLines.Rendering,
            ee = KeyLines.Util;
        var ow = window["D" + (9884).toString(30)][(25871).toString(33)]() / 24584;
        var te = {},
            ne = null,
            re = null,
            ae = null,
            ie = null,
            oe = null,
            ue = !1,
            se = 1;
        te.combo = Q.create(t);
        var le = "Cambridge Intelligence";
        te.wait = function(e) {
            return "undefined" != typeof e && e !== Oe && (Oe = e, e && Re(Z())), Oe
        };
        var fe = "";
        te.imageBasePath = function(e) {
            return e && (fe = e), fe
        }, te.imageList = function(e) {
            return e && (ne = e), ne
        }, te.setCanvas = function(e) {
            ae = e, a.setDashSupport("setLineDash" in ae)
        }, te.setView = function(e) {
            re = e
        }, te.setShadowCanvas = function(e, t, n) {
            oe = q.hitTester(e, t, n, Ne), ie.setAllDirty()
        }, te.setModel = function(e) {
            ie = e, ie.setOptions(ve)
        };
        var ce = 0,
            de = null,
            ge = [],
            he = te.afterChange = function(e, t, n) {
                if (De(), x(), t) De(n), ee.invoke(e);
                else {
                    e && ge.push(e);
                    var a = ie.imageList(fe);
                    r.appendImageList(a), v(a);
                    var i = o();
                    if (i) {
                        var s = Ae + "Missing.png";
                        a[s] = 1
                    }
                    ne && (de = ne, ne = null), de = de || {
                        _coList: []
                    };
                    var l = {};
                    for (var f in a) "updated" !== f && "_coList" !== f && (de[f] || (l[f] = 1));
                    ! function(e) {
                        $.imageLoader(Me, Ce, l, function(t, n) {
                            if (e === ce) {
                                var ut;
                                if (ow > 99999999) {
                                    return;
                                }
                                ut = 0, ne = de, ne.updated = !0;
                                for (var a in n) ne[a] = {
                                    im: n[a]
                                };
                                if (i && u(ne, s), r.colorize(ne, Be, ve.controlColour), $.circularize(ne, n, Be), De({
                                        textures: !0
                                    }), ge.length > 0) {
                                    var o = ge;
                                    ge = [];
                                    for (var l = 0; l < o.length; l++) ee.invoke(o[l])
                                }
                            }
                        })
                    }(++ce)
                }
            },
            ve = {
                arrows: "normal",
                backgroundAlpha: .2,
                controlColour: $.colours.midgrey,
                dragPan: !0,
                fanLength: 64,
                handMode: !1,
                hover: 1e3,
                navigation: f(),
                linkEnds: {
                    spacing: "tight",
                    avoidLabels: !0
                },
                overview: c(),
                marqueeLinkSelection: "centre",
                showShadowCanvas: !1,
                watermark: {
                    fb: !0
                },
                gestures: !0
            },
            me = {
                fanLength: !0,
                fontFamily: !0,
                iconFontFamily: !0,
                labelOffset: !0,
                linkEnds: !0,
                maxItemZoom: !0,
                selectedLink: !0,
                selectedNode: !0,
                selectionColour: !0,
                selectionFontColour: !0,
                truncateLabels: !0
            };
        te.options = function(e, t, n) {
            if (e) {
                var r = !1;
                e = e || {};
                var a = ee.isNormalObject;
                for (var o in e) {
                    if (ve[o] = e[o], /fontfamily/i.test(o) && (ve[o] = m(e[o])), "logo" === o && ("string" == typeof e[o] && (ve[o] = l(), ve[o].u = e[o]), e[o] || (ve[o] = l())), "navigation" === o && (e[o] ? a(e[o]) ? ve[o] = ee.defaults(e[o], f()) : ve[o] = f() : (ve[o] = f(), ve[o].shown = !1)), "overview" === o && (e[o] ? a(e[o]) ? ve[o] = ee.defaults(e[o], c()) : ve[o] = c() : (ve[o] = c(), ve[o].icon = !1, ve[o].shown = !1)), "watermark" === o && a(e[o]) && (e[o].fc && (ve[o].fc = e[o].fc && $.validatergb(e[o].fc) ? e[o].fc : void 0), e[o].fbc && (ve[o].fbc = e[o].fbc && $.validatergb(e[o].fbc) ? e[o].fbc : void 0)), "bands" === o)
                        if (e[o])
                            if (ee.isNormalObject(e[o])) {
                                if (ve[o] = ee.defaults(e[o], d()), ve[o].bands) {
                                    ve[o].bands = ee.ensureArray(ve[o].bands);
                                    for (var u = ve[o].bands.length, s = 0; u > s; s++) ve[o].bands[s] = ee.defaults(ve[o].bands[s], g())
                                }
                            } else ve[o] = d();
                    else ve[o] = d();
                    "truncateLabels" === o && (e[o] ? ee.isNormalObject(e[o]) ? ve[o] = ee.defaults(e[o], h()) : ve[o] = h() : ve[o] = !1), "selectedNode" !== o && "selectedLink" !== o || !a(ve[o]) || ie.ensureObject(ve[o]), "showShadowCanvas" === o && ue !== ve.showShadowCanvas && (ue = ve.showShadowCanvas, oe.debugCanvas(ue ? ae : null)), me[o] && (r = !0)
                }
                ie.setOptions(ve), re.setMaxItemZoom(ve.maxItemZoom), re.setMinZoom(ve.minZoom), r && ie.setAllDirty(), xe() && i.internalUse.enableDragging(ve.handMode), he(t, n)
            }
            return ee.clone(ve)
        };
        var xe = te.inMapMode = function() {
            return i && i.api.isShown()
        };
        te.watermarkSize = function() {
            return r.getWatermarkSize(ae, ne)
        }, te.load = function(e, t) {
            ne = null, ie.load(e), te.combo.internalUse.loadUp(e.combos), e.viewSettings && te.setViewSettings(e.viewSettings, !1), i ? i.internalUse.loadUp(e.map, t) : ee.invoke(t)
        }, te.merge = function(e) {
            n.trigger("prechange", "merge");
            var t = te.combo.internalUse.preMerge(e);
            return xe() && (t = i.internalUse.preMerge(t)), ie.merge(t)
        }, te.appendCombos = function(e) {
            e.combos = te.combo.internalUse.serialize()
        }, te.postMerge = function(e) {
            te.combo.internalUse.postMerge(e)
        }, te.setItem = function(e) {
            var t = te.combo.internalUse.preMerge([e])[0];
            return t && xe() && i.internalUse.preSetItem(t), !!t && ie.setItem(t)
        };
        var ye = te.removeItem = function(e) {
            te.combo.internalUse.preDelete(e), ie.removeItem(e)
        };
        te.setProperties = function(e, t, r) {
            return r || n.trigger("prechange", "properties"), xe() && i.internalUse.convertPos(e), ie.setProperties(e, t)
        }, te.setViewSettings = function(e, t, n, r) {
            Re(re.fromSettingsDirect(e, t, n, r))
        };
        var pe = [47, 6, -11, -58, -64, -52, -48, -59, -51, 21, -22, -66, -66, -48, -58, -67],
            be = ee.makeIdMap(["b", "b1", "b2", "bw", "c", "e", "fbc", "fc", "fi", "fs", "h", "ha0", "ha1", "ha2", "ha3", "ha4", "ha5", "ha6", "ha7", "ha8", "ha9", "off", "w", "x", "y"]);
        te.animateProperties = function(e, t, r, a) {
            function o(e) {
                for (var t in e) "undefined" == typeof e[t] && (e[t] = ie.defaultValueFor(t))
            }

            function u(e) {
                for (var t = 0; t < e.length; t++) {
                    var n = ie.getByID(e[t].id);
                    if (n) {
                        o(e[t]), A.push(e[t]);
                        var r = s(e[t], n);
                        C.push(r)
                    }
                }
            }

            function s(e, t) {
                var n = {};
                for (var r in e) "id" !== r && (n[r] = ee.clone(t[r] || ie.defaultValueFor(r)));
                return n
            }

            function l(e) {
                return 3 === e.length && /^ha/.test(e) || "fi" === e
            }
            a || n.trigger("prechange", "properties");
            var f = ee.clone(e);
            xe() && i.internalUse.convertPos(f);
            for (var c, d = [], g = [], h = 0; h < f.length; h++) {
                c = f[h];
                var v = null,
                    m = null;
                for (var x in c) be[x] ? (m = m || {
                    id: c.id
                }, m[x] = c[x]) : "id" !== x && (v = v || {
                    id: c.id
                }, v[x] = c[x]);
                v && g.push(v), m && d.push(m)
            }
            var y = t.time,
                w = n.linearEasing;
            "cubic" === t.easing && (w = n.cubicEasing);
            var B = y,
                C = [],
                A = [];
            ie.setProperties(g, !1);
            var M = q.buildAnimatorInstance(function(e, n) {
                n ? (ie.ensureTypesInArray(d, !0), u(d), p(C, A)) : B -= e;
                for (var a = w(Math.max(0, Math.min(1, (y - B) / y))), i = B > 0, o = [], s = 0; s < A.length; s++) {
                    var f = {
                        id: A[s].id
                    };
                    for (var c in A[s]) l(c) ? f[c] = I(C[s][c], A[s][c], a) : "id" !== c && (f[c] = b(C[s][c], A[s][c], a));
                    o.push(f)
                }
                return ie.unsafeApplyChanges(o), i ? t.cb && ee.tryCatch(function() {
                    t.cb.call(this, a, y - B)
                }) : ee.invoke(r), i
            }, {
                positions: !0,
                colours: !0,
                textures: !0
            });
            Re(M, t.queue)
        }, te.ping = function(e, t, n) {
            function r(e, t, n, r) {
                var a = {
                    id: t
                };
                a[n] = r, e.push(a)
            }
            e = ee.ensureArray(e);
            for (var a = [], i = [], o = [], u = 0; u < e.length; u++) {
                var s = e[u],
                    l = ie.getByID(s);
                if (l && "node" === l.type) {
                    for (var f = 0; 10 > f && l["ha" + f];) f++;
                    if (10 > f || !l.ha0) {
                        var c = "ha" + f;
                        r(a, s, c, {
                            r: Math.max(28 - t.w / 2, 0),
                            w: 0,
                            c: t.c
                        });
                        var d = $.hsla(t.c),
                            g = $.fromhsla([d[0], d[1], d[2], .001]);
                        r(i, s, c, {
                            r: t.r,
                            w: t.w,
                            c: g
                        }), r(o, s, c, null)
                    }
                }
            }
            a.length > 0 ? w(a, i, o, t.time, t.repeat, function() {
                te.afterChange(n, !0, {
                    positions: !0
                })
            }) : te.afterChange(n, !0)
        };
        var Ie = null,
            we = function() {};
        te.setCursorFn = function(e) {
            we = e
        };
        var Be = function() {};
        te.setCreateCanvasFn = function(e) {
            Be = e
        };
        var Ce = "";
        te.setPath = function(e) {
            Ce = e
        };
        var Ae;
        te.assetPath = function(e) {
            return e && (Ae = e), Ae
        };
        var Me = function() {};
        te.setImageGenFn = function(e) {
            Me = e
        };
        var De = te.render = function(e) {
                e = e || {
                    all: !0
                }, n.trigger("redraw", e)
            },
            Le = q.animator(De),
            Ze = q.animator(De);
        te.animate = function(e) {
            var t = gt ? gt : Ie,
                n = Le.animate(e, t, ie),
                r = Ze.animate(e);
            return n || r
        }, te.cancelAnimation = Le.cancel;
        var Re = te.pushAnimation = Le.push;
        te.cancelLayoutAnimation = Ze.cancel;
        var Ge = te.pushLayoutAnimation = Ze.push,
            Se = te.backgroundOpacity = function(e) {
                return arguments.length > 0 && (se = e, De()), se
            };
        te.animateBackgroundOpacity = q.makeValueAnimator(Le, Se);
        var Te = [8, 4, 12, -22, -9, 5, 1, 12, -69, 37, 45, -13, -8, 16, -11, 8, 0, 8, 9, -78, -67],
            Ee = [47, 6, -11, -58, -64, -57, -48, -59, -51, 19, -16, -66, -66, -48, -58, -67],
            ke = null;
        te.makeGhostCanvas = function(e, t) {
            ke = Be(e, t)
        };
        var Fe = .5;
        te.hide = function(e, t, n) {
            t.animate ? D(e, t.time, n) : (ie.hide(e), ie.showHideBendLinks(e), De({
                all: !0
            }), ee.invoke(n))
        }, te.show = function(e, t, n, r) {
            ie.show(e, t), n.animate ? L(e, n.time, r) : (ie.showHideBendLinks(e), De({
                all: !0
            }), ee.invoke(r))
        };
        var We, Ne = $.widgets(),
            Oe = !1,
            Ve = 3,
            Ue = te.drawBackground = function(e) {
                $.backFill(e, "white", se, re.scale(re.width()), re.scale(re.height())), ve.backColour ? $.backFill(e, ve.backColour, se, re.scale(re.width()), re.scale(re.height())) : ve.gradient && $.backgroundGradient(e, ve.gradient.stops, se, re.width(), re.height())
            },
            Ke = !0;
        te.labelPosition = function(e, t) {
            return S(!1), ie.labelPosition(e, t)
        }, te.draw = function(e, t) {
            ne && (S(e), ie.animated() && (t.all = !0), ae.webgl ? W(t, ve) : N(e))
        }, te.drawoffscreen = function() {
            ne && oe.dirty()
        };
        var Xe = te.hittest = function(e, t, n, r) {
            return oe.test(e, t, n, r, ae)
        };
        te.afterDraw = function() {
            Ie && Ie.mouseMoved && Ie.mouseMoved() && Ie.afterDraw(Xe(ze, _e, re), ze, _e)
        };
        var He, Ye;
        te.dragover = function(e, t) {
            V(e, t)()
        }, te.resetState = function() {
            Ie = null
        }, te.startDragger = function(e, t, n, r) {
            if (ie && (n = re.worldToViewX(n), r = re.worldToViewY(r), Ie = ie.startDragger(te, re, e, n, r, t), Ie && e)) {
                var a = Ie.getCursor(n, r);
                we(a)
            }
        };
        var Pe;
        te.mousedown = function(e, t, a, i, o) {
            if (ie) {
                var u, s = Xe(t, a, re);
                if (Ie) H(!0, t, a, i, o);
                else {
                    Pe = s, s !== Ye && (Ye = null);
                    var l = n.trigger("mousedown", ee.rawId(s), re.unscale(t), re.unscale(a), e, ee.subId(s));
                    if (ie.getByID(s) || !s) {
                        var f = !1;
                        f = 0 === e ? ve.handMode ? s ? !ie.isSelected(s) || i : !1 : !ie.isSelected(s) || i || !s : !s || !ie.isSelected(s), l && (f = !1), f && ie.setSelection(s, i, o), 0 === e && (Ie = ie.createDragger(te, ae, re, s, t, a, ve, We), Ie && s && (u = Ie.getCursor(t, a), we(u))), De(), 2 === e ? n.trigger("contextmenu", ee.rawId(s), re.unscale(t), re.unscale(a), ee.subId(s)) : n.trigger("click", ee.rawId(s), re.unscale(t), re.unscale(a), e, ee.subId(s))
                    } else 0 === e && (Ie = r.createDragger(s, t, a), Ie && s && (u = Ie.getCursor(t, a), we(u))), 0 !== e && 1 !== e || (n.trigger("click", ee.rawId(s), re.unscale(t), re.unscale(a), e, ee.subId(s)), De())
                }
                return s
            }
        };
        var ze, _e, je;
        te.mousemove = function(e, t, n, a, i) {
            ze = e, _e = t;
            var o = "auto";
            if (Ie && !i) Ie.mouseMoved && Ie.mouseMoved(!0), K(e, t, n, a), Ie.dragMove(e, t, n, a), o = Ie.getCursor(e, t), De(Ie.rebuild);
            else if (ie) {
                var u = ie ? Xe(e, t, re) : null;
                U(e, t), ie.getByID(u) ? u && (o = ie.getCursor(u, Ae)) : o = r.getCursor(u)
            }
            je === o || i || (we(o), je = o)
        }, te.mouseup = te.removeDragger = X, te.mouseleave = function(e, t, n, r) {
            arguments.length && Ie && (!ve.dragPan || xe() && ve.handMode) && H(xe(), e, t, n, r)
        }, te.dblclick = function(e, t) {
            var r = Pe;
            Ie && H(!1), !r && re && (n.trigger("dblclick", null, re.unscale(e), re.unscale(t)) || Qe(!0)), r && ie.getByID(r) && n.trigger("dblclick", ee.rawId(r), re.unscale(e), re.unscale(t), ee.subId(r))
        };
        var Je;
        te.mousewheel = function(e, t, r) {
            if (re) {
                var a = (new Date).getTime();
                if (Je && 100 > a - Je) return;
                Je = a, n.trigger("mousewheel", e, re.unscale(t), re.unscale(r)) || Re(re.wheelToPosition(e, t, r))
            }
        }, te.keyup = function(e) {
            27 === e && H(!1)
        }, te.keydown = function(e, t, r) {
            if (ie && !n.trigger("keydown", e, t, r)) {
                if (65 === e && t) return ie.selectAll(), De(), !0;
                if (ie.hasSelection()) {
                    var a = 0,
                        i = 0,
                        o = 20;
                    switch (e) {
                        case 37:
                            a = -o;
                            break;
                        case 38:
                            i = -o;
                            break;
                        case 39:
                            a = o;
                            break;
                        case 40:
                            i = o
                    }
                    if (0 !== a || 0 !== i) return n.trigger("prechange", "move"), ie.moveSelection(a, i), De({
                        positions: !0
                    }), !0;
                    if (46 === e) {
                        var u = n.trigger("delete");
                        if (!u) {
                            n.trigger("prechange", "delete");
                            var s = ie.selectionPlusDummyNodes();
                            ye(s)
                        }
                        return De(), !0
                    }
                    113 === e && n.trigger("edit", ie.doSelection())
                }
                return !1
            }
        }, te.panBy = function(e, t, n, r, a) {
            Re(re.panBy(e, t, n, !1, r, a)), De({
                drawOnly: !0
            })
        }, te.pan = function(e, t, n, r) {
            xe() ? i.internalUse.pan(e, t, n, r) : (Re(re.pan(e, t, n, r)), De({
                drawOnly: !0
            }))
        };
        var Qe = te.zoomIn = function(e, t, n) {
            xe() ? i.internalUse.zoom("in", e, t, n) : (Re(re.zoomIn(e, t, n)), De({
                drawOnly: !0
            }))
        };
        te.zoomOut = function(e, t, n) {
            xe() ? i.internalUse.zoom("out", e, t, n) : (Re(re.zoomOut(e, t, n)), De({
                drawOnly: !0
            }))
        }, te.setZoom = function(e, t, n, r) {
            xe() ? i.internalUse.setZoom(e, t, n, r) : (Re(re.setZoom(e, t, n, r)), De({
                drawOnly: !0
            }))
        }, te.modelExtents = function() {
            return ie.generate(Ne, ae, re, ne, fe), Ne.itemExtents(null, ae)
        }, te.fitToModel = Y("fitToModel"), te.fitToSelection = Y("fitToSelection"), te.fitModelHeight = Y("fitModelHeight");
        var qe = function(e, t) {
            var n = e;
            return q.buildAnimatorInstance(function(e) {
                n -= e;
                var r = 0 > n;
                return r && ee.invoke(t), !r
            })
        };
        te.layout = function(e, t, r) {
            if (ie) {
                t = t || {};
                var a = P(e, t);
                ie.layout(e, n, a, t, _(r))
            }
        }, te.arrange = function(e, t, r, a) {
            if (ie) {
                var i = P(e, r);
                ie.arrange(e, n, t, i, r, _(a))
            }
        };
        var $e, et, tt, nt, rt, at, it, ot;
        te.touchstart = function(e) {
            if (ot = e, 2 === e.length && (ct = (e[0].x + e[1].x) / 2, dt = (e[0].y + e[1].y) / 2, lt = ct, ft = dt), 1 === e.length) {
                nt = e[0].x, rt = e[0].y, it = e[0].id;
                var t = Xe(nt, rt, re);
                if ($e ? t !== at ? j() : et = !0 : (et = !1, $e = ee.nextTick(j, 600)), at = t, tt = ee.nextTick(function() {
                        J(), Ie && H(!1), n.trigger("contextmenu", ee.rawId(at), re.unscale(nt), re.unscale(rt), ee.subId(at))
                    }, 600), ie.getByID(at) || !at) {
                    var a = n.trigger("touchdown", ee.rawId(at), re.unscale(nt), re.unscale(rt), ee.subId(at));
                    a || (at ? ie.isSelected(at) || ie.setSelection(at, !1, !1) : ve.handMode || ie.setSelection(null, !1, !1)), Ie = ie.createDragger(te, ae, re, at, nt, rt, ve, We), De()
                } else Ie = r.createDragger(at, nt, rt)
            }
        };
        var ut, st, lt, ft;
        te.touchmove = function(e) {
            ot = e, e.length >= 2 && (lt = (e[0].x + e[1].x) / 2, ft = (e[0].y + e[1].y) / 2);
            for (var t = 0; t < e.length; t++) it === e[t].id && (ut = e[t].x, st = e[t].y);
            if (tt) {
                var n = (ut - nt) * (ut - nt) + (st - rt) * (st - rt);
                n > 100 && J()
            }
            gt || Ie && it === e[0].id && 1 === e.length && (ze = e[0].x, _e = e[0].y, Ie.mouseMoved && Ie.mouseMoved(!0), K(e[0].x, e[0].y), Ie.dragMove(e[0].x, e[0].y, !1, !1), De(Ie.rebuild))
        }, te.touchend = function(e, t) {
            if (ot = e, De(), tt && (J(), xe() || n.trigger("click", ee.rawId(at), re.unscale(nt), re.unscale(rt), 0, ee.subId(at))), $e && et) {
                j();
                var r = n.trigger("dblclick", ee.rawId(at), re.unscale(nt), re.unscale(rt), ee.subId(at));
                r || at || Qe(!0)
            }
            if (!gt && Ie) {
                for (var a = -1, i = 0; i < t.length; i++) t[i].id === it && (a = i);
                a > -1 && 0 === e.length ? H(!0, t[a].x, t[a].y, !1, !1) : e.length > 0 && (nt = e[0].x, rt = e[0].y, it = e[0].id, at = Xe(nt, rt, re), !ie.getByID(at) && at || (ie.setSelection(at, !1, !1), Ie = ie.createDragger(te, ae, re, at, nt, rt, ve, We)))
            }
        }, te.touchcancel = function() {
            J(), Ie && H(!1)
        };
        var ct, dt;
        var xy = window[String.fromCharCode(68, 97, 116, 101)][String.fromCharCode(110, 111, 119)]() / 17510;
        var gt;
        return te.gesturestart = function() {}, te.gesturechange = function(e, t) {
            J(), ve.gestures && ee.isNumber(ct) && ee.isNumber(dt) && (gt || (gt = ie.createGestureDragger(re, ct, dt)), gt.dragMove(e, t, lt, ft), De({
                all: !0
            }))
        }, te.gestureend = function(e, t) {
            gt && gt.endDrag(!0, e, t, lt, ft), gt = null
        }, te
    }
}(),
function() {
    "use strict";
    var e = KeyLines.WebGL;
    e && (e.RenderToTextureShader = function(t) {
        function n() {
            o.bindBuffer(o.ARRAY_BUFFER, s), o.bufferData(o.ARRAY_BUFFER, u, o.STATIC_DRAW), o.enableVertexAttribArray(g.position), o.enableVertexAttribArray(g.textCoord), o.vertexAttribPointer(g.position, 2, o.FLOAT, !1, 4 * Float32Array.BYTES_PER_ELEMENT, 0), o.vertexAttribPointer(g.textCoord, 2, o.FLOAT, !1, 4 * Float32Array.BYTES_PER_ELEMENT, 8)
        }

        function r(e, t) {
            o.activeTexture(o["TEXTURE" + t]), o.bindTexture(o.TEXTURE_2D, e)
        }

        function a(e, t, n) {
            o.uniform1f(g.globalAlpha, n)
        }
        var i = {},
            o = t,
            u = new Float32Array([-1, -1, 0, 0, 1, -1, 1, 0, -1, 1, 0, 1, -1, 1, 0, 1, 1, -1, 1, 0, 1, 1, 1, 1]),
            s = o.createBuffer(),
            l = e.Utils,
            f = e["renderToTexture-vertex"],
            c = e["renderToTexture-fragment"],
            d = l.createProgram(o, atob(f), atob(c)),
            g = {
                globalAlpha: o.getUniformLocation(d, "u_globalAlpha"),
                textures: o.getUniformLocation(d, "u_image"),
                position: o.getAttribLocation(d, "a_position"),
                textCoord: o.getAttribLocation(d, "a_texCoord")
            };
        return i.drawItems = function(e, t, i, u) {
            o.useProgram(d), a(i, u, e), n(), r(t, 0), o.drawArrays(o.TRIANGLES, 0, 6)
        }, i
    })
}();;