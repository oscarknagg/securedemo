This is the source code for the KeyLines security event demo (see Wandera Wiki https://snappli.atlassian.net/wiki/display/RAD/Security+events+overview).

If you are using NodeJS as your server, just run this file 'node server.js'

Running server.js has a dependency on the express module being available, which can be installed (once node is installed) by running

 >  npm install express

If you can't run Node or express just use a simple python server
    
 > python -m SimpleHTTPServer 8080

Then you can navigate to http://localhost:8080 in your web browser to see the demo running

This demo relies on a Neo4j database. 

Use the db called 'securedemo.db' by doing:
cd /Users/oscarknagg/Downloads/neo4j-community-3.0.4/data/databases/
cp securedemo.db graph.db

The steps required to populate the database are documented in https://snappli.atlassian.net/wiki/display/RAD/Example+3+-+Security+events and https://snappli.atlassian.net/wiki/display/RAD/Security+events+overview.
