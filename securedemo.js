//
//     Copyright © 2011-2016 Cambridge Intelligence Limited.
//     All rights reserved.
//
//     Sample Code
//
//!    Use the Cypher language to query a Neo4j database with the time bar.
//
//     CONNECT TO THE DATABASE IN FOLDER 'securesimple.db'
//

var colour = {klBg: '#ffffff', red: 'rgb(200, 50, 29)', lightFont: '#222222'},
  selectionColours = ['rgb(114, 179, 0)', 'rgb(255, 153, 0)', 'rgb(178, 38, 9)'],
  deviceHash = {}, isSmallDevices = false,
  chart, timebar, tbState = { locked: false, range: false, allItems: 0 };

/************************************/
/* Set up events                    */
/************************************/

function calcDegrees() {
  return chart.graph().degrees({value: 'numCommits'});
}

function calcDeviceSize() {
  $('#deviceChangeIconOn').toggleClass('active btn-kl', isSmallDevices);
  $('#deviceChangeIconOff').toggleClass('active btn-kl', !isSmallDevices);
  var sizes = [];
  if (isSmallDevices) { // make some devices bigger based on criteria
    var degrees = calcDegrees(), maxDegree = _.max(degrees) + 1;
    sizes = _.map(deviceHash, function (device, id) {
      // max degree and all degrees is sometimes 1 at end of play range
      return {id: id, e: !degrees[id] ? 1 : Math.min( (((degrees[id] / maxDegree) * 100) + 1), 10 )};
    });
  } else { // restore all devices to normal size
    sizes = _.map(deviceHash, function (node) {
      return {id: node.id, e: 1};
    });
  }
  return sizes;
}

function changeDeviceSize() {
  isSmallDevices = !isSmallDevices;
  chart.animateProperties(calcDeviceSize(), {time: 500}, function () {
  });
}

function changeForeground(itemIds) {
  var criteria;
  if (_.isEmpty(itemIds)) {
    criteria = function () { return true; };
  } else {
    criteria = function (item) { return itemIds[item.id]; };
  }
  chart.foreground(criteria, {type: 'link'});
}

function showSelection() {
  var neighboursLinks = [],
      neighboursNodes = [],
      foreground = {},
      visualProperties = {},
      selection = chart.selection(),
      itemsSelected = chart.getItem(selection);
    
  // clear all selections
  timebar.selection([]);
  
  // Reset all visual properties of nodes
  chart.each({ type: 'node' }, function(node) {
    visualProperties[node.id] = { id: node.id, ha0: false, ha1: false };
  });
  
  function filterSelection(itemsSelected) {
    var threatIds = [],
        deviceIds = [];
    _.each(itemsSelected, function(item) {
      // Looking only for threats nodes
      if (item.d.nodeType === 'threat') {
        threatIds.push(item.id);
      }
      // Looking only for devices nodes
      if (item.d.nodeType === 'device') {
        deviceIds.push(item.id);
      }
    });
    return { threats: threatIds.slice(0, selectionColours.length), devices: deviceIds };
  }
  
  // we only colour the first few threats of the selection
  var itemsFiltered = filterSelection(itemsSelected);
    
  // Apply a selection style for other items selected
  _.each(itemsFiltered.devices, function(idSelected) {
      visualProperties[idSelected].ha1 = {w: 8, r: 70, c: 'rgb(79, 134, 198)'};
  });
  
  // Compute the selections
  _.each(itemsFiltered.threats, function (idSelected, index) {
    var nSelection = chart.graph().neighbours(idSelected, { all: false });
    neighboursLinks.push({id: nSelection.links, index: index, c: selectionColours[index]});
    _.each(nSelection.links, function (id) {
      foreground[id] = true;
    });
    neighboursNodes[index] = chart.getItem(nSelection.nodes);
    visualProperties[idSelected].ha0 = {w: 8, r: 30, c: selectionColours[index]};
  });
  changeForeground(foreground);
  timebar.selection(neighboursLinks);
  
  // Add visual properties on connected nodes
  _.each(neighboursNodes, function (neighbours, index) {
    _.each(neighbours, function(node) {
      if (!node.hi) { // Only if there are visible
        visualProperties[node.id].ha0 = {w: 8, r: 50, c: selectionColours[index]};
      }
    });
  });
  chart.setProperties(_.values(visualProperties));
}

function timebarChanged(unlock) {
  if (unlock || !tbState.locked) {
    var newRange = timebar.range(),
        oldRange = tbState.range = tbState.range || newRange;
    // Check if the data is already collected
    if (newRange.dt1.getTime() < oldRange.dt1.getTime() || newRange.dt2.getTime() > oldRange.dt2.getTime()) {
      // Save the new range
      tbState.range = newRange;
      // Collect more data
      processData(newRange.dt1.getTime(), newRange.dt2.getTime(), function (charData) {
        // Merge the data
        mergeData(charData, function () {
          // Apply filter to the chart
          filterAfterChanged(newRange.dt1, newRange.dt2, function() {
            // Apply 'structural' layout only if there is new data to show
            var nbItems = charData.items.length,
                layout = 'structural';
            if (nbItems > tbState.allItems) {
              layout = 'structural';
              tbState.allItems = nbItems;
            }
            if (isSmallDevices) {
              chart.setProperties(calcDeviceSize());
            }
            chart.layout(layout, {}, unlock);
          });
        });
      });
    } else { // No need to collect more data
      filterAfterChanged(oldRange.dt1, oldRange.dt2, function() {
        chart.layout('structural', {}, unlock);
      });
    }
  }

  function filterAfterChanged(start, end, callback) {
    var links = [];
    // Filter according the selection
    chart.filter(timebar.inRange, { animate: false, type: 'link' }, function() {
      // Refresh the selection
      if (chart.selection().length > 0) {
        showSelection();
      }
      var ids = timebar.getIds(start, end);
      // Update the link properties colors and glyph
      _.each(chart.getItem(ids), function(link) {
        links.push(updateLink(link, start.getTime(), end.getTime()));
      });
      // Refresh the links and refresh the layout
      chart.setProperties(links, false, callback);
    });
  }
}

function setupEvents() {
  // toggle changing device sizes on chart
  $('#deviceChangeIcon').click(changeDeviceSize);
  // Run the layout
  $('#layout').click(function() { chart.layout('structural', {tightness: 5}); });
  // show selection in time bar and foreground/background on chart
  chart.bind('selectionchange', showSelection);
  // Override the default behaviour
  timebar.bind('dblclick', function() {
    timebarChanged(function() {
      tbState.locked = false;
    });
  });
  // Prevent multiple change from dragging
  timebar.bind('dragstart', function() {
    tbState.locked = true;
  });
  // Prevent multiple change from dragging
  timebar.bind('dragcomplete', function() {
    timebarChanged(function() {
      tbState.locked = false;
    });
  });
  // When the timebar change fetch and merge data
  timebar.bind('change', timebarChanged);
}

/************************************/
/* Load KeyLines and general setup  */
/************************************/

function loadData(chartData) {
//  chart.load(chartData, chart.layout);
  chart.load(chartData);
  chart.layout('structural');
  timebar.load(chartData, function () {
    timebar.zoom('fit', {animate: false});
  });
}

function mergeData(chartData, cb) {
  chart.merge(chartData.items, cb);
  timebar.merge(chartData.items);
}

function processData(startDate, endDate, cb) {
  // Create the query to get the data
  var query = createQuery({ start: startDate, end: endDate });
  // Call the Cypher Endpoint and then load the data into KeyLines
  callCypher(query, function (json) {
    // Transform the items from the Neo4j format to the KeyLines one
    var items = makeKeyLinesItems(json);
    // Callback with the chartdata
    cb({type: "LinkChart", items: items});
  });
}

function klReady(err, components) {
  chart = components[0];
  timebar = components[1];

  setupEvents();
  processData(new Date('2016/07/04').getTime(), new Date('2016/07/05').getTime(), loadData);
}

$(function () {
  KeyLines.paths({ assets: 'assets/',
    flash: { swf: 'swf/keylines.swf', swfObject: 'js/swfobject.js', expressInstall: 'swf/expressInstall.swf' }
  });

  var chartOptions = {
    backColour: colour.klBg,
    overview: false,
    selectionColour: 'rgba(0, 0, 0, 1)',
    handMode: true,
    dragPan: false,
    //logo: 'images/neo4jtb/neo4j-blue-logo.png'
  };

  var tbOptions = {
    backColour: colour.klBg,
    histogram: {colour: '#777', highlightColour: '#CCC'},
    sliderColour: 'rgba(0, 0, 0, 0.7)',
    scale: {highlightColour: '#e6ffff'},
    fontColour: colour.lightFont,
    showPlay: false
  };

  KeyLines.create([
    {id: 'kl', type: 'chart', options: chartOptions},
    {id: 'tl', type: 'timebar', options: tbOptions}
    ], klReady);
});

/*************************************/
/* Calling Neo4j to get data */
/*************************************/

function updateQueryField(queryString) {
  $('#cypherquery').text(queryString);
}

function makeKeyLinesItems(json) {
  var items = [], linksHash = {};
  _.each(json.results[0].data, function (entry) {
    // Makes nodes
    _.each(entry.graph.nodes, function (node) {
      items.push(makeNode(node));
    });
    // Makes links
    _.each(entry.graph.relationships, function (link) {
      makeLink(link, linksHash);
    });
  });
  return items.concat(_.values(linksHash));
}

function prepareQuery(query, params) {
  // Refresh the UI with the new query
  updateQueryField(query
      .replace(/\{start\}/, params.start)
      .replace(/\{end\}/, params.end));
  // Use the new Cypher Endpoint for transactions (introduced with Neo4j 2.x)
  return {
    statements: [{
      statement: query,
      parameters: params,
      resultDataContents: ['graph']
    }]
  };
}

function createQuery(parameters) {
  return prepareQuery('MATCH (t:Threat)-[c]-(d:Device) WHERE c.date >= {start} AND c.date <= {end} RETURN *;', parameters);
}

function callCypher(query, callback) {
  $.ajax({
    type: 'POST',
    // Change this URL according to your Neo4j URL
    url:'http://localhost:7474/db/data/transaction/commit',
    data: JSON.stringify(query),
     // If you're using Neo4j 2.2 uncomment the following 3 lines
     // and set your Database username and password:
     headers: {
       Authorization: 'Basic '+btoa('neo4j:wandera')
     },
    dataType: 'json',
    contentType: 'application/json'
  })
  .fail(reportError)
  .done(callback);
}

function reportError(jqXHR, _, errorString){
  // report the error code: very useful to debug some connection issues with Neo4J
  console.log('Error reported from Neo4J: '+ jqXHR.status + ' - ' + errorString );
}

/***********************************************************************/
/* Parsing Cypher response format to KeyLines chart format             */
/***********************************************************************/

function getType(labels) {
  return labels[0].toLowerCase() === 'threat' ? 'threat' : 'device';
}

function makeNode(nodeItem) {
  var baseType = getType(nodeItem.labels),
      itemId = nodeItem.id,
      item = nodeItem.properties,
      isThreat = baseType === 'threat',
      label = isThreat ? item.name : truncateDeviceName(item.guid),
      node = {id: itemId, type: 'node', t: label,
        c: isThreat ? 'rgba(255, 0, 0, 1)' : '#0088CC',
        ci: isThreat ? true : false,
        e: isThreat ? 2 : 0.5,
        fbc: 'rgba(0, 0, 0, 0)',
        fc: colour.lightFont,
        d: {item: item, nodeType: baseType}
      };
  if (baseType === 'device') {
    deviceHash[itemId] = node;
  }
  return node;
}

function makeLink(linkItem, linksHash) {
  var threatId = linkItem.startNode,
      deviceId = linkItem.endNode,
      linkId = threatId + '-' + deviceId,
      item = linkItem.properties, link, currentLink = linksHash[linkId];

  if (!currentLink) { // create link
    link = {type: 'link', id: linkId, id1: threatId, id2: deviceId, a2: true, dt: [item.date], d: {changes: {}, numCommits: 1}};
    link.d.changes[item.date] = {'additions': item.additions, 'deletions': item.deletions};
    // Save the link
    linksHash[linkId] = link;
  } else { // update link
    // numCommits
    currentLink.d.numCommits++;
    // add date if not stored
    if (!currentLink.d.changes[item.date]) {
      currentLink.dt.push(item.date);
    }
    if (currentLink.d.changes[item.date]) {
      currentLink.d.changes[item.date].additions += item.additions;
      currentLink.d.changes[item.date].deletions += item.deletions;
    } else {
      currentLink.d.changes[item.date] = {'additions': item.additions, 'deletions': item.deletions};
    }
  }
}

function makeGlyph(link, startDate, endDate) {
  var sumAdditions = 0, sumDeletions = 0, utcDate;
  _.each(link.d.changes, function (change, date) {
    utcDate = UTCfy(date);
    if (utcDate >= startDate && utcDate <= endDate) {
      sumAdditions += change.additions;
      sumDeletions += change.deletions;
    }
  });
//  link.g = [{c: 'green', t: '' + sumAdditions, p: 'ne'}, {c: 'red', t: '' + sumDeletions, p: 'nw'}];
  link.w = 1; //(Math.sqrt(sumAdditions + sumDeletions) / 5)  +  ((sumAdditions + sumDeletions) / 25);
}

function updateLink(link, startDate, endDate) {
    link.c = calcLinkColour(link, startDate, endDate); // Change the colour
    makeGlyph(link, startDate, endDate); // Change the glyph
    return link;
}

function getNodeIcon(item, type) {
  return type === 'threat' ? 'images/icons/threat.png' : 'images/icons/mobilephone2.png';
}

function deviceNameFromPath (path) {
  if (_.contains(path, '/')) {
    path = path.split('/');
    path = path[path.length - 1];
  }
  return path;
}

function truncateDeviceName(name) {
  name = deviceNameFromPath(name);
  if (name.length > 10) {
    name = '...' + name.substring(name.length - 10);
  }
  return name;
}

function chooseDeviceIcon(name) {
  return 'images/icons/documents1.png';
}

function UTCfy(timestamp) {
  var dt = new Date(+timestamp);
  return (new Date(dt.valueOf() + dt.getTimezoneOffset() * 60000)).getTime();
}

function calcLinkColour(link, startDate, endDate) {
  var keys = _.keys(link.d.changes), change, utcDate;
  var vals = _.reduce(keys, function (memo, date) {
    utcDate = UTCfy(date);
    if (utcDate >= startDate && utcDate <= endDate) {
      change = link.d.changes[date];
      memo.a += change.additions;
      memo.d += change.deletions;
    }
    return memo;
  }, {a: 0, d: 0});
  return calcColour(vals.a, vals.d);
}

function calcColour(green, red) {
  var sum = green + red,
      r = red / sum,
      g = green / sum;
  //return 'rgba(' + Math.ceil(255 * r) + ',' + Math.ceil(255 * g) + ',0,1)';
  return '#696969';
}
